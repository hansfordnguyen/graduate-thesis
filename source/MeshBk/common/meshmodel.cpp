/****************************************************************************
* MeshLab                                                           o o     *
* A versatile mesh processing toolbox                             o     o   *
*                                                                _   O  _   *
* Copyright(C) 2005                                                \/)\/    *
* Visual Computing Lab                                            /\/|      *
* ISTI - Italian National Research Council                           |      *
*                                                                    \      *
* All rights reserved.                                                      *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License (http://www.gnu.org/licenses/gpl.txt)          *
* for more details.                                                         *
*                                                                           *
****************************************************************************/


#include <QString>
#include <QtGlobal>
#include <QFileInfo>
#include "meshmodel.h"
#include <wrap/gl/math.h>
#include "scriptinterface.h"
#include <vcg/complex/append.h>


using namespace vcg;


//deletes each meshModel
MeshDocument::~MeshDocument()
{
  foreach(MeshModel *mmp, meshList)
    delete mmp;
  foreach(RasterModel* rmp,rasterList)
      delete rmp;
}

//returns the mesh ata given position in the list
MeshModel *MeshDocument::getMesh(int i)
{
  foreach(MeshModel *mmp, meshList)
  {
    if(mmp->id() == i) return mmp;
  }
  //assert(0);
  return 0;
}

MeshModel *MeshDocument::getMesh(QString name)
{
    foreach(MeshModel *mmp, meshList)
            {
                if(mmp->shortName() == name) return mmp;
            }
    //assert(0);
    return 0;
}

MeshModel *MeshDocument::getMeshByFullName(QString pathName)
{
    foreach(MeshModel *mmp, meshList)
    {
        if(mmp->fullName() == pathName) return mmp;
    }
    //assert(0);
    return 0;
}


void MeshDocument::setCurrentMesh( int i)
{
    if(i<0)
    {
        currentMesh=0;
        return;
    }
    currentMesh = getMesh(i);
    emit currentMeshChanged(i);
    assert(currentMesh);
}

//returns the raster at a given position in the list
RasterModel *MeshDocument::getRaster(int i)
{
  foreach(RasterModel *rmp, rasterList)
  {
    if(rmp->id() == i) return rmp;
  }
  //assert(0);
  return 0;
}

//if i is <0 it means that no currentRaster is set
void MeshDocument::setCurrentRaster( int i)
{
  if(i<0)
  {
      currentRaster=0;
      return;
  }

  foreach(RasterModel *rmp, rasterList)
  {
    if(rmp->id() == i)
    {
      currentRaster = rmp;
      return;
    }
  }
  assert(0);
  return;
}

template <class LayerElement>
QString NameDisambiguator(QList<LayerElement*> &elemList, QString meshLabel )
{
  QString newName=meshLabel;
  typename QList<LayerElement*>::iterator mmi;

  for(mmi=elemList.begin(); mmi!=elemList.end(); ++mmi)
  {
    if((*mmi)->label() == newName) // if duplicated name found
    {
      QFileInfo fi((*mmi)->label());
      QString baseName = fi.baseName(); //  all characters in the file up to the first '.' Eg "/tmp/archive.tar.gz" -> "archive"
      QString suffix = fi.suffix();
      bool ok;

      // if name ends with a number between parenthesis (XXX),
      // it was himself a duplicated name, and we need to
      // just increase the number between parenthesis
      int numDisamb;
      int startDisamb;
      int endDisamb;

      startDisamb = baseName.lastIndexOf("(");
      endDisamb   = baseName.lastIndexOf(")");
      if((startDisamb!=-1)&&(endDisamb!=-1))
        numDisamb = (baseName.mid((startDisamb+1),(endDisamb-startDisamb-1))).toInt(&ok);
      else
        numDisamb = 0;

      if(startDisamb!=-1)
        newName = baseName.left(startDisamb)+ "(" + QString::number(numDisamb+1) + ")";
      else
        newName = baseName + "(" + QString::number(numDisamb+1) + ")";

      if (suffix != QString(""))
      newName = newName + "." + suffix;

      // now recurse to see if the new name is free
      newName = NameDisambiguator(elemList, newName);
    }
  }
  return newName;
}

/*
 When you create a new mesh it can be either a newly created one or an opened one.
 If it is an opened one the fullpathname is meaningful and the label, by default is just the short name.
 If it is a newly created one the fullpath is an empty string and the user has to provide a label.
 */

MeshModel * MeshDocument::addNewMesh(QString fullPath, QString label, bool setAsCurrent,const RenderMode& rm)
{
  QString newlabel = NameDisambiguator(this->meshList,label);

  if(!fullPath.isEmpty())
  {
      QFileInfo fi(fullPath);
      fullPath = fi.absoluteFilePath();
  }

  MeshModel *newMesh = new MeshModel(this,qPrintable(fullPath),newlabel);
  meshList.push_back(newMesh);
  emit meshSetChanged();
  emit meshAdded(newMesh->id(),rm);
  if(setAsCurrent)
    this->setCurrentMesh(newMesh->id());
  return newMesh;
}

bool MeshDocument::delMesh(MeshModel *mmToDel)
{
    removeOneVolumeFromGlobalVolume(mmToDel);

    if(!meshList.removeOne(mmToDel))
        return false;
    if((currentMesh == mmToDel) && (meshList.size() != 0))
        setCurrentMesh(this->meshList.at(0)->id());
    else if (meshList.size() == 0)
            setCurrentMesh(-1);

    int index = mmToDel->id();
    delete mmToDel;

    emit meshSetChanged();
    emit meshRemoved(index);
    return true;
}

RasterModel * MeshDocument::addNewRaster(/*QString fullPathFilename*/)
{
  QFileInfo info(fullPathFilename);
  QString newLabel=info.fileName();
  QString newName = NameDisambiguator(this->rasterList, newLabel);

  RasterModel *newRaster=new RasterModel(this, newLabel);
    rasterList.push_back(newRaster);

    //Add new plane
  //Plane *plane = new Plane(newRaster, fullPathFilename, QString());
  //newRaster->addPlane(plane);

    this->setCurrentRaster(newRaster->id());

  emit rasterSetChanged();
    return newRaster;
}

bool MeshDocument::delRaster(RasterModel *rasterToDel)
{
    QMutableListIterator<RasterModel *> i(rasterList);

    while (i.hasNext())
    {
        RasterModel *r = i.next();

        if (r==rasterToDel)
        {
            i.remove();
            delete rasterToDel;
        }
    }

    if(currentRaster == rasterToDel)
    {
        if (rasterList.size() > 0)
            setCurrentRaster(rasterList.at(0)->id());
        else
            setCurrentRaster(-1);
    }
    emit rasterSetChanged();

    return true;
}

bool MeshDocument::hasBeenModified()
{
    int ii = 0;
    while(ii < meshList.size())
    {
        if (meshList[ii]->meshModified())
            return true;
        ++ii;
    }
    return false;
}

void MeshDocument::updateRenderStateMeshes(const QList<int>& mm,const int meshupdatemask)
{
    static QTime currTime  = QTime::currentTime();
    if(currTime.elapsed()< 100)
        return;
    for (QList<int>::const_iterator mit = mm.begin();mit != mm.end();++mit)
    {
        MeshModel* mesh = getMesh(*mit);
        if (mesh != NULL)
            renderState().update(mesh->id(),mesh->cm,meshupdatemask);
    }
    if ((mm.size() > 0) && (meshupdatemask != MeshModel::MM_NONE))
        emit documentUpdated();
    currTime.start();
}

void MeshDocument::updateRenderStateRasters(const QList<int>& rm,const int rasterupdatemask)
{
    static QTime currTime = QTime::currentTime();
    if(currTime.elapsed()< 100)
        return;
    for (QList<int>::const_iterator rit = rm.begin();rit != rm.end();++rit)
    {
        RasterModel* raster = getRaster(*rit);
        if (raster != NULL)
            renderState().update(raster->id(),*raster,rasterupdatemask);
    }
    if ((rm.size() > 0) && (rasterupdatemask != RasterModel::RM_NONE))
        emit documentUpdated();
    currTime.start();
}

void MeshDocument::setVolumeProperty(VolumeProperty *property)
{
    volumeProperty = property;
//    connect(volumeProperty, SIGNAL(colorTableChanged(vcg::CallBackPos*, bool)), this, SLOT(updateTexture3DRgbaData(vcg::CallBackPos*, bool)));
    connect(this, SIGNAL(texture3DRgbaDataUpdated()), volumeProperty, SLOT(updateTexture3dRgbaDataSuccess()));
}

void MeshDocument::updateRenderState(const QList<int>& mm,const int meshupdatemask,const QList<int>& rm,const int rasterupdatemask)
{
    static QTime currTime = QTime::currentTime();
    if(currTime.elapsed()< 100)
        return;
    for (QList<int>::const_iterator mit = mm.begin();mit != mm.end();++mit)
    {
        MeshModel* mesh = getMesh(*mit);
        if (mesh != NULL)
            renderState().update(mesh->id(),mesh->cm,meshupdatemask);
    }
    for (QList<int>::const_iterator rit = rm.begin();rit != rm.end();++rit)
    {
        RasterModel* raster = getRaster(*rit);
        if (raster != NULL)
            renderState().update(raster->id(),*raster,rasterupdatemask);
    }
    if (((mm.size() > 0) && (meshupdatemask != MeshModel::MM_NONE)) || (rm.size() > 0 && (rasterupdatemask != RasterModel::RM_NONE)))
        emit documentUpdated();
    currTime.start();
}

MeshDocument::MeshDocument() : QObject(),rendstate(),Log(),xmlhistory()
{
    meshIdCounter=0;
    rasterIdCounter=0;
    currentMesh = 0;
    currentRaster = 0;
    busy=false;
}

void MeshDocument::setTextureBBoxMillimeter() {
    MeshDocument dummyMeshDoc;
    textureBBoxMillimeter = new MeshModel(&dummyMeshDoc, "Full name", "Bound Box Texture 3D");

    vcg::Point3f p0(0.0f,                       0.0f,                           0.0f);
    vcg::Point3f p1(0.0f,                       measurementMillimeter.Y(),      0.0f);
    vcg::Point3f p2(measurementMillimeter.X(),  measurementMillimeter.Y(),      0.0f);
    vcg::Point3f p3(measurementMillimeter.X(),  0.0f,                           0.0f);
    vcg::Point3f p4(0.0f,                       0.0f,                           measurementMillimeter.Z());
    vcg::Point3f p5(0.0f,                       measurementMillimeter.Y(),      measurementMillimeter.Z());
    vcg::Point3f p6(measurementMillimeter.X(),  measurementMillimeter.Y(),      measurementMillimeter.Z());
    vcg::Point3f p7(measurementMillimeter.X(),  0.0f,                           measurementMillimeter.Z());

    std::vector<vcg::Point3f> points;
    points.push_back(p0);
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
    points.push_back(p4);
    points.push_back(p5);
    points.push_back(p6);
    points.push_back(p7);

    vcg::tri::Allocator<CMeshO>::AddVertices(textureBBoxMillimeter->cm, 8);
    vcg::tri::Allocator<CMeshO>::AddFaces(textureBBoxMillimeter->cm, 12);

    //Init vertices
    for (int i = 0; i < 8; i++) {
        textureBBoxMillimeter->cm.vert[i].P()[0] = points.at(i).X();
        textureBBoxMillimeter->cm.vert[i].P()[1] = points.at(i).Y();
        textureBBoxMillimeter->cm.vert[i].P()[2] = points.at(i).Z();
    }

    // Init faces
    textureBBoxMillimeter->cm.face[0].V(0) = &textureBBoxMillimeter->cm.vert[0];
    textureBBoxMillimeter->cm.face[0].V(1) = &textureBBoxMillimeter->cm.vert[1];
    textureBBoxMillimeter->cm.face[0].V(2) = &textureBBoxMillimeter->cm.vert[3];

    textureBBoxMillimeter->cm.face[1].V(0) = &textureBBoxMillimeter->cm.vert[1];
    textureBBoxMillimeter->cm.face[1].V(1) = &textureBBoxMillimeter->cm.vert[2];
    textureBBoxMillimeter->cm.face[1].V(2) = &textureBBoxMillimeter->cm.vert[3];

    textureBBoxMillimeter->cm.face[2].V(0) = &textureBBoxMillimeter->cm.vert[4];
    textureBBoxMillimeter->cm.face[2].V(1) = &textureBBoxMillimeter->cm.vert[7];
    textureBBoxMillimeter->cm.face[2].V(2) = &textureBBoxMillimeter->cm.vert[5];

    textureBBoxMillimeter->cm.face[3].V(0) = &textureBBoxMillimeter->cm.vert[6];
    textureBBoxMillimeter->cm.face[3].V(1) = &textureBBoxMillimeter->cm.vert[5];
    textureBBoxMillimeter->cm.face[3].V(2) = &textureBBoxMillimeter->cm.vert[7];

    textureBBoxMillimeter->cm.face[4].V(0) = &textureBBoxMillimeter->cm.vert[2];
    textureBBoxMillimeter->cm.face[4].V(1) = &textureBBoxMillimeter->cm.vert[6];
    textureBBoxMillimeter->cm.face[4].V(2) = &textureBBoxMillimeter->cm.vert[3];

    textureBBoxMillimeter->cm.face[5].V(0) = &textureBBoxMillimeter->cm.vert[3];
    textureBBoxMillimeter->cm.face[5].V(1) = &textureBBoxMillimeter->cm.vert[6];
    textureBBoxMillimeter->cm.face[5].V(2) = &textureBBoxMillimeter->cm.vert[7];

    textureBBoxMillimeter->cm.face[6].V(0) = &textureBBoxMillimeter->cm.vert[0];
    textureBBoxMillimeter->cm.face[6].V(1) = &textureBBoxMillimeter->cm.vert[5];
    textureBBoxMillimeter->cm.face[6].V(2) = &textureBBoxMillimeter->cm.vert[1];

    textureBBoxMillimeter->cm.face[7].V(0) = &textureBBoxMillimeter->cm.vert[0];
    textureBBoxMillimeter->cm.face[7].V(1) = &textureBBoxMillimeter->cm.vert[4];
    textureBBoxMillimeter->cm.face[7].V(2) = &textureBBoxMillimeter->cm.vert[5];

    textureBBoxMillimeter->cm.face[8].V(0) = &textureBBoxMillimeter->cm.vert[1];
    textureBBoxMillimeter->cm.face[8].V(1) = &textureBBoxMillimeter->cm.vert[6];
    textureBBoxMillimeter->cm.face[8].V(2) = &textureBBoxMillimeter->cm.vert[2];

    textureBBoxMillimeter->cm.face[9].V(0) = &textureBBoxMillimeter->cm.vert[1];
    textureBBoxMillimeter->cm.face[9].V(1) = &textureBBoxMillimeter->cm.vert[5];
    textureBBoxMillimeter->cm.face[9].V(2) = &textureBBoxMillimeter->cm.vert[6];

    textureBBoxMillimeter->cm.face[10].V(0) = &textureBBoxMillimeter->cm.vert[0];
    textureBBoxMillimeter->cm.face[10].V(1) = &textureBBoxMillimeter->cm.vert[3];
    textureBBoxMillimeter->cm.face[10].V(2) = &textureBBoxMillimeter->cm.vert[7];

    textureBBoxMillimeter->cm.face[11].V(0) = &textureBBoxMillimeter->cm.vert[0];
    textureBBoxMillimeter->cm.face[11].V(1) = &textureBBoxMillimeter->cm.vert[7];
    textureBBoxMillimeter->cm.face[11].V(2) = &textureBBoxMillimeter->cm.vert[4];

    textureBBoxMillimeter->UpdateBoxAndNormals();
}

void MeshDocument::setMeasurementMillimeter(Measurement3f measurement) {
    this->measurementMillimeter = measurement;
}

void MeshDocument::setMeasurementPixel(Measurement3i measurement) {
    this->measurementPixel = measurement;
    initGlobalTexture3DData(measurement);
}

void MeshDocument::initGlobalTexture3DData(Measurement3i measurement) {
    int w = measurement.X();
    int h = measurement.Y();
    int n = measurement.Z();

    int intensitySize = w * h * n;

    globalTexture3DIntensityData = new signed short[intensitySize];
    assert(globalTexture3DIntensityData);

    for (int i = 0; i < intensitySize; i++) {
        globalTexture3DIntensityData[i] = 0;
    }

    int rgbaSize = intensitySize * 4;

    globalTexture3DRgbaData = new uchar[rgbaSize];
    assert(globalTexture3DRgbaData);

    for (int i = 0; i < rgbaSize; i++) {
        globalTexture3DRgbaData[i] = 0;
    }

    glGenTextures(1, &globalTexture3DAddr);

    glBindTexture(GL_TEXTURE_3D, globalTexture3DAddr);

    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, w, h, n, 0, GL_RGBA, GL_UNSIGNED_BYTE, globalTexture3DRgbaData);

    glBindTexture(GL_TEXTURE_3D, 0);
}

void MeshDocument::addOneVolumeToGlobalVolume(MeshModel *mm, vcg::CallBackPos *cb) {
    addOneVolumeToGlobalTexture3D(mm, cb);

    updateTexture3DRgbaData(cb);
}

void MeshDocument::addOneVolumeToGlobalTexture3D(MeshModel *mm, CallBackPos *cb) {
    if (mm->isVisibleVolume) // It already rendered
        return;

    mm->isVisibleVolume = true;
    numOfVisibleVolume++;

    int w = measurementPixel.X();
    int h = measurementPixel.Y();
    int n = measurementPixel.Z();

    int intensitySize = w * h * n;

    cv::Mat global(1, intensitySize, CV_16SC1, globalTexture3DIntensityData);
    cv::Mat newVolume(1, intensitySize, CV_16SC1, mm->texture3DRawData);

    cv::Mat result = cv::Mat::zeros(1, intensitySize, CV_16SC1);
    cv::bitwise_or(global, newVolume, result);

//    std::memcpy(globalTexture3DIntensityData, result.data, intensitySize*sizeof(uchar));
    for (int i = 0; i < intensitySize; i++) {
        globalTexture3DIntensityData[i] = result.at<signed short>(0, i);
    }
    emit updateHistogram();
}

void MeshDocument::removeOneVolumeFromGlobalVolume(MeshModel *mm, vcg::CallBackPos* cb) {
    if (!mm->isVisibleVolume) // It already didn't render
        return;

    mm->isVisibleVolume = false;
    numOfVisibleVolume--;

    int w = measurementPixel.X();
    int h = measurementPixel.Y();
    int n = measurementPixel.Z();

    int intensitySize = w * h * n;
    cv::Mat global(1, intensitySize, CV_16SC1, globalTexture3DIntensityData);
    cv::Mat mmVolumeMask(1, intensitySize, CV_16SC1, mm->texture3DMaskData);

    cv::Mat mmVolumeMaskRevert = cv::Mat::zeros(1, intensitySize, CV_16SC1);
    cv::bitwise_not(mmVolumeMask, mmVolumeMaskRevert);

    cv::Mat result = cv::Mat::zeros(1, intensitySize, CV_16SC1);
    cv::bitwise_and(global, mmVolumeMaskRevert, result);

//    std::memcpy(globalTexture3DIntensityData, result.data, intensitySize*sizeof(uchar));
    for (int i = 0; i < intensitySize; i++) {
        globalTexture3DIntensityData[i] = result.at<signed short>(0, i);
    }

    updateTexture3DRgbaData(cb);
    emit updateHistogram();
}

void MeshDocument::updateTexture3DRgbaData(vcg::CallBackPos *cb, bool isUseIndex) {
    qDebug() << "updateTexture3DRgbaData";

    int w = measurementPixel.X();
    int h = measurementPixel.Y();
    int n = measurementPixel.Z();

    int intensitySize = w * h * n;

    uchar r, g, b, a;

//    if (isUseIndex) {
//        int index = 0;
//        int count = 100;
//        int step = intensitySize / count;
//        count = intensitySize / step;

//        // If use index, firt, hide all voxels
//        for (int i = 0; i < intensitySize; i++) {
//            globalTexture3DRgbaData[4*i] = 0;
//            globalTexture3DRgbaData[4*i + 1] = 0;
//            globalTexture3DRgbaData[4*i + 2] = 0;
//            globalTexture3DRgbaData[4*i + 3] = 0;

//            if (i % step == 0) {
//                if (cb)
//                    cb(100 * index * 1.0/ count, "Updating texture data");
//                index++;
//            }
//        }
//        // Then just show out selections
//        if (volumeProperty) {
//            QMap<int, bool>::Iterator it = volumeProperty->indexMap.begin();
//            for(; it != volumeProperty->indexMap.end(); ++it) {
//                globalTexture3DRgbaData[4*it.key()] = globalTexture3DIntensityData[it.key()] - 1024;
//                globalTexture3DRgbaData[4*it.key() + 1] = globalTexture3DIntensityData[it.key()] - 1024;
//                globalTexture3DRgbaData[4*it.key() + 2] = globalTexture3DIntensityData[it.key()] - 1024;
//                globalTexture3DRgbaData[4*it.key() + 3] = 255;
//            }
//        }
//    } else {
//        int index = 0;
//        int count = 100;
//        int step = intensitySize / count;
//        count = intensitySize / step;

//        for (int i = 0; i < intensitySize; i++) {
//            if (volumeProperty) {
//                vcg::Color4f color;
//                color = volumeProperty->intMap[globalTexture3DIntensityData[i] - 1024]; // Get back real intensity by subtracting COMPLEMENT part that was add at funcs DicomInfo::getOriginIntensityImage(complement);
//                r = (uchar)color[0]; // Will be changed by mapIntensity funcs
//                g = (uchar)color[1]; // Will be changed by mapIntensity funcs
//                b = (uchar)color[2]; // Will be changed by mapIntensity funcs
//                a = (uchar)color[3]; // Will be changed by mapIntensity funcs

//            } else {
//                r = g = b = (uchar) (globalTexture3DIntensityData[i] - 1024);
//                a = globalTexture3DIntensityData[i] == 0 ? 0 : 50;
//            }
//            globalTexture3DRgbaData[4*i] = r;
//            globalTexture3DRgbaData[4*i + 1] = g;
//            globalTexture3DRgbaData[4*i + 2] = b;
//            globalTexture3DRgbaData[4*i + 3] = a;
//            if (i % step == 0) {
//                if (cb)
//                    cb(100 * index * 1.0/ count, "Updating texture data");
//                index++;
//            }
//        }
//    }

    int index = 0;
    int count = 100;
    int step = intensitySize / count;
    count = intensitySize / step;

    for (int i = 0; i < intensitySize; i++) {
        if (rootNode) {
            vcg::Color4f color;
            color = volumeProperty->intMap[globalTexture3DIntensityData[i] - 1024]; // Get back real intensity by subtracting COMPLEMENT part that was add at funcs DicomInfo::getOriginIntensityImage(complement);
            color = rootNode->transfunc(globalTexture3DIntensityData, i, rootNode->viewOptions);
            r = (uchar)color[0]; // Will be changed by mapIntensity funcs
            g = (uchar)color[1]; // Will be changed by mapIntensity funcs
            b = (uchar)color[2]; // Will be changed by mapIntensity funcs
            a = (uchar)color[3]; // Will be changed by mapIntensity funcs

        } else {
            r = g = b = (uchar) (globalTexture3DIntensityData[i] - 1024);
            a = globalTexture3DIntensityData[i] == 0 ? 0 : 50;
        }
        globalTexture3DRgbaData[4*i] = r;
        globalTexture3DRgbaData[4*i + 1] = g;
        globalTexture3DRgbaData[4*i + 2] = b;
        globalTexture3DRgbaData[4*i + 3] = a;
        if (i % step == 0) {
            if (cb)
                cb(100 * index * 1.0/ count, "Updating texture data");
            index++;
        }
    }


    glBindTexture(GL_TEXTURE_3D, globalTexture3DAddr);
    glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, w, h, n, GL_RGBA, GL_UNSIGNED_BYTE, globalTexture3DRgbaData);
    glBindTexture(GL_TEXTURE_3D, 0);
    emit texture3DRgbaDataUpdated();
    emit documentUpdated();
}

void MeshDocument::updateTexture3DRgbaDataFromTF(TreeItem *item, vcg::CallBackPos *cb)
{
    int w = measurementPixel.X();
    int h = measurementPixel.Y();
    int n = measurementPixel.Z();

    int intensitySize = w * h * n;
    int index = 0;
    int count = 100;
    int step = intensitySize / count;
    count = intensitySize / step;

    for (int i = 0; i < intensitySize; i++) {
        vcg::Color4f color(0, 0, 0, 0);
        if (item->opacity[i]) {
            TransFunc func = item->transfunc;
            NodeViewOptions options = item->viewOptions;
            color = func(globalTexture3DIntensityData, i, options);
        }

        globalTexture3DRgbaData[4*i] = color[0];
        globalTexture3DRgbaData[4*i + 1] = color[1];
        globalTexture3DRgbaData[4*i + 2] = color[2];
        globalTexture3DRgbaData[4*i + 3] = color[3];

        if (i % step == 0) {
            if (cb)
                cb(100 * index * 1.0/ count, "Updating texture data");
            index++;
        }
    }

    glBindTexture(GL_TEXTURE_3D, globalTexture3DAddr);
    glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, w, h, n, GL_RGBA, GL_UNSIGNED_BYTE, globalTexture3DRgbaData);
    glBindTexture(GL_TEXTURE_3D, 0);
    emit texture3DRgbaDataUpdated();
    emit documentUpdated();
}

void MeshDocument::updateTexture3DRgbaDataFromCombinedItems(QList<TreeItem *> viewItems, CallBackPos *cb)
{
    int w = measurementPixel.X();
    int h = measurementPixel.Y();
    int n = measurementPixel.Z();

    int intensitySize = w * h * n;
    int index = 0;
    int count = 100;
    int step = intensitySize / count;
    count = intensitySize / step;

    for (int i = 0; i < intensitySize; i++) {
        vcg::Color4f color(0.0f, 0.0f, 0.0f, 0.0f);

        for(int idx = viewItems.size()-1; idx >= 0; idx--) {
            TreeItem *item = viewItems.at(idx);
            if (item->opacity[i]) {
                TransFunc func = item->transfunc;
                NodeViewOptions option = item->viewOptions;
                color = func(globalTexture3DIntensityData, i, option);
                break;
            }
        }

        globalTexture3DRgbaData[4*i] = color[0];
        globalTexture3DRgbaData[4*i + 1] = color[1];
        globalTexture3DRgbaData[4*i + 2] = color[2];
        globalTexture3DRgbaData[4*i + 3] = color[3];

        if (i % step == 0) {
            if (cb)
                cb(100 * index * 1.0/ count, "Updating texture data");
            index++;
        }
    }

    glBindTexture(GL_TEXTURE_3D, globalTexture3DAddr);
    glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, w, h, n, GL_RGBA, GL_UNSIGNED_BYTE, globalTexture3DRgbaData);
    glBindTexture(GL_TEXTURE_3D, 0);
    emit texture3DRgbaDataUpdated();
    emit documentUpdated();
}

void MeshDocument::renderVolumeUses3DTextureAlg(Point3f viewPos, vcg::Point3f viewDir, float lamda) {

    if (numOfVisibleVolume <= 0) {
        qDebug() << "MeshDoc does not have visible volume";
        return;
    }

//    if (currentMesh->isSlice) {
//        qDebug() << "Current mesh is a slice mesh";
//        return;
//    }

    if(!globalTexture3DAddr) {
        qDebug() << "MeshDoc does not have texture3D";
        return;
    }

    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPushMatrix();

    glEnable(GL_ALPHA_TEST );
    glAlphaFunc( GL_GREATER, (1.0f/255.0f));

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

    glEnable(GL_TEXTURE_3D);
    glBindTexture(GL_TEXTURE_3D, globalTexture3DAddr);

    viewDir.Normalize();

    Plane3f viewPlane;
    viewPlane.Init(viewPos, -viewDir);

    Point3f maxDistanceVertex = getVertexMaxDistanceFromView(textureBBoxMillimeter, viewPlane);
    Point3f minDistanceVertex = getVertexMinDistanceFromView(textureBBoxMillimeter, viewPlane);

    float maxDistance = vcg::SignedDistancePointPlane(maxDistanceVertex, viewPlane);
    float minDistance = vcg::SignedDistancePointPlane(minDistanceVertex, viewPlane);

    if (minDistance > 0 && maxDistance > 0) {
        Point3f planeNormal = -viewDir;
        Point3f planeSamplePoint = maxDistanceVertex;

        while(vcg::SignedDistancePointPlane(planeSamplePoint, viewPlane) > minDistance) {
            planeSamplePoint -= planeNormal*lamda;
            if (planeSamplePoint.X() == minDistanceVertex.X() && planeSamplePoint.Y() == minDistanceVertex.Y() && planeSamplePoint.Z() == minDistanceVertex.Z()){
                break;
            }

            Plane3f cuttingPlane; cuttingPlane.Init(planeSamplePoint, planeNormal);

            MeshModel* cuttingMesh = getIntersectionWithAPlane(textureBBoxMillimeter, cuttingPlane);

            for (int i = 0; i < cuttingMesh->cm.fn; i++) {
                Point3f v0 = cuttingMesh->cm.face[i].V(0)->P();
                Point3f v1 = cuttingMesh->cm.face[i].V(1)->P();
                Point3f v2 = cuttingMesh->cm.face[i].V(2)->P();

                glBegin(GL_TRIANGLES);
                    glTexCoord3f(v0.X()/measurementMillimeter.X(), v0.Y()/measurementMillimeter.Y(), v0.Z()/measurementMillimeter.Z());
                    glVertex3f(v0.X(), v0.Y(), v0.Z());

                    glTexCoord3f(v1.X()/measurementMillimeter.X(), v1.Y()/measurementMillimeter.Y(), v1.Z()/measurementMillimeter.Z());
                    glVertex3f(v1.X(), v1.Y(), v1.Z());

                    glTexCoord3f(v2.X()/measurementMillimeter.X(), v2.Y()/measurementMillimeter.Y(), v2.Z()/measurementMillimeter.Z());
                    glVertex3f(v2.X(), v2.Y(), v2.Z());
                glEnd();
            }
            delete cuttingMesh;
        }
    }
    else if (minDistance <= 0 && maxDistance > 0){
        Point3f planeNormal = -viewDir;
        Point3f planeSamplePoint = maxDistanceVertex;

        while(vcg::SignedDistancePointPlane(planeSamplePoint, viewPlane) > 0.0f) {
            planeSamplePoint -= planeNormal*lamda;
            Plane3f cuttingPlane; cuttingPlane.Init(planeSamplePoint, planeNormal);

            MeshModel* cuttingMesh = getIntersectionWithAPlane(textureBBoxMillimeter, cuttingPlane);

            for (int i = 0; i < cuttingMesh->cm.fn; i++) {
                Point3f v0 = cuttingMesh->cm.face[i].V(0)->P();
                Point3f v1 = cuttingMesh->cm.face[i].V(1)->P();
                Point3f v2 = cuttingMesh->cm.face[i].V(2)->P();

                glBegin(GL_TRIANGLES);
                    glTexCoord3f(v0.X()/measurementMillimeter.X(), v0.Y()/measurementMillimeter.Y(), v0.Z()/measurementMillimeter.Z());
                    glVertex3f(v0.X(), v0.Y(), v0.Z());

                    glTexCoord3f(v1.X()/measurementMillimeter.X(), v1.Y()/measurementMillimeter.Y(), v1.Z()/measurementMillimeter.Z());
                    glVertex3f(v1.X(), v1.Y(), v1.Z());

                    glTexCoord3f(v2.X()/measurementMillimeter.X(), v2.Y()/measurementMillimeter.Y(), v2.Z()/measurementMillimeter.Z());
                    glVertex3f(v2.X(), v2.Y(), v2.Z());
                glEnd();
            }
            delete cuttingMesh;
        }
    }
    glBindTexture(GL_TEXTURE_3D, 0);

    glPopMatrix();
    glPopAttrib();
}


MeshModel* MeshDocument::getIntersectionWithAPlane(MeshModel *mesh, Plane3f plane) {

    MeshModel* base = mesh;
    MeshModel* orig = mesh;

    mesh->updateDataMask(MeshModel::MM_FACEFACETOPO);

    if (vcg::tri::Clean<CMeshO>::CountNonManifoldEdgeFF(base->cm) > 0 || vcg::tri::Clean<CMeshO>::CountNonManifoldVertexFF(base->cm, false) != 0) {
        qDebug() << "Mesh is not two manifold, cannot apply cut";
        return 0;
    }

    MeshDocument dummyMeshDoc;

    MeshModel* cap = new MeshModel(&dummyMeshDoc, "Full name for cap", "Cap");
    MeshModel* intersectionMesh = new MeshModel(&dummyMeshDoc, "Full name for intersection mesh", "Intersection mesh");

    vcg::IntersectionPlaneMesh<CMeshO, CMeshO, float>(orig->cm, plane, cap->cm);
    vcg::tri::Clean<CMeshO>::RemoveDuplicateVertex(cap->cm);

    vcg::tri::CapEdgeMesh(cap->cm, intersectionMesh->cm);
    intersectionMesh->UpdateBoxAndNormals();
    delete cap;
    return intersectionMesh;
}

vcg::Point3f MeshDocument::getVertexMaxDistanceFromView(MeshModel *m, Plane3f viewPlane) {

    Point3f maxDistancePoint = m->cm.vert[0].P();
    float max = vcg::SignedDistancePointPlane(maxDistancePoint, viewPlane);
    for (int i = 1; i < m->cm.vn; i++) {
        Point3f point = m->cm.vert[i].P();
        float distance = vcg::SignedDistancePointPlane(point, viewPlane);
        if (distance > max) {
            max = distance;
            maxDistancePoint = point;
        }
    }

    return maxDistancePoint;
}

vcg::Point3f MeshDocument::getVertexMinDistanceFromView(MeshModel *m, Plane3f viewPlane) {

    Point3f minDistancePoint = m->cm.vert[0].P();
    float min = vcg::SignedDistancePointPlane(minDistancePoint, viewPlane);
    for (int i = 1; i < m->cm.vn; i++) {
        Point3f point = m->cm.vert[i].P();
        float distance = vcg::SignedDistancePointPlane(point, viewPlane);
        if (distance < min) {
            min = distance;
            minDistancePoint = point;
        }
    }

    return minDistancePoint;
}


void MeshModel::Clear()
{
  meshModified() = false;
  glw.m=&cm;
  // These data are always active on the mesh
  currentDataMask = MM_NONE;
  currentDataMask |= MM_VERTCOORD | MM_VERTNORMAL | MM_VERTFLAG ;
  currentDataMask |= MM_FACEVERT  | MM_FACENORMAL | MM_FACEFLAG ;

  visible=true;
  cm.Tr.SetIdentity();
  cm.sfn=0;
  cm.svn=0;
}

void MeshModel::UpdateBoxAndNormals()
{
  tri::UpdateBounding<CMeshO>::Box(cm);
  if(cm.fn>0) {
    tri::UpdateNormal<CMeshO>::PerFaceNormalized(cm);
    tri::UpdateNormal<CMeshO>::PerVertexAngleWeighted(cm);
  }
}

MeshModel::MeshModel(MeshDocument *_parent, QString fullFileName, QString labelName)
:MeshLabRenderMesh()
{

  Clear();
  parent=_parent;
  _id=parent->newMeshId();
  if(!fullFileName.isEmpty())   this->fullPathFileName=fullFileName;
  if(!labelName.isEmpty())     this->_label=labelName;
}

MeshModel::~MeshModel() {
    if (texture3DRawData != NULL)
        delete [] texture3DRawData;

    if (texture3DRGBAData != NULL)
        delete [] texture3DRGBAData;

    if (voxelData != NULL)
        delete [] voxelData;
}

QString MeshModel::relativePathName() const
{
  QDir documentDir (documentPathName());
  QString relPath=documentDir.relativeFilePath(this->fullPathFileName);

  if(relPath.size()>1 && relPath[0]=='.' &&  relPath[1]=='.')
      qDebug("Error we have a mesh that is not in the same folder of the project: %s ",qPrintable(relPath));

  return relPath;
}

QString MeshModel::documentPathName() const
{
  return parent->pathName();
}

int MeshModel::io2mm(int single_iobit)
{
    switch(single_iobit)
    {
        case tri::io::Mask::IOM_NONE					: return  MM_NONE;
        case tri::io::Mask::IOM_VERTCOORD		: return  MM_VERTCOORD;
        case tri::io::Mask::IOM_VERTCOLOR		: return  MM_VERTCOLOR;
        case tri::io::Mask::IOM_VERTFLAGS		: return  MM_VERTFLAG;
        case tri::io::Mask::IOM_VERTQUALITY	: return  MM_VERTQUALITY;
        case tri::io::Mask::IOM_VERTNORMAL		: return  MM_VERTNORMAL;
        case tri::io::Mask::IOM_VERTTEXCOORD : return  MM_VERTTEXCOORD;
        case tri::io::Mask::IOM_VERTRADIUS		: return  MM_VERTRADIUS;

        case tri::io::Mask::IOM_FACEINDEX   		: return  MM_FACEVERT  ;
        case tri::io::Mask::IOM_FACEFLAGS   		: return  MM_FACEFLAG  ;
        case tri::io::Mask::IOM_FACECOLOR   		: return  MM_FACECOLOR  ;
        case tri::io::Mask::IOM_FACEQUALITY 		: return  MM_FACEQUALITY;
        case tri::io::Mask::IOM_FACENORMAL  		: return  MM_FACENORMAL ;

        case tri::io::Mask::IOM_WEDGTEXCOORD 		: return  MM_WEDGTEXCOORD;
        case tri::io::Mask::IOM_WEDGCOLOR				: return  MM_WEDGCOLOR;
        case tri::io::Mask::IOM_WEDGNORMAL   		: return  MM_WEDGNORMAL  ;

        case tri::io::Mask::IOM_BITPOLYGONAL   	: return  MM_POLYGONAL  ;

        default:
            assert(0);
            return MM_NONE;  // FIXME: Returning this is not the best solution (!)
            break;
    } ;
}

void MeshModel::initializeTexture3D(QString organFullPath, Measurement3i measurementPixel, vcg::CallBackPos *cb) {

    DicomProcess dp;
    QMap<int, DataSlice> dataSliceMap = dp.extractOrganSlices(organFullPath, cb);

    int w = measurementPixel.X();
    int h = measurementPixel.Y();
    int n = measurementPixel.Z(); // n is number of slice
    int rawSize = w * h * n;

    texture3DRawData = new signed short[rawSize];
    texture3DMaskData = new signed short[rawSize];
    assert(texture3DRawData);
    assert(texture3DMaskData);

    for (int i = 0; i < n; i++) {
        cv::Mat mat = dataSliceMap[i].mat;
        cv::Mat mask = dataSliceMap[i].mask;

        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                texture3DRawData[i*(h*w) + y*w + x] = mat.at<signed short>(y, x);
                texture3DMaskData[i*(h*w) + y*w + x] = mask.at<signed short>(y, x);
            }
        }
    }

//    int rgbaSize = rawSize * 4; // Convert luminance to rgba type
//    texture3DRGBAData = new uchar[rgbaSize];
//    assert(texture3DRGBAData);

//    for (int idx = 0; idx < rawSize; idx++) {
//        uchar value = texture3DRawData[idx];
//        texture3DRGBAData[idx*4] = value;
//        texture3DRGBAData[idx*4 + 1] = value;
//        texture3DRGBAData[idx*4 + 2] = value;
//        texture3DRGBAData[idx*4 + 3] = value == 0 ? 0 : 50; // default opacity is 50
    //    }
}

void MeshModel::initializeTexture3DPatient(QString path, Measurement3i measurementPixel, CallBackPos *cb)
{
    QDir dirOriginal(path);
    assert(dirOriginal.exists());

    QFileInfoList originalDicoms = dirOriginal.entryInfoList();
    QMap<int, DataSlice> dataSliceMap;

    int idx = 0;
    int count = 100;
    int step = originalDicoms.size() / count;
    count = originalDicoms.size() / step;

    for (int i = 0; i < originalDicoms.size(); i++) {
        try {
            QFileInfo file = originalDicoms.at(i);
            DicomInfo dicom(file.absoluteFilePath());
            DataSlice slice;

            slice.instanceNumber = dicom.getInstanceNumber();
            slice.xScale = dicom.getPixelSpacingX();
            slice.yScale = dicom.getPixelSpacingY();
            slice.zScale = dicom.getInstanceNumber() * dicom.getSliceThickness();
            slice.zSpace = dicom.getSliceThickness();
            slice.mat = dicom.getOrginIntensityImage(1024);

            dataSliceMap[slice.instanceNumber] = slice;
        } catch (imebra::StreamReadError exception) {
            std::cout << "imebra::StreamReadError" << std::endl;
        } catch (imebra::StreamOpenError exception) {
            std::cout << "imebra::StreamOpenError" << std::endl;
        } catch (imebra::MissingTagError exception) {
            std::cout << "imebra::MissingTagError" << std::endl;
        } catch (imebra::StreamEOFError exception) {
            std::cout << "imebra::StreamEOFError" << std::endl;
        } catch (imebra::CodecWrongFormatError exception) {
            std::cout << "imebra::CodecWrongFormatError" << std::endl;
        }

        if (i % step == 0) {
            if (cb) {
                cb(100 * idx * 1.0 / count, "Extracting slice");
            }
            idx++;
        }
    }

    int w = measurementPixel.X();
    int h = measurementPixel.Y();
    int n = measurementPixel.Z(); // n is number of slice
    int rawSize = w * h * n;

    texture3DRawData = new signed short[rawSize];
    texture3DMaskData = new signed short[rawSize];
    assert(texture3DRawData);
    assert(texture3DMaskData);

    for (int i = 0; i < n; i++) {
        cv::Mat mat = dataSliceMap[i].mat;
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                texture3DRawData[i*(h*w) + y*w + x] = mat.at<signed short>(y, x);
                texture3DMaskData[i*(h*w) + y*w + x] = -1;
            }
        }
    }

    qDebug() << "Initialize texture done";
}

Plane::Plane(const Plane& pl)
{
    semantic = pl.semantic;
    fullPathFileName = pl.fullPathFileName;
    image = QImage(pl.image);
}

Plane::Plane(const QString pathName, const int _semantic)
{
    semantic =_semantic;
    fullPathFileName = pathName;

    image = QImage(pathName);
}

RasterModel::RasterModel(MeshDocument *parent, QString _rasterName)
: MeshLabRenderRaster()
{
  _id=parent->newRasterId();
  par = parent;
  this->_label= _rasterName;
  visible=true;
}

RasterModel::RasterModel()
: MeshLabRenderRaster()
{

}


MeshLabRenderRaster::MeshLabRenderRaster()
{

}

MeshLabRenderRaster::MeshLabRenderRaster( const MeshLabRenderRaster& rm )
:shot(rm.shot),planeList()
{
    for(QList<Plane*>::const_iterator it = rm.planeList.begin();it != rm.planeList.end();++it)
    {
        planeList.push_back(new Plane(**it));
        if (rm.currentPlane == *it)
            currentPlane = planeList[planeList.size() - 1];
    }
}

void MeshLabRenderRaster::addPlane(Plane *plane)
{
    planeList.append(plane);
    currentPlane = plane;
}

MeshLabRenderRaster::~MeshLabRenderRaster()
{
    currentPlane = NULL;
    for(int ii = 0;ii < planeList.size();++ii)
        delete planeList[ii];
}

void MeshModelState::create(int _mask, MeshModel* _m)
{
    m=_m;
    changeMask=_mask;
    if(changeMask & MeshModel::MM_VERTCOLOR)
    {
        vertColor.resize(m->cm.vert.size());
        std::vector<Color4b>::iterator ci;
        CMeshO::VertexIterator vi;
        for(vi = m->cm.vert.begin(), ci = vertColor.begin(); vi != m->cm.vert.end(); ++vi, ++ci)
            if(!(*vi).IsD()) (*ci)=(*vi).C();
    }

    if(changeMask & MeshModel::MM_VERTQUALITY)
    {
        vertQuality.resize(m->cm.vert.size());
        std::vector<float>::iterator qi;
        CMeshO::VertexIterator vi;
        for(vi = m->cm.vert.begin(), qi = vertQuality.begin(); vi != m->cm.vert.end(); ++vi, ++qi)
            if(!(*vi).IsD()) (*qi)=(*vi).Q();
    }

    if(changeMask & MeshModel::MM_VERTCOORD)
    {
        vertCoord.resize(m->cm.vert.size());
        std::vector<Point3f>::iterator ci;
        CMeshO::VertexIterator vi;
        for(vi = m->cm.vert.begin(), ci = vertCoord.begin(); vi != m->cm.vert.end(); ++vi, ++ci)
             if(!(*vi).IsD()) (*ci)=(*vi).P();
    }

    if(changeMask & MeshModel::MM_VERTNORMAL)
    {
        vertNormal.resize(m->cm.vert.size());
        std::vector<Point3f>::iterator ci;
        CMeshO::VertexIterator vi;
        for(vi = m->cm.vert.begin(), ci = vertNormal.begin(); vi != m->cm.vert.end(); ++vi, ++ci)
             if(!(*vi).IsD()) (*ci)=(*vi).N();
    }

    if(changeMask & MeshModel::MM_FACEFLAGSELECT)
    {
        faceSelection.resize(m->cm.face.size());
        std::vector<bool>::iterator ci;
        CMeshO::FaceIterator fi;
        for(fi = m->cm.face.begin(), ci = faceSelection.begin(); fi != m->cm.face.end(); ++fi, ++ci)
         if(!(*fi).IsD()) (*ci) = (*fi).IsS();
    }

    if(changeMask & MeshModel::MM_VERTFLAGSELECT)
    {
        vertSelection.resize(m->cm.vert.size());
        std::vector<bool>::iterator ci;
        CMeshO::VertexIterator vi;
        for(vi = m->cm.vert.begin(), ci = vertSelection.begin(); vi != m->cm.vert.end(); ++vi, ++ci)
            if(!(*vi).IsD()) (*ci) = (*vi).IsS();
    }

  if(changeMask & MeshModel::MM_TRANSFMATRIX)
      Tr = m->cm.Tr;
  if(changeMask & MeshModel::MM_CAMERA)
      this->shot = m->cm.shot;
}

bool MeshModelState::apply(MeshModel *_m)
{
  if(_m != m)
      return false;
    if(changeMask & MeshModel::MM_VERTCOLOR)
    {
        if(vertColor.size() != m->cm.vert.size()) return false;
        std::vector<Color4b>::iterator ci;
        CMeshO::VertexIterator vi;
        for(vi = m->cm.vert.begin(), ci = vertColor.begin(); vi != m->cm.vert.end(); ++vi, ++ci)
            if(!(*vi).IsD()) (*vi).C()=(*ci);
    }
    if(changeMask & MeshModel::MM_VERTQUALITY)
    {
        if(vertQuality.size() != m->cm.vert.size()) return false;
        std::vector<float>::iterator qi;
        CMeshO::VertexIterator vi;
        for(vi = m->cm.vert.begin(), qi = vertQuality.begin(); vi != m->cm.vert.end(); ++vi, ++qi)
            if(!(*vi).IsD()) (*vi).Q()=(*qi);
    }

    if(changeMask & MeshModel::MM_VERTCOORD)
    {
        if(vertCoord.size() != m->cm.vert.size()) return false;
        std::vector<Point3f>::iterator ci;
        CMeshO::VertexIterator vi;
        for(vi = m->cm.vert.begin(), ci = vertCoord.begin(); vi != m->cm.vert.end(); ++vi, ++ci)
            if(!(*vi).IsD()) (*vi).P()=(*ci);
    }

    if(changeMask & MeshModel::MM_VERTNORMAL)
    {
        if(vertNormal.size() != m->cm.vert.size()) return false;
        std::vector<Point3f>::iterator ci;
        CMeshO::VertexIterator vi;
        for(vi = m->cm.vert.begin(), ci=vertNormal.begin(); vi != m->cm.vert.end(); ++vi, ++ci)
            if(!(*vi).IsD()) (*vi).N()=(*ci);

        //now reset the face normals
        tri::UpdateNormal<CMeshO>::PerFaceNormalized(m->cm);
    }

    if(changeMask & MeshModel::MM_FACEFLAGSELECT)
    {
        if(faceSelection.size() != m->cm.face.size()) return false;
        std::vector<bool>::iterator ci;
        CMeshO::FaceIterator fi;
        for(fi = m->cm.face.begin(), ci = faceSelection.begin(); fi != m->cm.face.end(); ++fi, ++ci)
        {
            if((*ci))
                (*fi).SetS();
            else
                (*fi).ClearS();
        }
    }

    if(changeMask & MeshModel::MM_VERTFLAGSELECT)
    {
        if(vertSelection.size() != m->cm.vert.size()) return false;
        std::vector<bool>::iterator ci;
        CMeshO::VertexIterator vi;
        for(vi = m->cm.vert.begin(), ci = vertSelection.begin(); vi != m->cm.vert.end(); ++vi, ++ci)
        {
            if((*ci))
                (*vi).SetS();
            else
                (*vi).ClearS();
        }
    }


    if(changeMask & MeshModel::MM_TRANSFMATRIX)
        m->cm.Tr=Tr;
    if(changeMask & MeshModel::MM_CAMERA)
        m->cm.shot = this->shot;

    return true;
}

/**** DATAMASK STUFF ****/

bool MeshModel::hasDataMask(const int maskToBeTested) const
{
  return ((currentDataMask & maskToBeTested)!= 0);
}
  void MeshModel::updateDataMask(MeshModel *m)
{
  updateDataMask(m->currentDataMask);
}

void MeshModel::updateDataMask(int neededDataMask)
{
    if((neededDataMask & MM_FACEFACETOPO)!=0)
    {
        if (!hasDataMask(MM_FACEFACETOPO))
            cm.face.EnableFFAdjacency();
        tri::UpdateTopology<CMeshO>::FaceFace(cm);
    }
    if((neededDataMask & MM_VERTFACETOPO)!=0)
    {
        if (!hasDataMask(MM_VERTFACETOPO))
        {
            cm.vert.EnableVFAdjacency();
            cm.face.EnableVFAdjacency();
        }
        tri::UpdateTopology<CMeshO>::VertexFace(cm);
    }
    if( ( (neededDataMask & MM_WEDGTEXCOORD)!=0)	&& !hasDataMask(MM_WEDGTEXCOORD)) 	cm.face.EnableWedgeTexCoord();
    if( ( (neededDataMask & MM_FACECOLOR)!=0)			&& !hasDataMask(MM_FACECOLOR))			cm.face.EnableColor();
    if( ( (neededDataMask & MM_FACEQUALITY)!=0)		&& !hasDataMask(MM_FACEQUALITY))		cm.face.EnableQuality();
    if( ( (neededDataMask & MM_FACEMARK)!=0)			&& !hasDataMask(MM_FACEMARK))				cm.face.EnableMark();
    if( ( (neededDataMask & MM_VERTMARK)!=0)			&& !hasDataMask(MM_VERTMARK))				cm.vert.EnableMark();
    if( ( (neededDataMask & MM_VERTCURV)!=0)			&& !hasDataMask(MM_VERTCURV))				cm.vert.EnableCurvature();
    if( ( (neededDataMask & MM_VERTCURVDIR)!=0)		&& !hasDataMask(MM_VERTCURVDIR))		cm.vert.EnableCurvatureDir();
    if( ( (neededDataMask & MM_VERTRADIUS)!=0)		&& !hasDataMask(MM_VERTRADIUS))			cm.vert.EnableRadius();
    if( ( (neededDataMask & MM_VERTTEXCOORD)!=0)  && !hasDataMask(MM_VERTTEXCOORD))		cm.vert.EnableTexCoord();

    //  if(  ( (neededDataMask & MM_FACEFLAGBORDER) && !hasDataMask(MM_FACEFLAGBORDER) ) ||
    //       ( (neededDataMask & MM_VERTFLAGBORDER) && !hasDataMask(MM_VERTFLAGBORDER) )    )
    //  {
    //    if( (currentDataMask & MM_FACEFACETOPO) || (neededDataMask & MM_FACEFACETOPO))
    //         tri::UpdateFlags<CMeshO>::FaceBorderFromFF(cm);
    //    else tri::UpdateFlags<CMeshO>::FaceBorderFromNone(cm);
    //    tri::UpdateFlags<CMeshO>::VertexBorderFromFace(cm);
    //  }

    currentDataMask |= neededDataMask;
 }

void MeshModel::clearDataMask(int unneededDataMask)
{
  if( ( (unneededDataMask & MM_VERTFACETOPO)!=0)	&& hasDataMask(MM_VERTFACETOPO)) {cm.face.DisableVFAdjacency();
                                                                                    cm.vert.DisableVFAdjacency(); }
  if( ( (unneededDataMask & MM_FACEFACETOPO)!=0)	&& hasDataMask(MM_FACEFACETOPO))	cm.face.DisableFFAdjacency();

  if( ( (unneededDataMask & MM_WEDGTEXCOORD)!=0)	&& hasDataMask(MM_WEDGTEXCOORD)) 	cm.face.DisableWedgeTexCoord();
  if( ( (unneededDataMask & MM_FACECOLOR)!=0)			&& hasDataMask(MM_FACECOLOR))			cm.face.DisableColor();
  if( ( (unneededDataMask & MM_FACEQUALITY)!=0)		&& hasDataMask(MM_FACEQUALITY))		cm.face.DisableQuality();
  if( ( (unneededDataMask & MM_FACEMARK)!=0)			&& hasDataMask(MM_FACEMARK))			cm.face.DisableMark();
  if( ( (unneededDataMask & MM_VERTMARK)!=0)			&& hasDataMask(MM_VERTMARK))			cm.vert.DisableMark();
  if( ( (unneededDataMask & MM_VERTCURV)!=0)			&& hasDataMask(MM_VERTCURV))			cm.vert.DisableCurvature();
  if( ( (unneededDataMask & MM_VERTCURVDIR)!=0)		&& hasDataMask(MM_VERTCURVDIR))		cm.vert.DisableCurvatureDir();
  if( ( (unneededDataMask & MM_VERTRADIUS)!=0)		&& hasDataMask(MM_VERTRADIUS))		cm.vert.DisableRadius();
  if( ( (unneededDataMask & MM_VERTTEXCOORD)!=0)	&& hasDataMask(MM_VERTTEXCOORD))	cm.vert.DisableTexCoord();

  currentDataMask = currentDataMask & (~unneededDataMask);
}

void MeshModel::Enable(int openingFileMask)
{
  if( openingFileMask & tri::io::Mask::IOM_VERTTEXCOORD )
      updateDataMask(MM_VERTTEXCOORD);
  if( openingFileMask & tri::io::Mask::IOM_WEDGTEXCOORD )
      updateDataMask(MM_WEDGTEXCOORD);
  if( openingFileMask & tri::io::Mask::IOM_VERTCOLOR    )
      updateDataMask(MM_VERTCOLOR);
  if( openingFileMask & tri::io::Mask::IOM_FACECOLOR    )
      updateDataMask(MM_FACECOLOR);
  if( openingFileMask & tri::io::Mask::IOM_VERTRADIUS   ) updateDataMask(MM_VERTRADIUS);
  if( openingFileMask & tri::io::Mask::IOM_CAMERA       ) updateDataMask(MM_CAMERA);
  if( openingFileMask & tri::io::Mask::IOM_VERTQUALITY  ) updateDataMask(MM_VERTQUALITY);
  if( openingFileMask & tri::io::Mask::IOM_FACEQUALITY  ) updateDataMask(MM_FACEQUALITY);
  if( openingFileMask & tri::io::Mask::IOM_BITPOLYGONAL ) updateDataMask(MM_POLYGONAL);
}

bool& MeshModel::meshModified()
{
    return this->modified;
}

MeshLabRenderMesh::MeshLabRenderMesh()
:glw(),cm()
{
}

MeshLabRenderMesh::MeshLabRenderMesh(CMeshO& mesh )
:glw(),cm()
{
    vcg::tri::Append<CMeshO,CMeshO>::MeshCopy(cm,mesh);
    //cm.Tr = mesh.Tr;
    cm.Tr.SetIdentity();
    cm.sfn = mesh.sfn;
    cm.svn = mesh.svn;
    glw.m = &cm;
}

bool MeshLabRenderMesh::render(vcg::GLW::DrawMode dm,vcg::GLW::ColorMode colm,vcg::GLW::TextureMode tm )
{
    if (glw.m != NULL)
    {
        glPushAttrib(GL_ALL_ATTRIB_BITS);
        glPushMatrix();
        glMultMatrix(glw.m->Tr);
        if( (colm == vcg::GLW::CMPerFace)  && (!vcg::tri::HasPerFaceColor(*glw.m)) )
            colm=vcg::GLW::CMNone;
        if( (tm == vcg::GLW::TMPerWedge )&& (!vcg::tri::HasPerWedgeTexCoord(*glw.m)) )
            tm=vcg::GLW::TMNone;
        if( (tm == vcg::GLW::TMPerWedgeMulti )&& (!vcg::tri::HasPerWedgeTexCoord(*glw.m)))
            tm=vcg::GLW::TMNone;
        if( (tm == vcg::GLW::TMPerVert )&& (!vcg::tri::HasPerVertexTexCoord(*glw.m)))
            tm=vcg::GLW::TMNone;
        glw.Draw(dm,colm,tm);
        glPopMatrix();
        glPopAttrib();
        return true;
    }
    return false;
}

bool MeshLabRenderMesh::renderSelectedFace()
{
    if (glw.m != NULL)
    {
        glPushAttrib(GL_ALL_ATTRIB_BITS);
        glEnable(GL_POLYGON_OFFSET_FILL);
        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glDepthMask(GL_FALSE);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) ;
        glColor4f(1.0f,0.0,0.0,.3f);
        glPolygonOffset(-1.0, -1);
        CMeshO::FaceIterator fi;
        glPushMatrix();
        glMultMatrix(glw.m->Tr);
        glBegin(GL_TRIANGLES);
        glw.m->sfn=0;
        for(fi=glw.m->face.begin();fi!=glw.m->face.end();++fi)
        {
            if(!(*fi).IsD() && (*fi).IsS())
            {
                glVertex((*fi).cP(0));
                glVertex((*fi).cP(1));
                glVertex((*fi).cP(2));
                ++glw.m->sfn;
            }
        }
        glEnd();
        glPopMatrix();
        glPopAttrib();
        return true;
    }
    return false;
}

bool MeshLabRenderMesh::renderSelectedVert()
{
    if (glw.m != NULL)
    {
        glPushAttrib(GL_ALL_ATTRIB_BITS);
        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glDepthMask(GL_FALSE);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) ;
        glColor4f(1.0f,0.0,0.0,.3f);
        glDepthRange(0.00,0.999);
        glPointSize(3.0);
        glPushMatrix();
        glMultMatrix(glw.m->Tr);
        glBegin(GL_POINTS);
        glw.m->svn=0;
        CMeshO::VertexIterator vi;
        for(vi=glw.m->vert.begin();vi!=glw.m->vert.end();++vi)
        {
            if(!(*vi).IsD() && (*vi).IsS())
            {
                glVertex((*vi).cP());
                ++glw.m->svn;
            }
        }
        glEnd();
        glPopMatrix();
        glPopAttrib();
        return true;
    }
    return false;
}

MeshLabRenderMesh::~MeshLabRenderMesh()
{
    glw.m = NULL;
    cm.Clear();
    CMeshO::VertContainer tempVert;
    CMeshO::FaceContainer tempFace;
    cm.vert.swap(tempVert);
    cm.face.swap(tempFace);
}

MeshLabRenderState::MeshLabRenderState()
:_meshmap(),_meshmut(QReadWriteLock::Recursive),_rastermut(QReadWriteLock::Recursive)
{

}

MeshLabRenderState::~MeshLabRenderState()
{
    clearState();
}

bool MeshLabRenderState::update(const int id,CMeshO& mm,const int updateattributesmask)
{
    if (updateattributesmask == MeshModel::MM_NONE)
        return false;
    lockRenderState(MESH,WRITE);
    QMap<int,MeshLabRenderMesh*>::iterator it = _meshmap.find(id);
    if (it != _meshmap.end())
    {
        MeshLabRenderMesh* rm = *it;

        if (!(updateattributesmask & MeshModel::MM_VERTCOLOR) &
            !(updateattributesmask & MeshModel::MM_VERTCOORD) &
            !(updateattributesmask & MeshModel::MM_VERTQUALITY) &
            !(updateattributesmask & MeshModel::MM_VERTNORMAL) &
            !(updateattributesmask & MeshModel::MM_FACEFLAGSELECT) &
            !(updateattributesmask & MeshModel::MM_VERTFLAGSELECT) &
            !(updateattributesmask & MeshModel::MM_TRANSFMATRIX) &
            !(updateattributesmask & MeshModel::MM_CAMERA))
        {
            remove(it);
            _meshmap[id] = new MeshLabRenderMesh(mm);
            unlockRenderState(MESH);
            return true;
        }

        if (updateattributesmask & MeshModel::MM_VERTCOLOR)
        {
            if(mm.vert.size() != (rm->cm.vert.size()))
            {
                unlockRenderState(MESH);
                return false;
            }
            else
            {
                CMeshO::VertexIterator rmvi = rm->cm.vert.begin();
                for(CMeshO::ConstVertexIterator mmvi = mm.vert.begin(); mmvi != mm.vert.end(); ++mmvi, ++rmvi)
                    if(!(*mmvi).IsD())
                        (*rmvi).C()=(*mmvi).cC();
            }
        }

        if (updateattributesmask & MeshModel::MM_VERTCOORD)
        {
            if(mm.vert.size() != (rm->cm.vert.size()))
            {
                unlockRenderState(MESH);
                return false;
            }
            else
            {
                CMeshO::VertexIterator rmvi = rm->cm.vert.begin();
                for(CMeshO::ConstVertexIterator mmvi = mm.vert.begin(); mmvi != mm.vert.end(); ++mmvi, ++rmvi)
                    if(!(*mmvi).IsD())
                        (*rmvi).P()=(*mmvi).cP();
            }
        }

        if (updateattributesmask & MeshModel::MM_VERTQUALITY)
        {
            if(mm.vert.size() != (rm->cm.vert.size()))
            {
                unlockRenderState(MESH);
                return false;
            }
            else
            {
                CMeshO::VertexIterator rmvi = rm->cm.vert.begin();
                for(CMeshO::ConstVertexIterator mmvi = mm.vert.begin(); mmvi != mm.vert.end(); ++mmvi, ++rmvi)
                    if(!(*mmvi).IsD())
                        (*rmvi).Q()=(*mmvi).cQ();
            }
        }

        if	(updateattributesmask & MeshModel::MM_VERTNORMAL)
        {
            if(mm.vert.size() != (rm->cm.vert.size()))
            {
                unlockRenderState(MESH);
                return false;
            }
            else
            {
                CMeshO::VertexIterator rmvi = rm->cm.vert.begin();
                for(CMeshO::ConstVertexIterator mmvi = mm.vert.begin(); mmvi != mm.vert.end(); ++mmvi, ++rmvi)
                    if(!(*mmvi).IsD())
                        (*rmvi).N()=(*mmvi).cN();
            }
        }

        if(updateattributesmask & MeshModel::MM_FACEFLAGSELECT)
        {
            if(mm.face.size() != rm->cm.face.size())
            {
                unlockRenderState(MESH);
                return false;
            }
            CMeshO::FaceIterator rmfi = rm->cm.face.begin();
            for(CMeshO::ConstFaceIterator mmfi = mm.face.begin(); mmfi != mm.face.end(); ++mmfi, ++rmfi)
            {
                if ((!(*mmfi).IsD()) && ((*mmfi).IsS()))
                    (*rmfi).SetS();
                else
                    if (!(*mmfi).IsS())
                        (*rmfi).ClearS();
            }
        }

        if(updateattributesmask & MeshModel::MM_VERTFLAGSELECT)
        {
            if(mm.vert.size() != (rm->cm.vert.size()))
            {
                unlockRenderState(MESH);
                return false;
            }
            else
            {
                CMeshO::VertexIterator rmvi = rm->cm.vert.begin();
                for(CMeshO::ConstVertexIterator mmvi = mm.vert.begin(); mmvi != mm.vert.end(); ++mmvi, ++rmvi)
                {
                    if ((!(*mmvi).IsD()) && ((*mmvi).IsS()))
                        (*rmvi).SetS();
                    else
                        if (!(*mmvi).IsS())
                            (*rmvi).ClearS();
                }
            }
        }

        if(updateattributesmask & MeshModel::MM_TRANSFMATRIX)
            rm->cm.Tr = mm.Tr;
        if(updateattributesmask & MeshModel::MM_CAMERA)
            rm->cm.shot = mm.shot;
        unlockRenderState(MESH);
        return true;
    }
    unlockRenderState(MESH);
    return false;
}

void MeshLabRenderState::add(const int id,CMeshO& mm )
{
    lockRenderState(MESH,WRITE);
    if (!_meshmap.contains(id))
    {
        _meshmap[id] = new MeshLabRenderMesh(mm);
    }
    unlockRenderState(MESH);
}

QMap<int,MeshLabRenderMesh*>::iterator MeshLabRenderState::remove(QMap<int,MeshLabRenderMesh*>::iterator it )
{
    lockRenderState(MESH,WRITE);
    if (it != _meshmap.end())
    {
        MeshLabRenderMesh* tmp = it.value();
        delete tmp;
        QMap<int,MeshLabRenderMesh*>::iterator tmpit = _meshmap.erase(it);
        unlockRenderState(MESH);
        return tmpit;
    }
    unlockRenderState(MESH);
    return _meshmap.end();
}

void MeshLabRenderState::clearState()
{
    lockRenderState(MESH,WRITE);
    QMap<int,MeshLabRenderMesh*>::iterator it = _meshmap.begin();
    while(it != _meshmap.end())
        it = remove(it);
    unlockRenderState(MESH);
    lockRenderState(RASTER,WRITE);
    QMap<int,MeshLabRenderRaster*>::iterator itr = _rastermap.begin();
    while(itr != _rastermap.end())
        itr = remove(itr);
    unlockRenderState(RASTER);
}

void MeshLabRenderState::copyBack( const int /*id*/,CMeshO& /*mm*/ ) const
{
//	mm.Clear();
//	mm.vert.swap(CMeshO::VertContainer());
//	mm.face.swap(CMeshO::FaceContainer());
//	vcg::tri::Append<CMeshO,CMeshO>::MeshCopy(mm,_rendermap[id]->cm);
}

void MeshLabRenderState::render( const int id,vcg::GLW::DrawMode dm,vcg::GLW::ColorMode cm,vcg::GLW::TextureMode tm  )
{
    lockRenderState(MESH,READ);
    QMap<int,MeshLabRenderMesh*>::const_iterator it = _meshmap.find(id);
    if (it != _meshmap.end())
        it.value()->render(dm,cm,tm);
    unlockRenderState(MESH);
}

void MeshLabRenderState::render(vcg::GLW::DrawMode dm,vcg::GLW::ColorMode cm,vcg::GLW::TextureMode tm  )
{
    lockRenderState(MESH,READ);
    for(QMap<int,MeshLabRenderMesh*>::iterator it = _meshmap.begin();it != _meshmap.end();++it)
        (*it)->render(dm,cm,tm);
    unlockRenderState(MESH);
}

bool MeshLabRenderState::isEntityInRenderingState( const int id,const MESHLAB_RENDER_ENTITY ent)
{
    bool found = false;
    switch(ent)
    {
        case (MESH):
        {
            lockRenderState(MESH,READ);
            found = _meshmap.contains(id);
            unlockRenderState(MESH);
        }

        case (RASTER):
        {
            lockRenderState(RASTER,READ);
            found = _meshmap.contains(id);
            unlockRenderState(RASTER);
        }
    }
    return found;
}

void MeshLabRenderState::add( const int id,const MeshLabRenderRaster& rm )
{
    lockRenderState(RASTER,WRITE);
    if (!_rastermap.contains(id))
    {
        _rastermap[id] = new MeshLabRenderRaster(rm);
    }
    unlockRenderState(RASTER);
}

bool MeshLabRenderState::update( const int id,const MeshLabRenderRaster& rm,const int updateattributesmask)
{
    if (updateattributesmask & RasterModel::RM_NONE)
        return false;
    lockRenderState(RASTER,WRITE);
    QMap<int,MeshLabRenderRaster*>::iterator it = _rastermap.find(id);
    if (it != _rastermap.end())
    {
        remove(it);
        _rastermap[id] = new MeshLabRenderRaster(rm);
        unlockRenderState(RASTER);
        return true;
    }
    unlockRenderState(RASTER);
    return false;
}

QMap<int,MeshLabRenderRaster*>::iterator MeshLabRenderState::remove( QMap<int,MeshLabRenderRaster*>::iterator it )
{
    lockRenderState(RASTER,WRITE);
    if (it != _rastermap.end())
    {
        MeshLabRenderRaster* tmp = it.value();
        delete tmp;
        QMap<int,MeshLabRenderRaster*>::iterator tmpit = _rastermap.erase(it);
        unlockRenderState(RASTER);
        return tmpit;
    }
    unlockRenderState(RASTER);
    return _rastermap.end();
}

void MeshLabRenderState::lockRenderState( const MESHLAB_RENDER_ENTITY ent,const MESHLAB_RENDER_STATE_ACTION act )
{
    switch(ent)
    {
        case (MESH):
        {
            lockReadOrWrite(_meshmut,act);
            break;
        }
        case (RASTER):
        {
            lockReadOrWrite(_rastermut,act);
            break;
        }
    }
}

void MeshLabRenderState::unlockRenderState( const MESHLAB_RENDER_ENTITY ent )
{
    switch(ent)
    {
        case (MESH):
        {
            _meshmut.unlock();
            break;
        }
        case (RASTER):
        {
            _rastermut.unlock();
            break;
        }
    }
}

void MeshLabRenderState::lockReadOrWrite( QReadWriteLock& mutex,const MESHLAB_RENDER_STATE_ACTION act )
{
    switch(act)
    {
        case (READ):
        {
            mutex.lockForRead();
            break;
        }
        case (WRITE):
        {
            mutex.lockForWrite();
            break;
        }
    }
}

VolumeProperty::VolumeProperty(QWidget *parent) : QWidget(parent) {

}
VolumeProperty::~VolumeProperty(){}

IntensityColorTable::IntensityColorTable()
{
    initPreset();
    updateColorTable();
}
void MeshDocument::updateLimitedPoints(vcg::Point3f p0, vcg::Point3f p1, vcg::Point3f p2, vcg::Point3f p3, vcg::Point3f p4, vcg::Point3f p5, vcg::Point3f p6, vcg::Point3f p7) {
    limitedP0 = p0;
    limitedP1 = p1;
    limitedP2 = p2;
    limitedP3 = p3;
    limitedP4 = p4;
    limitedP5 = p5;
    limitedP6 = p6;
    limitedP7 = p7;
}

Color4f IntensityColorTable::mapIntensity(float intensity)
{
    // If there is no color map, just return the intensity
    if (colorDict.size() == 0) return Color4f(intensity, intensity, intensity, 255);
    QMap<float, Color4f>::Iterator i = colorDict.lowerBound(intensity);
    if (i == colorDict.begin()) {
        return colorDict[i.key()];
    } else {
        float lowerBound = (i - 1).key();
        float upperBound = (i.key());
        Color4f lowerColor = colorDict[lowerBound];
        Color4f upperColor = colorDict[upperBound];

        // Linear Interpolation
        float red = lowerColor[0] +
                1.0 * (intensity - lowerBound) *
                (upperColor[0] - lowerColor[0]) / (upperBound - lowerBound);
        float green = lowerColor[1] +
                (intensity - lowerBound) *
                1.0 * (upperColor[2] - lowerColor[1]) / (upperBound - lowerBound);
        float blue = lowerColor[2] +
                (intensity - lowerBound) *
                1.0 * (upperColor[2] - lowerColor[2]) / (upperBound - lowerBound);
        float alpha = lowerColor[3] +
                1.0 * (intensity - lowerBound) *
                (upperColor[3] - lowerColor[3]) / (upperBound - lowerBound);
        return Color4f(red, green ,blue , alpha);
    }
}

void MeshDocument::updateLimitedSphereParams(Point3f center, float radius) {
    sphereCenter = center;
    sphereRadius = radius;
}

void IntensityColorTable::setColorPoint(float intensity, Color4f color)
{

}

void IntensityColorTable::initPreset()
{
    // Init color list preset
//    colorDict[-1024] = Color4f(0, 0, 0, 0);
//    colorDict[-86.9767] = Color4f(0, 0.25098 * 255, 255, 0);
//    colorDict[45.3791] = Color4f(255, 0, 0, 0.169643 * 255);
//    colorDict[139.919] = Color4f(255, 0.894893 * 255, 0.894893 * 255, 0.589286 * 255);
//    colorDict[347.907] = Color4f(255, 255, 0.25098 * 255, 0.607143 * 255);
//    colorDict[1023] = Color4f(255, 255, 255, 0.607143 * 255);
//    colorDict[3071] = Color4f(0.827451 * 255, 0.658824 * 255, 255, 0.616071 * 255);

    colorDict[-1024] = Color4f(0, 0, 0, 0);
    colorDict[149.113] = Color4f(0, 0, 0, 0);
    colorDict[157.884] = Color4f(0.501961 * 255, 0.25098 * 255, 0, 0.482143 * 255);
    colorDict[339.96] = Color4f(0.695386 * 255, 0.59603 * 255, 0.36886 * 255, 0.660714 * 255);
    colorDict[388.526] = Color4f(0.854902 * 255, 0.85098 * 255, 0.827451 * 255, 0.830357 * 255);
    colorDict[1197.95] = Color4f(255, 255, 255, 0.839286 * 255);
    colorDict[1024] = Color4f(255, 255, 255, 0.848214 * 255);
}

void MeshDocument::drawLimitedShape() {

    switch (currentLimitedShape) {
    case Cube:
        drawLimitedBox();
        drawAxes();
        break;
    case Sphere:
        drawLimitedSphere();
        drawAxes();
        break;
    default:
        break;
    }
}

void MeshDocument::drawLimitedBox() {
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPushMatrix();

    glUseProgram(limitedShaderPro);

    glShadeModel(GL_SMOOTH);
    glDisable(GL_ALPHA_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBlendEquation(GL_FUNC_ADD);
    glDisable(GL_DEPTH_TEST);
    glColor4f(limitedCubeColor.redF(), limitedCubeColor.greenF(), limitedCubeColor.blueF(), 1);

    glBegin(GL_QUADS);
        glVertex3f(limitedP0.X(), limitedP0.Y(), limitedP0.Z());
        glVertex3f(limitedP1.X(), limitedP1.Y(), limitedP1.Z());
        glVertex3f(limitedP2.X(), limitedP2.Y(), limitedP2.Z());
        glVertex3f(limitedP3.X(), limitedP3.Y(), limitedP3.Z());

        glVertex3f(limitedP4.X(), limitedP4.Y(), limitedP4.Z());
        glVertex3f(limitedP5.X(), limitedP5.Y(), limitedP5.Z());
        glVertex3f(limitedP6.X(), limitedP6.Y(), limitedP6.Z());
        glVertex3f(limitedP7.X(), limitedP7.Y(), limitedP7.Z());

        glVertex3f(limitedP0.X(), limitedP0.Y(), limitedP0.Z());
        glVertex3f(limitedP1.X(), limitedP1.Y(), limitedP1.Z());
        glVertex3f(limitedP5.X(), limitedP5.Y(), limitedP5.Z());
        glVertex3f(limitedP4.X(), limitedP4.Y(), limitedP4.Z());

        glVertex3f(limitedP1.X(), limitedP1.Y(), limitedP1.Z());
        glVertex3f(limitedP2.X(), limitedP2.Y(), limitedP2.Z());
        glVertex3f(limitedP6.X(), limitedP6.Y(), limitedP6.Z());
        glVertex3f(limitedP5.X(), limitedP5.Y(), limitedP5.Z());

        glVertex3f(limitedP2.X(), limitedP2.Y(), limitedP2.Z());
        glVertex3f(limitedP3.X(), limitedP3.Y(), limitedP3.Z());
        glVertex3f(limitedP7.X(), limitedP7.Y(), limitedP7.Z());
        glVertex3f(limitedP6.X(), limitedP6.Y(), limitedP6.Z());

        glVertex3f(limitedP3.X(), limitedP3.Y(), limitedP3.Z());
        glVertex3f(limitedP0.X(), limitedP0.Y(), limitedP0.Z());
        glVertex3f(limitedP4.X(), limitedP4.Y(), limitedP4.Z());
        glVertex3f(limitedP7.X(), limitedP7.Y(), limitedP7.Z());
    glEnd();

    glUseProgram(0);

    glPopMatrix();
    glPopAttrib();

    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPushMatrix();

    glColor4f(limitedCubeColor.redF(), limitedCubeColor.greenF(), limitedCubeColor.blueF(), 1);
    glBegin(GL_LINE_LOOP);
            glVertex3f(limitedP0.X(), limitedP0.Y(), limitedP0.Z());
            glVertex3f(limitedP1.X(), limitedP1.Y(), limitedP1.Z());
            glVertex3f(limitedP2.X(), limitedP2.Y(), limitedP2.Z());
            glVertex3f(limitedP3.X(), limitedP3.Y(), limitedP3.Z());
        glEnd();

        glBegin(GL_LINE_LOOP);
            glVertex3f(limitedP4.X(), limitedP4.Y(), limitedP4.Z());
            glVertex3f(limitedP5.X(), limitedP5.Y(), limitedP5.Z());
            glVertex3f(limitedP6.X(), limitedP6.Y(), limitedP6.Z());
            glVertex3f(limitedP7.X(), limitedP7.Y(), limitedP7.Z());
        glEnd();

        glBegin(GL_LINES);
            glVertex3f(limitedP0.X(), limitedP0.Y(), limitedP0.Z());
            glVertex3f(limitedP4.X(), limitedP4.Y(), limitedP4.Z());

            glVertex3f(limitedP1.X(), limitedP1.Y(), limitedP1.Z());
            glVertex3f(limitedP5.X(), limitedP5.Y(), limitedP5.Z());

            glVertex3f(limitedP2.X(), limitedP2.Y(), limitedP2.Z());
            glVertex3f(limitedP6.X(), limitedP6.Y(), limitedP6.Z());

            glVertex3f(limitedP3.X(), limitedP3.Y(), limitedP3.Z());
            glVertex3f(limitedP7.X(), limitedP7.Y(), limitedP7.Z());
        glEnd();
    glPopMatrix();
    glPopAttrib();
}

IntensityColorTable::~IntensityColorTable()
{

}

void MeshDocument::drawLimitedSphere() {

    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPushMatrix();

        glUseProgram(limitedShaderPro);

        glShadeModel(GL_SMOOTH);
        glDisable(GL_ALPHA_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glBlendEquation(GL_FUNC_ADD);
        glDisable(GL_DEPTH_TEST);
        glColor4f(limitedSphereColor.redF(), limitedSphereColor.greenF(), limitedSphereColor.blueF(), 1);

        int numOfSlice = 100;
        int numOfStack = 20;
        GLUquadric *quad = gluNewQuadric();
        glTranslatef(sphereCenter.X(), sphereCenter.Y(), sphereCenter.Z());
        gluSphere(quad, sphereRadius, numOfSlice, numOfStack);

        glUseProgram(0);

    glPopAttrib();
    glPushAttrib(GL_ALL_ATTRIB_BITS);
        glColor4f(limitedSphereColor.redF(), limitedSphereColor.greenF(), limitedSphereColor.blueF(), 1);
        drawLimitedWireframeSphere(numOfSlice, numOfStack);
    glPopMatrix();
    glPopAttrib();
}

void MeshDocument::drawLimitedWireframeSphere(int slices, int stacks)
{
    /*
     * Draw circle on each stack
     * */
    int halfStacks = stacks / 2;
    for (int s = 0; s < halfStacks; s++) {
        // Calculate translation along z-axis
        float transZ = (sphereRadius/halfStacks) * s;

        // Calculate circle radius
        float r = sqrt(sphereRadius*sphereRadius - transZ*transZ);

        // Draw upper-half of sphere
        glPushMatrix();
            glTranslatef(0.0f, 0.0f, transZ);
            drawCircle(r);
        glPopMatrix();

        // Draw under-half of sphere
        glPushMatrix();
            glTranslatef(0.0f, 0.0f, -transZ);
            drawCircle(r);
        glPopMatrix();
    } // end for s = 0 : halfStacks

    /*
     * Draw circle on each slice
     * */
    int halfSlices = slices / 10;
    for (int s = 0; s < halfSlices; s++) {
        float rotateAngle = 180.0f / halfSlices * s;

        glPushMatrix();
            glRotatef(rotateAngle, 0.0f, 0.0f, 1.0f);
            glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
            drawCircle(sphereRadius);
        glPopMatrix();


        glPushMatrix();
            glRotatef(rotateAngle, 0.0f, 0.0f, 1.0f);
            glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
            drawCircle(sphereRadius);
        glPopMatrix();

    } // end for s = 0 : halfSlices
}

void MeshDocument::drawCircle(float radius)
{
    glLineWidth(2.0f);
    glBegin(GL_LINE_LOOP);

        int step = 100;
        for (int i = 0; i < step; i++) {
            float angle = 2 * M_PI * i / step;
            float x = radius * cos(angle);
            float y = radius * sin(angle);
            glVertex2f(x, y);
        }

    glEnd();
}

void MeshDocument::drawAxes()
{
    const float AXIS_EXTRA = 10.0f;
    const float CYLINDER_BASE = 5.0;
    const float CYLINDER_TOP = 0.0;
    const float CYLINDER_HEIGHT = 10.0;
    const float CYLINDER_SLICE = 20;
    const float CYLINDER_STACK = 20;

    glPushAttrib(GL_ALL_ATTRIB_BITS);

    glLineWidth(2.0f);
    GLUquadric *quad = gluNewQuadric();

    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(measurementMillimeter.X() + AXIS_EXTRA, 0.0f, 0.0f);
    glEnd();
    glPushMatrix();
    glTranslatef(measurementMillimeter.X() + AXIS_EXTRA, 0.0f, 0.0f);
    glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
    gluCylinder(quad, CYLINDER_BASE, CYLINDER_TOP, CYLINDER_HEIGHT, CYLINDER_SLICE, CYLINDER_STACK);
    glPopMatrix();

    glColor3f(0.0f, 1.0f, 0.0f);
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, measurementMillimeter.Y() + AXIS_EXTRA, 0.0f);
    glEnd();
    glPushMatrix();
    glTranslatef(0.0f, measurementMillimeter.Y() + AXIS_EXTRA, 0.0f);
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    gluCylinder(quad, CYLINDER_BASE, CYLINDER_TOP, CYLINDER_HEIGHT, CYLINDER_SLICE, CYLINDER_STACK);
    glPopMatrix();

    glColor3f(0.0f, 0.0f, 1.0f);
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, measurementMillimeter.Z() + AXIS_EXTRA);
    glEnd();
    glPushMatrix();
    glTranslatef(0.0f, 0.0f, measurementMillimeter.Z() + AXIS_EXTRA);
    gluCylinder(quad, CYLINDER_BASE, CYLINDER_TOP, CYLINDER_HEIGHT, CYLINDER_SLICE, CYLINDER_STACK);
    glPopMatrix();

    glPopAttrib();
}

void IntensityColorTable::updateColorTable()
{
    for (int i = grayLeftBound; i <= grayRightBound; ++i) {
        colorTable[i] = mapIntensity(i);
    }
}

void IntensityColorTable::setColorDict(QMap<float, Color4f> newColorDict)
{
    this->colorDict.clear(); // Firstly, clear the curent colorDict
    QMap<float, Color4f>::Iterator it = newColorDict.begin();
    for(; it != newColorDict.end(); ++it) {
        colorDict[it.key()] = it.value();
    }
    updateColorTable();
}

QString IntensityColorTable::colorDictAttributeString()
{
    QString result = QString::number(colorDict.size());
    QMap<float, Color4f>::Iterator it = colorDict.begin();
    for (; it != colorDict.end(); ++it) {
        result += " " + QString::number(it.key()) + " " + QString::number(it.value()[0] * 1.0 / 255) + " "
                + QString::number(it.value()[1] * 1.0 / 255) + " "
                + QString::number(it.value()[2] * 1.0 / 255) + " "
                + QString::number(it.value()[3] * 1.0 / 255);
    }
    return result;
}

void MeshDocument::initLimitedShapeShader() {
    const char* vs = {
        "varying vec3 N;"
        "varying vec3 I;"
        "varying vec4 Cs;"

        "void main(){"
        "   vec4 P = gl_ModelViewMatrix * gl_Vertex;"
        "   I = P.xyz - vec3(0);"
        "   N = gl_NormalMatrix * gl_Normal;"
        "   Cs = gl_Color;"
        "   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;"
        "}"
    };

    const char* fs = {
        "varying vec3 N;"
        "varying vec3 I;"
        "varying vec4 Cs;"

        "void main(){"
        "   float edgefalloff = 1.0;"
        "   float intensity = 1.0;"
        "   float ambient = 0.1;"

        "   float opac = dot(normalize(-N), normalize(-I));"
        "   opac = abs(opac);"
        "   opac = ambient + intensity*(1.0 - pow(opac, edgefalloff));"

        "   gl_FragColor = opac * Cs;"
        "   gl_FragColor.a = opac;"
        "}"
    };

    GLuint v = glCreateShader(GL_VERTEX_SHADER);
    GLuint f = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(v, 1, &vs, NULL);
    glShaderSource(f, 1, &fs, NULL);

    glCompileShader(v);
    glCompileShader(f);

    GLint vstatus, fstatus;
    glGetShaderiv(v, GL_COMPILE_STATUS, &vstatus);
    glGetShaderiv(f, GL_COMPILE_STATUS, &fstatus);

    qDebug() << "Vertex status:" << vstatus;
    qDebug() << "Fragment status:" << fstatus;

    if (vstatus && fstatus) {
        qDebug() << "Sphere shader compiles successfully";
        limitedShaderPro = glCreateProgram();

        glAttachShader(limitedShaderPro, v);
        glAttachShader(limitedShaderPro, f);
        glLinkProgram(limitedShaderPro);
    } else {
        qDebug() << "Sphere shader fails";
    }
}

TreeItem::TreeItem(const QVector<QVariant> &data, TreeItem *parent)
{
    parentItem = parent;
    itemData = data;
    colorTable = new IntensityColorTable();
    viewOptions.isUseColorTable = false;
    transfuncOptions.type = TransfuncTypes::NONE;
    histogram = new QMap<int, int>();
}

TreeItem::~TreeItem()
{
    qDeleteAll(childItems);
    delete opacity;
    delete colorTable;
    delete histogram;
    colorTable = NULL;
    opacity = NULL;
    histogram = NULL;
}

void TreeItem::appendChild(TreeItem *child)
{
    childItems.append(child);
}

bool TreeItem::insertChildren(int position, int count, int columns)
{
    if (position < 0 || position > childItems.size())
        return false;
    for (int row = 0; row < count; ++row){
        QVector<QVariant> data(columns);
        TreeItem *item = new TreeItem(data, this);
        item->setTransFunc(NULL);
        childItems.insert(position, item);
    }
}

bool TreeItem::removeChildren(int position, int count)
{
    if (position < 0 || position + count > childItems.size())
        return false;
    for (int row = 0; row < count; ++row) {
        delete childItems.takeAt(position);
    }
    return true;
}

TreeItem *TreeItem::child(int row)
{
    return childItems.value(row);
}

int TreeItem::childCount() const
{
    return childItems.count();
}

int TreeItem::columnCount() const
{
    return itemData.count();
}

QVariant TreeItem::data(int column) const
{
    return itemData.value(column);
}

int TreeItem::childNumber() const
{
    if (parentItem) {
        return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));
    } else {
        qDebug() <<  "row: parentItem does not exist";
        return 0;
    }
}

TreeItem *TreeItem::parent()
{
    return parentItem;
}

bool TreeItem::setData(int column, const QVariant &value)
{
    if (column < 0 || column >= itemData.size())
        return false;
    itemData[column] = value;
    return true;
}

bool TreeItem::insertColumns(int position, int columns)
{
    if (position < 0 || position > itemData.size())
        return false;
    for (int column = 0; column < columns; ++column) {
        itemData.insert(position, QVariant());
    }
    foreach(TreeItem *child, childItems) {
        child->insertColumns(position, columns);
    }
    return true;
}

void TreeItem::setTransFunc(TransFunc f)
{
    transfunc = f;
}

TransFunc TreeItem::getTransFunc()
{
    return transfunc;
}

void TreeItem::setTransFuncOptions(TransfuncOptions options)
{
    transfuncOptions = options;
}

TransfuncOptions TreeItem::getTransFuncOptions()
{
    return transfuncOptions;
}

void TreeItem::updateHistogram(signed short *originalIntensity, CallBackPos *cb)
{
    histogram->clear();
    if (opacity == NULL) return;
    if (originalIntensity == NULL) return;

    int index = 0;
    int count = 100;
    int step = this->maxIndex / count;
    count = this->maxIndex / step;

    for (int i = 0; i < this->maxIndex; ++i) {
        if (i % step == 0) {
            if (cb)
                cb(100 * index * 1.0/ count, "Building histogram");
            index++;
        }
        int intensity = originalIntensity[i] - 1024;
        if (intensity == -1024) continue;
        if (opacity[i] == 1)
            (*histogram)[intensity]++;
    }
    // Reset dirty flag
    this->isDirty = false;
    QString name = this->data(0).toString();
    QString asterisk = name.left(1);
    if (asterisk == "*"){
        this->setData(0, name.right(name.size() - 1)); // Remove asterisk (if any)
    }
}

void TreeItem::setDirtyChildren(TreeItem *parent)
{
    if (parent == NULL) return;
    for (int i = 0; i < parent->childCount(); ++i){
        TreeItem *child = parent->child(i);
        QString name = child->data(0).toString();
        child->setData(0, "*" + name);
        child->isDirty = true;
        // Recursive
        setDirtyChildren(child);
    }
}

bool TreeItem::isHasDirtyChildren()
{
    bool isHas = false;
    for (int i = 0; i < this->childCount(); ++i) {
        TreeItem* child = this->child(i);
        isHas = child->isDirty;
        return isHas;
    }
    return isHas;
}

vcg::Color4f defaultTransferfunction(signed short *intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (options.opacity[index] == 1) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}
