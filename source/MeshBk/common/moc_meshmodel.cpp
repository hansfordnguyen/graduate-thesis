/****************************************************************************
** Meta object code from reading C++ file 'meshmodel.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "meshmodel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'meshmodel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_VolumeProperty[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      36,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_VolumeProperty[] = {
    "VolumeProperty\0\0colorTableChanged()\0"
    "updateTexture3dRgbaDataSuccess()\0"
};

void VolumeProperty::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VolumeProperty *_t = static_cast<VolumeProperty *>(_o);
        switch (_id) {
        case 0: _t->colorTableChanged(); break;
        case 1: _t->updateTexture3dRgbaDataSuccess(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData VolumeProperty::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VolumeProperty::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VolumeProperty,
      qt_meta_data_VolumeProperty, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VolumeProperty::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VolumeProperty::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VolumeProperty::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VolumeProperty))
        return static_cast<void*>(const_cast< VolumeProperty*>(this));
    return QWidget::qt_metacast(_clname);
}

int VolumeProperty::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void VolumeProperty::colorTableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_MeshDocument[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   14,   13,   13, 0x05,
      44,   13,   13,   13, 0x05,
      67,   13,   13,   13, 0x05,
      93,   84,   13,   13, 0x05,
     119,   14,   13,   13, 0x05,
     136,   13,   13,   13, 0x05,
     155,   13,   13,   13, 0x05,
     173,   13,   13,   13, 0x05,
     191,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
     220,  218,   13,   13, 0x0a,
     268,   13,   13,   13, 0x2a,
     311,   13,   13,   13, 0x2a,
     343,  337,   13,   13, 0x0a,
     407,  402,   13,   13, 0x2a,
     459,  448,   13,   13, 0x0a,
     546,  536,   13,   13, 0x2a,

       0        // eod
};

static const char qt_meta_stringdata_MeshDocument[] = {
    "MeshDocument\0\0index\0currentMeshChanged(int)\0"
    "meshDocumentModified()\0meshSetChanged()\0"
    "index,rm\0meshAdded(int,RenderMode)\0"
    "meshRemoved(int)\0rasterSetChanged()\0"
    "documentUpdated()\0updateHistogram()\0"
    "texture3DRgbaDataUpdated()\0,\0"
    "updateTexture3DRgbaData(vcg::CallBackPos*,bool)\0"
    "updateTexture3DRgbaData(vcg::CallBackPos*)\0"
    "updateTexture3DRgbaData()\0item,\0"
    "updateTexture3DRgbaDataFromTF(TreeItem*,vcg::CallBackPos*)\0"
    "item\0updateTexture3DRgbaDataFromTF(TreeItem*)\0"
    "viewItems,\0"
    "updateTexture3DRgbaDataFromCombinedItems(QList<TreeItem*>,vcg::CallBac"
    "kPos*)\0"
    "viewItems\0"
    "updateTexture3DRgbaDataFromCombinedItems(QList<TreeItem*>)\0"
};

void MeshDocument::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MeshDocument *_t = static_cast<MeshDocument *>(_o);
        switch (_id) {
        case 0: _t->currentMeshChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->meshDocumentModified(); break;
        case 2: _t->meshSetChanged(); break;
        case 3: _t->meshAdded((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< RenderMode(*)>(_a[2]))); break;
        case 4: _t->meshRemoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->rasterSetChanged(); break;
        case 6: _t->documentUpdated(); break;
        case 7: _t->updateHistogram(); break;
        case 8: _t->texture3DRgbaDataUpdated(); break;
        case 9: _t->updateTexture3DRgbaData((*reinterpret_cast< vcg::CallBackPos*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 10: _t->updateTexture3DRgbaData((*reinterpret_cast< vcg::CallBackPos*(*)>(_a[1]))); break;
        case 11: _t->updateTexture3DRgbaData(); break;
        case 12: _t->updateTexture3DRgbaDataFromTF((*reinterpret_cast< TreeItem*(*)>(_a[1])),(*reinterpret_cast< vcg::CallBackPos*(*)>(_a[2]))); break;
        case 13: _t->updateTexture3DRgbaDataFromTF((*reinterpret_cast< TreeItem*(*)>(_a[1]))); break;
        case 14: _t->updateTexture3DRgbaDataFromCombinedItems((*reinterpret_cast< QList<TreeItem*>(*)>(_a[1])),(*reinterpret_cast< vcg::CallBackPos*(*)>(_a[2]))); break;
        case 15: _t->updateTexture3DRgbaDataFromCombinedItems((*reinterpret_cast< QList<TreeItem*>(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MeshDocument::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MeshDocument::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_MeshDocument,
      qt_meta_data_MeshDocument, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MeshDocument::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MeshDocument::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MeshDocument::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MeshDocument))
        return static_cast<void*>(const_cast< MeshDocument*>(this));
    return QObject::qt_metacast(_clname);
}

int MeshDocument::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void MeshDocument::currentMeshChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MeshDocument::meshDocumentModified()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void MeshDocument::meshSetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void MeshDocument::meshAdded(int _t1, RenderMode _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MeshDocument::meshRemoved(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MeshDocument::rasterSetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void MeshDocument::documentUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 6, 0);
}

// SIGNAL 7
void MeshDocument::updateHistogram()
{
    QMetaObject::activate(this, &staticMetaObject, 7, 0);
}

// SIGNAL 8
void MeshDocument::texture3DRgbaDataUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 8, 0);
}
QT_END_MOC_NAMESPACE
