/********************************************************************************
** Form generated from reading UI file 'volumepropertywidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VOLUMEPROPERTYWIDGET_H
#define UI_VOLUMEPROPERTYWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "colorgraph.h"
#include "histogramwidget.h"

QT_BEGIN_NAMESPACE

class Ui_VolumePropertyWidget
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    ColorGraph *colorGraph;
    QTableWidget *colorTable;
    QHBoxLayout *horizontalLayout;
    QPushButton *resetButton;
    QPushButton *applyButton;
    QSpacerItem *horizontalSpacer_2;
    QProgressBar *progressbar;
    QHBoxLayout *renderlayout;
    QLabel *label;
    QComboBox *comboBox;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *trainButton;
    QPushButton *applyHistogram;
    QSpacerItem *horizontalSpacer;
    HistogramWidget *histogramGraph;
    QHBoxLayout *lefthor;
    QLabel *label_3;
    QSlider *leftBoundSlider;
    QHBoxLayout *righthor;
    QLabel *label_4;
    QSlider *rightBoundSlider;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *VolumePropertyWidget)
    {
        if (VolumePropertyWidget->objectName().isEmpty())
            VolumePropertyWidget->setObjectName(QString::fromUtf8("VolumePropertyWidget"));
        VolumePropertyWidget->resize(400, 603);
        verticalLayout = new QVBoxLayout(VolumePropertyWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(2, 2, 2, 2);
        label_2 = new QLabel(VolumePropertyWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setStyleSheet(QString::fromUtf8("padding: 2px;"));

        verticalLayout->addWidget(label_2);

        colorGraph = new ColorGraph(VolumePropertyWidget);
        colorGraph->setObjectName(QString::fromUtf8("colorGraph"));
        colorGraph->setMinimumSize(QSize(320, 70));
        colorGraph->setCursor(QCursor(Qt::PointingHandCursor));

        verticalLayout->addWidget(colorGraph);

        colorTable = new QTableWidget(VolumePropertyWidget);
        colorTable->setObjectName(QString::fromUtf8("colorTable"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(colorTable->sizePolicy().hasHeightForWidth());
        colorTable->setSizePolicy(sizePolicy);
        colorTable->setMinimumSize(QSize(0, 0));
        colorTable->setMaximumSize(QSize(16777215, 270));

        verticalLayout->addWidget(colorTable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        resetButton = new QPushButton(VolumePropertyWidget);
        resetButton->setObjectName(QString::fromUtf8("resetButton"));

        horizontalLayout->addWidget(resetButton);

        applyButton = new QPushButton(VolumePropertyWidget);
        applyButton->setObjectName(QString::fromUtf8("applyButton"));

        horizontalLayout->addWidget(applyButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        progressbar = new QProgressBar(VolumePropertyWidget);
        progressbar->setObjectName(QString::fromUtf8("progressbar"));
        progressbar->setMaximumSize(QSize(16777215, 10));
        progressbar->setValue(24);

        verticalLayout->addWidget(progressbar);

        renderlayout = new QHBoxLayout();
        renderlayout->setObjectName(QString::fromUtf8("renderlayout"));
        label = new QLabel(VolumePropertyWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);

        renderlayout->addWidget(label);

        comboBox = new QComboBox(VolumePropertyWidget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        renderlayout->addWidget(comboBox);


        verticalLayout->addLayout(renderlayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        trainButton = new QPushButton(VolumePropertyWidget);
        trainButton->setObjectName(QString::fromUtf8("trainButton"));

        horizontalLayout_2->addWidget(trainButton);

        applyHistogram = new QPushButton(VolumePropertyWidget);
        applyHistogram->setObjectName(QString::fromUtf8("applyHistogram"));

        horizontalLayout_2->addWidget(applyHistogram);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout_2);

        histogramGraph = new HistogramWidget(VolumePropertyWidget);
        histogramGraph->setObjectName(QString::fromUtf8("histogramGraph"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(histogramGraph->sizePolicy().hasHeightForWidth());
        histogramGraph->setSizePolicy(sizePolicy2);
        histogramGraph->setMinimumSize(QSize(0, 150));

        verticalLayout->addWidget(histogramGraph);

        lefthor = new QHBoxLayout();
        lefthor->setObjectName(QString::fromUtf8("lefthor"));
        label_3 = new QLabel(VolumePropertyWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy1.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy1);
        label_3->setMinimumSize(QSize(100, 0));

        lefthor->addWidget(label_3);

        leftBoundSlider = new QSlider(VolumePropertyWidget);
        leftBoundSlider->setObjectName(QString::fromUtf8("leftBoundSlider"));
        leftBoundSlider->setOrientation(Qt::Horizontal);

        lefthor->addWidget(leftBoundSlider);


        verticalLayout->addLayout(lefthor);

        righthor = new QHBoxLayout();
        righthor->setObjectName(QString::fromUtf8("righthor"));
        label_4 = new QLabel(VolumePropertyWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(100, 0));

        righthor->addWidget(label_4);

        rightBoundSlider = new QSlider(VolumePropertyWidget);
        rightBoundSlider->setObjectName(QString::fromUtf8("rightBoundSlider"));
        rightBoundSlider->setOrientation(Qt::Horizontal);

        righthor->addWidget(rightBoundSlider);


        verticalLayout->addLayout(righthor);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);


        retranslateUi(VolumePropertyWidget);

        QMetaObject::connectSlotsByName(VolumePropertyWidget);
    } // setupUi

    void retranslateUi(QWidget *VolumePropertyWidget)
    {
        VolumePropertyWidget->setWindowTitle(QApplication::translate("VolumePropertyWidget", "Form", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("VolumePropertyWidget", "Color Table", 0, QApplication::UnicodeUTF8));
        resetButton->setText(QApplication::translate("VolumePropertyWidget", "Reset", 0, QApplication::UnicodeUTF8));
        applyButton->setText(QApplication::translate("VolumePropertyWidget", "Apply", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("VolumePropertyWidget", "Render Method", 0, QApplication::UnicodeUTF8));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("VolumePropertyWidget", "3D Texture Mapping", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("VolumePropertyWidget", "CPU Raycasting", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("VolumePropertyWidget", "GPU Raycasting", 0, QApplication::UnicodeUTF8)
        );
        trainButton->setText(QApplication::translate("VolumePropertyWidget", "Train", 0, QApplication::UnicodeUTF8));
        applyHistogram->setText(QApplication::translate("VolumePropertyWidget", "Apply", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("VolumePropertyWidget", "Left bound", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("VolumePropertyWidget", "Right bound", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class VolumePropertyWidget: public Ui_VolumePropertyWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VOLUMEPROPERTYWIDGET_H
