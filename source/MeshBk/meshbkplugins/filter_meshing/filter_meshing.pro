include (../../shared.pri)

QMAKE_CXXFLAGS += -std=c++11 #T

INCLUDEPATH += ../../external/muparser_v132/include
INCLUDEPATH += ../../external \
                ../../meshbkplugins \

LIBS += -L../../external/lib/opencv -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc #T
LIBS += -L../../external/lib/imebra -limebra #T
LIBS += -L../../distrib/plugins -lutils_opendicomsfolder

HEADERS       += $$VCGDIR/vcg/complex/algorithms/clean.h\
		quadric_simp.h \ 
		quadric_tex_simp.h \ 
		meshfilter.h 

SOURCES       += meshfilter.cpp \
		quadric_simp.cpp \ 
        quadric_tex_simp.cpp

TARGET        = filter_meshing

