include (../../shared.pri)

QMAKE_CXXFLAGS += -std=c++11 #T

INCLUDEPATH += ../../external/muparser_v132/include
INCLUDEPATH += ../../external \
                ../../meshbkplugins \

LIBS += -L../../external/lib/opencv -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc #T
LIBS += -L../../external/lib/imebra -limebra #T
LIBS += -L../../distrib/plugins -lutils_opendicomsfolder

HEADERS       += filter_func.h

SOURCES       += filter_func.cpp

TARGET        = filter_func

# Note: we need static libs so when building muparser lib use 
# ./configure --enable-shared=no

win32-msvc2005:LIBS += ../../external/lib/win32-msvc2005/muparser.lib
win32-msvc2008:LIBS += ../../external/lib/win32-msvc2008/muparser.lib
win32-msvc2010:LIBS += ../../external/lib/win32-msvc2010/muparser.lib
win32-msvc2012:LIBS += ../../external/lib/win32-msvc2012/muparser.lib
macx:LIBS            += $$MACLIBDIR/libmuparser.a
linux-g++:LIBS       += ../../external/lib/linux-g++/libmuparser.a
linux-g++-32:LIBS       += ../../external/lib/linux-g++-32/libmuparser.a
linux-g++-64:LIBS       += ../../external/lib/linux-g++-64/libmuparser.a
win32-g++:LIBS		 += ../../external/lib/win32-gcc/libmuparser.a
