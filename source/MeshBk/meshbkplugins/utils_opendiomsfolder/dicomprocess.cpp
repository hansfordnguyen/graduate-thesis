#include "dicomprocess.h"
DicomProcess::DicomProcess() {

}

DicomProcess::DicomProcess(QString in, QString out, QString name) {
    folderIn = in;
    folderOut = out;
    fileName = name;
}

void DicomProcess::startProcess() {
    if (folderIn.length() != 0 && folderOut.length() != 0 && fileName.length() != 0) {
        writeVolumeDataPlyFile(folderIn, folderOut, fileName);
    }
}

cv::Mat DicomProcess::extractAnOrgran(cv::Mat original, cv::Mat mask) {
    assert(original.cols == mask.cols);
    assert(original.rows == mask.rows);

    cv::Mat compositeMat = cv::Mat::zeros(original.rows, original.cols, CV_16SC1);
    cv::bitwise_and(original, mask, compositeMat);

    return compositeMat;
}

QMap<int, DataSlice> DicomProcess::extractOrganSlices(QString folderIn, vcg::CallBackPos *cb) {
    QDir dirIn(folderIn);
    assert(dirIn.exists());

    QFileInfoList maskDicoms = dirIn.entryInfoList();
    QMap<QString, QString> maskMap;
    for (int i = 0; i < maskDicoms.size(); i++) {
        QFileInfo file = maskDicoms.at(i);
        maskMap.insert(file.fileName(), file.absoluteFilePath());
    }

    QString originalFolder = getOriginalFolder(folderIn);
    QDir dirOriginal(originalFolder);
//    assert(dirOriginal.exists());
    if (!dirOriginal.exists()) {
        QMessageBox::critical(NULL, tr("PATIENT_DICOM Not Found"), tr("Do not find PATIENT_DICOM folder"));
        return QMap<int, DataSlice>();
    }

    QFileInfoList originalDicoms = dirOriginal.entryInfoList();
    QMap<QString, QString> originalMap;
    for (int i = 0; i < originalDicoms.size(); i++) {
        QFileInfo file = originalDicoms.at(i);
        originalMap.insert(file.fileName(), file.absoluteFilePath());
    }

    int idx = 0;
    int i = 0;
    int count = 100;
    int step = maskMap.size() / count;
    count = maskMap.size() / step;
    QMap<int, DataSlice> dataSliceMap;
    QMap<QString, QString>::iterator it;
    for (it = maskMap.begin(); it != maskMap.end(); ++it) {
        try {
            QString maskFile = it.value();
            DicomInfo maskInfo(maskFile);
            cv::Mat maskMat = maskInfo.getOrginIntensityImage(0);

            for (int y = 0; y < maskMat.rows; y++) {
                for (int x = 0; x < maskMat.cols; x++) {
                    signed short origin = maskMat.at<signed short>(y, x);
                    signed short shift = origin << 8;
                    maskMat.at<signed short>(y, x) = origin | shift;
                }
            }

            QString originalFile = originalMap[it.key()];
            DicomInfo originalInfo(originalFile);
            cv::Mat originalMat = originalInfo.getOrginIntensityImage(1024);

            cv::Mat organSlide = extractAnOrgran(originalMat, maskMat);

            float scaleX = (float) maskInfo.getPixelSpacingX();
            float scaleY = (float) maskInfo.getPixelSpacingY();
            float scaleZ = ((float) maskInfo.getInstanceNumber()) * maskInfo.getSliceThickness();
            float zSpace = (float) maskInfo.getSliceThickness();

            DataSlice dataSlice;
            dataSlice.mat = organSlide;
            dataSlice.mask = maskMat;
            dataSlice.xScale = scaleX;
            dataSlice.yScale = scaleY;
            dataSlice.zScale = scaleZ;
            dataSlice.zSpace = zSpace;
            dataSlice.instanceNumber = maskInfo.getInstanceNumber();

            dataSliceMap.insert(maskInfo.getInstanceNumber(), dataSlice);
        } catch (imebra::StreamReadError exception) {
            std::cout << "imebra::StreamReadError" << std::endl;
        } catch (imebra::StreamOpenError exception) {
            std::cout << "imebra::StreamOpenError" << std::endl;
        } catch (imebra::MissingTagError exception) {
            std::cout << "imebra::MissingTagError" << std::endl;
        } catch (imebra::StreamEOFError exception) {
            std::cout << "imebra::StreamEOFError" << std::endl;
        } catch (imebra::CodecWrongFormatError exception) {
            std::cout << "imebra::CodecWrongFormatError" << std::endl;
        }
        if (i % step == 0) {
            if (cb) {
                QString status("Extracting slices of organ " + dirIn.dirName());
                cb(100 * idx * 1.0 / count, status.toAscii().data());
            }
            idx++;
        }
        i++;
    }

    return dataSliceMap;
}

QString DicomProcess::writeVolumeDataPlyFile(QString folderIn, QString folderOut, QString fileName) {
    QDir dirIn(folderIn);
    assert(dirIn.exists());

    QFileInfoList maskDicoms = dirIn.entryInfoList();
    QMap<QString, QString> maskMap;
    for (int i = 0; i < maskDicoms.size(); i++) {
        QFileInfo file = maskDicoms.at(i);
        maskMap.insert(file.fileName(), file.absoluteFilePath());
    }

    QString originalFolder = getOriginalFolder(folderIn);
    QDir dirOriginal(originalFolder);
//    assert(dirOriginal.exists());
    if (!dirOriginal.exists()) {
        emit writeFinished();
        emit dirNotFoundMessage();
        return "";
    }

    QFileInfoList originalDicoms = dirOriginal.entryInfoList();
    QMap<QString, QString> originalMap;
    for (int i = 0; i < originalDicoms.size(); i++) {
        QFileInfo file = originalDicoms.at(i);
        originalMap.insert(file.fileName(), file.absoluteFilePath());
    }

    QList<DataPoint> dataPointList;
    QMap<QString, QString>::iterator it;
    for (it = maskMap.begin(); it != maskMap.end(); ++it) {
        try {
            QString maskFile = it.value();
            QString originalFile = originalMap[it.key()];

            DicomInfo maskInfo(maskFile);
            DicomInfo originalInfo(originalFile);

            cv::Mat maskMat = maskInfo.getImage();
            cv::Mat originalMat = originalInfo.getImage();

            cv::Mat organSlide = extractAnOrgran(originalMat, maskMat);

            double scalseX = maskInfo.getPixelSpacingX();
            double scalseY = maskInfo.getPixelSpacingY();
            assert(scalseX == scalseY);

            float z = maskInfo.getInstanceNumber() * (((float)maskInfo.getSliceThickness()) / scalseX);
            addDataPoint(dataPointList, organSlide, z);
        } catch (imebra::StreamReadError e) {
            std::cout << "imebra::StreamReadError" << std::endl;
        }
    }

    QString plyFile = folderOut + "/" + fileName;
    QFile file(plyFile);
    file.open(QFile::WriteOnly);
    QTextStream outStream(&file);
    outStream << "ply" << "\n";
    outStream << "format ascii 1.0" << "\n";
    outStream << "element vertex " << dataPointList.size() <<"\n";
    outStream << "property float x" << "\n";
    outStream << "property float y" << "\n";
    outStream << "property float z" << "\n";
    outStream << "property uchar red" << "\n";
    outStream << "property uchar green" << "\n";
    outStream << "property uchar blue" << "\n";
    outStream << "property uchar alpha" << "\n";
    outStream << "end_header" << "\n";

    QList<DataPoint>::iterator its;
    for (its = dataPointList.begin(); its != dataPointList.end(); ++its) {
        outStream << its->x << " " << its->y << " " << its->z << " " << its->value << " " << its->value << " " << its->value << " " << its->alpha << "\n";
    }

    file.close();

    emit writeFinished();
    emit importVolumeData(plyFile);

    return plyFile;

}

QString DicomProcess::getOriginalFolder(QString maskFolder) {
    QDir maskDir(maskFolder);
    maskDir.cd("./../..");
    qDebug() << "GrantParents folder: " << maskDir.absolutePath();
    qDebug() << "Original folder: " << maskDir.absoluteFilePath(ORIGINAL_FOLDER_NAME);

    QString originalFolder = maskDir.absoluteFilePath(ORIGINAL_FOLDER_NAME);

    return originalFolder;
}

void DicomProcess::addDataPoint(QList<DataPoint> &list, cv::Mat mat, float z) {
    DataPoint data;

    int rows = mat.rows;
    int cols = mat.cols;
    for (int y = 0; y < rows; y++) {
        data.y = (float) y;
        data.z = z;
        for (int x = 0; x < cols; x++) {
            data.x = (float) x;
            uchar value = mat.at<uchar>(y, x);
            if (value != 0 && value != 255) {
                data.value = value;
                data.alpha = 255;
                list.push_back(data);
            }
        }
    }
}

DicomProcess::~DicomProcess() {

}
