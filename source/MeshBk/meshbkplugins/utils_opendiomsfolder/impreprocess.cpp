#include "impreprocess.h"

cv::Mat ImPreProcess::edgeDetect(cv::Mat image) {
    int highThreshold = 500;
    int ratio = 0.4;

    cv::Mat edgeImage = cv::Mat::zeros(image.size(), CV_8UC1);
    cv::Canny(image, edgeImage, 1, 100);

    return edgeImage;
}

QList<QPair<int, int> > ImPreProcess::getEdgePoints(cv::Mat image) {

    QList<QPair<int, int> > list;
    QPair<int, int> pair;
    int rows = image.rows;
    int cols = image.cols;

    for(int x = 0; x < cols; x++) {
        pair.first = x;
        for (int y = 0; y < rows; y++) {
            pair.second = y;
            if (image.at<uchar>(y, x) != 0) {
                list.push_back(pair);
            }

        }
    }

    return list;
}
