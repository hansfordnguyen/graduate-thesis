/****************************************************************************
 * MeshLab                                                           o o     *
 * A versatile mesh processing toolbox                             o     o   *
 *                                                                _   O  _   *
 * Copyright(C) 2005                                                \/)\/    *
 * Visual Computing Lab                                            /\/|      *
 * ISTI - Italian National Research Council                           |      *
 *                                                                    \      *
 * All rights reserved.                                                      *
 *                                                                           *
 * This program is free software; you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation; either version 2 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License (http://www.gnu.org/licenses/gpl.txt)          *
 * for more details.                                                         *
 *                                                                           *
 ****************************************************************************/
#include "edit_slice.h"

#include <main/glarea.h>
#include <vcg/math/perlin_noise.h>
#include <vcg/complex/algorithms/intersection.h>
#include <vcglib/wrap/gl/glu_tessellator_cap.h>
#include <vcglib/vcg/complex/algorithms/create/platonic.h>
#include <vcglib/wrap/gui/trackutils.h>


using namespace std;
using namespace vcg;

EditSlicePlugin::EditSlicePlugin()
{
    isDefaultMouseEvent = false;
}

EditSlicePlugin::~EditSlicePlugin() {}

const QString EditSlicePlugin::Info() {
    return tr("MeshBk Slice Tool");
}

bool EditSlicePlugin::StartEdit(MeshModel& m, GLArea * parent)
{
    glarea = parent;
    glarea->setMouseTracking(true);

//    parent->setCursor(QCursor(QPixmap(":/images/cursor_paint.png"),1,1));

    // initialize once and for all the radius (will remain the same all the time)

    // Retrieve mesh document
    if (parent) {
        this->md = &(parent->mvc()->meshDoc);
    }
    if (this->md) {
        this->meshListSize = this->md->meshList.size();
        this->cuttingOrigin = this->md->mm()->cm.bbox.Center();
    }

    // Init cutting normal
    this->cuttingNormal = vcg::Point3f(0, 0, 1);
    this->boundingMesh = initBoundingSphere();

    // Init cutting Plane
    initCuttingPlane(this->cuttingNormal, this->cuttingOrigin);
    this->isDefaultMouseEvent = true;
    this->controlMode = DEFAULT;
    this->rotation = vcg::Quaternionf(0, this->cuttingNormal);


    if (glarea->editorExistsForAction(glarea->getCurrentEditAction()) != 0) {
        connect(this, SIGNAL(updateIntersectionMesh(MeshModel*, MeshDocument *, vcg::Point3f)), glarea->sliceView, SLOT(updateIntersectionMesh(MeshModel*, MeshDocument*, vcg::Point3f)));
    }
    this->lastPoint = Point3f(0, 0, 0); // Set defaut last point
    emitUpdateSignal();
    return true;
}

void EditSlicePlugin::EndEdit(MeshModel &/*m*/, GLArea * /*parent*/)
{
    glarea->setMouseTracking(false);
    if (this->cuttingMesh != 0) {
        this->md->delMesh(this->cuttingMesh);
    }

    if (this->boundingMesh) {
        this->md->delMesh(this->boundingMesh);
    }

    if (this->tempSliceMesh) {
        this->md->delMesh(this->tempSliceMesh);
    }
    this->md->sliceMesh = NULL;
}

void EditSlicePlugin::mousePressEvent(QMouseEvent * event, MeshModel &, GLArea * gla)
{
    this->isDrag = true;
    switch (controlMode) {
    case DEFAULT:
        this->isDefaultMouseEvent = true;
        break;
    case TRANSLATE:
        this->isDefaultMouseEvent = false;
        this->lastPoint = vcg::Point3f(event->x(), glarea->height() - event->y(), 0);
        break;
    case ROTATE:
        this->isDefaultMouseEvent = false;
        this->lastPoint = vcg::Point3f(event->x(), glarea->height() - event->y(), 0);
        this->rotation = vcg::Quaternionf(0, this->cuttingNormal.Normalize());
        break;
    default:
        break;
    }

    gla->update();
}

void EditSlicePlugin::mouseMoveEvent(QMouseEvent* event, MeshModel & , GLArea * gla)
{
    vcg::Point3f hitNew(event->x(), glarea->height() - event->y(), 0);
//    if (Distance(hitNew, this->lastPoint) < 5) return;
    vcg::Box3f bbox = this->md->mm()->cm.bbox;
    if (!this->isDrag) return;

    switch (controlMode) {
    case DEFAULT:
        return;
        break;
    case TRANSLATE: {
        float offset = 1.5;
        float sign = (hitNew.Y() - this->lastPoint.Y());
        if (qAbs(sign) < 0.0001) return;
        sign = qAbs(sign) / sign;

        vcg::Matrix44f translateback_mat;
        translateback_mat.SetIdentity();
        vcg::Point3f translatePoint = this->cuttingOrigin /*+ (this->cuttingNormal * sign).normalized()*/;
        translateback_mat.SetTranslate(this->cuttingNormal * sign * offset);

        if (isOutOfBox(this->md->mm()->cm.bbox, translateback_mat * this->cuttingOrigin)) return; // Prevent moving cutting plane out of bounding box
        this->cuttingMesh->cm.Tr = translateback_mat * this->cuttingMesh->cm.Tr;
        this->lastPoint = hitNew;
        this->cuttingOrigin = translateback_mat * this->cuttingOrigin;
        break;
    }
    case ROTATE: {
        vcg::Point3f pointOld = vcg::trackutils::HitSphere(&(glarea->trackball), this->lastPoint);
        vcg::Point3f pointNew = vcg::trackutils::HitSphere(&(glarea->trackball), hitNew);
        Point3f tcenter = glarea->trackball.center;

        vcg::Point3f axis = (pointNew - tcenter) ^ (pointOld - tcenter);
        float phi = Distance(pointNew, pointOld) / glarea->trackball.radius;
        vcg::Quaternionf rot = vcg::Quaternionf(-phi, axis);
        this->cuttingNormal = rot.Rotate(this->cuttingNormal).Normalize();
        this->rotation = vcg::Quaternionf(0, this->cuttingNormal);

        this->lastPoint = hitNew;

        vcg::Point3f center = this->cuttingMesh->cm.bbox.Center();
        vcg::Matrix44f rotate_mat;
        vcg::Matrix44f translate_mat;
        vcg::Matrix44f translateback_mat;
        translateback_mat.SetIdentity();
        translate_mat.SetIdentity();
        translate_mat.SetIdentity();
        translate_mat.SetTranslate(vcg::Point3f(-center.X(),-center.Y(),-center.Z()));
        translateback_mat.SetTranslate(center);
        rotate_mat.SetRotateRad(-phi, axis);
        this->cuttingMesh->cm.Tr = translateback_mat * rotate_mat * translate_mat * this->cuttingMesh->cm.Tr;
        break;
    }
    default:
        break;
    }
    emitUpdateSignal();
    gla->update();
}

void EditSlicePlugin::mouseReleaseEvent(QMouseEvent * event, MeshModel &, GLArea * gla)
{
    this->isDrag = false;
    emitUpdateSignal();
    gla->update();
}

void EditSlicePlugin::keyReleaseEvent  (QKeyEvent * event, MeshModel &/*m*/, GLArea *){

}
void EditSlicePlugin::keyPressEvent(QKeyEvent *event, MeshModel &/*m*/, GLArea *){
    switch (event->key()) {
    case Qt::Key_Space:
        this->controlMode = DEFAULT;
        break;
    case Qt::Key_R:
        this->controlMode = ROTATE;
        break;
    case Qt::Key_T:
        this->controlMode = TRANSLATE;
        break;
    case Qt::Key_Enter:
    case Qt::Key_Return: {
        // Iterate through current mesh list and extract cutting slice
        for (int i = 0; i < this->meshListSize; ++i) {
            MeshModel* slice = getIntersectionWithAPlane(this->md->textureBBoxMillimeter, this->cuttingNormal, this->cuttingOrigin, vcg::Color4b::Black);
            if (slice->cm.fn == 0) {
                this->md->delMesh(slice);
            } else {
                slice->isSlice = true;
//                slice->texture3D = this->md->mm()->texture3D;
                slice->texture3DRawData = this->md->mm()->texture3DRawData;
                slice->texture3DRGBAData = this->md->mm()->texture3DRGBAData;
            }
        }
        this->RealTimeLog("OPERATION DONE", "", "");
        break;
    }
    case Qt::Key_X:
        this->controlMode = TRANSLATE;
        this->cuttingNormal = Point3f(1, 0, 0);
        this->cuttingOrigin = this->md->mm()->cm.bbox.Center();
        this->md->delMesh(this->cuttingMesh);
        initCuttingPlane(this->cuttingNormal, this->cuttingOrigin);
        emitUpdateSignal();
        break;
    case Qt::Key_Y:
        this->controlMode = TRANSLATE;
        this->cuttingNormal = Point3f(0, 2, 0);
        this->cuttingOrigin = this->md->mm()->cm.bbox.Center();
        this->md->delMesh(this->cuttingMesh);
        initCuttingPlane(this->cuttingNormal, this->cuttingOrigin);
        emitUpdateSignal();
        break;
    case Qt::Key_Z:
        this->controlMode = TRANSLATE;
        this->cuttingNormal = Point3f(0, 0, 1);
        this->cuttingOrigin = this->md->mm()->cm.bbox.Center();
        this->md->delMesh(this->cuttingMesh);
        initCuttingPlane(this->cuttingNormal, this->cuttingOrigin);
        emitUpdateSignal();
        break;
    default:
        break;
    }
}

void EditSlicePlugin::tabletEvent(QTabletEvent * event, MeshModel & , GLArea * gla)
{
//    if(!(paintbox->getPressureFrameEnabled()))
//        paintbox->enablePressureFrame();

//    event->accept();

//    // if event is down, start a new stroke: clean zbuff
//    if(event->type() == QEvent::TabletPress)
//    {
//        if (zbuffer != NULL) delete zbuffer; zbuffer = NULL;
//        current_brush.size = paintbox->getSize();
//        current_brush.opacity = paintbox->getOpacity();
//        current_brush.hardness = paintbox->getHardness();
//    }

    //    pushInputEvent(event->type(), event->pos(), event->modifiers(), event->pressure(), (event->pointerType() == QTabletEvent::Eraser)? Qt::RightButton : Qt::LeftButton, gla);
    gla->update();
}

/**
 * Since only on a Decorate call it is possible to obtain correct values
 * from OpenGL, all operations are performed during the execution of this
 * method and not where mouse events are processed.
 *
 */
void EditSlicePlugin::Decorate(MeshModel &m, GLArea * gla)
{
    QString helpText = "";
    switch (controlMode) {
    case TRANSLATE:
        helpText += "<b>TRANSLATE MODE ACTIVE</b>";
        break;
    case ROTATE:
        helpText += "<b>ROTATE MODE ACTIVE</b>";
        break;
    default:
        helpText += "<b>DEFAULT MODE ACTIVE</b>";
        break;
    }
    helpText += "<br>-------------------------------------------------<br>Press <b>RETURN</b> to slice<br>Press <b>SPACE</b> for <b>DEFAULT MODE</b><br>Press <b>R</b> for <b>ROTATE MODE</b><br>Press <b>T</b> for <b>TRANSLATE MODE</b>";
    this->RealTimeLog("Mesh Slice Tool", "", qPrintable(helpText));
    glarea = gla;

    return;
}

MeshModel* EditSlicePlugin::initBoundingBoxMesh(MeshModel* currentMesh) {
    RenderMode bboxRm;
    bboxRm.drawMode = vcg::GLW::DMNone;
    MeshModel* bboxMesh = this->md->addNewMesh("","Bounding Box Mesh",false, bboxRm);
    currentMesh->UpdateBoxAndNormals();

    vcg::Box3f bbox = currentMesh->cm.bbox;
    vcg::Point3f max = bbox.max;
    vcg::Point3f min = bbox.min;
    vcg::Point3f cen = bbox.Center();
    vcg::Point3f p0(min.X(), min.Y(), max.Z());
    vcg::Point3f p1(max.X(), min.Y(), max.Z());
    vcg::Point3f p2(max.X(), max.Y(), max.Z());
    vcg::Point3f p3(min.X(), max.Y(), max.Z());
    vcg::Point3f p4(min.X(), min.Y(), min.Z());
    vcg::Point3f p5(max.X(), min.Y(), min.Z());
    vcg::Point3f p6(max.X(), max.Y(), min.Z());
    vcg::Point3f p7(min.X(), max.Y(), min.Z());
    std::vector<vcg::Point3f> points;
    points.push_back(p0);
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
    points.push_back(p4);
    points.push_back(p5);
    points.push_back(p6);
    points.push_back(p7);
    vcg::tri::Allocator<CMeshO>::AddVertices(bboxMesh->cm, 8); // The are only 4 vertices
    vcg::tri::Allocator<CMeshO>::AddFaces(bboxMesh->cm, 12); // There are only 2 faces

    // Init vertices
    for (int i = 0; i < 8; ++i) {
        bboxMesh->cm.vert[i].P()[0] = points.at(i).X();
        bboxMesh->cm.vert[i].P()[1] = points.at(i).Y();
        bboxMesh->cm.vert[i].P()[2] = points.at(i).Z();
    }
    // Init faces
    bboxMesh->cm.face[0].V(0) = &bboxMesh->cm.vert[0];
    bboxMesh->cm.face[0].V(1) = &bboxMesh->cm.vert[1];
    bboxMesh->cm.face[0].V(2) = &bboxMesh->cm.vert[3];

    bboxMesh->cm.face[1].V(0) = &bboxMesh->cm.vert[1];
    bboxMesh->cm.face[1].V(1) = &bboxMesh->cm.vert[2];
    bboxMesh->cm.face[1].V(2) = &bboxMesh->cm.vert[3];

    bboxMesh->cm.face[2].V(0) = &bboxMesh->cm.vert[4];
    bboxMesh->cm.face[2].V(1) = &bboxMesh->cm.vert[7];
    bboxMesh->cm.face[2].V(2) = &bboxMesh->cm.vert[5];

    bboxMesh->cm.face[3].V(0) = &bboxMesh->cm.vert[6];
    bboxMesh->cm.face[3].V(1) = &bboxMesh->cm.vert[5];
    bboxMesh->cm.face[3].V(2) = &bboxMesh->cm.vert[7];

    bboxMesh->cm.face[4].V(0) = &bboxMesh->cm.vert[2];
    bboxMesh->cm.face[4].V(1) = &bboxMesh->cm.vert[6];
    bboxMesh->cm.face[4].V(2) = &bboxMesh->cm.vert[3];
    bboxMesh->cm.face[5].V(0) = &bboxMesh->cm.vert[3];
    bboxMesh->cm.face[5].V(1) = &bboxMesh->cm.vert[6];
    bboxMesh->cm.face[5].V(2) = &bboxMesh->cm.vert[7];

    bboxMesh->cm.face[6].V(0) = &bboxMesh->cm.vert[0];
    bboxMesh->cm.face[6].V(1) = &bboxMesh->cm.vert[5];
    bboxMesh->cm.face[6].V(2) = &bboxMesh->cm.vert[1];

    bboxMesh->cm.face[7].V(0) = &bboxMesh->cm.vert[0];
    bboxMesh->cm.face[7].V(1) = &bboxMesh->cm.vert[4];
    bboxMesh->cm.face[7].V(2) = &bboxMesh->cm.vert[5];

    bboxMesh->cm.face[8].V(0) = &bboxMesh->cm.vert[1];
    bboxMesh->cm.face[8].V(1) = &bboxMesh->cm.vert[6];
    bboxMesh->cm.face[8].V(2) = &bboxMesh->cm.vert[2];

    bboxMesh->cm.face[9].V(0) = &bboxMesh->cm.vert[1];
    bboxMesh->cm.face[9].V(1) = &bboxMesh->cm.vert[5];
    bboxMesh->cm.face[9].V(2) = &bboxMesh->cm.vert[6];

    bboxMesh->cm.face[10].V(0) = &bboxMesh->cm.vert[0];
    bboxMesh->cm.face[10].V(1) = &bboxMesh->cm.vert[3];
    bboxMesh->cm.face[10].V(2) = &bboxMesh->cm.vert[7];

    bboxMesh->cm.face[11].V(0) = &bboxMesh->cm.vert[0];
    bboxMesh->cm.face[11].V(1) = &bboxMesh->cm.vert[7];
    bboxMesh->cm.face[11].V(2) = &bboxMesh->cm.vert[4];

    bboxMesh->UpdateBoxAndNormals();
    return bboxMesh;
}

MeshModel* EditSlicePlugin::initBoundingSphere() {
    RenderMode rm;
    int rec = 3;
    rm.drawMode = vcg::GLW::DMNone;
    rm.colorMode = vcg::GLW::CMPerVert;
    MeshModel *m = this->md->addNewMesh("", "Bounding sphere", false, rm);
    m->cm.face.EnableFFAdjacency();
    m->updateDataMask(MeshModel::MM_FACEFACETOPO);
    assert(vcg::tri::HasPerVertexTexCoord(m->cm) == false);
    vcg::tri::Sphere<CMeshO>(m->cm, rec);

    for(CMeshO::VertexIterator vi = m->cm.vert.begin(); vi != m->cm.vert.end(); ++vi) {
        Box3f bbox = this->md->mm()->cm.bbox;
        vi->P() = vi->P() * (bbox.Diag() / 2.0);
        vi->P()[0] = vi->P()[0] + bbox.Center().X();
        vi->P()[1] = vi->P()[1] + bbox.Center().Y();
        vi->P()[2] = vi->P()[2] + bbox.Center().Z();
    }
    m->UpdateBoxAndNormals();
    update();
    return m;
}

MeshModel* EditSlicePlugin::getIntersectionWithAPlane(MeshModel* inputMesh, vcg::Point3f planeAxis, vcg::Point3f planeCenter, Color4b color) {
    planeAxis.Normalize();

    float planeOffset = 0;
    vcg::Plane3f slicingPlane;

    vcg::Box3f bbox = inputMesh->cm.bbox;
    MeshModel* base = inputMesh;
    MeshModel* orig= inputMesh;

    inputMesh->updateDataMask(MeshModel::MM_FACEFACETOPO);
    //actual cut of the mesh
    if (vcg::tri::Clean<CMeshO>::CountNonManifoldEdgeFF(base->cm)>0 || (vcg::tri::Clean<CMeshO>::CountNonManifoldVertexFF(base->cm,false) != 0))
    {
        qDebug() << "Mesh is not two manifold, cannot apply filter";
        return 0;
    }

    planeCenter = planeCenter + planeAxis * planeOffset * (bbox.Diag() / 2.0);


    //planeCenter+=planeAxis*planeDist ;
    slicingPlane.Init(planeCenter,planeAxis);

    //this is used to generate svg slices
    RenderMode rm;
    if (color[3] == 0) {
        rm.colorMode = vcg::GLW::CMNone;
        rm.drawMode = vcg::GLW::DMNone;
    } else {
        rm.colorMode = vcg::GLW::CMPerVert;
        rm.drawMode = vcg::GLW::DMSmooth;
    }
    MeshModel* cap = this->md->addNewMesh("","planar_intersection_points", false, rm);
    vcg::IntersectionPlaneMesh<CMeshO, CMeshO, float>(orig->cm, slicingPlane, cap->cm );
    vcg::tri::Clean<CMeshO>::RemoveDuplicateVertex(cap->cm);

    MeshModel* intersectionMesh = this->md->addNewMesh("","planar_intersection_face", false, rm);
    vcg::tri::CapEdgeMesh(cap->cm, intersectionMesh->cm);
    intersectionMesh->UpdateBoxAndNormals();
    this->md->delMesh(cap);

    intersectionMesh->updateDataMask(MeshModel::MM_VERTCOLOR);
    tri::UpdateColor<CMeshO>::PerVertexConstant(intersectionMesh->cm, color);
    update();
    return intersectionMesh;
}

void EditSlicePlugin::emitUpdateSignal()
{
    if (this->tempSliceMesh != NULL) {
        this->md->delMesh(this->tempSliceMesh);
    }
    this->tempSliceMesh = NULL;
    this->tempSliceMesh = getIntersectionWithAPlane(this->md->textureBBoxMillimeter, this->cuttingNormal, this->cuttingOrigin, vcg::Color4b(62, 128, 248, 0));
    this->md->sliceMesh = this->tempSliceMesh;
    this->md->sliceNorm = this->cuttingNormal.Normalize();
//    emit updateIntersectionMesh(this->tempSliceMesh, this->md, this->cuttingNormal.Normalize());
}

void EditSlicePlugin::update()
{
    glarea->update();
    glarea->sliceView->update();
}

void EditSlicePlugin::initCuttingPlane(vcg::Point3f planeAxis, vcg::Point3f origin) {
    if (this->boundingMesh) {
        this->cuttingMesh = getIntersectionWithAPlane(this->boundingMesh, planeAxis, cuttingOrigin);
    }
    update();
}

bool isOutOfBox(Box3f box, Point3f point)
{
    Point3f min = box.min;
    Point3f max = box.max;
    float offset = 1;
    // x coordinate
    if (point.X() - offset  < min.X()) return true;
    if (point.X() + offset > max.X()) return true;
    // y coordinate
    if (point.Y() - offset < min.Y()) return true;
    if (point.Y() + offset > max.Y()) return true;
    // Z coordinate
    if (point.Z() - offset < min.Z()) return true;
    if (point.Z() + offset > max.Z()) return true;
    return false;
}
