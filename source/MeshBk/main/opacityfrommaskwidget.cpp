#include "opacityfrommaskwidget.h"
#include "ui_opacityfrommaskwidget.h"

QProgressBar* OpacityFromMaskWidget::progressBar;

OpacityFromMaskWidget::OpacityFromMaskWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OpacityFromMaskWidget)
{
    ui->setupUi(this);
    initializeWidget();
}

OpacityFromMaskWidget::~OpacityFromMaskWidget()
{
    delete ui;
}

void OpacityFromMaskWidget::initializeWidget()
{
    ui->progressBar->setMaximumHeight(10);
    ui->progressBar->setMaximum(100);
    ui->progressBar->setMinimum(0);
    ui->progressBar->hide();
    progressBar = ui->progressBar;
}

bool OpacityFromMaskWidget::QCallBack(const int pos, const char *str)
{
    progressBar->show();
    progressBar->setValue(pos);
}

void OpacityFromMaskWidget::setTFOptions()
{
    currentParentNode->transfuncOptions.type = TransfuncTypes::OPACITY_MASK;

    TransfuncOptions tfOptions;
    tfOptions.type = TransfuncTypes::NONE;
    NodeViewOptions viewOption;
    viewOption.opacity = currentApplyNode->opacity;
    viewOption.colorTable = currentApplyNode->colorTable;
    currentApplyNode->transfunc = tfOpacityFromMask;
    currentApplyNode->transfuncOptions = tfOptions;
    currentApplyNode->viewOptions = viewOption;
    currentApplyNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);

    TransfuncOptions itfOptions;
    itfOptions.type = TransfuncTypes::NONE;
    NodeViewOptions viewOptionInverted;
    viewOptionInverted.opacity = currentOtherNode->opacity;
    viewOptionInverted.colorTable = currentOtherNode->colorTable;
    currentOtherNode->transfunc = tfOpacityFromMaskInverted;
    currentOtherNode->transfuncOptions = itfOptions;
    currentOtherNode->viewOptions = viewOptionInverted;
    currentOtherNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
}

void OpacityFromMaskWidget::startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *organ, TreeItem *other)
{
    if (type == TransfuncTypes::OPACITY_MASK) {
        if (md != NULL) {
            currentParentNode = parentNode;
            currentApplyNode = organ;
            currentOtherNode = other;

            ui->applyButton->show();
        }
    }
}

void OpacityFromMaskWidget::on_importMaskButton_clicked()
{
    QFileDialog folderChooser;
    folderChooser.setFileMode(QFileDialog::Directory);
    folderChooser.setOption(QFileDialog::ShowDirsOnly);

    QString maskFolder = folderChooser.getExistingDirectory(this, tr("Open Mask Folder"), "");
    if (maskFolder != "") {
        ui->importMaskEditText->setText(maskFolder);
    }
}

void OpacityFromMaskWidget::on_applyButton_clicked()
{
    int m = md->measurementPixel.X(); // Width of slice
    int n = md->measurementPixel.Y(); // Height of slice
    int h = md->measurementPixel.Z(); // Number of slice

    int size = m * n * h;
    for (int i = 0; i < size; i++) {
        currentApplyNode->opacity[i] = 0;
        currentOtherNode->opacity[i] = currentParentNode->opacity[i];
    }

    QDir folder(ui->importMaskEditText->text());
    folder.setFilter(QDir::Files);
    folder.setSorting(QDir::Name);

    QFileInfoList files = folder.entryInfoList();
    if (files.size() < h) {
        QMessageBox::warning(this, tr("Opening Problems"), tr("Number of masking file is less than neccessary, please try again!"));
        return;
    }

    progressBar->show();
    int progressMaxValue = files.size();

    for (int i = 0; i < files.size(); i++) {
        QCallBack((i+1) * 100 / progressMaxValue, "Reading files");

        QFileInfo file = files.at(i);

        try {
            DicomInfo dicom(file.absoluteFilePath());

            unsigned long instanceNumber = dicom.getInstanceNumber();
            if (instanceNumber < 0 || instanceNumber > h) {
                QMessageBox::warning(this, tr("Mapping Problems"), tr("Instance number is out of index, please try again!"));
                return;
            }

            cv::Mat slice = dicom.getOrginIntensityImage(0);
            int rows = slice.rows;
            int cols = slice.cols;

            if (cols != m || rows != n) {
                QMessageBox::warning(this, tr("Mapping Problems"), tr("Size of slice is out of index, please try again!"));
                return;
            }

            for (int y = 0; y < rows; y++) {
                for (int x = 0; x < cols; x++) {
                    if (slice.at<signed short>(y, x) != 0) {
                        int index = instanceNumber*m*n+ y*m + x;
                        currentApplyNode->opacity[index] = 1 * currentParentNode->opacity[index];
                        currentOtherNode->opacity[index] = 0;
                    }
                }
            } // end for each pixel in slice
        }catch (imebra::StreamOpenError exception) {
            std::cout << "imebra::StreamOpenError" << std::endl;
        } catch (imebra::MissingTagError exception) {
            std::cout << "imebra::MissingTagError" << std::endl;
        } catch (imebra::StreamEOFError exception) {
            std::cout << "imebra::StreamEOFError" << std::endl;
        } catch (imebra::CodecWrongFormatError exception) {
            std::cout << "imebra::CodecWrongFormatError" << std::endl;
        }
    } // end for each file

    setTFOptions();
    ui->applyButton->hide();
}

vcg::Color4f tfOpacityFromMask(signed short intensityArray[], int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (options.opacity[index] != 0) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

vcg::Color4f tfOpacityFromMaskInverted(signed short *intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;
    if (options.opacity[index] != 0) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}
