#ifndef VOLUMEPROPERTYWIDGET_H
#define VOLUMEPROPERTYWIDGET_H

#include "../common/interfaces.h"
#include <QMap>
#include <QWidget>
#include <QColorDialog>
#include <QProgressBar>
#include <histogramwidget.h>
#include <treemanagerwidget.h>
#include <predictneuralnetwork.h>
namespace Ui {
class VolumePropertyWidget;
}

class VolumePropertyWidget : public VolumeProperty
{
    Q_OBJECT

public:
    explicit VolumePropertyWidget(QWidget *parent = 0);
    ~VolumePropertyWidget();
    vcg::Color4f mapIntensity(float intensity);
    vcg::Color4f mapIndex(int index);
    static bool QCallBack(const int pos, const char *str);
    QMap<int, int> histogram;
    MeshDocument* md = NULL;
    void setMeshDocument(MeshDocument *md);
    TreeItem *currentSelectedNode = NULL;
    void applyHistogram(TransfuncOptions options, TreeItem* parentNode, TreeItem* applyNode, TreeItem* otherNode, signed short* intensityData, vcg::CallBackPos* cb);
private:
    // Public attributes
    Ui::VolumePropertyWidget *ui;
    QMap<float, vcg::Color4f> colorDictPreset;
    QMap<float, vcg::Color4f> currentMap;
    QList<QPair<float, vcg::Color4f>> colorListPreset;
    QSignalMapper *colorMapper;
    QSignalMapper *grayMapper;
    QSignalMapper *alphaMapper;
    QSignalMapper *removeMapper;
    TransfuncTypes currentTransFuncType = TransfuncTypes::NONE;
    TreeItem *currentApplyNode = NULL;
    TreeItem *currentOtherNode = NULL;
    TreeItem *currentParentNode = NULL;
    int threshold[2] = {-1024, 1023};

    // Public methods
    void initPreset();
    void initCurrentMap();
    QMap<float, vcg::Color4f> getCurrentColorMap() { return currentMap;}
    signed short* medianFilter();
    QList<int> getNeighbors(int centerIndex);

private slots:
    void usedColorTableChange(int isChecked);
    void updateHistogram();
    void on_applyTFButton_clicked();
    void startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *organ, TreeItem *other);
    void on_applyButton_clicked();
    void enableApplyButton();

signals:
    void updateColorGraph();
    void applyButtonClicked();
    void endTransferFunc(TransfuncTypes type);
public slots:
    void selectedNodeChanged(TreeItem *selectedItem);
    void colorTableChanged();
    void reloadHistogram();
};

#endif // VOLUMEPROPERTYWIDGET_H
vcg::Color4f tfhistogram(signed short intensityArray[], int index, NodeViewOptions options);
vcg::Color4f tfhistograminverted(signed short* intensityArray, int index, NodeViewOptions options);
vcg::Color4f tfprobabilitymodelinverted(signed short* intensityArray, int index, NodeViewOptions options);
vcg::Color4f tfprobabilitymodel(signed short* intensityArray, int index, NodeViewOptions options);
vcg::Color4f tfMedianFilter(signed short* intensityArray, int index, NodeViewOptions options);
vcg::Color4f tfMedianFilterInverted(signed short* intensityArray, int index, NodeViewOptions options);

