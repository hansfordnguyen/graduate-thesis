#ifndef SLICEVIEWWIDGET_H
#define SLICEVIEWWIDGET_H

#include <QWidget>
#include <common/interfaces.h>
#include "sliceview.h"
namespace Ui {
class SliceViewWidget;
}

class SliceViewWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SliceViewWidget(QWidget *parent = 0);
    ~SliceViewWidget();
    MeshDocument *md;
    SliceView *sliceView;
    void setMeshDocument(MeshDocument *md);
    QMap<int, bool> neighborGrowing();
    bool isRelevant(int mean, int index);
    bool isGrowing(float rate, int index);
    TransfuncTypes currentTransFuncType = TransfuncTypes::NONE;
    TreeItem *currentApplyNode = NULL;
    TreeItem *currentOtherNode = NULL;
    TreeItem *currentParentNode = NULL;
    float *gradientManigtude = NULL;
    float *termindationData = NULL;
    void calcGradientArray(vcg::CallBackPos *cb);
    void resizeEvent(QResizeEvent *event);
    void setToolboxVisibility(bool state);
    static bool QCallBack(const int pos, const char *str);
    static QProgressBar *progressbar;

public slots:
    void startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *applyNode, TreeItem *otherNode);
    void onBrightnessChanged(int value);
private slots:
    void on_clearSelectionButton_clicked();
    void on_startSelectButton_clicked(bool checked);
    void on_applyTFButton_clicked();
    void on_loadTerminationDataButton_clicked();
    void on_calcTerminationDataButton_clicked();
    void on_pushButton_clicked();

    void on_resetBrightnessButton_clicked();

signals:
    void endTransferFunc(TransfuncTypes type);
private:
    Ui::SliceViewWidget *ui;
    float gradient(int index);
    void prepareTerminateDate(vcg::CallBackPos* cb = 0);
    vcg::Point3i indexToCoords(int index, int width, int height);
    int coordsToIndex(vcg::Point3i coords, int width, int height);
    vcg::Point3i paddingIndex(vcg::Point3i coord, int width, int height, int numSlice);
    void loadTerminationData(vcg::CallBackPos* cb = 0);
    void loadTerminationDataBinary(vcg::CallBackPos* cb = 0);
    QList<int> getNeighbors(int index, int width, int height, int sliceNum, vcg::Point3i radius);
};

#endif // SLICEVIEWWIDGET_H
vcg::Color4f tfRegionGrowing(signed short* intensityArray, int index, NodeViewOptions options);
