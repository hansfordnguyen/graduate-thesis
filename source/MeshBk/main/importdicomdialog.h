#ifndef IMPORTDICOMDIALOG_H
#define IMPORTDICOMDIALOG_H

#include "mainwindow.h"
#include <QPair>
#include <QDialog>
#include <QList>
#include <QFileInfoList>
#include <QDebug>
#include <QAbstractButton>

#include <opencv2/opencv.hpp>
#include <limits.h>

#include "utils_opendiomsfolder/dicominfo.h"
#include "common/interfaces.h"

enum TableColume{CHECKCOL = 0, NAMECOL, PATHCOL};
namespace Ui {
class ImportDICOMDialog;
}

typedef struct OrganItem{
    QString organLabel;
    QString organDirPath;
    OrganItem(QString label, QString path): organLabel(label), organDirPath(path){}
} OrganItem;

typedef vcg::Point3i Measurement3i;

typedef vcg::Point3f Measurement3f;

class ImportDICOMDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ImportDICOMDialog(QWidget *parent = 0);
    ~ImportDICOMDialog();
    static bool QCallBack(const int pos, const char *str);

private slots:
    void on_openFolderBtn_clicked();
    void onOrganClick(int row, int col);
    void on_dialogButtons_clicked(QAbstractButton *button);

    void on_dialogButtons_accepted();
    void organSelectionChange(bool state);
    void importAllToggle(bool state);
private:
    Ui::ImportDICOMDialog *ui;
    QString patientFolderPath = "";

    MainWindow *mainWindow;

    Measurement3f measurementMillimeter;
    Measurement3i measurementPixel;

    QList<OrganItem> getOrganItems(QString patientDirPath);
    QList<OrganItem> importData(QString patientDirPath);
    QList<OrganItem> importData();

    Measurement3f getMesurementMillimter() {return this->measurementMillimeter;}
    Measurement3i getMesurementPixel() {return this->measurementPixel;}

    void loadOrganIntoMeshDocument(QString name, QString organPath, vcg::CallBackPos* = 0);
    void loadMultiOrganIntoMeshDocument(QList<QPair<QString, QString>> nameAndPath, vcg::CallBackPos* = 0);
    void loadAllOrganIntoMeshDocument(QString path, vcg::CallBackPos* = 0);
    void addOrganMesh(QString name, QString organPath, vcg::CallBackPos* = 0);
    void addOrganVolume(QString organPath, vcg::CallBackPos* = 0);
    void addOrganTexture(QString organPath, vcg::CallBackPos* = 0);
    static QProgressBar *progressbar;
    static QLabel *statusLabel;
    void generateOrganTabel(QList<OrganItem> organList);
    QList<QPair<QString, QString>> selectionPaths;
    QList<cv::Point3f> boundBoxVertices;
};

#endif // IMPORTDICOMDIALOG_H
