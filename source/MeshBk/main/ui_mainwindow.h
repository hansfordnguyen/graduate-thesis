/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDockWidget>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMdiArea>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QTableWidget>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "intensitygradienthistwidget.h"
#include "opacityfromfilewidget.h"
#include "opacityfrommaskwidget.h"
#include "sliceviewwidget.h"
#include "spaceselectionwidget.h"
#include "treemanagerwidget.h"
#include "volumepropertywidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *menuExit;
    QAction *menuApplyFilter;
    QAction *menuOpenFile;
    QAction *menuOpenFolder;
    QAction *buttonImportMesh;
    QAction *menuViewMeshLayer;
    QAction *butonImportDICOM;
    QAction *menuColoringFilter;
    QAction *menuXrayShader;
    QAction *showTrackBallAct;
    QAction *menuViewAxis;
    QAction *actionImport_DICOM;
    QAction *menuViewVolumeProperty;
    QAction *menuViewVolume;
    QAction *menuViewTreeManager;
    QAction *menuViewSliceView;
    QAction *actionSlice_Tool;
    QAction *menuGeneratePointCloud;
    QAction *actionOpen_project;
    QAction *actionSave_Project;
    QAction *actionSave_Project_as;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QMdiArea *mdiarea;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuView;
    QToolBar *mainToolbar;
    QDockWidget *volumePropertyDock;
    QWidget *volumePropertyDockContent;
    QVBoxLayout *verticalLayout_3;
    VolumePropertyWidget *volumeProperty;
    QDockWidget *sliceDock;
    QWidget *dockWidgetContents_2;
    QVBoxLayout *verticalLayout_5;
    SliceViewWidget *sliceViewWidget;
    QDockWidget *layersDock;
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout;
    QTableWidget *layerTable;
    QVBoxLayout *sliderLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QSlider *backgroundColorSlider;
    QPushButton *resetButton;
    QDockWidget *treeDock;
    QWidget *dockWidgetContents_3;
    QVBoxLayout *verticalLayout_4;
    TreeManagerWidget *treeManagerWidget;
    QDockWidget *histogram2DDock;
    QWidget *dockWidgetContents_4;
    QVBoxLayout *verticalLayout_6;
    IntensityGradientHistWidget *histogram2DWidget;
    QDockWidget *spaceSelectionDock;
    QWidget *dockWidgetContents_5;
    QVBoxLayout *verticalLayout_7;
    SpaceSelectionWidget *spaceSelectionWidget;
    QDockWidget *opacityFromFileDock;
    QWidget *dockWidgetContents_6;
    QVBoxLayout *verticalLayout_8;
    OpacityFromFileWidget *opacityFromFileWidget;
    QDockWidget *opacityFromMaskDock;
    QWidget *dockWidgetContents_7;
    QVBoxLayout *verticalLayout_9;
    OpacityFromMaskWidget *opacityFromMaskWidget;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(858, 904);
        MainWindow->setStyleSheet(QString::fromUtf8("#volumePropertyDock {\n"
"	background-color: rgb(107, 107, 107);\n"
"}"));
        menuExit = new QAction(MainWindow);
        menuExit->setObjectName(QString::fromUtf8("menuExit"));
        menuApplyFilter = new QAction(MainWindow);
        menuApplyFilter->setObjectName(QString::fromUtf8("menuApplyFilter"));
        menuOpenFile = new QAction(MainWindow);
        menuOpenFile->setObjectName(QString::fromUtf8("menuOpenFile"));
        menuOpenFolder = new QAction(MainWindow);
        menuOpenFolder->setObjectName(QString::fromUtf8("menuOpenFolder"));
        buttonImportMesh = new QAction(MainWindow);
        buttonImportMesh->setObjectName(QString::fromUtf8("buttonImportMesh"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/imortMesh.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonImportMesh->setIcon(icon);
        menuViewMeshLayer = new QAction(MainWindow);
        menuViewMeshLayer->setObjectName(QString::fromUtf8("menuViewMeshLayer"));
        menuViewMeshLayer->setCheckable(true);
        menuViewMeshLayer->setChecked(false);
        butonImportDICOM = new QAction(MainWindow);
        butonImportDICOM->setObjectName(QString::fromUtf8("butonImportDICOM"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/importDICOM.png"), QSize(), QIcon::Disabled, QIcon::Off);
        butonImportDICOM->setIcon(icon1);
        menuColoringFilter = new QAction(MainWindow);
        menuColoringFilter->setObjectName(QString::fromUtf8("menuColoringFilter"));
        menuXrayShader = new QAction(MainWindow);
        menuXrayShader->setObjectName(QString::fromUtf8("menuXrayShader"));
        menuXrayShader->setCheckable(true);
        showTrackBallAct = new QAction(MainWindow);
        showTrackBallAct->setObjectName(QString::fromUtf8("showTrackBallAct"));
        menuViewAxis = new QAction(MainWindow);
        menuViewAxis->setObjectName(QString::fromUtf8("menuViewAxis"));
        menuViewAxis->setCheckable(true);
        actionImport_DICOM = new QAction(MainWindow);
        actionImport_DICOM->setObjectName(QString::fromUtf8("actionImport_DICOM"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/importicon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionImport_DICOM->setIcon(icon2);
        menuViewVolumeProperty = new QAction(MainWindow);
        menuViewVolumeProperty->setObjectName(QString::fromUtf8("menuViewVolumeProperty"));
        menuViewVolumeProperty->setCheckable(true);
        menuViewVolumeProperty->setChecked(false);
        menuViewVolume = new QAction(MainWindow);
        menuViewVolume->setObjectName(QString::fromUtf8("menuViewVolume"));
        menuViewVolume->setCheckable(true);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/volumeShow.png"), QSize(), QIcon::Normal, QIcon::On);
        menuViewVolume->setIcon(icon3);
        menuViewTreeManager = new QAction(MainWindow);
        menuViewTreeManager->setObjectName(QString::fromUtf8("menuViewTreeManager"));
        menuViewTreeManager->setCheckable(true);
        menuViewTreeManager->setChecked(true);
        menuViewSliceView = new QAction(MainWindow);
        menuViewSliceView->setObjectName(QString::fromUtf8("menuViewSliceView"));
        menuViewSliceView->setCheckable(true);
        menuViewSliceView->setChecked(false);
        actionSlice_Tool = new QAction(MainWindow);
        actionSlice_Tool->setObjectName(QString::fromUtf8("actionSlice_Tool"));
        actionSlice_Tool->setCheckable(true);
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/planarIntersection.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSlice_Tool->setIcon(icon4);
        menuGeneratePointCloud = new QAction(MainWindow);
        menuGeneratePointCloud->setObjectName(QString::fromUtf8("menuGeneratePointCloud"));
        actionOpen_project = new QAction(MainWindow);
        actionOpen_project->setObjectName(QString::fromUtf8("actionOpen_project"));
        actionSave_Project = new QAction(MainWindow);
        actionSave_Project->setObjectName(QString::fromUtf8("actionSave_Project"));
        actionSave_Project_as = new QAction(MainWindow);
        actionSave_Project_as->setObjectName(QString::fromUtf8("actionSave_Project_as"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        mdiarea = new QMdiArea(centralWidget);
        mdiarea->setObjectName(QString::fromUtf8("mdiarea"));

        verticalLayout_2->addWidget(mdiarea);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 858, 20));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QString::fromUtf8("menuEdit"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        MainWindow->setMenuBar(menuBar);
        mainToolbar = new QToolBar(MainWindow);
        mainToolbar->setObjectName(QString::fromUtf8("mainToolbar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolbar);
        volumePropertyDock = new QDockWidget(MainWindow);
        volumePropertyDock->setObjectName(QString::fromUtf8("volumePropertyDock"));
        volumePropertyDock->setMinimumSize(QSize(413, 47));
        volumePropertyDock->setMaximumSize(QSize(413, 524287));
        volumePropertyDock->setAutoFillBackground(false);
        volumePropertyDock->setStyleSheet(QString::fromUtf8("#volumePropertyDock::title{\n"
"background-color: rgb(206, 206, 206);\n"
"padding: 2px;\n"
"}"));
        volumePropertyDock->setAllowedAreas(Qt::LeftDockWidgetArea|Qt::RightDockWidgetArea);
        volumePropertyDockContent = new QWidget();
        volumePropertyDockContent->setObjectName(QString::fromUtf8("volumePropertyDockContent"));
        volumePropertyDockContent->setMinimumSize(QSize(340, 0));
        volumePropertyDockContent->setStyleSheet(QString::fromUtf8("#volumePropertyDockContent{\n"
"background-color: rgb(206, 206, 206);\n"
"border: 1px solid rgb(138, 138, 138);\n"
"}"));
        verticalLayout_3 = new QVBoxLayout(volumePropertyDockContent);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        volumeProperty = new VolumePropertyWidget(volumePropertyDockContent);
        volumeProperty->setObjectName(QString::fromUtf8("volumeProperty"));
        volumeProperty->setEnabled(true);
        volumeProperty->setMinimumSize(QSize(150, 0));

        verticalLayout_3->addWidget(volumeProperty);

        volumePropertyDock->setWidget(volumePropertyDockContent);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), volumePropertyDock);
        sliceDock = new QDockWidget(MainWindow);
        sliceDock->setObjectName(QString::fromUtf8("sliceDock"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(sliceDock->sizePolicy().hasHeightForWidth());
        sliceDock->setSizePolicy(sizePolicy);
        sliceDock->setMaximumSize(QSize(524287, 524287));
        sliceDock->setStyleSheet(QString::fromUtf8("padding: 0px;margin: 0px;"));
        sliceDock->setFeatures(QDockWidget::AllDockWidgetFeatures);
        sliceDock->setAllowedAreas(Qt::LeftDockWidgetArea|Qt::RightDockWidgetArea);
        dockWidgetContents_2 = new QWidget();
        dockWidgetContents_2->setObjectName(QString::fromUtf8("dockWidgetContents_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(dockWidgetContents_2->sizePolicy().hasHeightForWidth());
        dockWidgetContents_2->setSizePolicy(sizePolicy1);
        dockWidgetContents_2->setStyleSheet(QString::fromUtf8("padding: 0px; margin: 0px;"));
        verticalLayout_5 = new QVBoxLayout(dockWidgetContents_2);
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setSizeConstraint(QLayout::SetNoConstraint);
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        sliceViewWidget = new SliceViewWidget(dockWidgetContents_2);
        sliceViewWidget->setObjectName(QString::fromUtf8("sliceViewWidget"));

        verticalLayout_5->addWidget(sliceViewWidget);

        sliceDock->setWidget(dockWidgetContents_2);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), sliceDock);
        layersDock = new QDockWidget(MainWindow);
        layersDock->setObjectName(QString::fromUtf8("layersDock"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(layersDock->sizePolicy().hasHeightForWidth());
        layersDock->setSizePolicy(sizePolicy2);
        layersDock->setMinimumSize(QSize(300, 270));
        layersDock->setMaximumSize(QSize(524287, 524287));
        layersDock->setCursor(QCursor(Qt::ArrowCursor));
        layersDock->setFloating(false);
        layersDock->setAllowedAreas(Qt::LeftDockWidgetArea|Qt::RightDockWidgetArea);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        verticalLayout = new QVBoxLayout(dockWidgetContents);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetMaximumSize);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        layerTable = new QTableWidget(dockWidgetContents);
        layerTable->setObjectName(QString::fromUtf8("layerTable"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(layerTable->sizePolicy().hasHeightForWidth());
        layerTable->setSizePolicy(sizePolicy3);
        layerTable->setStyleSheet(QString::fromUtf8(""));

        verticalLayout->addWidget(layerTable);

        sliderLayout = new QVBoxLayout();
        sliderLayout->setSpacing(6);
        sliderLayout->setObjectName(QString::fromUtf8("sliderLayout"));
        sliderLayout->setSizeConstraint(QLayout::SetMinimumSize);
        label = new QLabel(dockWidgetContents);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy4);

        sliderLayout->addWidget(label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        backgroundColorSlider = new QSlider(dockWidgetContents);
        backgroundColorSlider->setObjectName(QString::fromUtf8("backgroundColorSlider"));
        backgroundColorSlider->setMaximum(100);
        backgroundColorSlider->setOrientation(Qt::Horizontal);

        horizontalLayout->addWidget(backgroundColorSlider);

        resetButton = new QPushButton(dockWidgetContents);
        resetButton->setObjectName(QString::fromUtf8("resetButton"));

        horizontalLayout->addWidget(resetButton);


        sliderLayout->addLayout(horizontalLayout);


        verticalLayout->addLayout(sliderLayout);

        layersDock->setWidget(dockWidgetContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), layersDock);
        treeDock = new QDockWidget(MainWindow);
        treeDock->setObjectName(QString::fromUtf8("treeDock"));
        dockWidgetContents_3 = new QWidget();
        dockWidgetContents_3->setObjectName(QString::fromUtf8("dockWidgetContents_3"));
        verticalLayout_4 = new QVBoxLayout(dockWidgetContents_3);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        treeManagerWidget = new TreeManagerWidget(dockWidgetContents_3);
        treeManagerWidget->setObjectName(QString::fromUtf8("treeManagerWidget"));

        verticalLayout_4->addWidget(treeManagerWidget);

        treeDock->setWidget(dockWidgetContents_3);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(1), treeDock);
        histogram2DDock = new QDockWidget(MainWindow);
        histogram2DDock->setObjectName(QString::fromUtf8("histogram2DDock"));
        dockWidgetContents_4 = new QWidget();
        dockWidgetContents_4->setObjectName(QString::fromUtf8("dockWidgetContents_4"));
        verticalLayout_6 = new QVBoxLayout(dockWidgetContents_4);
        verticalLayout_6->setSpacing(0);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        histogram2DWidget = new IntensityGradientHistWidget(dockWidgetContents_4);
        histogram2DWidget->setObjectName(QString::fromUtf8("histogram2DWidget"));

        verticalLayout_6->addWidget(histogram2DWidget);

        histogram2DDock->setWidget(dockWidgetContents_4);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), histogram2DDock);
        spaceSelectionDock = new QDockWidget(MainWindow);
        spaceSelectionDock->setObjectName(QString::fromUtf8("spaceSelectionDock"));
        dockWidgetContents_5 = new QWidget();
        dockWidgetContents_5->setObjectName(QString::fromUtf8("dockWidgetContents_5"));
        verticalLayout_7 = new QVBoxLayout(dockWidgetContents_5);
        verticalLayout_7->setSpacing(0);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        spaceSelectionWidget = new SpaceSelectionWidget(dockWidgetContents_5);
        spaceSelectionWidget->setObjectName(QString::fromUtf8("spaceSelectionWidget"));

        verticalLayout_7->addWidget(spaceSelectionWidget);

        spaceSelectionDock->setWidget(dockWidgetContents_5);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), spaceSelectionDock);
        opacityFromFileDock = new QDockWidget(MainWindow);
        opacityFromFileDock->setObjectName(QString::fromUtf8("opacityFromFileDock"));
        dockWidgetContents_6 = new QWidget();
        dockWidgetContents_6->setObjectName(QString::fromUtf8("dockWidgetContents_6"));
        verticalLayout_8 = new QVBoxLayout(dockWidgetContents_6);
        verticalLayout_8->setSpacing(0);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        opacityFromFileWidget = new OpacityFromFileWidget(dockWidgetContents_6);
        opacityFromFileWidget->setObjectName(QString::fromUtf8("opacityFromFileWidget"));

        verticalLayout_8->addWidget(opacityFromFileWidget);

        opacityFromFileDock->setWidget(dockWidgetContents_6);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), opacityFromFileDock);
        opacityFromMaskDock = new QDockWidget(MainWindow);
        opacityFromMaskDock->setObjectName(QString::fromUtf8("opacityFromMaskDock"));
        dockWidgetContents_7 = new QWidget();
        dockWidgetContents_7->setObjectName(QString::fromUtf8("dockWidgetContents_7"));
        verticalLayout_9 = new QVBoxLayout(dockWidgetContents_7);
        verticalLayout_9->setSpacing(5);
        verticalLayout_9->setContentsMargins(11, 11, 11, 11);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(0, 0, 0, 0);
        opacityFromMaskWidget = new OpacityFromMaskWidget(dockWidgetContents_7);
        opacityFromMaskWidget->setObjectName(QString::fromUtf8("opacityFromMaskWidget"));

        verticalLayout_9->addWidget(opacityFromMaskWidget);

        opacityFromMaskDock->setWidget(dockWidgetContents_7);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), opacityFromMaskDock);
        layersDock->raise();
        treeDock->raise();
        histogram2DDock->raise();
        spaceSelectionDock->raise();
        opacityFromFileDock->raise();
        opacityFromMaskDock->raise();

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuFile->addAction(menuOpenFile);
        menuFile->addAction(menuOpenFolder);
        menuFile->addAction(actionOpen_project);
        menuFile->addAction(actionSave_Project);
        menuFile->addAction(actionSave_Project_as);
        menuFile->addAction(menuExit);
        menuEdit->addAction(menuApplyFilter);
        menuEdit->addAction(menuGeneratePointCloud);
        menuEdit->addAction(menuColoringFilter);
        menuView->addSeparator();
        menuView->addAction(menuViewMeshLayer);
        menuView->addAction(menuViewVolumeProperty);
        menuView->addAction(menuViewTreeManager);
        menuView->addAction(menuViewSliceView);
        menuView->addAction(menuXrayShader);
        menuView->addAction(showTrackBallAct);
        menuView->addSeparator();
        menuView->addAction(menuViewVolume);
        menuView->addAction(menuViewAxis);
        mainToolbar->addAction(actionImport_DICOM);
        mainToolbar->addAction(buttonImportMesh);
        mainToolbar->addAction(butonImportDICOM);
        mainToolbar->addSeparator();
        mainToolbar->addAction(menuViewVolume);
        mainToolbar->addAction(actionSlice_Tool);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        menuExit->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        menuApplyFilter->setText(QApplication::translate("MainWindow", "Apply Filter", 0, QApplication::UnicodeUTF8));
        menuOpenFile->setText(QApplication::translate("MainWindow", "Open File", 0, QApplication::UnicodeUTF8));
        menuOpenFolder->setText(QApplication::translate("MainWindow", "Open Folder", 0, QApplication::UnicodeUTF8));
        buttonImportMesh->setText(QApplication::translate("MainWindow", "Import Mesh", 0, QApplication::UnicodeUTF8));
        menuViewMeshLayer->setText(QApplication::translate("MainWindow", "Mesh Layer", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        menuViewMeshLayer->setToolTip(QApplication::translate("MainWindow", "Show Mesh Layer Dock", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        butonImportDICOM->setText(QApplication::translate("MainWindow", "Import DICOM", 0, QApplication::UnicodeUTF8));
        menuColoringFilter->setText(QApplication::translate("MainWindow", "Per Face Color Function", 0, QApplication::UnicodeUTF8));
        menuXrayShader->setText(QApplication::translate("MainWindow", "Shader xray", 0, QApplication::UnicodeUTF8));
        showTrackBallAct->setText(QApplication::translate("MainWindow", "Show Track Ball", 0, QApplication::UnicodeUTF8));
        menuViewAxis->setText(QApplication::translate("MainWindow", "Show Axis", 0, QApplication::UnicodeUTF8));
        actionImport_DICOM->setText(QApplication::translate("MainWindow", "Import DICOM", 0, QApplication::UnicodeUTF8));
        menuViewVolumeProperty->setText(QApplication::translate("MainWindow", "Volume Property", 0, QApplication::UnicodeUTF8));
        menuViewVolume->setText(QApplication::translate("MainWindow", "Volume Rendering", 0, QApplication::UnicodeUTF8));
        menuViewTreeManager->setText(QApplication::translate("MainWindow", "Tree Manager", 0, QApplication::UnicodeUTF8));
        menuViewSliceView->setText(QApplication::translate("MainWindow", "Slice View", 0, QApplication::UnicodeUTF8));
        actionSlice_Tool->setText(QApplication::translate("MainWindow", "Slice Tool", 0, QApplication::UnicodeUTF8));
        actionSlice_Tool->setShortcut(QApplication::translate("MainWindow", "Ctrl+D", 0, QApplication::UnicodeUTF8));
        menuGeneratePointCloud->setText(QApplication::translate("MainWindow", "Generate Point Cloud", 0, QApplication::UnicodeUTF8));
        actionOpen_project->setText(QApplication::translate("MainWindow", "Open Project", 0, QApplication::UnicodeUTF8));
        actionOpen_project->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0, QApplication::UnicodeUTF8));
        actionSave_Project->setText(QApplication::translate("MainWindow", "Save Project", 0, QApplication::UnicodeUTF8));
        actionSave_Project->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0, QApplication::UnicodeUTF8));
        actionSave_Project_as->setText(QApplication::translate("MainWindow", "Save Project as", 0, QApplication::UnicodeUTF8));
        actionSave_Project_as->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+S", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menuEdit->setTitle(QApplication::translate("MainWindow", "Edit", 0, QApplication::UnicodeUTF8));
        menuView->setTitle(QApplication::translate("MainWindow", "View", 0, QApplication::UnicodeUTF8));
        mainToolbar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0, QApplication::UnicodeUTF8));
        volumePropertyDock->setWindowTitle(QApplication::translate("MainWindow", "Volume Properties", 0, QApplication::UnicodeUTF8));
        sliceDock->setWindowTitle(QApplication::translate("MainWindow", "Slice View", 0, QApplication::UnicodeUTF8));
        layersDock->setWindowTitle(QString());
        label->setText(QApplication::translate("MainWindow", "Brightness", 0, QApplication::UnicodeUTF8));
        resetButton->setText(QApplication::translate("MainWindow", "Reset", 0, QApplication::UnicodeUTF8));
        treeDock->setWindowTitle(QApplication::translate("MainWindow", "Tree Manager", 0, QApplication::UnicodeUTF8));
        histogram2DDock->setWindowTitle(QApplication::translate("MainWindow", "Histogram 2D", 0, QApplication::UnicodeUTF8));
        spaceSelectionDock->setWindowTitle(QApplication::translate("MainWindow", "Space Selection", 0, QApplication::UnicodeUTF8));
        opacityFromFileDock->setWindowTitle(QApplication::translate("MainWindow", "Opacity From File", 0, QApplication::UnicodeUTF8));
        opacityFromMaskDock->setWindowTitle(QApplication::translate("MainWindow", "Opacity From Mask", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
