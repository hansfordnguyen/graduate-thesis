#include "spaceselectionwidget.h"
#include "ui_spaceselectionwidget.h"

QProgressBar* SpaceSelectionWidget::progressBar;

SpaceSelectionWidget::SpaceSelectionWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SpaceSelectionWidget)
{
    ui->setupUi(this);

    initializeWidgets();
}

SpaceSelectionWidget::~SpaceSelectionWidget()
{
    delete ui;
}

bool SpaceSelectionWidget::QCallBack(const int pos, const char *str)
{
    progressBar->show();
    progressBar->setValue(pos);
}

void SpaceSelectionWidget::initializeWidgets()
{
    ui->cubeFrontLabel->setFixedWidth(50);
    ui->cubeBackLabel->setFixedWidth(50);
    ui->cubeLeftLabel->setFixedWidth(50);
    ui->cubeRightLabel->setFixedWidth(50);
    ui->cubeTopLabel->setFixedWidth(50);
    ui->cubeRightLabel->setFixedWidth(50);
    ui->cubeColorLabel->setFixedWidth(50);

    ui->sphereCenterXLabel->setFixedWidth(60);
    ui->sphereCenterYLabel->setFixedWidth(60);
    ui->sphereCenterZLabel->setFixedWidth(60);
    ui->sphereRadiusLabel->setFixedWidth(60);
    ui->sphereColorLabel->setFixedWidth(60);

    ui->progressBar->setMaximumHeight(10);
    ui->progressBar->setMaximum(100);
    ui->progressBar->setMinimum(0);
    ui->progressBar->hide();
    progressBar = ui->progressBar;

    connect(ui->cubeFrontSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBoundingCubeParams()));
    connect(ui->cubeBackSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBoundingCubeParams()));
    connect(ui->cubeLeftSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBoundingCubeParams()));
    connect(ui->cubeRightSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBoundingCubeParams()));
    connect(ui->cubeTopSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBoundingCubeParams()));
    connect(ui->cubeBottomSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBoundingCubeParams()));

    connect(ui->sphereCenterXSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBoundingSphereParams()));
    connect(ui->sphereCenterYSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBoundingSphereParams()));
    connect(ui->sphereCenterZSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBoundingSphereParams()));
    connect(ui->sphereRadiusSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBoundingSphereParams()));
}

void SpaceSelectionWidget::initializeBoundingCube()
{
    float bboxWidth = sliceCols * sliceXSpace + 3;
    float bboxHeight = sliceRows * sliceYSpace + 3;
    float bboxDepth = (numOfSlice - 1) * sliceZSpace + 3;

    ui->cubeFrontSlider->setEnabled(true);
    ui->cubeBackSlider->setEnabled(true);
    ui->cubeLeftSlider->setEnabled(true);
    ui->cubeRightSlider->setEnabled(true);
    ui->cubeTopSlider->setEnabled(true);
    ui->cubeBottomSlider->setEnabled(true);

    ui->cubeFrontSlider->setMinimum(-3);
    ui->cubeFrontSlider->setMaximum(bboxHeight);
    ui->cubeFrontSlider->setValue(-3);

    ui->cubeBackSlider->setMinimum(-3);
    ui->cubeBackSlider->setMaximum(bboxHeight);
    ui->cubeBackSlider->setValue(bboxHeight);

    ui->cubeLeftSlider->setMinimum(-3);
    ui->cubeLeftSlider->setMaximum(bboxWidth);
    ui->cubeLeftSlider->setValue(-3);

    ui->cubeRightSlider->setMinimum(-3);
    ui->cubeRightSlider->setMaximum(bboxWidth);
    ui->cubeRightSlider->setValue(bboxWidth);

    ui->cubeTopSlider->setMinimum(-3);
    ui->cubeTopSlider->setMaximum(bboxDepth);
    ui->cubeTopSlider->setValue(bboxDepth);

    ui->cubeBottomSlider->setMinimum(-3);
    ui->cubeBottomSlider->setMaximum(bboxDepth);
    ui->cubeBottomSlider->setValue(-3);

    vcg::Point3f p0(ui->cubeLeftSlider->value(), ui->cubeFrontSlider->value(), ui->cubeBottomSlider->value());
    vcg::Point3f p1(ui->cubeRightSlider->value(), ui->cubeFrontSlider->value(), ui->cubeBottomSlider->value());
    vcg::Point3f p2(ui->cubeRightSlider->value(), ui->cubeBackSlider->value(), ui->cubeBottomSlider->value());
    vcg::Point3f p3(ui->cubeLeftSlider->value(), ui->cubeBackSlider->value(), ui->cubeBottomSlider->value());
    vcg::Point3f p4(ui->cubeLeftSlider->value(), ui->cubeFrontSlider->value(), ui->cubeTopSlider->value());
    vcg::Point3f p5(ui->cubeRightSlider->value(), ui->cubeFrontSlider->value(), ui->cubeTopSlider->value());
    vcg::Point3f p6(ui->cubeRightSlider->value(), ui->cubeBackSlider->value(), ui->cubeTopSlider->value());
    vcg::Point3f p7(ui->cubeLeftSlider->value(), ui->cubeBackSlider->value(), ui->cubeTopSlider->value());

    this->md->updateLimitedPoints(p0, p1, p2, p3, p4, p5, p6, p7);
    this->gla->updatePaintAreaForcefully();

}

void SpaceSelectionWidget::initializeBoundingSphere()
{
    float bboxWidth = sliceCols * sliceXSpace;
    float bboxHeight = sliceRows * sliceYSpace;
    float bboxDepth = (numOfSlice - 1) * sliceZSpace;

    ui->sphereCenterXSlider->setEnabled(true);
    ui->sphereCenterYSlider->setEnabled(true);
    ui->sphereCenterZSlider->setEnabled(true);
    ui->sphereRadiusSlider->setEnabled(true);

    ui->sphereCenterXSlider->setMinimum(0);
    ui->sphereCenterXSlider->setMaximum(bboxWidth);
    ui->sphereCenterXSlider->setValue(bboxWidth/2);

    ui->sphereCenterYSlider->setMinimum(0);
    ui->sphereCenterYSlider->setMaximum(bboxHeight);
    ui->sphereCenterYSlider->setValue(bboxHeight/2);

    ui->sphereCenterZSlider->setMinimum(0);
    ui->sphereCenterZSlider->setMaximum(bboxDepth);
    ui->sphereCenterZSlider->setValue(bboxDepth/2);

    float maxDiagonal = sqrt(bboxWidth*bboxWidth +
                             bboxHeight*bboxHeight +
                             bboxDepth*bboxDepth);

    ui->sphereRadiusSlider->setMinimum(0);
    ui->sphereRadiusSlider->setMaximum(maxDiagonal);
    ui->sphereRadiusSlider->setValue(maxDiagonal/2);

    vcg::Point3f center(ui->sphereCenterXSlider->value(),
                        ui->sphereCenterYSlider->value(),
                        ui->sphereCenterZSlider->value());
    float radius = ui->sphereRadiusSlider->value();

    this->md->updateLimitedSphereParams(center, radius);
}

void SpaceSelectionWidget::setMeshDoc(MeshDocument *meshDoc)
{
    this->md = meshDoc;
    this->md->currentLimitedShape = MeshDocument::LimitedShape::None;
}

void SpaceSelectionWidget::setGLA(GLArea *glarea)
{
    this->gla = glarea;
}

BoundingCube SpaceSelectionWidget::buildBoundingCube()
{
    float modelViewBoxFront = ui->cubeFrontSlider->value();
    float modelViewBoxBack = ui->cubeBackSlider->value();
    float modelViewBoxLeft = ui->cubeLeftSlider->value();
    float modelViewBoxRight = ui->cubeRightSlider->value();
    float modelViewBoxTop = ui->cubeTopSlider->value();
    float modelViewBoxBottom = ui->cubeBottomSlider->value();

    int voxelBoxFront = (int)(((float)modelViewBoxFront) / sliceYSpace);
    int voxelBoxBack = (int)(((float)modelViewBoxBack) / sliceYSpace);
    int voxelBoxLeft = (int)(((float)modelViewBoxLeft) / sliceXSpace);
    int voxelBoxRight = (int)(((float)modelViewBoxRight) / sliceXSpace);
    int voxelBoxTop = (int)(((float)modelViewBoxTop) / sliceZSpace);
    int voxelBoxBottom = (int)(((float)modelViewBoxBottom) / sliceZSpace);

    int minX = std::min(voxelBoxLeft, voxelBoxRight);
    int maxX = std::max(voxelBoxLeft, voxelBoxRight);
    int minY = std::min(voxelBoxFront, voxelBoxBack);
    int maxY = std::max(voxelBoxFront, voxelBoxBack);
    int minZ = std::min(voxelBoxTop, voxelBoxBottom);
    int maxZ = std::max(voxelBoxTop, voxelBoxBottom);

    BoundingCube boundingCube;
    boundingCube.minX = minX;
    boundingCube.maxX = maxX;
    boundingCube.minY = minY;
    boundingCube.maxY = maxY;
    boundingCube.minZ = minZ;
    boundingCube.maxZ = maxZ;

    return boundingCube;
}

BoundingSphere SpaceSelectionWidget::buildBoundingSphere()
{
    BoundingSphere sphere;
    sphere.centerX = ui->sphereCenterXSlider->value();
    sphere.centerY = ui->sphereCenterYSlider->value();
    sphere.centerZ = ui->sphereCenterZSlider->value();
    sphere.radius = ui->sphereRadiusSlider->value();

    return sphere;
}

bool SpaceSelectionWidget::mapping(BoundingCube boundingCube, BoundingSphere boundingSphere, vcg::CallBackPos *cb)
{
    int size = sliceRows * sliceCols * numOfSlice;

    int currentProgress = 0;
    int value = 0;

    switch (this->md->currentLimitedShape) {
    case MeshDocument::LimitedShape::Cube:
        progressBar->show();

        for(int i = 0; i < size; i++) {
            // Update progress bar
            value = (i * 1.0 / (size-1)) * 100;
            if (value - currentProgress > 0) {
                currentProgress = value;
                cb(currentProgress, "");
            }

            if (currentParentNode->opacity[i] == 0) {
                currentApplyNode->opacity[i] = 0;
                currentOtherNode->opacity[i] = 0;
                continue;

            } else {
                currentApplyNode->opacity[i] = 0;
                currentOtherNode->opacity[i] = 1;

                //Check if voxel inside bounding cube
                int z = i / (sliceCols*sliceRows);

                if (z > boundingCube.minZ && z < boundingCube.maxZ) {
                    int y = (i % (sliceCols*sliceRows)) / sliceCols;

                    if (y > boundingCube.minY && y < boundingCube.maxY) {
                        int x = (i %(sliceCols*sliceRows)) % sliceCols;

                        if (x > boundingCube.minX && x < boundingCube.maxX) {
                            currentApplyNode->opacity[i] = 1;
                            currentOtherNode->opacity[i] = 0;
                        } // end if x in CubeRange
                    } // end if y in CubeRange
                } // end if z in CubeRange
            } // end else currentParentNode->opacity == 0
        } // end foreach rawIntensity

        progressBar->hide();

        break; // end case LimitedShape::Cube
    case MeshDocument::LimitedShape::Sphere:
        progressBar->show();

        float centerX = boundingSphere.centerX;
        float centerY = boundingSphere.centerY;
        float centerZ = boundingSphere.centerZ;

        float radius = boundingSphere.radius;

        for(int i = 0; i < size; i++) {
            // Update progress bar
            value = (i * 1.0 / (size-1)) * 100;
            if (value - currentProgress > 0) {
                currentProgress = value;
                cb(currentProgress, "");
            }

            if (currentParentNode->opacity[i] == 0) {
                currentApplyNode->opacity[i] = 0;
                currentOtherNode->opacity[i] = 0;
                continue;
            }

            //Check if voxel inside Sphere Limited
            int z = i / (sliceCols*sliceRows);
            int y = (i % (sliceCols*sliceRows)) / sliceCols;
            int x = (i %(sliceCols*sliceRows)) % sliceCols;

            float modelViewX = x * sliceXSpace;
            float modelViewY = y * sliceYSpace;
            float modelViewZ = z * sliceZSpace;

            float distanceFromCenter = sqrt((modelViewX - centerX)*(modelViewX - centerX) +
                                            (modelViewY - centerY)*(modelViewY - centerY) +
                                            (modelViewZ - centerZ)*(modelViewZ - centerZ));

            if (distanceFromCenter < radius) {
                currentApplyNode->opacity[i] = 1;
                currentOtherNode->opacity[i] = 0;
            } else {
                currentApplyNode->opacity[i] = 0;
                currentOtherNode->opacity[i] = 1;
            }// end if else < radius
        } // end for()

        progressBar->hide();

        break; // end case LimitedShape::Sphere
    } // end switch

    return true;
}

void SpaceSelectionWidget::setTFOptions()
{
    TransfuncOptions tfOptions;
    NodeViewOptions viewOption;
    tfOptions.type = TransfuncTypes::NONE;
    viewOption.opacity = currentApplyNode->opacity;
    viewOption.colorTable = currentApplyNode->colorTable;
    currentApplyNode->transfuncOptions = tfOptions;
    currentApplyNode->viewOptions = viewOption;
    currentApplyNode->transfunc = tfSpaceSelection;
    currentApplyNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);

    TransfuncOptions itfOptions;
    itfOptions.type = TransfuncTypes::SPACE_SELECTION;
    NodeViewOptions viewOptionInverted;
    viewOptionInverted.opacity = currentOtherNode->opacity;
    viewOptionInverted.colorTable = currentOtherNode->colorTable;
    currentOtherNode->transfunc = tfSpaceSelectionInverted;
    currentOtherNode->transfuncOptions = itfOptions;
    currentOtherNode->viewOptions = viewOptionInverted;
    currentOtherNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);

    currentParentNode->transfuncOptions.type = TransfuncTypes::SPACE_SELECTION;
    currentParentNode->transfuncOptions.isAppliedTF = true;
    currentParentNode->transfuncOptions.boundingCubeFront = ui->cubeFrontSlider->value();
    currentParentNode->transfuncOptions.boundingCubeBack = ui->cubeBackSlider->value();
    currentParentNode->transfuncOptions.boundingCubeLeft= ui->cubeLeftSlider->value();
    currentParentNode->transfuncOptions.boundingCubeRight = ui->cubeRightSlider->value();
    currentParentNode->transfuncOptions.boundingCubeTop = ui->cubeTopSlider->value();
    currentParentNode->transfuncOptions.boundingCubeBottom = ui->cubeBottomSlider->value();
    currentParentNode->transfuncOptions.boundingCubeColor =  md->limitedCubeColor;
    currentParentNode->transfuncOptions.boundingSphereX = ui->sphereCenterXSlider->value();
    currentParentNode->transfuncOptions.boundingSphereY = ui->sphereCenterYSlider->value();
    currentParentNode->transfuncOptions.boundingSphereZ = ui->sphereCenterZSlider->value();
    currentParentNode->transfuncOptions.boundingSphereRadius = ui->sphereRadiusSlider->value();
    currentParentNode->transfuncOptions.boundingSphereColor = md->limitedSphereColor;
}

void SpaceSelectionWidget::startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *organ, TreeItem *other)
{
    if (type == TransfuncTypes::SPACE_SELECTION) {

        if (this->md != NULL) {
            currentApplyNode = organ;
            currentOtherNode = other;
            currentParentNode = parentNode;

            int sliceCols = this->md->measurementPixel.X();
            int sliceRows = this->md->measurementPixel.Y();
            int numOfSlice = this->md->measurementPixel.Z();
            float xSpace = this->md->measurementMillimeter.X() / ((float)sliceCols);
            float ySpace = this->md->measurementMillimeter.Y() / ((float)sliceRows);
            float zSpace = this->md->measurementMillimeter.Z() / ((float)(numOfSlice-1));

            this->setSliceMetaData(sliceRows, sliceCols, numOfSlice);
            this->setCoefficients(xSpace, ySpace, zSpace);
            this->initializeBoundingCube();
            this->initializeBoundingSphere();
            this->md->initLimitedShapeShader();
            ui->cubeColorButton->setStyleSheet("background-color: rgba(" +
                                               QString::number(md->limitedCubeColor.red())+ "," +
                                               QString::number(md->limitedCubeColor.green()) + "," +
                                               QString::number(md->limitedCubeColor.blue()) + "," + "255" + ")");
            ui->sphereColorButton->setStyleSheet("background-color: rgba(" +
                                               QString::number(md->limitedSphereColor.red())+ "," +
                                               QString::number(md->limitedSphereColor.green()) + "," +
                                               QString::number(md->limitedSphereColor.blue()) + "," + "255" + ")");

            ui->applyTFButton->setVisible(true);

            // Detect limited shape
            switch (ui->boundRegionTabs->currentIndex()) {
            case 0:
                this->md->currentLimitedShape = MeshDocument::LimitedShape::Cube;
                break;
            case 1:
                this->md->currentLimitedShape = MeshDocument::LimitedShape::Sphere;
                break;
            default:
                break;
            }

            this->gla->updatePaintAreaForcefully();
        }
    }
}

void SpaceSelectionWidget::selectedNodeChanged(TreeItem *selectedItem)
{
    currentSelectedNode = selectedItem;
    if (selectedItem == NULL) return;
    TransfuncOptions options = currentSelectedNode->transfuncOptions;
    this->md->currentLimitedShape = MeshDocument::LimitedShape::None;
    switch (options.type) {
        case TransfuncTypes::SPACE_SELECTION:{
        if (options.isAppliedTF) {
            ui->cubeFrontSlider->setValue(options.boundingCubeFront);
            ui->cubeBackSlider->setValue(options.boundingCubeBack);
            ui->cubeLeftSlider->setValue(options.boundingCubeLeft);
            ui->cubeRightSlider->setValue(options.boundingCubeRight);
            ui->cubeTopSlider->setValue(options.boundingCubeTop);
            ui->cubeBottomSlider->setValue(options.boundingCubeBottom);

            ui->sphereCenterXSlider->setValue(options.boundingSphereX);
            ui->sphereCenterYSlider->setValue(options.boundingSphereY);
            ui->sphereCenterZSlider->setValue(options.boundingSphereZ);
            ui->sphereRadiusSlider->setValue(options.boundingSphereRadius);

            md->limitedCubeColor = options.boundingCubeColor;
            md->limitedSphereColor = options.boundingSphereColor;

            ui->cubeColorButton->setStyleSheet("background-color: rgba(" +
                                               QString::number(md->limitedCubeColor.red())+ "," +
                                               QString::number(md->limitedCubeColor.green()) + "," +
                                               QString::number(md->limitedCubeColor.blue()) + "," + "255" + ")");
            ui->sphereColorButton->setStyleSheet("background-color: rgba(" +
                                               QString::number(md->limitedSphereColor.red())+ "," +
                                               QString::number(md->limitedSphereColor.green()) + "," +
                                               QString::number(md->limitedSphereColor.blue()) + "," + "255" + ")");

            // Detect limited shape
            switch (ui->boundRegionTabs->currentIndex()) {
            case 0:
                this->md->currentLimitedShape = MeshDocument::LimitedShape::Cube;
                break;
            case 1:
                this->md->currentLimitedShape = MeshDocument::LimitedShape::Sphere;
                break;
            default:
                this->md->currentLimitedShape = MeshDocument::LimitedShape::None;
                break;
            }

        }
        break;
    }
    }
    gla->updatePaintAreaForcefully();
}

void SpaceSelectionWidget::changeBoundingCubeParams()
{
    vcg::Point3f p0(ui->cubeLeftSlider->value(), ui->cubeFrontSlider->value(), ui->cubeBottomSlider->value());
    vcg::Point3f p1(ui->cubeRightSlider->value(), ui->cubeFrontSlider->value(), ui->cubeBottomSlider->value());
    vcg::Point3f p2(ui->cubeRightSlider->value(), ui->cubeBackSlider->value(), ui->cubeBottomSlider->value());
    vcg::Point3f p3(ui->cubeLeftSlider->value(), ui->cubeBackSlider->value(), ui->cubeBottomSlider->value());
    vcg::Point3f p4(ui->cubeLeftSlider->value(), ui->cubeFrontSlider->value(), ui->cubeTopSlider->value());
    vcg::Point3f p5(ui->cubeRightSlider->value(), ui->cubeFrontSlider->value(), ui->cubeTopSlider->value());
    vcg::Point3f p6(ui->cubeRightSlider->value(), ui->cubeBackSlider->value(), ui->cubeTopSlider->value());
    vcg::Point3f p7(ui->cubeLeftSlider->value(), ui->cubeBackSlider->value(), ui->cubeTopSlider->value());

    this->md->updateLimitedPoints(p0, p1, p2, p3, p4, p5, p6, p7);
    this->gla->updatePaintAreaForcefully();
}

void SpaceSelectionWidget::changeBoundingSphereParams()
{
    vcg::Point3f center(ui->sphereCenterXSlider->value(),
                        ui->sphereCenterYSlider->value(),
                        ui->sphereCenterZSlider->value());
    float radius = ui->sphereRadiusSlider->value();

    this->md->updateLimitedSphereParams(center, radius);
    this->gla->updatePaintAreaForcefully();
}



void SpaceSelectionWidget::on_cubeColorButton_clicked()
{
    QColor color = QColorDialog::getColor();
    this->md->limitedCubeColor = color;
    ui->cubeColorButton->setStyleSheet("background-color: rgba(" +
                                       QString::number(md->limitedCubeColor.red())+ "," +
                                       QString::number(md->limitedCubeColor.green()) + "," +
                                       QString::number(md->limitedCubeColor.blue()) + "," + "255" + ")");
    gla->updatePaintAreaForcefully();
}

void SpaceSelectionWidget::on_sphereColorButton_clicked()
{
    QColor color = QColorDialog::getColor();
    this->md->limitedSphereColor = color;
    ui->sphereColorButton->setStyleSheet("background-color: rgba(" +
                                       QString::number(md->limitedSphereColor.red())+ "," +
                                       QString::number(md->limitedSphereColor.green()) + "," +
                                       QString::number(md->limitedSphereColor.blue()) + "," + "255" + ")");
    gla->updatePaintAreaForcefully();
}

void SpaceSelectionWidget::on_boundRegionTabs_currentChanged(int index)
{
    if (index == 0) {
        this->md->currentLimitedShape = MeshDocument::LimitedShape::Cube;
    } else if (index == 1) {
        this->md->currentLimitedShape = MeshDocument::LimitedShape::Sphere;
    } else if (index == 2) {
        this->md->currentLimitedShape = MeshDocument::LimitedShape::Organ;
    } else {
        this->md->currentLimitedShape = MeshDocument::LimitedShape::Cube;
    }

    this->gla->updatePaintAreaForcefully();
}

void SpaceSelectionWidget::on_applyTFButton_clicked()
{
    BoundingCube boundingCube = buildBoundingCube();
    BoundingSphere boundingSphere = buildBoundingSphere();

    mapping(boundingCube, boundingSphere, QCallBack);

    setTFOptions();

    ui->applyTFButton->hide();
    md->currentLimitedShape = MeshDocument::LimitedShape::None;
    gla->updatePaintAreaForcefully();
}

vcg::Color4f tfSpaceSelection(signed short intensityArray[], int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (options.opacity[index] != 0) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

vcg::Color4f tfSpaceSelectionInverted(signed short *intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (options.opacity[index] != 0) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}
