#ifndef HISTOGRAMWIDGET_H
#define HISTOGRAMWIDGET_H

#include <QWidget>

#include "../common/interfaces.h"
#include <QGLWidget>
class ColorPoint {
public:
    QPoint *location = NULL;
    vcg::Color4f color;
    int intensity;
    inline bool operator ==(ColorPoint p) {
        return p.intensity == this->intensity;
    }
    inline bool operator >(ColorPoint p) {
        return p.intensity > this->intensity;
    }
    inline bool operator >=(ColorPoint p) {
        return p.intensity >= this->intensity;
    }
    inline bool operator <(ColorPoint p) {
        return p.intensity < this->intensity;
    }
    inline bool operator <=(ColorPoint p) {
        return p.intensity <= this->intensity;
    }
    inline bool operator !=(ColorPoint p) {
        return p.intensity != this->intensity;
    }
};

class HistogramWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit HistogramWidget(QWidget *parent = 0);
    ~HistogramWidget();

    void resizeGL();
    void initializeGL();
    void paintGL();
    void setColorDict(QMap<float, vcg::Color4f>* color);
    float grayMax = 1024;
    float grayMin = -1023;
    QMap<int, int> *histogram = NULL;
    int max = 100;
    int leftBound = grayMin;
    int rightBound = grayMax;
    QMap<float, vcg::Color4f> *colorDict = NULL;
//    QList<QPoint*> currentPinPoints; // TODO delete qpoint pointer
    QMap<int, QPoint*> sortMap;
    QPoint lasthit;
    ColorPoint *activePoint; // Point right beneath the cursor
    QList<ColorPoint*> currentColorPoints;
    void updateColorTableToPoints();
    void setHistogram(QMap<int, int> *histogram);
    void clearHistogram();
    bool isDrawColor = true;
    void setIsDrawColor(bool isDraw) {this->isDrawColor = isDraw; update();}
    QList<QMap<int, int>> externalHistogramList;
    QList<vcg::Color4f> externalColorList;
    void addExternalHistogram(QMap<int, int> histo);
    void resetExternalHistogramList();
    vcg::Color4f histogramColor;
    QColor backgroundColor;

public slots:
    void updatePinPoints();
    void removeColorPoint(ColorPoint *colorPoint);

signals:
    void selectedPointChange(ColorPoint* colorPoint);
    void colorDictChanged(QMap<float, vcg::Color4f>* colorDict);
    void colorTableChanged();
private:
    void drawCirle(QPointF center, vcg::Color4f color);
    void drawNumber(QPointF center, QString number);
    float alphaOffset = 10;
    float colorPointRadius = 5;
    float colorSelectedPointRadius = 7;
    void drawCircelScreen(QList<ColorPoint*> &points);
    void drawCircelScreen(ColorPoint* points);
    void drawLinesScreen(QList<ColorPoint*> &points);
    void drawSelectedPointInfor();
    bool isDrag = false;
    QPoint screenToWorld(int x, int y);
    QPoint worldToScreen(int x, int y);
protected:
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    bool isPointing = false;
};

bool isInCircle(QPoint center, int radus, QPoint point);

#endif // HISTOGRAMWIDGET_H
