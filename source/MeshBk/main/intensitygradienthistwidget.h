#ifndef INTENSITYGRADIENTHISTWIDGET_H
#define INTENSITYGRADIENTHISTWIDGET_H

#include <QWidget>
#include <QProgressBar>
#include <opencv2/opencv.hpp>
#include "histogramcolorview.h"
#include "../common/meshmodel.h"
#include "glarea.h"
#include <treemanagerwidget.h>
using namespace vcg;

namespace Ui {
class IntensityGradientHistWidget;
}

struct RegionRange{
    float minIntensity;
    float maxIntensity;
    float minGradient;
    float maxGradient;
    QColor color;
};

class IntensityGradientHistWidget : public QWidget
{
    Q_OBJECT

public:
    explicit IntensityGradientHistWidget(QWidget *parent = 0);
    ~IntensityGradientHistWidget();

    static bool QCallBack(const int pos, const char* str);
    void initializeWidgets();

public:
    static QProgressBar* progressBar;

    int sliceRows = 0;
    int sliceCols = 0;
    int numOfSlice = 0;
    float sliceXSpace = 0.0f;
    float sliceYSpace = 0.0f;
    float sliceZSpace = 0.0f;

    signed short* inputRawIntensity = NULL;
    uchar* inputRgbaData = NULL;

//    uchar* optionFilterRgbaData = NULL;

    float* gradientManigtude = NULL;
    uchar* filteredRegionRgbaData = NULL;

    int intensityAxisSize = 2048; // Hardcode: range [-1024 1023], Ox of 2D Histogram
    int gradManigtudeAxisSize = 1000; // Oy of 2D Histogram

    double maxGradientManigtude = 0.0;

    MeshDocument* md;
    GLArea* gla;

public:
    TreeItem *currentApplyNode = NULL;
    TreeItem *currentOtherNode = NULL;
    TreeItem *currentParentNode = NULL;

public:
    inline void setInputRawIntensity(signed short* raw);
    inline void setSliceMetaData(int rows, int cols, int num) {
        this->sliceRows = rows;
        this->sliceCols = cols;
        this->numOfSlice = num;
    }
    inline void setCoefficients(float xSpace, float ySpace, float zSpace){
        this->sliceXSpace = xSpace;
        this->sliceYSpace = ySpace;
        this->sliceZSpace = zSpace;
    }
    inline void setIntensitySize(int size) {this->intensityAxisSize = size;}
    inline void setGradientManigSize(int size) {this->gradManigtudeAxisSize = size;}
    void setMeshDoc(MeshDocument* meshDoc);
    void setGLA(GLArea* glarea);

public:
    int** buildHistogram(CallBackPos *cb);
    void rebuildHistogram(CallBackPos *cb);
    void updateHistogramView(int** histogram2D);
    void applyTransferFunction();

public:
    QList<RegionRange> buildRegionRangeList(QList<HistogramRegionRect> regionRectList);
    bool mapRegionRange(QList<RegionRange> regionRangeList, CallBackPos *cb);
    void updateTFOutputAndOption();

private slots:

    void on_applyTFButton_clicked();

public slots:
    void startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *organ, TreeItem *other);

private:
    Ui::IntensityGradientHistWidget *ui;
signals:
    void endTransferFunc();
    void histogramUpdated(cv::Mat mat);
};

#endif // INTENSITYGRADIENTHISTWIDGET_H

vcg::Color4f tfhistogram2D(signed short intensityArray[], int index, NodeViewOptions options);
vcg::Color4f tfhistogram2DInverted(signed short* intensityArray, int index, NodeViewOptions options);
