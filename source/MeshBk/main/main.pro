include (../general.pri)
DESTDIR = ../distrib
EXIF_DIR = ../external/jhead-2.95
MAIN_VCGDIR = ../vcglib
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x000000
QT += opengl
QMAKE_CXXFLAGS += -std=c++11 #T

INCLUDEPATH *= . \
            .. \
            ../.. \
            ../main \
            ../meshbkplugins \
            $$MAIN_VCGDIR \
            $$GLEWDIR/include \
            $$EXIF_DIR

INCLUDEPATH += ../external
LIBS += -L../external/lib/opencv -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc #T

LIBS += -L../external/lib/imebra -limebra #T
LIBS += -L../distrib/plugins -lutils_opendicomsfolder

DEPENDPATH += $$VCGDIR \
    $$MAIN_VCGDIR/vcg \
    $$MAIN_VCGDIR/wrap

SOURCES += main.cpp\
            mainwindow.cpp \
            glarea.cpp \
            multiViewer_Container.cpp \
            $$MAIN_VCGDIR/wrap/gui/trackball.cpp \
            $$MAIN_VCGDIR/wrap/gui/trackmode.cpp \
            glarea_setting.cpp \
            filterthread.cpp \
            ../common/interfaces.cpp \
            ../common/filterparameter.cpp \
            samplefilter.cpp \
#            edit_paint_factory.cpp \
#            paintbox.cpp \
#            edit_paint.cpp \
#            layerDialog.cpp \
            rendermodeactions.cpp \
            sliceview.cpp \
            importdicomdialog.cpp \
    volumepropertywidget.cpp \
    colorgraph.cpp \
    histogramwidget.cpp \
    tfcombodelegate.cpp \
#    treeitem.cpp \
    treemanagerwidget.cpp \
    treemodel.cpp \
    predictneuralnetwork.cpp \
    sliceviewwidget.cpp \
    histogramcolorwidget.cpp \
    histogramcolorview.cpp \
    intensitygradienthistwidget.cpp \
    spaceselectionwidget.cpp \
    opacityfromfilewidget.cpp \
    detectboundary.cpp \
    opacityfrommaskwidget.cpp

HEADERS  += ../common/interfaces.h \
            mainwindow.h \
            glarea.h \
            multiViewer_Container.h \
            glarea_setting.h \
            $$MAIN_VCGDIR/wrap/gui/trackball.h \
            $$MAIN_VCGDIR/wrap/gui/trackmode.h \
            $$MAIN_VCGDIR/wrap/gl/trimesh.h \
            filterthread.h \
            ../common/filterparameter.h \
            samplefilter.h \
#            edit_paint_factory.h \
#            cloneview.h \
#            colorframe.h \
#            paintbox.h \
#            edit_paint.h \
#            layerDialog.h \
            rendermodeactions.h \
            sliceview.h \
            importdicomdialog.h \
    volumepropertywidget.h \
    colorgraph.h \
    histogramwidget.h \
    tfcombodelegate.h \
#    treeitem.h \
    treemanagerwidget.h \
    treemodel.h \
    predictneuralnetwork.h \
    sliceviewwidget.h \
    histogramcolorwidget.h \
    histogramcolorview.h \
    intensitygradienthistwidget.h \
    spaceselectionwidget.h \
    opacityfromfilewidget.h \
    detectboundary.h \
    opacityfrommaskwidget.h

FORMS    += mainwindow.ui \
#            paintbox.ui \
#            layerDialog.ui \
    importdicomdialog.ui \
    volumepropertywidget.ui \
    histogramwidget.ui \
    treemanagerwidget.ui \
    sliceviewwidget.ui \
    histogramcolorwidget.ui \
    intensitygradienthistwidget.ui \
    spaceselectionwidget.ui \
    opacityfromfilewidget.ui \
    opacityfrommaskwidget.ui


        win32-msvc2005: RCC_DIR = $(ConfigurationName)
        win32-msvc2008: RCC_DIR = $(ConfigurationName)
        #win32-msvc2010: RCC_DIR = $(ConfigurationName)
        #win32-msvc2012: RCC_DIR = $(ConfigurationName)


RESOURCES = meshlab.qrc \
#            edit_paint.qrc

# to add windows icon
win32:RC_FILE = meshlab.rc

# ## the xml info list
# ## the next time the app open a new extension
QMAKE_INFO_PLIST = ../install/info.plist

# to add MacOS icon
ICON = images/meshlab.icns

# note that to add the file icons on the mac the following line does not work.
# You have to copy the file by hand into the meshlab.app/Contents/Resources directory.
# ICON += images/meshlab_obj.icns
QT += opengl
QT += xml
QT += xmlpatterns
QT += network
QT += script


# the following line is needed to avoid mismatch between
# the awful min/max macros of windows and the limits max
win32:DEFINES += NOMINMAX

# the following line is to hide the hundred of warnings about the deprecated
# old printf are all around the code
win32-msvc2005:DEFINES += _CRT_SECURE_NO_DEPRECATE
win32-msvc2008:DEFINES += _CRT_SECURE_NO_DEPRECATE
win32-msvc2010:DEFINES += _CRT_SECURE_NO_DEPRECATE
win32-msvc2012:DEFINES += _CRT_SECURE_NO_DEPRECATE

# Uncomment these if you want to experiment with newer gcc compilers
# (here using the one provided with macports)
# macx-g++:QMAKE_CXX=g++-mp-4.3
# macx-g++:QMAKE_CXXFLAGS_RELEASE -= -Os
# macx-g++:QMAKE_CXXFLAGS_RELEASE += -O3

CONFIG += stl

macx:LIBS		+= -L../external/lib/macx -ljhead ../common/libcommon.dylib
macx32:LIBS		+= -L../external/lib/macx32 -ljhead ../common/libcommon.dylib
macx64:LIBS		+= -L../external/lib/macx64 -ljhead ../common/libcommon.dylib
macx:QMAKE_POST_LINK ="cp -P ../common/libcommon.1.dylib ../distrib/meshlab.app/Contents/MacOS; install_name_tool -change libcommon.1.dylib @executable_path/libcommon.1.dylib ../distrib/meshlab.app/Contents/MacOS/meshlab"

win32-msvc2005:LIBS		+= -L../external/lib/win32-msvc2005 -ljhead -L../distrib -lcommon
win32-msvc2008:LIBS		+= -L../external/lib/win32-msvc2008 -ljhead -L../distrib -lcommon
win32-msvc2010:LIBS		+= -L../external/lib/win32-msvc2010 -ljhead -L../distrib -lcommon
win32-msvc2012:LIBS		+= -L../external/lib/win32-msvc2012 -ljhead -L../distrib -lcommon
win32-g++:LIBS        	+= -L../external/lib/win32-gcc -ljhead -L../distrib -lcommon

#CONFIG(release,debug | release) {
#	win32-msvc2005:release:LIBS     += -L../common/release -lcommon
#	win32-msvc2008:release:LIBS     += -L../common/release -lcommon
#	win32-g++:release:LIBS 			+= -L../common/release -lcommon
#}

linux-g++:LIBS += -L../external/lib/linux-g++ -ljhead -L../distrib -lcommon -lGLU
linux-g++:QMAKE_RPATHDIR += ../distrib
linux-g++-32:LIBS += -L../external/lib/linux-g++-32 -ljhead -L../distrib -lcommon -lGLU
linux-g++-32:QMAKE_RPATHDIR += ../distrib
linux-g++-64:LIBS += -L../external/lib/linux-g++-64 -ljhead -L../distrib -lcommon -lGLU
linux-g++-64:QMAKE_RPATHDIR += ../distrib

# uncomment in your local copy only in emergency cases.
# We should never be too permissive
# win32-g++:QMAKE_CXXFLAGS += -fpermissive

# The following define is needed in gcc to remove the asserts
win32-g++:DEFINES += NDEBUG
CONFIG(debug, debug|release):win32-g++:release:DEFINES -= NDEBUG

OTHER_FILES += \
    brain.ply
