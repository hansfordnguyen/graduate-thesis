/********************************************************************************
** Form generated from reading UI file 'histogramcolorwidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HISTOGRAMCOLORWIDGET_H
#define UI_HISTOGRAMCOLORWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "colorgraph.h"
#include "histogramwidget.h"

QT_BEGIN_NAMESPACE

class Ui_HistogramColorWidget
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *refreshHistoButton;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *loadHistogram;
    QPushButton *resetHistogramButton;
    QVBoxLayout *verticalLayout_8;
    QSlider *leftBoundSlider;
    QSlider *rightBoundSlider;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    HistogramWidget *histogramWidget;
    ColorGraph *colorGraphWidget;
    QSlider *backgroundSlider;
    QHBoxLayout *buttongroup;
    QPushButton *removePinPointButton;
    QSpacerItem *horizontalSpacer;
    QComboBox *presetCombobox;
    QCheckBox *isUseColoorCheckbox;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *HistogramColorWidget)
    {
        if (HistogramColorWidget->objectName().isEmpty())
            HistogramColorWidget->setObjectName(QString::fromUtf8("HistogramColorWidget"));
        HistogramColorWidget->resize(423, 300);
        verticalLayout = new QVBoxLayout(HistogramColorWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        refreshHistoButton = new QPushButton(HistogramColorWidget);
        refreshHistoButton->setObjectName(QString::fromUtf8("refreshHistoButton"));

        horizontalLayout->addWidget(refreshHistoButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        loadHistogram = new QPushButton(HistogramColorWidget);
        loadHistogram->setObjectName(QString::fromUtf8("loadHistogram"));

        horizontalLayout->addWidget(loadHistogram);

        resetHistogramButton = new QPushButton(HistogramColorWidget);
        resetHistogramButton->setObjectName(QString::fromUtf8("resetHistogramButton"));

        horizontalLayout->addWidget(resetHistogramButton);


        verticalLayout->addLayout(horizontalLayout);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        leftBoundSlider = new QSlider(HistogramColorWidget);
        leftBoundSlider->setObjectName(QString::fromUtf8("leftBoundSlider"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(leftBoundSlider->sizePolicy().hasHeightForWidth());
        leftBoundSlider->setSizePolicy(sizePolicy);
        leftBoundSlider->setMinimumSize(QSize(395, 0));
        leftBoundSlider->setMaximum(1000);
        leftBoundSlider->setOrientation(Qt::Horizontal);

        verticalLayout_8->addWidget(leftBoundSlider);

        rightBoundSlider = new QSlider(HistogramColorWidget);
        rightBoundSlider->setObjectName(QString::fromUtf8("rightBoundSlider"));
        sizePolicy.setHeightForWidth(rightBoundSlider->sizePolicy().hasHeightForWidth());
        rightBoundSlider->setSizePolicy(sizePolicy);
        rightBoundSlider->setMinimumSize(QSize(395, 0));
        rightBoundSlider->setMaximum(1000);
        rightBoundSlider->setValue(1000);
        rightBoundSlider->setOrientation(Qt::Horizontal);

        verticalLayout_8->addWidget(rightBoundSlider);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        histogramWidget = new HistogramWidget(HistogramColorWidget);
        histogramWidget->setObjectName(QString::fromUtf8("histogramWidget"));
        sizePolicy.setHeightForWidth(histogramWidget->sizePolicy().hasHeightForWidth());
        histogramWidget->setSizePolicy(sizePolicy);
        histogramWidget->setMinimumSize(QSize(395, 150));

        verticalLayout_2->addWidget(histogramWidget);

        colorGraphWidget = new ColorGraph(HistogramColorWidget);
        colorGraphWidget->setObjectName(QString::fromUtf8("colorGraphWidget"));
        sizePolicy.setHeightForWidth(colorGraphWidget->sizePolicy().hasHeightForWidth());
        colorGraphWidget->setSizePolicy(sizePolicy);
        colorGraphWidget->setMinimumSize(QSize(395, 23));

        verticalLayout_2->addWidget(colorGraphWidget);


        horizontalLayout_2->addLayout(verticalLayout_2);

        backgroundSlider = new QSlider(HistogramColorWidget);
        backgroundSlider->setObjectName(QString::fromUtf8("backgroundSlider"));
        backgroundSlider->setMaximumSize(QSize(15, 16777215));
        backgroundSlider->setStyleSheet(QString::fromUtf8("background-color: black;\n"
""));
        backgroundSlider->setMaximum(100);
        backgroundSlider->setOrientation(Qt::Vertical);

        horizontalLayout_2->addWidget(backgroundSlider);


        verticalLayout_8->addLayout(horizontalLayout_2);


        verticalLayout->addLayout(verticalLayout_8);

        buttongroup = new QHBoxLayout();
        buttongroup->setObjectName(QString::fromUtf8("buttongroup"));
        removePinPointButton = new QPushButton(HistogramColorWidget);
        removePinPointButton->setObjectName(QString::fromUtf8("removePinPointButton"));

        buttongroup->addWidget(removePinPointButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        buttongroup->addItem(horizontalSpacer);

        presetCombobox = new QComboBox(HistogramColorWidget);
        presetCombobox->setObjectName(QString::fromUtf8("presetCombobox"));

        buttongroup->addWidget(presetCombobox);

        isUseColoorCheckbox = new QCheckBox(HistogramColorWidget);
        isUseColoorCheckbox->setObjectName(QString::fromUtf8("isUseColoorCheckbox"));
        isUseColoorCheckbox->setChecked(false);

        buttongroup->addWidget(isUseColoorCheckbox);


        verticalLayout->addLayout(buttongroup);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(HistogramColorWidget);

        QMetaObject::connectSlotsByName(HistogramColorWidget);
    } // setupUi

    void retranslateUi(QWidget *HistogramColorWidget)
    {
        HistogramColorWidget->setWindowTitle(QApplication::translate("HistogramColorWidget", "Form", 0, QApplication::UnicodeUTF8));
        refreshHistoButton->setText(QApplication::translate("HistogramColorWidget", "Refesh", 0, QApplication::UnicodeUTF8));
        loadHistogram->setText(QApplication::translate("HistogramColorWidget", "Load", 0, QApplication::UnicodeUTF8));
        resetHistogramButton->setText(QApplication::translate("HistogramColorWidget", "Reset", 0, QApplication::UnicodeUTF8));
        removePinPointButton->setText(QApplication::translate("HistogramColorWidget", "Remove", 0, QApplication::UnicodeUTF8));
        isUseColoorCheckbox->setText(QApplication::translate("HistogramColorWidget", "Use Color", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class HistogramColorWidget: public Ui_HistogramColorWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HISTOGRAMCOLORWIDGET_H
