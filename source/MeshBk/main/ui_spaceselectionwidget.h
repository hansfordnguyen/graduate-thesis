/********************************************************************************
** Form generated from reading UI file 'spaceselectionwidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPACESELECTIONWIDGET_H
#define UI_SPACESELECTIONWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SpaceSelectionWidget
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *boundRegionTabs;
    QWidget *boundCubeTab;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_9;
    QLabel *cubeFrontLabel;
    QSlider *cubeFrontSlider;
    QHBoxLayout *horizontalLayout_4;
    QLabel *cubeBackLabel;
    QSlider *cubeBackSlider;
    QHBoxLayout *horizontalLayout_5;
    QLabel *cubeLeftLabel;
    QSlider *cubeLeftSlider;
    QHBoxLayout *horizontalLayout_6;
    QLabel *cubeRightLabel;
    QSlider *cubeRightSlider;
    QHBoxLayout *horizontalLayout_7;
    QLabel *cubeTopLabel;
    QSlider *cubeTopSlider;
    QHBoxLayout *horizontalLayout_8;
    QLabel *cubeBottomLabel;
    QSlider *cubeBottomSlider;
    QHBoxLayout *horizontalLayout_15;
    QLabel *cubeColorLabel;
    QPushButton *cubeColorButton;
    QWidget *boundSphereTab;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_10;
    QLabel *sphereCenterXLabel;
    QSlider *sphereCenterXSlider;
    QHBoxLayout *horizontalLayout_11;
    QLabel *sphereCenterYLabel;
    QSlider *sphereCenterYSlider;
    QHBoxLayout *horizontalLayout_12;
    QLabel *sphereCenterZLabel;
    QSlider *sphereCenterZSlider;
    QHBoxLayout *horizontalLayout_13;
    QLabel *sphereRadiusLabel;
    QSlider *sphereRadiusSlider;
    QHBoxLayout *horizontalLayout_16;
    QLabel *sphereColorLabel;
    QPushButton *sphereColorButton;
    QWidget *boundOrganTab;
    QSpacerItem *verticalSpacer;
    QPushButton *applyTFButton;
    QProgressBar *progressBar;

    void setupUi(QWidget *SpaceSelectionWidget)
    {
        if (SpaceSelectionWidget->objectName().isEmpty())
            SpaceSelectionWidget->setObjectName(QString::fromUtf8("SpaceSelectionWidget"));
        SpaceSelectionWidget->resize(400, 560);
        verticalLayout = new QVBoxLayout(SpaceSelectionWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        boundRegionTabs = new QTabWidget(SpaceSelectionWidget);
        boundRegionTabs->setObjectName(QString::fromUtf8("boundRegionTabs"));
        boundCubeTab = new QWidget();
        boundCubeTab->setObjectName(QString::fromUtf8("boundCubeTab"));
        verticalLayout_2 = new QVBoxLayout(boundCubeTab);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        cubeFrontLabel = new QLabel(boundCubeTab);
        cubeFrontLabel->setObjectName(QString::fromUtf8("cubeFrontLabel"));

        horizontalLayout_9->addWidget(cubeFrontLabel);

        cubeFrontSlider = new QSlider(boundCubeTab);
        cubeFrontSlider->setObjectName(QString::fromUtf8("cubeFrontSlider"));
        cubeFrontSlider->setEnabled(false);
        cubeFrontSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_9->addWidget(cubeFrontSlider);


        verticalLayout_2->addLayout(horizontalLayout_9);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        cubeBackLabel = new QLabel(boundCubeTab);
        cubeBackLabel->setObjectName(QString::fromUtf8("cubeBackLabel"));

        horizontalLayout_4->addWidget(cubeBackLabel);

        cubeBackSlider = new QSlider(boundCubeTab);
        cubeBackSlider->setObjectName(QString::fromUtf8("cubeBackSlider"));
        cubeBackSlider->setEnabled(false);
        cubeBackSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_4->addWidget(cubeBackSlider);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        cubeLeftLabel = new QLabel(boundCubeTab);
        cubeLeftLabel->setObjectName(QString::fromUtf8("cubeLeftLabel"));

        horizontalLayout_5->addWidget(cubeLeftLabel);

        cubeLeftSlider = new QSlider(boundCubeTab);
        cubeLeftSlider->setObjectName(QString::fromUtf8("cubeLeftSlider"));
        cubeLeftSlider->setEnabled(false);
        cubeLeftSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_5->addWidget(cubeLeftSlider);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        cubeRightLabel = new QLabel(boundCubeTab);
        cubeRightLabel->setObjectName(QString::fromUtf8("cubeRightLabel"));

        horizontalLayout_6->addWidget(cubeRightLabel);

        cubeRightSlider = new QSlider(boundCubeTab);
        cubeRightSlider->setObjectName(QString::fromUtf8("cubeRightSlider"));
        cubeRightSlider->setEnabled(false);
        cubeRightSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_6->addWidget(cubeRightSlider);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        cubeTopLabel = new QLabel(boundCubeTab);
        cubeTopLabel->setObjectName(QString::fromUtf8("cubeTopLabel"));

        horizontalLayout_7->addWidget(cubeTopLabel);

        cubeTopSlider = new QSlider(boundCubeTab);
        cubeTopSlider->setObjectName(QString::fromUtf8("cubeTopSlider"));
        cubeTopSlider->setEnabled(false);
        cubeTopSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_7->addWidget(cubeTopSlider);


        verticalLayout_2->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        cubeBottomLabel = new QLabel(boundCubeTab);
        cubeBottomLabel->setObjectName(QString::fromUtf8("cubeBottomLabel"));

        horizontalLayout_8->addWidget(cubeBottomLabel);

        cubeBottomSlider = new QSlider(boundCubeTab);
        cubeBottomSlider->setObjectName(QString::fromUtf8("cubeBottomSlider"));
        cubeBottomSlider->setEnabled(false);
        cubeBottomSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_8->addWidget(cubeBottomSlider);


        verticalLayout_2->addLayout(horizontalLayout_8);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        cubeColorLabel = new QLabel(boundCubeTab);
        cubeColorLabel->setObjectName(QString::fromUtf8("cubeColorLabel"));

        horizontalLayout_15->addWidget(cubeColorLabel);

        cubeColorButton = new QPushButton(boundCubeTab);
        cubeColorButton->setObjectName(QString::fromUtf8("cubeColorButton"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(cubeColorButton->sizePolicy().hasHeightForWidth());
        cubeColorButton->setSizePolicy(sizePolicy);

        horizontalLayout_15->addWidget(cubeColorButton);


        verticalLayout_2->addLayout(horizontalLayout_15);

        boundRegionTabs->addTab(boundCubeTab, QString());
        boundSphereTab = new QWidget();
        boundSphereTab->setObjectName(QString::fromUtf8("boundSphereTab"));
        verticalLayout_7 = new QVBoxLayout(boundSphereTab);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        sphereCenterXLabel = new QLabel(boundSphereTab);
        sphereCenterXLabel->setObjectName(QString::fromUtf8("sphereCenterXLabel"));

        horizontalLayout_10->addWidget(sphereCenterXLabel);

        sphereCenterXSlider = new QSlider(boundSphereTab);
        sphereCenterXSlider->setObjectName(QString::fromUtf8("sphereCenterXSlider"));
        sphereCenterXSlider->setEnabled(false);
        sphereCenterXSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_10->addWidget(sphereCenterXSlider);


        verticalLayout_5->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        sphereCenterYLabel = new QLabel(boundSphereTab);
        sphereCenterYLabel->setObjectName(QString::fromUtf8("sphereCenterYLabel"));

        horizontalLayout_11->addWidget(sphereCenterYLabel);

        sphereCenterYSlider = new QSlider(boundSphereTab);
        sphereCenterYSlider->setObjectName(QString::fromUtf8("sphereCenterYSlider"));
        sphereCenterYSlider->setEnabled(false);
        sphereCenterYSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_11->addWidget(sphereCenterYSlider);


        verticalLayout_5->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        sphereCenterZLabel = new QLabel(boundSphereTab);
        sphereCenterZLabel->setObjectName(QString::fromUtf8("sphereCenterZLabel"));

        horizontalLayout_12->addWidget(sphereCenterZLabel);

        sphereCenterZSlider = new QSlider(boundSphereTab);
        sphereCenterZSlider->setObjectName(QString::fromUtf8("sphereCenterZSlider"));
        sphereCenterZSlider->setEnabled(false);
        sphereCenterZSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_12->addWidget(sphereCenterZSlider);


        verticalLayout_5->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        sphereRadiusLabel = new QLabel(boundSphereTab);
        sphereRadiusLabel->setObjectName(QString::fromUtf8("sphereRadiusLabel"));

        horizontalLayout_13->addWidget(sphereRadiusLabel);

        sphereRadiusSlider = new QSlider(boundSphereTab);
        sphereRadiusSlider->setObjectName(QString::fromUtf8("sphereRadiusSlider"));
        sphereRadiusSlider->setEnabled(false);
        sphereRadiusSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_13->addWidget(sphereRadiusSlider);


        verticalLayout_5->addLayout(horizontalLayout_13);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        sphereColorLabel = new QLabel(boundSphereTab);
        sphereColorLabel->setObjectName(QString::fromUtf8("sphereColorLabel"));

        horizontalLayout_16->addWidget(sphereColorLabel);

        sphereColorButton = new QPushButton(boundSphereTab);
        sphereColorButton->setObjectName(QString::fromUtf8("sphereColorButton"));
        sizePolicy.setHeightForWidth(sphereColorButton->sizePolicy().hasHeightForWidth());
        sphereColorButton->setSizePolicy(sizePolicy);

        horizontalLayout_16->addWidget(sphereColorButton);


        verticalLayout_5->addLayout(horizontalLayout_16);


        verticalLayout_7->addLayout(verticalLayout_5);

        boundRegionTabs->addTab(boundSphereTab, QString());
        boundOrganTab = new QWidget();
        boundOrganTab->setObjectName(QString::fromUtf8("boundOrganTab"));
        boundRegionTabs->addTab(boundOrganTab, QString());

        verticalLayout->addWidget(boundRegionTabs);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        applyTFButton = new QPushButton(SpaceSelectionWidget);
        applyTFButton->setObjectName(QString::fromUtf8("applyTFButton"));

        verticalLayout->addWidget(applyTFButton);

        progressBar = new QProgressBar(SpaceSelectionWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);
        progressBar->setTextVisible(false);

        verticalLayout->addWidget(progressBar);


        retranslateUi(SpaceSelectionWidget);

        boundRegionTabs->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SpaceSelectionWidget);
    } // setupUi

    void retranslateUi(QWidget *SpaceSelectionWidget)
    {
        SpaceSelectionWidget->setWindowTitle(QApplication::translate("SpaceSelectionWidget", "Form", 0, QApplication::UnicodeUTF8));
        cubeFrontLabel->setText(QApplication::translate("SpaceSelectionWidget", "Front", 0, QApplication::UnicodeUTF8));
        cubeBackLabel->setText(QApplication::translate("SpaceSelectionWidget", "Back", 0, QApplication::UnicodeUTF8));
        cubeLeftLabel->setText(QApplication::translate("SpaceSelectionWidget", "Left", 0, QApplication::UnicodeUTF8));
        cubeRightLabel->setText(QApplication::translate("SpaceSelectionWidget", "Right", 0, QApplication::UnicodeUTF8));
        cubeTopLabel->setText(QApplication::translate("SpaceSelectionWidget", "Top", 0, QApplication::UnicodeUTF8));
        cubeBottomLabel->setText(QApplication::translate("SpaceSelectionWidget", "Bottom", 0, QApplication::UnicodeUTF8));
        cubeColorLabel->setText(QApplication::translate("SpaceSelectionWidget", "Color", 0, QApplication::UnicodeUTF8));
        cubeColorButton->setText(QString());
        boundRegionTabs->setTabText(boundRegionTabs->indexOf(boundCubeTab), QApplication::translate("SpaceSelectionWidget", "Cube", 0, QApplication::UnicodeUTF8));
        sphereCenterXLabel->setText(QApplication::translate("SpaceSelectionWidget", "Center X", 0, QApplication::UnicodeUTF8));
        sphereCenterYLabel->setText(QApplication::translate("SpaceSelectionWidget", "Center Y", 0, QApplication::UnicodeUTF8));
        sphereCenterZLabel->setText(QApplication::translate("SpaceSelectionWidget", "Center Z", 0, QApplication::UnicodeUTF8));
        sphereRadiusLabel->setText(QApplication::translate("SpaceSelectionWidget", "Radius", 0, QApplication::UnicodeUTF8));
        sphereColorLabel->setText(QApplication::translate("SpaceSelectionWidget", "Color", 0, QApplication::UnicodeUTF8));
        sphereColorButton->setText(QString());
        boundRegionTabs->setTabText(boundRegionTabs->indexOf(boundSphereTab), QApplication::translate("SpaceSelectionWidget", "Sphere", 0, QApplication::UnicodeUTF8));
        boundRegionTabs->setTabText(boundRegionTabs->indexOf(boundOrganTab), QApplication::translate("SpaceSelectionWidget", "Organ", 0, QApplication::UnicodeUTF8));
        applyTFButton->setText(QApplication::translate("SpaceSelectionWidget", "Apply Transfer Function", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SpaceSelectionWidget: public Ui_SpaceSelectionWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPACESELECTIONWIDGET_H
