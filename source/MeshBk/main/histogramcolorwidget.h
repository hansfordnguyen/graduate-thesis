#ifndef HISTOGRAMCOLORWIDGET_H
#define HISTOGRAMCOLORWIDGET_H

#include <QWidget>
#include <histogramwidget.h>
#include <colorgraph.h>
#include "volumepropertywidget.h"


namespace Ui {
class HistogramColorWidget;
}

class HistogramColorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit HistogramColorWidget(QWidget *parent = 0);
    ~HistogramColorWidget();
    HistogramWidget *histogramWidget = NULL;
    ColorGraph *colorGraphWidget = NULL;
    IntensityColorTable *colorTable;
    void setColorTable(IntensityColorTable *colorTable);
    ColorPoint* currentSelectedPoint = NULL;
    QMap<int, int> histogram;
    vcg::CallBackPos *QCallBack = 0;
    void setBoundingSliderVisibility(bool isShown);
    void setIsUseColorTable(bool isUsed);
    bool isUseColorTable = true;
    void resetBoundingSlider();
    QSlider *leftSlider = NULL;
    QSlider *rightSlider = NULL;
    bool isSliderChanged = false;
    QMap<QString, QMap<float, vcg::Color4f>*> loadPreset();
    QMap<QString, QMap<float, vcg::Color4f>*> presetList;

signals:
    void removeColorPoint(ColorPoint *colorPoint);
    void colorTableChanged();
    void reloadHistogram();
    void enableApplyButton();
public slots:
    void onThresholdChange(int value);
    void initHistogram();
    void selectedPointChanged(ColorPoint *colorPoint);
    void backgroundSliderChanged(int value);
    void presetChanged(QString key);
private slots:
    void on_removePinPointButton_clicked();
    void on_loadHistogram_clicked();
    QMap<int, int> startLoadFromFolder(vcg::CallBackPos *cb = 0);
    void on_resetHistogramButton_clicked();
    void on_isUseColoorCheckbox_stateChanged(int arg1);    
    void on_refreshHistoButton_clicked();

private:
    Ui::HistogramColorWidget *ui;
};

#endif // HISTOGRAMCOLORWIDGET_H
