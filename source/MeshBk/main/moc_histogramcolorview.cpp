/****************************************************************************
** Meta object code from reading C++ file 'histogramcolorview.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "histogramcolorview.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'histogramcolorview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_HistogramColorView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x05,

 // slots: signature, parameters, type, tag, flags
      69,   65,   19,   19, 0x0a,
     107,   19,   19,   19, 0x0a,
     134,  128,   19,   19, 0x0a,
     156,  128,   19,   19, 0x0a,
     179,  128,   19,   19, 0x0a,
     204,  128,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_HistogramColorView[] = {
    "HistogramColorView\0\0"
    "paintAreaUpdated(QList<HistogramRegionRect>)\0"
    "mat\0updateHistogramMatAndTexture(cv::Mat)\0"
    "updateGLForcefully()\0index\0"
    "changeRegionName(int)\0changeRegionColor(int)\0"
    "changeRegionOpacity(int)\0removeRegion(int)\0"
};

void HistogramColorView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HistogramColorView *_t = static_cast<HistogramColorView *>(_o);
        switch (_id) {
        case 0: _t->paintAreaUpdated((*reinterpret_cast< QList<HistogramRegionRect>(*)>(_a[1]))); break;
        case 1: _t->updateHistogramMatAndTexture((*reinterpret_cast< cv::Mat(*)>(_a[1]))); break;
        case 2: _t->updateGLForcefully(); break;
        case 3: _t->changeRegionName((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->changeRegionColor((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->changeRegionOpacity((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->removeRegion((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData HistogramColorView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject HistogramColorView::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_HistogramColorView,
      qt_meta_data_HistogramColorView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HistogramColorView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HistogramColorView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HistogramColorView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HistogramColorView))
        return static_cast<void*>(const_cast< HistogramColorView*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int HistogramColorView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void HistogramColorView::paintAreaUpdated(QList<HistogramRegionRect> _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
