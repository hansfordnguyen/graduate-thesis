#include "importdicomdialog.h"
#include "ui_importdicomdialog.h"
#include "predictneuralnetwork.h"

#include <QFileDialog>
#include <QSignalMapper>
#include <QTableWidget>

QProgressBar *ImportDICOMDialog::progressbar;
QLabel *ImportDICOMDialog::statusLabel;
ImportDICOMDialog::ImportDICOMDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImportDICOMDialog)
{
    this->mainWindow = qobject_cast<MainWindow*>(parentWidget());
    ui->setupUi(this);
    // Init organ table
    QTableWidget *organTable = ui->organTable;
    organTable->setColumnCount(3);
    organTable->horizontalHeader()->setResizeMode(PATHCOL, QHeaderView::Stretch);
    organTable->setColumnWidth(CHECKCOL, 20);
    organTable->setColumnWidth(NAMECOL, 100);
//    organTable->setHorizontalHeaderItem(LOADCOL, new QTableWidgetItem("Load"));
    organTable->setHorizontalHeaderItem(NAMECOL, new QTableWidgetItem("Organs"));
    organTable->setHorizontalHeaderItem(PATHCOL, new QTableWidgetItem("Path"));
    organTable->setShowGrid(true);
    organTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    organTable->setSelectionMode(QAbstractItemView::SingleSelection);
    connect(organTable, SIGNAL(cellClicked(int,int)), this, SLOT(onOrganClick(int, int)));

    // Disable ok button by default because no row is selected
    QDialogButtonBox *dialogButtons = ui->dialogButtons;
    dialogButtons->button(QDialogButtonBox::Ok)->setEnabled( false );

    // Hide and set progress bar to busy mode
    progressbar = ui->progressBar;
    progressbar->hide();
    progressbar->setMaximum(100);
    progressbar->setMinimum(0);
    progressbar->setValue(0);

    statusLabel = ui->statusLabel;

    // Register event
    disconnect(dialogButtons, SIGNAL(accepted()), this, SLOT(accept())); //Prevent closing popup after click ok (in case user load many organ at a time)

    // Checking if DICOMPath already exists
    QString DICOMPath = this->mainWindow->meshDoc()->getDICOMPath();
    if (DICOMPath != "") {
        ui->patientDICOMPath->setText(DICOMPath);
        // Generating organ table
        QList<OrganItem> organList = importData(DICOMPath);
        generateOrganTabel(organList);
    }
    connect(ui->importAllCheckbox, SIGNAL(toggled(bool)), this, SLOT(importAllToggle(bool)));
}

ImportDICOMDialog::~ImportDICOMDialog()
{
    delete ui;
}

void ImportDICOMDialog::on_openFolderBtn_clicked()
{
    QFileDialog folderChooser;
    folderChooser.setFileMode(QFileDialog::Directory);
    folderChooser.setOption(QFileDialog::ShowDirsOnly);
    this->patientFolderPath = folderChooser.getExistingDirectory(this, tr("Open Patient DICOM Folder"), "");
    QLineEdit *patientPath = this->ui->patientDICOMPath;
    patientPath->setText(this->patientFolderPath);

    QList<OrganItem> organList = importData(this->patientFolderPath);
    generateOrganTabel(organList);

    // Set measurements for MeshDocument
    this->mainWindow->meshDoc()->setMeasurementMillimeter(this->measurementMillimeter);
    this->mainWindow->meshDoc()->setMeasurementPixel(this->measurementPixel);
    this->mainWindow->meshDoc()->setTextureBBoxMillimeter();
    // Set path
    this->mainWindow->meshDoc()->setDICOMPath(this->patientFolderPath);
}



void ImportDICOMDialog::onOrganClick(int row, int col)
{
//    QDialogButtonBox *dialogButtons = ui->dialogButtons;
//    dialogButtons->button(QDialogButtonBox::Ok)->setEnabled(true);
}

// BEGIN TUAN SECTION

QList<OrganItem> ImportDICOMDialog::importData() {
    return importData(this->patientFolderPath);
}

QList<OrganItem> ImportDICOMDialog::importData(QString patientDirPath) {
    QList<OrganItem> organs = getOrganItems(patientDirPath);

    QDir patientDir(patientDirPath);
    patientDir.setFilter(QDir::Files);
    patientDir.setSorting(QDir::Name);

    // Get number of slice (dicom) by getting instanceNumber has maximumvalue
    // This approach prevents that PATIENT_DICOM directory contains unexpected files (eg. hidden file, dummy file, ...)
    unsigned long maxInstanceNumber = 0;
    unsigned long minInstanceNumber = ULONG_MAX;
    QFileInfoList files = patientDir.entryInfoList();
    for (int i = 0; i < files.size(); i++) {
        QFileInfo file = files.at(i);

        try {
            DicomInfo dicom(file.absoluteFilePath());

            unsigned long instanceNumber = dicom.getInstanceNumber();
            maxInstanceNumber = instanceNumber > maxInstanceNumber ? instanceNumber : maxInstanceNumber;
            minInstanceNumber = instanceNumber < minInstanceNumber ? instanceNumber : minInstanceNumber;
        }
         catch (imebra::StreamOpenError exception) {
            std::cout << "imebra::StreamOpenError" << std::endl;
        } catch (imebra::MissingTagError exception) {
            std::cout << "imebra::MissingTagError" << std::endl;
        } catch (imebra::StreamEOFError exception) {
            std::cout << "imebra::StreamEOFError" << std::endl;
        } catch (imebra::CodecWrongFormatError exception) {
            std::cout << "imebra::CodecWrongFormatError" << std::endl;
        }
    }

    // Get a right format DICOM information
    int imageWidth = 0.0;
    int imageHeight = 0.0;
    double widthScale = 0.0;
    double heightScale = 0.0;
    double spaceSlicesScale = 0.0;

    for (int i = 0; i < files.size(); i++) {
        QFileInfo file = files.at(i);

        try {
            DicomInfo dicom(file.absoluteFilePath());

            cv::Mat image = dicom.getImage();
            imageWidth = image.cols;
            imageHeight = image.rows;

            widthScale = dicom.getPixelSpacingX();
            heightScale = dicom.getPixelSpacingY();
            spaceSlicesScale = dicom.getSliceThickness();

            // If it is right format, It won't throw exception. So that is enough
            break;
        }
         catch (imebra::StreamOpenError exception) {
            std::cout << "imebra::StreamOpenError" << std::endl;
        } catch (imebra::MissingTagError exception) {
            std::cout << "imebra::MissingTagError" << std::endl;
        } catch (imebra::StreamEOFError exception) {
            std::cout << "imebra::StreamEOFError" << std::endl;
        } catch (imebra::CodecWrongFormatError exception) {
            std::cout << "imebra::CodecWrongFormatError" << std::endl;
        }
    }

    // Construct bouding box in real world (millimeter unit)
    this->measurementMillimeter = Measurement3f(imageWidth*((float)widthScale),
                             imageHeight*((float)heightScale),
                             (maxInstanceNumber - minInstanceNumber + 1)*((float)spaceSlicesScale));

    this->measurementPixel = Measurement3i(imageWidth, imageHeight, (maxInstanceNumber - minInstanceNumber + 1));

    qDebug() << "BBox:" << measurementMillimeter.X() << measurementMillimeter.Y() << measurementMillimeter.Z();
    qDebug() << "BBox pixel:" << measurementPixel.X() << measurementPixel.Y() << measurementPixel.Z();

    // Get 8 vertices of Bound Box
    cv::Point3f v0(0.0, 0.0, 0.0);
    cv::Point3f v1(measurementMillimeter.X(), 0.0, 0.0);
    cv::Point3f v2(measurementMillimeter.X(), measurementMillimeter.Y(), 0.0);
    cv::Point3f v3(0.0, measurementMillimeter.Y(), 0.0);
    cv::Point3f v4(0.0, 0.0, measurementMillimeter.Z());
    cv::Point3f v5(measurementMillimeter.X(), 0.0, measurementMillimeter.Z());
    cv::Point3f v6(measurementMillimeter.X(), measurementMillimeter.Y(), measurementMillimeter.Z());
    cv::Point3f v7(0.0, measurementMillimeter.Y(), measurementMillimeter.Z());

    boundBoxVertices.push_back(v0);
    boundBoxVertices.push_back(v1);
    boundBoxVertices.push_back(v2);
    boundBoxVertices.push_back(v3);
    boundBoxVertices.push_back(v4);
    boundBoxVertices.push_back(v5);
    boundBoxVertices.push_back(v6);
    boundBoxVertices.push_back(v7);

    return organs;
}

///
/// \brief ImportDICOMDialog::getOrganItems get all organs of database that bases on file structure as below <br>
///
/// root
///   |
///   --PATIENT_DICOM *
///   |
///   --MASKS_DICOM
///       |
///       --ogan1
///       |
///       --organ2
///       |
///       ......
///
/// \param patientDirPath PATIENT_DICOM directory full path
/// \return List of organs in OrganItem structure (eg. organ1, organ2, ...)
///
QList<OrganItem> ImportDICOMDialog::getOrganItems(QString patientDirPath) {
    QList<OrganItem> organs;

    QDir patientDir(patientDirPath); // PATIENT_DICOM directory

    QDir parent(patientDir); parent.cdUp(); // root directory

    if (!parent.cd("./MASKS_DICOM")) { // MASKS_DICOM directory
        qDebug() << "Alert message box: Can not find MASKS_DICOM";
        return organs;
    }

    parent.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    parent.setSorting(QDir::Name);



    QFileInfoList children = parent.entryInfoList();
    for (int i = 0; i < children.size(); i++) {
        QFileInfo child = children.at(i);

        if (child.isDir()) {
            QString organName = child.fileName();
            QString organFullPath = child.absoluteFilePath();

            OrganItem organItem(organName, organFullPath);
            organs.push_back(organItem);
        }

    }

    return organs;
}

void ImportDICOMDialog::on_dialogButtons_clicked(QAbstractButton *button)
{
    QDialogButtonBox *dialogButtons = this->ui->dialogButtons;
    if(button == dialogButtons->button(QDialogButtonBox::Ok) ){


        // Show out progress bar
        progressbar->setVisible(true);
        dialogButtons->button(QDialogButtonBox::Ok)->setEnabled(false); // Prevent importing many organ at a same time
        dialogButtons->button(QDialogButtonBox::Cancel)->setEnabled(false);
        ui->openFolderBtn->setDisabled(true);
        MeshDocument *md = this->mainWindow->meshDoc();
        qDebug() << "Current mesh Size: " + QString::number(md->meshList.size());

//        loadOrganIntoMeshDocument(name, path, QCallBack);
        if (ui->importAllCheckbox->isChecked()) {
            loadAllOrganIntoMeshDocument(patientFolderPath, QCallBack);
        } else {
            loadMultiOrganIntoMeshDocument(selectionPaths, QCallBack);
        }
        dialogButtons->button(QDialogButtonBox::Cancel)->setEnabled(true);
        ui->openFolderBtn->setDisabled(false);
        progressbar->hide();
        statusLabel->setText("");
        ui->organTable->clearSelection();
        return;
    }
}

void ImportDICOMDialog::on_dialogButtons_accepted()
{

}

void ImportDICOMDialog::organSelectionChange(bool state)
{
    // First reset the list
    selectionPaths.clear();
    QTableWidget *organTable = ui->organTable;
    for (int i = 0; i < organTable->rowCount(); ++i) {
        QCheckBox *checkbox = (QCheckBox*)organTable->cellWidget(i, CHECKCOL);
        if (checkbox->isChecked()) {
            QPair<QString, QString> pair;
            pair.first = organTable->item(i, NAMECOL)->text();
            pair.second = organTable->item(i, PATHCOL)->text();
            selectionPaths.push_back(pair);
        }
    }
    QDialogButtonBox *dialogButtons = ui->dialogButtons;
    if (selectionPaths.size() > 0) {
        // Enable import button
        dialogButtons->button(QDialogButtonBox::Ok)->setEnabled(true);
    } else {
        dialogButtons->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
}

void ImportDICOMDialog::importAllToggle(bool state)
{
    ui->organTable->setDisabled(state);
    QDialogButtonBox *dialogButtons = ui->dialogButtons;
    if (state) {
        if (ui->patientDICOMPath->text() != "")
            dialogButtons->button(QDialogButtonBox::Ok)->setEnabled(state);
    } else {
        if (selectionPaths.size() > 0) {
            dialogButtons->button(QDialogButtonBox::Ok)->setEnabled(true);
        } else {
            dialogButtons->button(QDialogButtonBox::Ok)->setEnabled(false);
        }
    }
}

void ImportDICOMDialog::loadOrganIntoMeshDocument(QString name, QString organPath, vcg::CallBackPos *cb) {
    if (ui->isGeneratePointCloud->isChecked()) {
        addOrganMesh(name, organPath, cb);
    } else {
        this->mainWindow->createEmptyMesh(name);
    }
    addOrganVolume(organPath, cb);
}

void ImportDICOMDialog::loadMultiOrganIntoMeshDocument(QList<QPair<QString, QString> > nameAndPath, vcg::CallBackPos *cb) {
    if (nameAndPath.size() < 1) {
        return;
    }
    MeshDocument* md = this->mainWindow->meshDoc();
    for (int i = 0; i < nameAndPath.size(); i++) {
        QString name = nameAndPath[i].first;
        QString path = nameAndPath[i].second;

        if (ui->isGeneratePointCloud->isChecked()) {
            addOrganMesh(name, path, cb);
        } else {
            this->mainWindow->importMesh(name, boundBoxVertices);
        }
        addOrganTexture(path, cb);
    }    
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int maxIndex = wp * hp * n;
//    histogram_matching(this->mainWindow->meshDoc()->globalTexture3DIntensityData, maxIndex, cb);
    TreeItem *rootNode = this->mainWindow->treeManager->addRootNode("Patient", cb);
    md->rootNode = rootNode;
    md->updateTexture3DRgbaData(cb);
}

void ImportDICOMDialog::loadAllOrganIntoMeshDocument(QString path, vcg::CallBackPos *cb)
{
    if (path == "") {
        return;
    }

    MeshDocument* md = this->mainWindow->meshDoc();
    this->mainWindow->importMesh("Bounding mesh", boundBoxVertices);

    MeshModel* currentMesh = md->mm();
    currentMesh->initializeTexture3DPatient(path, measurementPixel, cb);
    md->addOneVolumeToGlobalTexture3D(currentMesh, cb);

    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int maxIndex = wp * hp * n;
//    histogram_matching(this->mainWindow->meshDoc()->globalTexture3DIntensityData, maxIndex, cb);
    TreeItem *rootNode = this->mainWindow->treeManager->addRootNode("Patient", cb);
    md->rootNode = rootNode;
    md->updateTexture3DRgbaData(cb);
}

void ImportDICOMDialog::addOrganMesh(QString name, QString organPath, vcg::CallBackPos *cb) {
    Dicoms2Ply d2p;
    QList<cv::Point3f> points = d2p.transform2PointCloud(organPath, cb);
    this->mainWindow->importMesh(name, points);
}

void ImportDICOMDialog::addOrganVolume(QString organPath, vcg::CallBackPos *cb) {
    MeshModel* currentMesh = this->mainWindow->meshDoc()->mm();
    currentMesh->initializeTexture3D(organPath, measurementPixel, cb);
    this->mainWindow->meshDoc()->addOneVolumeToGlobalVolume(currentMesh, cb);
}

void ImportDICOMDialog::addOrganTexture(QString organPath, vcg::CallBackPos *cb) {
    MeshModel* currentMesh = this->mainWindow->meshDoc()->mm();
    currentMesh->initializeTexture3D(organPath, measurementPixel, cb);
    this->mainWindow->meshDoc()->addOneVolumeToGlobalTexture3D(currentMesh, cb);
}

void ImportDICOMDialog::generateOrganTabel(QList<OrganItem> organList)
{
    QTableWidget *organTable = ui->organTable;
    organTable->setRowCount(organList.size());

    for (int i = 0; i < organList.size(); ++i) {
        organTable->setItem(i, NAMECOL, new QTableWidgetItem(organList.at(i).organLabel));
        organTable->setItem(i, PATHCOL, new QTableWidgetItem(organList.at(i).organDirPath));
        QCheckBox *checkbox = new QCheckBox(this);
        organTable->setCellWidget(i, CHECKCOL, checkbox);
        connect(checkbox, SIGNAL(clicked(bool)), this, SLOT(organSelectionChange(bool)));
    }
}

bool ImportDICOMDialog::QCallBack(const int pos, const char *str)
{
    progressbar->show();
    progressbar->setValue(pos);
    progressbar->setEnabled(true);
    statusLabel->setText(str);
    return true;
}
