/********************************************************************************
** Form generated from reading UI file 'intensitygradienthistwidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INTENSITYGRADIENTHISTWIDGET_H
#define UI_INTENSITYGRADIENTHISTWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QTableWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "histogramcolorview.h"

QT_BEGIN_NAMESPACE

class Ui_IntensityGradientHistWidget
{
public:
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    HistogramColorView *histColorView;
    QHBoxLayout *horizontalLayout_2;
    QTableWidget *colorTable;
    QHBoxLayout *horizontalLayout_18;
    QCheckBox *keepIntensityCheckbox;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *applyTFButton;
    QHBoxLayout *horizontalLayout_14;
    QProgressBar *progressBar;

    void setupUi(QWidget *IntensityGradientHistWidget)
    {
        if (IntensityGradientHistWidget->objectName().isEmpty())
            IntensityGradientHistWidget->setObjectName(QString::fromUtf8("IntensityGradientHistWidget"));
        IntensityGradientHistWidget->resize(545, 657);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(IntensityGradientHistWidget->sizePolicy().hasHeightForWidth());
        IntensityGradientHistWidget->setSizePolicy(sizePolicy);
        verticalLayout_3 = new QVBoxLayout(IntensityGradientHistWidget);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        histColorView = new HistogramColorView(IntensityGradientHistWidget);
        histColorView->setObjectName(QString::fromUtf8("histColorView"));

        verticalLayout->addWidget(histColorView);


        verticalLayout_3->addLayout(verticalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        colorTable = new QTableWidget(IntensityGradientHistWidget);
        if (colorTable->columnCount() < 4)
            colorTable->setColumnCount(4);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        colorTable->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        colorTable->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        colorTable->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        colorTable->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        colorTable->setObjectName(QString::fromUtf8("colorTable"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(colorTable->sizePolicy().hasHeightForWidth());
        colorTable->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(colorTable);


        verticalLayout_3->addLayout(horizontalLayout_2);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        keepIntensityCheckbox = new QCheckBox(IntensityGradientHistWidget);
        keepIntensityCheckbox->setObjectName(QString::fromUtf8("keepIntensityCheckbox"));

        horizontalLayout_18->addWidget(keepIntensityCheckbox);


        verticalLayout_3->addLayout(horizontalLayout_18);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        applyTFButton = new QPushButton(IntensityGradientHistWidget);
        applyTFButton->setObjectName(QString::fromUtf8("applyTFButton"));

        horizontalLayout_3->addWidget(applyTFButton);


        verticalLayout_3->addLayout(horizontalLayout_3);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        progressBar = new QProgressBar(IntensityGradientHistWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(progressBar->sizePolicy().hasHeightForWidth());
        progressBar->setSizePolicy(sizePolicy2);
        progressBar->setValue(0);
        progressBar->setTextVisible(false);

        horizontalLayout_14->addWidget(progressBar);


        verticalLayout_3->addLayout(horizontalLayout_14);


        retranslateUi(IntensityGradientHistWidget);

        QMetaObject::connectSlotsByName(IntensityGradientHistWidget);
    } // setupUi

    void retranslateUi(QWidget *IntensityGradientHistWidget)
    {
        IntensityGradientHistWidget->setWindowTitle(QApplication::translate("IntensityGradientHistWidget", "Form", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem = colorTable->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("IntensityGradientHistWidget", "Name", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem1 = colorTable->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("IntensityGradientHistWidget", "Color", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem2 = colorTable->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("IntensityGradientHistWidget", "Opacity", 0, QApplication::UnicodeUTF8));
        keepIntensityCheckbox->setText(QApplication::translate("IntensityGradientHistWidget", "Keep raw intensity", 0, QApplication::UnicodeUTF8));
        applyTFButton->setText(QApplication::translate("IntensityGradientHistWidget", "Apply Transfer Function", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class IntensityGradientHistWidget: public Ui_IntensityGradientHistWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INTENSITYGRADIENTHISTWIDGET_H
