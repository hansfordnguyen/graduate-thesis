#include "detectboundary.h"

DetectBoundary::DetectBoundary()
{

}

DetectBoundary::Direction DetectBoundary::getCDirection(Point2i b, Point2i c)
{
    if (c.X() == b.X() && c.Y() > b.Y()) {
        return Direction::NORTH;
    } else if (c.X() == b.X() && c.Y() < b.Y()){
        return Direction::SOUTH;
    } else if (c.X() < b.X() && c.Y() == b.Y()) {
        return Direction::WEST;
    } else if (c.X() > b.X() && c.Y() == b.Y()) {
        return Direction::EAST;
    } else if (c.X() < b.X() && c.Y() > b.Y()) {
        return Direction::NORTH_WEST;
    } else if (c.X() > b.X() && c.Y() > b.Y()) {
        return Direction::NORTH_EAST;
    } else if (c.X() < b.X() && c.Y() < b.Y()) {
        return Direction::SOUTH_WEST;
    } else if (c.X() > b.X() && c.Y() < b.Y()) {
        return Direction::SOUTH_EAST;
    }

    return Direction::NONE;
}

Point2i DetectBoundary::findUperLeftPoint(uchar **slice, int width, int height)
{
    Point2i result(-1, -1);

    if (width < 0 || height < 0 || slice == NULL) {
        qDebug() << "Slice Null";
        return result;
    }

    for (int y = height - 1; y >= 0; y--) {
        for (int x = 0; x < width; x++) {
            if(slice[y][x] != 0) {
                result[0] = x;
                result[1] = y;
                return result;
            }
        }
    }

    return result;
}

Point2i DetectBoundary::findTopLeftPoint(uchar **slice, int width, int height)
{
    Point2i result(-1, -1);

    if (width < 0 || height < 0 || slice == NULL) {
        qDebug() << "Slice Null";
        return result;
    }

    for (int y = height - 1; y >= 0; y--) {
        for (int x = 0; x < width; x++) {
            if(slice[y][x] != 0) {
                result[0] = x;
                result[1] = y;
                return result;
            }
        }
    }

    return result;
}

Point2i DetectBoundary::findRightTopPoint(uchar **slice, int width, int height)
{
    Point2i result(-1, -1);

    if (width < 0 || height < 0 || slice == NULL) {
        qDebug() << "Slice Null";
        return result;
    }

    for (int x = width - 1; x >= 0; x--) {
        for (int y= height - 1; y >= 0; y--) {
            if(slice[y][x] != 0) {
                result[0] = x;
                result[1] = y;
                return result;
            }
        }
    }

    return result;
}

Point2i DetectBoundary::findBottomRightPoint(uchar **slice, int width, int height)
{
    Point2i result(-1, -1);

    if (width < 0 || height < 0 || slice == NULL) {
        qDebug() << "Slice Null";
        return result;
    }

    for (int y = 0; y < height; y++) {
        for (int x = width - 1; x >= 0; x--) {
            if(slice[y][x] != 0) {
                result[0] = x;
                result[1] = y;
                return result;
            }
        }
    }

    return result;
}

Point2i DetectBoundary::findLeftBottomPoint(uchar **slice, int width, int height)
{
    Point2i result(-1, -1);

    if (width < 0 || height < 0 || slice == NULL) {
        qDebug() << "Slice Null";
        return result;
    }

    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            if(slice[y][x] != 0) {
                result[0] = x;
                result[1] = y;
                return result;
            }
        }
    }

    return result;
}

QList<Point2i> DetectBoundary::findBoundaryPoints(Point2i uperLeft, uchar **slice, int width, int height)
{
    QList<Point2i> result;

    if (uperLeft.X() == -1 || uperLeft.Y() == -1)
        return result;

    // Setting b0, c0
    setB0(uperLeft);
    setC0(Point2i(uperLeft.X() - 1, uperLeft.Y()));

    // Setting b1, c1
    Point2i neighbor4(uperLeft.X() + 1, uperLeft.Y() + 1);
    Point2i neighbor5(uperLeft.X() + 1, uperLeft.Y());
    Point2i neighbor6(uperLeft.X() + 1, uperLeft.Y() - 1);
    Point2i neighbor7(uperLeft.X(), uperLeft.Y() - 1);
    Point2i neighbor8(uperLeft.X() - 1, uperLeft.Y() - 1);

    if (slice[neighbor5.Y()][neighbor5.X()] != 0) {
        setB1(neighbor5);
        setC1(neighbor4);
    } else if (slice[neighbor6.Y()][neighbor6.X()] != 0) {
        setB1(neighbor6);
        setC1(neighbor5);
    } else if (slice[neighbor7.Y()][neighbor7.X()] != 0) {
        setB1(neighbor7);
        setC1(neighbor6);
    } else if (slice[neighbor8.Y()][neighbor8.X()] != 0) {
        setB1(neighbor8);
        setC1(neighbor7);
    } else {
        return result;
    }
    result.push_back(getB0());
    result.push_back(getB1());

    // Start finding remain boundary points
    Point2i currentB0(-1, -1);
    Point2i currentC0(-1, -1);
    Point2i currentB1 = getB1();
    Point2i currentC1 = getC1();


    while(currentB0.X() != getB0().X() || currentB0.Y() != getB0().Y() ||
          currentB1.X() != getB1().X() || currentB1.Y() != getB1().Y()) {

        if (currentB0.X() == 0 || currentB0.X() == width - 1)
            break;

        if (currentB0.Y() == 0 || currentB0.X() == height - 1)
            break;

        currentB0 = currentB1;
        currentC0 = currentC1;

        Point2i neighbor1(-1, -1);
        Point2i neighbor2(-1, -1);
        Point2i neighbor3(-1, -1);
        Point2i neighbor4(-1, -1);
        Point2i neighbor5(-1, -1);
        Point2i neighbor6(-1, -1);
        Point2i neighbor7(-1, -1);
        Point2i neighbor8(-1, -1);

        Direction c0Direction = getCDirection(currentB0, currentC0);
        switch (c0Direction) {
        case Direction::WEST:
            neighbor1[0] = currentB0.X() - 1;   neighbor1[1] = currentB0.Y();
            neighbor2[0] = currentB0.X() - 1;   neighbor2[1] = currentB0.Y() + 1;
            neighbor3[0] = currentB0.X();       neighbor3[1] = currentB0.Y() + 1;
            neighbor4[0] = currentB0.X() + 1;   neighbor4[1] = currentB0.Y() + 1;
            neighbor5[0] = currentB0.X() + 1;   neighbor5[1] = currentB0.Y();
            neighbor6[0] = currentB0.X() + 1;   neighbor6[1] = currentB0.Y() - 1;
            neighbor7[0] = currentB0.X();       neighbor7[1] = currentB0.Y() - 1;
            neighbor8[0] = currentB0.X() - 1;   neighbor8[1] = currentB0.Y() - 1;
            break;
        case Direction::NORTH_WEST:
            neighbor1[0] = currentB0.X() - 1;   neighbor1[1] = currentB0.Y() + 1;
            neighbor2[0] = currentB0.X();       neighbor2[1] = currentB0.Y() + 1;
            neighbor3[0] = currentB0.X() + 1;   neighbor3[1] = currentB0.Y() + 1;
            neighbor4[0] = currentB0.X() + 1;   neighbor4[1] = currentB0.Y();
            neighbor5[0] = currentB0.X() + 1;   neighbor5[1] = currentB0.Y() - 1;
            neighbor6[0] = currentB0.X();       neighbor6[1] = currentB0.Y() - 1;
            neighbor7[0] = currentB0.X() - 1;   neighbor7[1] = currentB0.Y() - 1;
            neighbor8[0] = currentB0.X() - 1;   neighbor8[1] = currentB0.Y();
            break;
        case Direction::NORTH:
            neighbor1[0] = currentB0.X();       neighbor1[1] = currentB0.Y() + 1;
            neighbor2[0] = currentB0.X() + 1;   neighbor2[1] = currentB0.Y() + 1;
            neighbor3[0] = currentB0.X() + 1;   neighbor3[1] = currentB0.Y();
            neighbor4[0] = currentB0.X() + 1;   neighbor4[1] = currentB0.Y() - 1;
            neighbor5[0] = currentB0.X();       neighbor5[1] = currentB0.Y() - 1;
            neighbor6[0] = currentB0.X() - 1;   neighbor6[1] = currentB0.Y() - 1;
            neighbor7[0] = currentB0.X() - 1;   neighbor7[1] = currentB0.Y();
            neighbor8[0] = currentB0.X() - 1;   neighbor8[1] = currentB0.Y() + 1;
            break;
        case Direction::NORTH_EAST:
            neighbor1[0] = currentB0.X() + 1;   neighbor1[1] = currentB0.Y() + 1;
            neighbor2[0] = currentB0.X() + 1;   neighbor2[1] = currentB0.Y();
            neighbor3[0] = currentB0.X() + 1;   neighbor3[1] = currentB0.Y() - 1;
            neighbor4[0] = currentB0.X();       neighbor4[1] = currentB0.Y() - 1;
            neighbor5[0] = currentB0.X() - 1;   neighbor5[1] = currentB0.Y() - 1;
            neighbor6[0] = currentB0.X() - 1;   neighbor6[1] = currentB0.Y();
            neighbor7[0] = currentB0.X() - 1;   neighbor7[1] = currentB0.Y() + 1;
            neighbor8[0] = currentB0.X();       neighbor8[1] = currentB0.Y() + 1;
            break;
        case Direction::EAST:
            neighbor1[0] = currentB0.X() + 1;   neighbor1[1] = currentB0.Y();
            neighbor2[0] = currentB0.X() + 1;   neighbor2[1] = currentB0.Y() - 1;
            neighbor3[0] = currentB0.X();       neighbor3[1] = currentB0.Y() - 1;
            neighbor4[0] = currentB0.X() - 1;   neighbor4[1] = currentB0.Y() - 1;
            neighbor5[0] = currentB0.X() - 1;   neighbor5[1] = currentB0.Y();
            neighbor6[0] = currentB0.X() - 1;   neighbor6[1] = currentB0.Y() + 1;
            neighbor7[0] = currentB0.X();       neighbor7[1] = currentB0.Y() + 1;
            neighbor8[0] = currentB0.X() + 1;   neighbor8[1] = currentB0.Y() + 1;
            break;
        case Direction::SOUTH_EAST:
            neighbor1[0] = currentB0.X() + 1;   neighbor1[1] = currentB0.Y() - 1;
            neighbor2[0] = currentB0.X();       neighbor2[1] = currentB0.Y() - 1;
            neighbor3[0] = currentB0.X() - 1;   neighbor3[1] = currentB0.Y() - 1;
            neighbor4[0] = currentB0.X() - 1;   neighbor4[1] = currentB0.Y();
            neighbor5[0] = currentB0.X() - 1;   neighbor5[1] = currentB0.Y() + 1;
            neighbor6[0] = currentB0.X();       neighbor6[1] = currentB0.Y() + 1;
            neighbor7[0] = currentB0.X() + 1;   neighbor7[1] = currentB0.Y() + 1;
            neighbor8[0] = currentB0.X() + 1;   neighbor8[1] = currentB0.Y();
            break;
        case Direction::SOUTH:
            neighbor1[0] = currentB0.X();       neighbor1[1] = currentB0.Y() - 1;
            neighbor2[0] = currentB0.X() - 1;   neighbor2[1] = currentB0.Y() - 1;
            neighbor3[0] = currentB0.X() - 1;   neighbor3[1] = currentB0.Y();
            neighbor4[0] = currentB0.X() - 1;   neighbor4[1] = currentB0.Y() + 1;
            neighbor5[0] = currentB0.X();       neighbor5[1] = currentB0.Y() + 1;
            neighbor6[0] = currentB0.X() + 1;   neighbor6[1] = currentB0.Y() + 1;
            neighbor7[0] = currentB0.X() + 1;   neighbor7[1] = currentB0.Y();
            neighbor8[0] = currentB0.X() + 1;   neighbor8[1] = currentB0.Y() - 1;
            break;
        case Direction::SOUTH_WEST:
            neighbor1[0] = currentB0.X() - 1;   neighbor1[1] = currentB0.Y() - 1;
            neighbor2[0] = currentB0.X() - 1;   neighbor2[1] = currentB0.Y();
            neighbor3[0] = currentB0.X() - 1;   neighbor3[1] = currentB0.Y() + 1;
            neighbor4[0] = currentB0.X();       neighbor4[1] = currentB0.Y() + 1;
            neighbor5[0] = currentB0.X() + 1;   neighbor5[1] = currentB0.Y() + 1;
            neighbor6[0] = currentB0.X() + 1;   neighbor6[1] = currentB0.Y();
            neighbor7[0] = currentB0.X() + 1;   neighbor7[1] = currentB0.Y() - 1;
            neighbor8[0] = currentB0.X();       neighbor8[1] = currentB0.Y() - 1;
            break;
        default:
            break;
        } // end switch

        if (slice[neighbor2.Y()][neighbor2.X()] != 0) {
            currentB1 = neighbor2;
            currentC1 = neighbor1;
        } else if (slice[neighbor3.Y()][neighbor3.X()] != 0) {
            currentB1 = neighbor3;
            currentC1 = neighbor2;
        } else if (slice[neighbor4.Y()][neighbor4.X()] != 0) {
            currentB1 = neighbor4;
            currentC1 = neighbor3;
        } else if (slice[neighbor5.Y()][neighbor5.X()] != 0) {
            currentB1 = neighbor5;
            currentC1 = neighbor4;
        } else if (slice[neighbor6.Y()][neighbor6.X()] != 0) {
            currentB1 = neighbor6;
            currentC1 = neighbor5;
        } else if (slice[neighbor7.Y()][neighbor7.X()] != 0) {
            currentB1 = neighbor7;
            currentC1 = neighbor6;
        } else if (slice[neighbor8.Y()][neighbor8.X()] != 0) {
            currentB1 = neighbor8;
            currentC1 = neighbor7;
        } else {
            return result;
        }

        result.push_back(currentB1);
    } // end while (currentB0 != getB0() && currentB1 != getB1())

    qDebug() << "End while (currentB0 != getB0() && currentB1 != getB1())";
}

QList<Point2i> DetectBoundary::findBoundaryPoints(Point2i startPoint, DetectBoundary::StartDirection startDir, uchar **slice, int width, int height)
{
    QList<Point2i> result;

    if (startPoint.X() == -1 || startPoint.Y() == -1)
        return result;

    // Setting b0
    setB0(startPoint);

    // Setting b1, c1
    Point2i neighbor1(startPoint.X() - 1, startPoint.Y());
    Point2i neighbor2(startPoint.X() - 1, startPoint.Y() + 1);
    Point2i neighbor3(startPoint.X(), startPoint.Y() + 1);
    Point2i neighbor4(startPoint.X() + 1, startPoint.Y() + 1);
    Point2i neighbor5(startPoint.X() + 1, startPoint.Y());
    Point2i neighbor6(startPoint.X() + 1, startPoint.Y() - 1);
    Point2i neighbor7(startPoint.X(), startPoint.Y() - 1);
    Point2i neighbor8(startPoint.X() - 1, startPoint.Y() - 1);

    switch (startDir) {
    case StartDirection::TOP_LEFT:
        setC0(neighbor1);
        if (slice[neighbor5.Y()][neighbor5.X()] != 0) {
            setB1(neighbor5);
            setC1(neighbor4);
        } else if (slice[neighbor6.Y()][neighbor6.X()] != 0) {
            setB1(neighbor6);
            setC1(neighbor5);
        } else if (slice[neighbor7.Y()][neighbor7.X()] != 0) {
            setB1(neighbor7);
            setC1(neighbor6);
        } else if (slice[neighbor8.Y()][neighbor8.X()] != 0) {
            setB1(neighbor8);
            setC1(neighbor7);
        } else {
            return result;
        }
        break;
    case StartDirection::RIGHT_TOP:
        setC0(neighbor3);
        if (slice[neighbor7.Y()][neighbor7.X()] != 0) {
            setB1(neighbor7);
            setC1(neighbor6);
        } else if (slice[neighbor8.Y()][neighbor8.X()] != 0) {
            setB1(neighbor8);
            setC1(neighbor7);
        } else if (slice[neighbor1.Y()][neighbor1.X()] != 0) {
            setB1(neighbor1);
            setC1(neighbor8);
        } else if (slice[neighbor2.Y()][neighbor2.X()] != 0) {
            setB1(neighbor2);
            setC1(neighbor1);
        } else {
            return result;
        }
        break;
    case StartDirection::BOTTOM_RIGHT:
        setC0(neighbor5);
        if (slice[neighbor1.Y()][neighbor1.X()] != 0) {
            setB1(neighbor1);
            setC1(neighbor8);
        } else if (slice[neighbor2.Y()][neighbor2.X()] != 0) {
            setB1(neighbor2);
            setC1(neighbor1);
        } else if (slice[neighbor3.Y()][neighbor3.X()] != 0) {
            setB1(neighbor3);
            setC1(neighbor2);
        } else if (slice[neighbor4.Y()][neighbor4.X()] != 0) {
            setB1(neighbor4);
            setC1(neighbor3);
        } else {
            return result;
        }
        break;
    case StartDirection::LEFT_BOTTOM:
        setC0(neighbor7);
        if (slice[neighbor3.Y()][neighbor3.X()] != 0) {
            setB1(neighbor3);
            setC1(neighbor2);
        } else if (slice[neighbor4.Y()][neighbor4.X()] != 0) {
            setB1(neighbor4);
            setC1(neighbor3);
        } else if (slice[neighbor5.Y()][neighbor5.X()] != 0) {
            setB1(neighbor5);
            setC1(neighbor4);
        } else if (slice[neighbor6.Y()][neighbor6.X()] != 0) {
            setB1(neighbor6);
            setC1(neighbor5);
        } else {
            return result;
        }
        break;
    }

    result.push_back(getB0());
    result.push_back(getB1());

    // Start finding remain boundary points
    Point2i currentB0(-1, -1);
    Point2i currentC0(-1, -1);
    Point2i currentB1 = getB1();
    Point2i currentC1 = getC1();


    while(currentB0.X() != getB0().X() || currentB0.Y() != getB0().Y() ||
          currentB1.X() != getB1().X() || currentB1.Y() != getB1().Y()) {

        if (currentB0.X() == 0 || currentB0.X() == width - 1)
            break;

        if (currentB0.Y() == 0 || currentB0.X() == height - 1)
            break;

        currentB0 = currentB1;
        currentC0 = currentC1;

        Point2i neighbor1(-1, -1);
        Point2i neighbor2(-1, -1);
        Point2i neighbor3(-1, -1);
        Point2i neighbor4(-1, -1);
        Point2i neighbor5(-1, -1);
        Point2i neighbor6(-1, -1);
        Point2i neighbor7(-1, -1);
        Point2i neighbor8(-1, -1);

        Direction c0Direction = getCDirection(currentB0, currentC0);
        switch (c0Direction) {
        case Direction::WEST:
            neighbor1[0] = currentB0.X() - 1;   neighbor1[1] = currentB0.Y();
            neighbor2[0] = currentB0.X() - 1;   neighbor2[1] = currentB0.Y() + 1;
            neighbor3[0] = currentB0.X();       neighbor3[1] = currentB0.Y() + 1;
            neighbor4[0] = currentB0.X() + 1;   neighbor4[1] = currentB0.Y() + 1;
            neighbor5[0] = currentB0.X() + 1;   neighbor5[1] = currentB0.Y();
            neighbor6[0] = currentB0.X() + 1;   neighbor6[1] = currentB0.Y() - 1;
            neighbor7[0] = currentB0.X();       neighbor7[1] = currentB0.Y() - 1;
            neighbor8[0] = currentB0.X() - 1;   neighbor8[1] = currentB0.Y() - 1;
            break;
        case Direction::NORTH_WEST:
            neighbor1[0] = currentB0.X() - 1;   neighbor1[1] = currentB0.Y() + 1;
            neighbor2[0] = currentB0.X();       neighbor2[1] = currentB0.Y() + 1;
            neighbor3[0] = currentB0.X() + 1;   neighbor3[1] = currentB0.Y() + 1;
            neighbor4[0] = currentB0.X() + 1;   neighbor4[1] = currentB0.Y();
            neighbor5[0] = currentB0.X() + 1;   neighbor5[1] = currentB0.Y() - 1;
            neighbor6[0] = currentB0.X();       neighbor6[1] = currentB0.Y() - 1;
            neighbor7[0] = currentB0.X() - 1;   neighbor7[1] = currentB0.Y() - 1;
            neighbor8[0] = currentB0.X() - 1;   neighbor8[1] = currentB0.Y();
            break;
        case Direction::NORTH:
            neighbor1[0] = currentB0.X();       neighbor1[1] = currentB0.Y() + 1;
            neighbor2[0] = currentB0.X() + 1;   neighbor2[1] = currentB0.Y() + 1;
            neighbor3[0] = currentB0.X() + 1;   neighbor3[1] = currentB0.Y();
            neighbor4[0] = currentB0.X() + 1;   neighbor4[1] = currentB0.Y() - 1;
            neighbor5[0] = currentB0.X();       neighbor5[1] = currentB0.Y() - 1;
            neighbor6[0] = currentB0.X() - 1;   neighbor6[1] = currentB0.Y() - 1;
            neighbor7[0] = currentB0.X() - 1;   neighbor7[1] = currentB0.Y();
            neighbor8[0] = currentB0.X() - 1;   neighbor8[1] = currentB0.Y() + 1;
            break;
        case Direction::NORTH_EAST:
            neighbor1[0] = currentB0.X() + 1;   neighbor1[1] = currentB0.Y() + 1;
            neighbor2[0] = currentB0.X() + 1;   neighbor2[1] = currentB0.Y();
            neighbor3[0] = currentB0.X() + 1;   neighbor3[1] = currentB0.Y() - 1;
            neighbor4[0] = currentB0.X();       neighbor4[1] = currentB0.Y() - 1;
            neighbor5[0] = currentB0.X() - 1;   neighbor5[1] = currentB0.Y() - 1;
            neighbor6[0] = currentB0.X() - 1;   neighbor6[1] = currentB0.Y();
            neighbor7[0] = currentB0.X() - 1;   neighbor7[1] = currentB0.Y() + 1;
            neighbor8[0] = currentB0.X();       neighbor8[1] = currentB0.Y() + 1;
            break;
        case Direction::EAST:
            neighbor1[0] = currentB0.X() + 1;   neighbor1[1] = currentB0.Y();
            neighbor2[0] = currentB0.X() + 1;   neighbor2[1] = currentB0.Y() - 1;
            neighbor3[0] = currentB0.X();       neighbor3[1] = currentB0.Y() - 1;
            neighbor4[0] = currentB0.X() - 1;   neighbor4[1] = currentB0.Y() - 1;
            neighbor5[0] = currentB0.X() - 1;   neighbor5[1] = currentB0.Y();
            neighbor6[0] = currentB0.X() - 1;   neighbor6[1] = currentB0.Y() + 1;
            neighbor7[0] = currentB0.X();       neighbor7[1] = currentB0.Y() + 1;
            neighbor8[0] = currentB0.X() + 1;   neighbor8[1] = currentB0.Y() + 1;
            break;
        case Direction::SOUTH_EAST:
            neighbor1[0] = currentB0.X() + 1;   neighbor1[1] = currentB0.Y() - 1;
            neighbor2[0] = currentB0.X();       neighbor2[1] = currentB0.Y() - 1;
            neighbor3[0] = currentB0.X() - 1;   neighbor3[1] = currentB0.Y() - 1;
            neighbor4[0] = currentB0.X() - 1;   neighbor4[1] = currentB0.Y();
            neighbor5[0] = currentB0.X() - 1;   neighbor5[1] = currentB0.Y() + 1;
            neighbor6[0] = currentB0.X();       neighbor6[1] = currentB0.Y() + 1;
            neighbor7[0] = currentB0.X() + 1;   neighbor7[1] = currentB0.Y() + 1;
            neighbor8[0] = currentB0.X() + 1;   neighbor8[1] = currentB0.Y();
            break;
        case Direction::SOUTH:
            neighbor1[0] = currentB0.X();       neighbor1[1] = currentB0.Y() - 1;
            neighbor2[0] = currentB0.X() - 1;   neighbor2[1] = currentB0.Y() - 1;
            neighbor3[0] = currentB0.X() - 1;   neighbor3[1] = currentB0.Y();
            neighbor4[0] = currentB0.X() - 1;   neighbor4[1] = currentB0.Y() + 1;
            neighbor5[0] = currentB0.X();       neighbor5[1] = currentB0.Y() + 1;
            neighbor6[0] = currentB0.X() + 1;   neighbor6[1] = currentB0.Y() + 1;
            neighbor7[0] = currentB0.X() + 1;   neighbor7[1] = currentB0.Y();
            neighbor8[0] = currentB0.X() + 1;   neighbor8[1] = currentB0.Y() - 1;
            break;
        case Direction::SOUTH_WEST:
            neighbor1[0] = currentB0.X() - 1;   neighbor1[1] = currentB0.Y() - 1;
            neighbor2[0] = currentB0.X() - 1;   neighbor2[1] = currentB0.Y();
            neighbor3[0] = currentB0.X() - 1;   neighbor3[1] = currentB0.Y() + 1;
            neighbor4[0] = currentB0.X();       neighbor4[1] = currentB0.Y() + 1;
            neighbor5[0] = currentB0.X() + 1;   neighbor5[1] = currentB0.Y() + 1;
            neighbor6[0] = currentB0.X() + 1;   neighbor6[1] = currentB0.Y();
            neighbor7[0] = currentB0.X() + 1;   neighbor7[1] = currentB0.Y() - 1;
            neighbor8[0] = currentB0.X();       neighbor8[1] = currentB0.Y() - 1;
            break;
        default:
            break;
        } // end switch

        if (slice[neighbor2.Y()][neighbor2.X()] != 0) {
            currentB1 = neighbor2;
            currentC1 = neighbor1;
        } else if (slice[neighbor3.Y()][neighbor3.X()] != 0) {
            currentB1 = neighbor3;
            currentC1 = neighbor2;
        } else if (slice[neighbor4.Y()][neighbor4.X()] != 0) {
            currentB1 = neighbor4;
            currentC1 = neighbor3;
        } else if (slice[neighbor5.Y()][neighbor5.X()] != 0) {
            currentB1 = neighbor5;
            currentC1 = neighbor4;
        } else if (slice[neighbor6.Y()][neighbor6.X()] != 0) {
            currentB1 = neighbor6;
            currentC1 = neighbor5;
        } else if (slice[neighbor7.Y()][neighbor7.X()] != 0) {
            currentB1 = neighbor7;
            currentC1 = neighbor6;
        } else if (slice[neighbor8.Y()][neighbor8.X()] != 0) {
            currentB1 = neighbor8;
            currentC1 = neighbor7;
        } else {
            return result;
        }

        result.push_back(currentB1);
    } // end while (currentB0 != getB0() && currentB1 != getB1())
}
