#include "treemanagerwidget.h"
#include "ui_treemanagerwidget.h"

#include <QDockWidget>
#include <QFileDialog>
#include <QMenu>

QProgressBar *TreeManagerWidget::progressbar;
TreeManagerWidget::TreeManagerWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TreeManagerWidget)
{
    ui->setupUi(this);
    initialize();
    progressbar = ui->progressbar;
    progressbar->setVisible(false);

    treeView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(treeView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onCustomContextMenuRequested(QPoint)));
}

void TreeManagerWidget::initialize()
{
    treeView = ui->treeView;
    treeModel = new TreeModel(this);
//    tree->header()->setVisible(false);
    treeView->setModel(treeModel);
    treeView->expandAll();
    for (int column = 0; column < treeModel->columnCount(); ++column) {
        treeView->resizeColumnToContents(column);
    }
    QItemSelectionModel *selectionModel = treeView->selectionModel();
    connect(selectionModel, SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(selectionChanged(QItemSelection,QItemSelection)));
    connect(treeModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(dataChanged(QModelIndex,QModelIndex)));

    TFComboDelegate* delegate = new TFComboDelegate(this);
    treeView->setItemDelegateForColumn(1, delegate);

    viewTable = ui->viewTable;
    viewTable->horizontalHeader()->setResizeMode(0, QHeaderView::Stretch);
    viewTable->setColumnWidth(1, 50);

    removeItemMapper = new QSignalMapper();

}

TreeManagerWidget::~TreeManagerWidget()
{
    delete ui;
}

void TreeManagerWidget::insertRow()
{
    QModelIndex index = treeView->selectionModel()->currentIndex();
    QAbstractItemModel *model = treeView->model();

    if (!model->insertRow(index.row() + 1, index.parent()))
        return;

    for (int column = 0; column < model->columnCount(index.parent()); ++column) {
        QModelIndex child = model->index(index.row() + 1, column, index.parent());
        model->setData(child, QVariant("[No data]"), Qt::EditRole);
        treeView->resizeColumnToContents(column);
    }
}

void TreeManagerWidget::insertChild()
{
    QModelIndex index = treeView->selectionModel()->currentIndex();
    QAbstractItemModel *model = treeView->model();
    if (model->columnCount(index) == 0) {
        if (!model->insertColumn(0, index))
            return;
    }
    if (!model->insertRow(0, index))
        return;
    for (int column = 0; column < model->columnCount(index); ++column) {
        QModelIndex child = model->index(0, column, index);
        model->setData(child, QVariant("[No data]"), Qt::EditRole);
        treeView->resizeColumnToContents(column);
        if (!model->headerData(column, Qt::Horizontal).isValid())
            model->setHeaderData(column, Qt::Horizontal, QVariant("[No header]"), Qt::EditRole);
    }

    treeView->selectionModel()->setCurrentIndex(model->index(0, 0, index), QItemSelectionModel::ClearAndSelect);
}

TreeItem* TreeManagerWidget::newChildNode(QString name, QModelIndex index)
{
    if (index == QModelIndex())
        index = treeView->selectionModel()->currentIndex();
    QAbstractItemModel *model = treeView->model();
    if (model->columnCount(index) == 0) {
        if (!model->insertColumn(0, index))
            return NULL;
    }
    if (!model->insertRow(0, index))
        return NULL;

    QModelIndex namecol = model->index(0, NAME_COL, index);
    model->setData(namecol, QVariant(name), Qt::EditRole);
    treeView->resizeColumnToContents(NAME_COL);
    if (!model->headerData(NAME_COL, Qt::Horizontal).isValid())
        model->setHeaderData(NAME_COL, Qt::Horizontal, QVariant("[No header]"), Qt::EditRole);

    QModelIndex transfunc = model->index(0, TRANSFUNC_COL, index);
    model->setData(transfunc, QVariant("None"), Qt::EditRole);
    treeView->resizeColumnToContents(TRANSFUNC_COL);
    if (!model->headerData(TRANSFUNC_COL, Qt::Horizontal).isValid())
        model->setHeaderData(TRANSFUNC_COL, Qt::Horizontal, QVariant("[No header]"), Qt::EditRole);

    TreeItem *item = treeModel->getItem(namecol);\
    // Init opacity
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int length = wp * hp * n;
    qDebug() << length;
    item->opacity = new bool[length];
    item->maxIndex = length - 1;
    for (int i = 0; i < length; ++i) {
        item->opacity[i] = 0;
    }

    treeView->selectionModel()->setCurrentIndex(model->index(0, 0, index), QItemSelectionModel::ClearAndSelect);
    return treeModel->getItem(namecol);
}

QPair<TreeItem *, QModelIndex> *TreeManagerWidget::insertNewNode(QString name, QModelIndex index)
{
    if (index == QModelIndex())
        index = treeView->selectionModel()->currentIndex();
    QAbstractItemModel *model = treeView->model();
    if (model->columnCount(index) == 0) {
        if (!model->insertColumn(0, index))
            return NULL;
    }
    if (!model->insertRow(0, index))
        return NULL;

    QModelIndex namecol = model->index(0, NAME_COL, index);
    model->setData(namecol, QVariant(name), Qt::EditRole);
    treeView->resizeColumnToContents(NAME_COL);
    if (!model->headerData(NAME_COL, Qt::Horizontal).isValid())
        model->setHeaderData(NAME_COL, Qt::Horizontal, QVariant("[No header]"), Qt::EditRole);

    QModelIndex transfunc = model->index(0, TRANSFUNC_COL, index);
    model->setData(transfunc, QVariant("None"), Qt::EditRole);
    treeView->resizeColumnToContents(TRANSFUNC_COL);
    if (!model->headerData(TRANSFUNC_COL, Qt::Horizontal).isValid())
        model->setHeaderData(TRANSFUNC_COL, Qt::Horizontal, QVariant("[No header]"), Qt::EditRole);

    TreeItem *item = treeModel->getItem(namecol);\
    // Init opacity
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int length = wp * hp * n;
    qDebug() << length;
    item->opacity = new bool[length];
    item->maxIndex = length - 1;
    for (int i = 0; i < length; ++i) {
        item->opacity[i] = 0;
    }

    treeView->selectionModel()->setCurrentIndex(model->index(0, 0, index), QItemSelectionModel::ClearAndSelect);
    return new QPair<TreeItem *, QModelIndex>(item, namecol);
}

void TreeManagerWidget::showContexMenu(TreeItem *item, const QPoint &globalPos)
{
    QMenu menu;
    menu.setStyleSheet("QMenu {background-color: #3d3d3d;color: #fff;paddding: 0;} QMenu::item:selected {color: #fff;background-color: #00aba9;}");
    // View specific node action
    QAction *viewSpecificNodeAction = new QAction("View <" + item->data(0).toString() + ">", 0);
    connect(viewSpecificNodeAction, SIGNAL(triggered(bool)), this , SLOT(viewSpecificNode()));
    menu.addAction(viewSpecificNodeAction);
    // Add to view action
    QAction *addToViewAction = new QAction("Add <" + item->data(0).toString() + "> to view", 0);
    connect(addToViewAction, SIGNAL(triggered(bool)), this , SLOT(addToView()));
    menu.addAction(addToViewAction);
    // Export mask action
    QAction *exportMaskAction = new QAction("Export <" + item->data(0).toString() + "> mask", 0);
    connect(exportMaskAction, SIGNAL(triggered(bool)), this , SLOT(exportMask()));
    menu.addAction(exportMaskAction);
    // Import mask action
    QAction *importMaskAction = new QAction("Import mask for <" + item->data(0).toString() + ">", 0);
    connect(importMaskAction, SIGNAL(triggered(bool)), this , SLOT(importMask()));
    menu.addAction(importMaskAction);
    // Import mask action
    QAction *resetMaskAction = new QAction("Reset mask of <" + item->data(0).toString() + ">", 0);
    connect(resetMaskAction, SIGNAL(triggered(bool)), this , SLOT(resetMask()));
    menu.addAction(resetMaskAction);
    // Remove action
    QAction *removeAction = new QAction("Remove <" + item->data(0).toString() + ">", 0);
    connect(removeAction, SIGNAL(triggered(bool)), this , SLOT(removeNode()));
    menu.addAction(removeAction);

    menu.exec(globalPos);
}

void TreeManagerWidget::removeViewItemAndChildren(TreeItem *parentItem)
{
    for (int i = 0; i < parentItem->childCount(); i++) {
        removeViewItemAndChildren(parentItem->child(i));
    }
    if (viewItems.indexOf(parentItem) != -1) {
        viewItems.removeOne(parentItem);
    }
}

TreeItem *TreeManagerWidget::getSelectedItem()
{
    const QModelIndex index = treeView->selectionModel()->currentIndex();
    QModelIndex namecol = treeModel->index(index.row(), NAME_COL, index.parent());
    TreeItem *selectionItem = treeModel->getItem(namecol);
    return selectionItem;
}

bool TreeManagerWidget::insertColumn()
{
    QAbstractItemModel *model = treeView->model();
    int column = treeView->selectionModel()->currentIndex().column();

    // Insert a column in the prent item.
    bool changed = model->insertColumn(column + 1);
    if (changed) {
        model->setHeaderData(column + 1, Qt::Horizontal, QVariant("[No data]"), Qt::EditRole);
    }
    return changed;
}

bool TreeManagerWidget::removeColumn()
{
    QAbstractItemModel *model = treeView->model();
    int column = treeView->selectionModel()->currentIndex().column();

    // Insert columns in each child of the parent item
    bool changed = model->removeColumn(column);
    return changed;
}

void TreeManagerWidget::removeRow()
{
    QModelIndex index = treeView->selectionModel()->currentIndex();
    QAbstractItemModel *model = treeView->model();

    QModelIndex namecol = treeModel->index(index.row(), NAME_COL, index.parent());

    TreeItem *selectionItem = treeModel->getItem(namecol);
    removeViewItemAndChildren(selectionItem);
//    selectionItem->isDeleted = true;

    updateViewTable();
    model->removeRow(index.row(), index.parent());
}

// Only use when import new organ, not splitting
TreeItem* TreeManagerWidget::addRootNode(QString name, vcg::CallBackPos *cb)
{
    QModelIndex index = treeView->selectionModel()->currentIndex();
    QAbstractItemModel *model = treeView->model();

    if (!model->insertRow(index.row() + 1, index.parent()))
        return NULL;
    QModelIndex namecol = model->index(index.row() + 1, NAME_COL, index.parent());
    model->setData(namecol, QVariant(name), Qt::EditRole);
    treeView->resizeColumnToContents(NAME_COL);

    QModelIndex tfcol = model->index(index.row() + 1, TRANSFUNC_COL, index.parent());
    model->setData(tfcol, QVariant("None"), Qt::EditRole);
    treeView->resizeColumnToContents(TRANSFUNC_COL);

    TreeItem *item = treeModel->getItem(namecol);\
    // Init opacity
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int length = wp * hp * n;
    item->opacity = new bool[length];
    item->maxIndex = length - 1;
    for (int i = 0; i < length; ++i) {
//        if (md->globalTexture3DIntensityData[i] != 0){
            item->opacity[i] = 1;
//        } else {
//            item->opacity[i] = 0;
//        }
    }
    item->updateHistogram(md->globalTexture3DIntensityData, cb);
    item->transfunc = defaultTransferfunction;

    TransfuncOptions defaultOptions;
//    defaultOptions.isUseColorTable = true; // Use colortable by default
    defaultOptions.type = TransfuncTypes::NONE;
    NodeViewOptions viewOptions;
    viewOptions.colorTable = item->colorTable;
    viewOptions.opacity = item->opacity;
    item->transfuncOptions = defaultOptions;
    item->viewOptions = viewOptions;

    return item;
}

QList<QPair<TransFunc, NodeViewOptions> > TreeManagerWidget::getListTransFunc(QModelIndex index)
{
    QList<QPair<TransFunc, NodeViewOptions>> listFuncs;
    QModelIndex seekRoot = index;
    while (seekRoot != QModelIndex()) {
        TreeItem *item = treeModel->getItem(seekRoot);
        TransFunc func = item->getTransFunc();
        if (func != NULL) {
            listFuncs.insert(0, qMakePair(func, item->viewOptions));
        }
        seekRoot = seekRoot.parent();
    }
    return listFuncs;
}

QList<TreeItem*> TreeManagerWidget::getListNodes(QModelIndex index)
{
    QList<TreeItem*> listNodes;
    QModelIndex seekRoot = index;
    while (seekRoot != QModelIndex()) {
        TreeItem *item = treeModel->getItem(seekRoot);
       listNodes.push_back(item);
        seekRoot = seekRoot.parent();
    }
    return listNodes;
}

void TreeManagerWidget::setMeshDocument(MeshDocument *md)
{
    TreeManagerWidget::md = md;
    // TODO connect signals
    connect(this, SIGNAL(viewThisItem(TreeItem*,vcg::CallBackPos*)), this->md, SLOT(updateTexture3DRgbaDataFromTF(TreeItem*,vcg::CallBackPos*)));
    connect(this, SIGNAL(viewSelectedItems(QList<TreeItem*>,vcg::CallBackPos*)), this->md, SLOT(updateTexture3DRgbaDataFromCombinedItems(QList<TreeItem*>,vcg::CallBackPos*)));
}

void TreeManagerWidget::selectionChanged(QItemSelection selected, QItemSelection deselected)
{
    // get the text of the selected item
    const QModelIndex index = treeView->selectionModel()->currentIndex();
    QModelIndex namecol = treeModel->index(index.row(), NAME_COL, index.parent());
    TreeItem *selectionItem = treeModel->getItem(namecol);
    if (selected.size() == 0) {
        emit selectedNodeChanged(NULL);
    } else {
        emit selectedNodeChanged(selectionItem);
    }
    switch (index.column()) {
    case TRANSFUNC_COL:{
        QString selectedText = index.data(Qt::DisplayRole).toString();
        // find out the hierarchy of the selected item
        int hierachyLevel = 1;
        QModelIndex seekRoot = index;
        while (seekRoot.parent() != QModelIndex()) {
            seekRoot = seekRoot.parent();
            hierachyLevel++;
        }
        QString showString = QString("%1, Level %2").arg(selectedText).arg(hierachyLevel);
        qDebug() << showString;
        break;
    }
    default:
        break;
    }
}

void TreeManagerWidget::dataChanged(QModelIndex topleft, QModelIndex bottomright)
{
    switch (topleft.column()) {
    case NAME_COL:{
        QModelIndex namecol = treeModel->index(bottomright.row(), NAME_COL, bottomright.parent());
        TreeItem *nameNode = treeModel->getItem(namecol);
        QString newName = nameNode->data(0).toString();
        if (nameNode->isDirty) {
            if (newName.left(1) != "*") {
                nameNode->setData(0, "*" + newName);
            }
        }
        updateViewTable();
        break;
    }
    }
}

void TreeManagerWidget::endTransferFunc(TransfuncTypes type)
{
    // TODO event when an transfer function completed
}

void TreeManagerWidget::onCustomContextMenuRequested(const QPoint &pos)
{
    QModelIndex clickIndex = treeView->indexAt(pos);
    if (clickIndex.isValid()) {
        QModelIndex index = treeView->currentIndex();
        if (index == QModelIndex()) return; // There is no need to show context menu if no node is selected
        QModelIndex namecol = treeModel->index(index.row(), NAME_COL, index.parent());
        TreeItem *item = treeModel->getItem(namecol);
        if (item) {
            //        showContexMenu(item, pos);
            showContexMenu(item, treeView->viewport()->mapToGlobal(pos));
        }
    }
}

void TreeManagerWidget::removeNode()
{
    QModelIndex index = treeView->currentIndex();
    if (index != QModelIndex()) {
        removeRow();
    }
}

void TreeManagerWidget::addToView()
{
    QModelIndex index = treeView->currentIndex();
    if (index != QModelIndex()) {
        QModelIndex namecol = treeModel->index(index.row(), NAME_COL, index.parent());
        TreeItem *item = treeModel->getItem(namecol);
        if (viewItems.indexOf(item) == -1) {
            viewItems.push_back(item);
        }
        updateViewTable();
    }
}

void TreeManagerWidget::exportMask()
{
    QModelIndex index = treeView->currentIndex();
    if (index != QModelIndex()) {
        QModelIndex namecol = treeModel->index(index.row(), NAME_COL, index.parent());
        TreeItem *selectionItem = treeModel->getItem(namecol);

        QString path = this->md->getDICOMPath();
        QDir patientDir(path);
        patientDir.cdUp();

        QString filter = "MeshBk opacity mask (*.mbk)";
        QString filename = QFileDialog::getSaveFileName(this, "Save to", patientDir.absolutePath() + "/opacityMask.mbk", filter, &filter);
        if (filename == "") return;
        QFile file(filename);
        file.open(QIODevice::WriteOnly);
        QDataStream out(&file);
        int maxIndex = selectionItem->maxIndex;
        out << (qint32) (maxIndex + 1);

        int index = 0;
        int count = 100;
        int step = maxIndex / count;
        if (step == 0) step = 1;
        count = maxIndex / step;

        for (int i = 0; i <= maxIndex; ++i) {
            out << (bool)selectionItem->opacity[i];
            if (i % step == 0) {
                QCallBack(100 * index * 1.0/ count, "Exporting mask data");
                index++;
            }
        }

        out.setVersion(QDataStream::Qt_4_8);
        file.close();
        progressbar->hide();
    }
}

void TreeManagerWidget::importMask()
{
    QModelIndex index = treeView->currentIndex();
    if (index != QModelIndex()) {
        QModelIndex namecol = treeModel->index(index.row(), NAME_COL, index.parent());
        TreeItem *selectionItem = treeModel->getItem(namecol);
        if (md  == NULL) return;
        QString path = this->md->getDICOMPath();
        QDir patientDir(path);
        patientDir.cdUp();

        QString filter = "MeshBk opacity mask (*.mbk)";
        QString fileName = QFileDialog::getOpenFileName(this, "Import Termination Data", patientDir.absolutePath(), filter, &filter);
        if (fileName == "")
            return;

        QFile file(fileName);
        file.open(QIODevice::ReadOnly);
        QDataStream in(&file);
        quint32 size;
        in >> size;

        int index = 0;
        int count = 100;
        int step = size / count;
        if (step == 0) step = 1;
        count = size / step;
        for (int i = 0; i < size; ++i) {
            bool mask;
            in >> mask;
            selectionItem->opacity[i] = mask;
            if (i % step == 0) {
                QCallBack(100 * index * 1.0/ count, "Importing mask data");
                index++;
            }
        }
        progressbar->hide();
        file.close();
        selectionItem->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
        emit selectedNodeChanged(selectionItem);
    }
}

void TreeManagerWidget::viewSpecificNode()
{
    const QModelIndex index = treeView->selectionModel()->currentIndex();
    if (index != QModelIndex()) {
        QModelIndex namecol = treeModel->index(index.row(), NAME_COL, index.parent());
        TreeItem *selectionItem = treeModel->getItem(namecol);
        emit viewButtonClick();
        emit viewThisItem(selectionItem, QCallBack);
    }
}

void TreeManagerWidget::updateViewTable()
{
    delete removeItemMapper;
    removeItemMapper = new QSignalMapper();

    viewTable->setRowCount(viewItems.size());

    for (int i = 0; i < viewItems.size(); i++) {
        TreeItem *item = viewItems.at(i);

        QLabel *name = new QLabel();
        name->setText(item->data(0).toString());
        viewTable->setCellWidget(i, 0, name);

        QPushButton *remove = new QPushButton();
        remove->setIcon(QIcon(":/images/delete.png"));
        remove->setIconSize(QSize(24, 24));
        viewTable->setCellWidget(i, 1, remove);
        connect(remove, SIGNAL(clicked(bool)), removeItemMapper, SLOT(map()));
        removeItemMapper->setMapping(remove, i);
    }

    connect(removeItemMapper, SIGNAL(mapped(int)), this, SLOT(removeViewItem(int)));
}

void TreeManagerWidget::removeViewItem(int index)
{
    removeItemMapper->removeMappings((QPushButton*) viewTable->cellWidget(index, 1));

    viewTable->removeRow(index);
    viewItems.removeAt(index);

    for (int i = 0; i < viewTable->rowCount(); i++) {
        removeItemMapper->setMapping((QPushButton *) viewTable->cellWidget(index, 1), index);
    }
}

void TreeManagerWidget::resetMask()
{
    QModelIndex index = treeView->currentIndex();
    if (index != QModelIndex()) {
        QModelIndex namecol = treeModel->index(index.row(), NAME_COL, index.parent());
        TreeItem *selectionItem = treeModel->getItem(namecol);
        int size = selectionItem->maxIndex + 1;
        int index = 0;
        int count = 100;
        int step = size / count;
        if (step == 0) step = 1;
        count = size / step;
        for (int i = 0; i < size; ++i) {
            selectionItem->opacity[i] = true;
            if (i % step == 0) {
                QCallBack(100 * index * 1.0/ count, "Resetting mask data");
                index++;
            }
        }
        progressbar->hide();
        selectionItem->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
    }
}

void TreeManagerWidget::on_splitButton_clicked()
{
//    insertChild();
    QModelIndex index = treeView->selectionModel()->currentIndex();
    QModelIndex namecol = treeModel->index(index.row(), NAME_COL, index.parent());
    QModelIndex transcol = treeModel->index(index.row(), TRANSFUNC_COL, index.parent());

    QString transfuncName = transcol.data(Qt::DisplayRole).toString();

    TreeItem *parent = treeModel->getItem(namecol);
    TreeItem *otherNode = newChildNode("Others", namecol);
    TreeItem *applyNode = newChildNode("New Organ", namecol);

    if (transfuncName == "Histogram") {
        emit startTransferFunc(TransfuncTypes::HISTOGRAM, parent, applyNode, otherNode);
    }
    if (transfuncName == "Probability Model") {
        emit startTransferFunc(TransfuncTypes::PROBABILITY_MODEL, parent, applyNode, otherNode);
    }
    if (transfuncName == "Region Growing") {
        emit startTransferFunc(TransfuncTypes::REGION_GROWING, parent, applyNode, otherNode);
    }
    if (transfuncName == "Histogram 2D") {
        emit startTransferFunc(TransfuncTypes::HISTOGRAM2D, parent, applyNode, otherNode);
    }
    if (transfuncName == "Median Filter") {
        emit startTransferFunc(TransfuncTypes::MEDIAN_FILTER, parent, applyNode, otherNode);
    }
    if (transfuncName == "Space Selection") {
        emit startTransferFunc(TransfuncTypes::SPACE_SELECTION, parent, applyNode, otherNode);
    }
    if (transfuncName == "Machine Learning Based") {
        emit startTransferFunc(TransfuncTypes::OPACITY_FILE, parent, applyNode, otherNode);
    }
    if (transfuncName == "Opacity From Mask") {
        emit startTransferFunc(TransfuncTypes::OPACITY_MASK, parent, applyNode, otherNode);
    }
    if (transfuncName == "None") {
        parent->transfuncOptions.type = TransfuncTypes::NONE;

        applyNode->transfunc = defaultTransferfunction;
        TransfuncOptions options;
        options.type = TransfuncTypes::NONE;
        NodeViewOptions viewOptions;
        viewOptions.colorTable = applyNode->colorTable;
        viewOptions.opacity = applyNode->opacity;
        applyNode->transfuncOptions = options;
        applyNode->viewOptions = viewOptions;

        TransfuncOptions optionsInverted;
        optionsInverted.type = TransfuncTypes::NONE;
        NodeViewOptions viewOptionInverted;
        viewOptionInverted.colorTable = otherNode->colorTable;
        viewOptionInverted.opacity = otherNode->opacity;
        otherNode->transfunc = defaultTransferfunction;
        otherNode->transfuncOptions = optionsInverted;
        otherNode->viewOptions = viewOptionInverted;

        applyNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
        otherNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
        progressbar->hide();
        return;
    }

}

void TreeManagerWidget::on_insertButton_clicked()
{
    insertRow();
}

void TreeManagerWidget::on_deleteButton_clicked()
{
    removeRow();
}

void TreeManagerWidget::on_viewButton_clicked()
{
    emit viewButtonClick();

    emit viewSelectedItems(viewItems, QCallBack);
}

bool TreeManagerWidget::QCallBack(const int pos, const char *str)
{
    progressbar->show();
    progressbar->setValue(pos);
}

void TreeManagerWidget::on_pushButton_clicked()
{
    if (viewItems.size() == 0) return;

    QString path = this->md->getDICOMPath();
    QDir patientDir(path);
    patientDir.cdUp();

    QString filter = "MeshBk opacity mask (*.mbk)";
    QString filename = QFileDialog::getSaveFileName(this, "Save to", patientDir.absolutePath() + "/opacityMask.mbk", filter, &filter);
    if (filename == "") return;
    QFile file(filename);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    int maxIndex = viewItems.at(0)->maxIndex;
    out << (qint32) (maxIndex + 1);

    int index = 0;
    int count = 100;
    int step = maxIndex / count;
    if (step == 0) step = 1;
    count = maxIndex / step;

    for (int i = 0; i < maxIndex; ++i) {
        bool flag = false;
        for (int j = 0; j < viewItems.size(); ++j) {
            flag = viewItems.at(j)->opacity[i];
            if (flag) break;
        }
        out << (bool)flag;
        if (i % step == 0) {
            QCallBack(100 * index * 1.0/ count, "Exporting mask data");
            index++;
        }
    }

    out.setVersion(QDataStream::Qt_4_8);
    file.close();
    progressbar->hide();
}
