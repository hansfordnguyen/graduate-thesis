﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMdiSubWindow>
#include <QFileDialog>
#include <QColorDialog>
#include <QStatusBar>
#include <QProgressBar>
#include <QThread>
#include <QColorDialog>
#include <QColor>
#include <QInputDialog>
#include <common/interfaces.h>
#include <common/pluginmanager.h>
#include <common/mlexception.h>
#include "multiViewer_Container.h"
#include <QMdiArea>
#include <QToolBar>
#include <QLabel>
#include <QPushButton>
#include "glarea.h"
#include "stdpardialog.h"
#include "samplefilter.h"
#include <utils_opendiomsfolder/dicoms2ply.h>
#include <vcg/complex/algorithms/inertia.h>
#include <vcg/space/color4.h>
#include <vcg/space/plane3.h>
#include <vcg/space/deprecated_point3.h>
#include <vcg/complex/algorithms/intersection.h>
#include <vcglib/wrap/gui/coordinateframe.h>
#include <vcglib/wrap/gl/glu_tessellator_cap.h>
#include "treemanagerwidget.h"
#include "detectboundary.h"

namespace Ui {
class MainWindow;
}

// Class MainWindow inherits from widget QMainWindow
class MainWindow : public QMainWindow {
    Q_OBJECT // This macro used as an flag to inform the compiler to generate appropriate codes.
    // This macro MUST be included in classes which use signals and slots

public:

    //CONSTRUCTOR AND DESTRUCTOR
    // Keyword "explicit" to prevent the compiler from doing implicit conversion.
    explicit MainWindow();
    ~MainWindow();

    // PUBLIC FUNCTIONS
    /** TODO: no explanation
     * @brief QCallBack
     * @param pos
     * @param str
     * @return
     */
    static bool QCallBack(const int pos, const char * str);

    /** TODO: no explanation
     * @brief loadMesh
     * @param fileName
     * @param pCurrentIOPlugin
     * @param mm
     * @param mask
     * @param prePar
     * @return
     */
    bool loadMesh(const QString& fileName,
                  MeshIOInterface *pCurrentIOPlugin,
                  MeshModel* mm,int& mask,
                  RichParameterSet* prePar);

    /** TODO: no explanation
     * @brief newProject
     * @param projName
     * @return
     */
    GLArea* createNewProject(const QString& projName = QString());

    bool importMesh(QString name, QList<cv::Point3f> points, vcg::CallBackPos *cb = 0);
    bool importMesh(QString name, QList<vcg::Point3f> points, vcg::CallBackPos *cb = 0);

    /**
     * @brief importMesh: handle loading a mesh and build up data structure
     * @param fileName: name of the mesh file (in our case, they are .ply files)
     * @return true if the loading succeed
     */
    bool importMesh(QString fileName);

    bool createEmptyMesh(QString = "Untitled");

    // When placing keyword const after function declaration, it means that the implicit "this" will become const
    // and, therefore, this function cannot change member data
    // FMI, the function declaration int Foo::Bar(int random_arg) is actually int Foo::Bar(Foo* this, int random_arg)
    /**
     * @brief GLA get the current gla object
     * @return
     */
    GLArea *GLA() const {
        MultiViewer_Container *mvc = currentViewContainer();
        if(!mvc) return 0;
        GLArea *glw =  qobject_cast<GLArea*>(mvc->currentView());
        return glw;
    }

    /**
     * @brief currentViewContainer: get the current multiview_container, which holds a GLA object
     * @return
     */
    MultiViewer_Container* currentViewContainer() const {
        MultiViewer_Container *mvc = qobject_cast<MultiViewer_Container *>(mdiarea->currentSubWindow());
        if(mvc) return mvc;
        if(mvc == 0 && mdiarea->currentSubWindow() != 0 ){
            mvc = qobject_cast<MultiViewer_Container *>(mdiarea->currentSubWindow()->widget());
            if(mvc) return mvc;
        }
        QList<QMdiSubWindow *> subwinList=mdiarea->subWindowList();
        foreach(QMdiSubWindow *subwinPtr,subwinList)
        {
            MultiViewer_Container *mvc = qobject_cast<MultiViewer_Container *>(subwinPtr);
            if(mvc) return mvc;
            if(mvc==0 && subwinPtr!=0){
                mvc = qobject_cast<MultiViewer_Container *>(subwinPtr->widget());
                if(mvc) return mvc;
            }
        }
        return 0;
    }

    /**
     * @brief meshDoc: get the MeshDocument object which belongs to the current active multiview_container
     * FMI, MeshDocument is the data structure holds the MeshModle which, in turn, store all data of the working mesh
     * @return
     */
    MeshDocument *meshDoc() {
        // when the expression of "assert" is false (or 0), an standard error will be thrown out, and the program
        // will be terminated
        assert(currentViewContainer());
        return &currentViewContainer()->meshDoc;
    }

    static QStatusBar *&globalStatusBar()
    {
        static QStatusBar *_qsb=0;
        return _qsb;
    }

    /**
     * @brief initToolBar: generate all available tools (get from plugin manager)
     */
    void initToolBar();

    /**
     * @brief initMenus: register events for menu items
     */
    void initMenus();

    /**
     * @brief initProject: init empty project //TODO just for test. Remove later
     */
    void initProject();
    /**
     * @brief initLayerDock: init right hand side dock which contains all layers (meshes)
     */
    void initLayerDock();

    /**
     * @brief initStatusBar init status bar with progress bar
     */
    void initStatusBar();

    /**
     * @brief openDICOMFolder: Handler for importing mesh from DICOM folder
     */
    void openDICOMFolder();

    vcg::GLW::TextureMode getBestTextureRenderModePerMesh(const int meshid);
    void setBestTextureModePerMesh(RenderModeAction* textact,const int meshid, RenderMode& rm);

    /**
     * @brief toggleLayerVisibility: turn a layer on or off
     * @param meshIndex: index of selected mesh in layerdock (start from 0)
     */
    void toggleLayerVisibility(int meshIndex);

    void toggleVolumeVisibility(int meshIndex);

    /**
     * @brief deleteLayer: delete a layer (mesh)
     * @param meshIndex: index of selected mesh in layerdock (start from 0)
     */
    void deleteLayer(int meshIndex);
    /**
     * @brief turnAllLightsOn: This function used to make sure that lights of all meshes has been turned on
     * after exporting
     */
    void turnAllLightsOn();
    void setBackgroundColor(QColor color);
    void initVolumePropertyDock();

    // PUBLIC ATTRIBUTE
    QMdiArea *mdiarea; // This widget allow user to create multiple sub-window inside
    // Manager for all plugins of the system, including io, edit, filer, etc.
    // Every feature in MeshBk is built as a plugin
    PluginManager PM;
    // Test randomize vertices plugin
    QDir lastUsedDirectory;  //This will hold the last directory that was used to load/save a file/project in
    // Every plugin need a so called "default parms" to be initialized. However, at this time, we do not need any
    // params like these
    RichParameterSet currentGlobalParams;
    RichParameterSet defaultGlobalParams;
    // Main toolbar of the program
    QToolBar *mainToolBar;
    MeshFilterInterface *poissonFilter;
    MeshFilterInterface *normalGenerateFilter;
    MeshFilterInterface *coloringFilter;
    MeshFilterInterface *invertFaceOrientationFilter;
    MeshRenderInterface *xrayShaderRender;

    QAction *coloringAction; // mabe coloringToolBar
    QAction *xrayShaderAction;
    QAction *poissonToolBar;
    QAction *normalGenerateToolBar;
    QAction *invertFaceOrientationToolBar;
    QAction *sliceToolAction;
    //    QAction *editPaintToolBar;
    // Actions for view mode
    QActionGroup *renderModeGroupAct;
    RenderModePointsAction *renderModePointsAct;
    RenderModeWireAction *renderModeWireAct;
    RenderModeFlatAction *renderModeFlatAct;
    RenderModeSmoothAction *renderModeSmoothAct;
    QSlider *backgroundColorSlider;
    TreeManagerWidget *treeManager;
    TreeItem *currentTreeItem;

private:
    Ui::MainWindow *ui;
    QThread *openFolderThread;
    static QProgressBar *qb;
    QColorDialog *colorDialog;
    QColor backupColor; // Backup color when user choose to cancel their coloring
    QColor defaultFaceColor;

    void backupCurrentFaceColor();
    void restorePreviousFaceColor();
public slots:
    void updateMenus();
    void applyEditMode();
    void suspendEditMode();
    void applyPoissonFilter();
    void applyNormalGenerateFilter();
    void applyColoringFilter(const QColor &);
    void applyInvertFacesOrientationFilter();
    void updateRenderMode();
    void applyXrayShaderRender();
    void resetRender();
    void startTransferFunc(TransfuncTypes transfuncType, TreeItem*, TreeItem*, TreeItem*);
    void endTransferFunc(TransfuncTypes type);
signals:
    void dispatchCustomSettings(RichParameterSet& rps);
    void updateLayerTable();
private slots:
    void onExitMenuClick();
    void onApplyFilterMenuClick();
    void onMenuBoundaryPointClick();
    void onOpenFileMenuClick();
    void onOpenFolderMenuClick();
    void onColoringMenuClick();
    void onButtonImportMeshClick();
    void onButonImportDICOMClick();
    void onlayerClick(int row, int col);
    void onMenuViewMeshLayerClick();
    void onLayerDockToggle(bool isShown);
    void onVolumePropertyDockToggle(bool isshown);
    void onTreeManagerDockToggle(bool isShown);
    void onSliceDockToggle(bool isShown);
    void onEditLayerName(int row, int column);
    void setProgressBusyMode();
    void setProgressPercentMode();
    void updateImportMesh(QString);
    void onStatisticButtonClick();
    void cancelColoringFilter();
    void mapLayerEvents(int index);
    void onMenuXrayShaderChanged();
    void onBackgroundChange(int value);
    void onResetBackground();
    void showTrackBall();
    void on_menuViewAxis_triggered(bool checked);
    void on_actionImport_DICOM_triggered();
    void on_menuViewVolumeProperty_triggered(bool checked);
    void on_menuViewVolume_triggered(bool checked);
    void updateLayerDock();
    void updateGLArea();
    void on_menuViewTreeManager_triggered(bool checked);
    void on_menuViewSliceView_triggered(bool checked);
    void changeCurrentTreeItem(TreeItem *item);
    void on_actionSlice_Tool_triggered();
    void onMenuGeneratePointCloudClick();
    void on_actionOpen_project_triggered();

    void on_actionSave_Project_triggered();

    void on_actionSave_Project_as_triggered();

public:
    QList<vcg::Point3f> detectBoundaryPoints(TreeItem *item);
    QList<Point3i> detectBoundaryPoints(TreeItem *item, DetectBoundary::StartDirection direction);
    QList<vcg::Point3f> applyPCA(QList<vcg::Point3f> boundaryPoints, int dim);
    void printMatrix(cv::Mat matrix);
    void showNormalMatrix(cv::Mat mat, QString name);
    TreeItem* readNode(QString dataPath, QDomElement element, QModelIndex index, QList<QFile*> *fileList, QList<TreeItem*> *nodeList);
    void writeNode(QString pathData, QDomDocument document, QDomElement parent, TreeItem* item, int &level);
    QString writeOpacity(QString pathData, TreeItem* item, int &level);
    QString writeIntensity(QString pathData);
    void saveProject(QString filename);
public:
    vcg::Point3i paddingIndex(vcg::Point3i coord, int width, int height, int numSlice);
};

#endif // MAINWINDOW_H
