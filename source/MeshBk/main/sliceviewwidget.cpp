#include "sliceviewwidget.h"
#include "ui_sliceviewwidget.h"

#include <QFileDialog>
QProgressBar *SliceViewWidget::progressbar;
SliceViewWidget::SliceViewWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SliceViewWidget)
{
    ui->setupUi(this);
    sliceView = ui->sliceView;

    QSlider *toleranceSlider = ui->toleranceSlider;
    toleranceSlider->setMaximum(100);
    toleranceSlider->setValue(30);
    ui->sliceView->tolerance = 30;
    connect(toleranceSlider, SIGNAL(valueChanged(int)), sliceView, SLOT(toleranceChanged(int)));
    ui->applyTFButton->setVisible(false); // Hide apply TF as default

    QMap<int, bool> map;
    map[0] = true;
    if (map[1]) qDebug() << "Hello there";
    QMap<int, bool>::Iterator it = map.begin();
    // Hide tf toolbox as default
    setToolboxVisibility(false);
    progressbar = ui->progressbar;
    progressbar->hide(); // Hide progressbar as default

    connect(ui->brightnessSlider, SIGNAL(valueChanged(int)), this, SLOT(onBrightnessChanged(int)));
}

SliceViewWidget::~SliceViewWidget()
{
    delete ui;
    delete gradientManigtude;
    delete termindationData;
}
void SliceViewWidget::setMeshDocument(MeshDocument *md){
    this->md = md;
    sliceView->md = this->md;
}

void SliceViewWidget::on_clearSelectionButton_clicked()
{
    ui->sliceView->selections.clear();
    ui->sliceView->selectionPoints.clear();
    ui->sliceView->selectionFlag.clear();
    ui->sliceView->update();
}

void SliceViewWidget::on_startSelectButton_clicked(bool checked)
{
    if (checked) {
        sliceView->clearAllSelectionData();
        sliceView->currentSlectionMode = SliceView::SelectionMode::CIRCLE;
    } else {
        sliceView->currentSlectionMode = SliceView::SelectionMode::NONE;
    }
}

QMap<int, bool> SliceViewWidget::neighborGrowing() {
//    calcGradientArray(NULL);
    bool *opacityMask = currentParentNode->opacity;
    QMap<int, bool> indexMap = sliceView->getIndexMap();
    long int total = 0;
    if (indexMap.size() == 0) return indexMap;
    if (md  == NULL) return indexMap;
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int maxIndex = wp * hp * n - 1;
    std::queue<int> queue;
    // Calculate mean & initialize queue
    QMap<int, bool>::Iterator it = indexMap.begin();
    unsigned long int sum= 0;
    for(; it != indexMap.end(); ++it) {
        // Calculate sum
        if (it.key() >= 0 && it.key() <= maxIndex && it.value()) {
            sum += md->globalTexture3DIntensityData[it.key()];
            total++;

            // Only compare with opacity of parent node
            if (opacityMask && opacityMask[it.key()] == 1)
                queue.push(it.key());
        } else {
            indexMap[it.key()] = false;
            qDebug() << "ERROR: neightborGrowing index out of range: " + QString::number(it.key());
        }
    }
    float mean = sum * 1.0 / total;
    vcg::Point3i raidus = vcg::Point3i(1, 1, 1);
    while (queue.size() > 0) {
        int currentIndex = queue.front();
        QList<int> neighbors = getNeighbors(currentIndex, wp, hp, n, raidus);
        std::vector<int> matches;
        foreach (int index, neighbors) {
            if (isRelevant(mean, index)) {
                matches.push_back(index);
            }
        }
        float percent = matches.size() * 1.0 / 26;
        for (int i = 0; i < matches.size(); ++i) {
           if (isGrowing(percent, matches.at(i)) && !indexMap[matches.at(i)]) {
               queue.push(matches.at(i));
           }
           // Recalculate mean
           if (!indexMap[matches.at(i)]){
               sum += md->globalTexture3DIntensityData[matches.at(i)];
               total++;
               mean = sum * 1.0 / total;
           }
           indexMap[matches.at(i)] = true * opacityMask[matches.at(i)];
        }
        queue.pop();
    }
    return indexMap;
}

bool SliceViewWidget::isRelevant(int mean, int index) {
    if (!currentParentNode) return false;
    if (currentParentNode->opacity[index] == 0) return false;
    if (ui->meanCheckbox->isChecked()) {
        if (md == NULL) {
            qDebug() << "ERROR: isRelevant md is NULL";
            return false;
        }
        int offset = ui->offsetSpinbox->value();
        int intens = md->globalTexture3DIntensityData[index];
        return (intens >= (mean - offset) && intens <= (mean + offset));
    } else return true;
}

void SliceViewWidget::startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *applyNode, TreeItem *otherNode) {
    switch (type) {
    case TransfuncTypes::REGION_GROWING:{
        currentParentNode = parentNode;
        currentApplyNode = applyNode;
        currentOtherNode = otherNode;
        currentTransFuncType = type;
        setToolboxVisibility(true);
        break;
    }
    default:
        break;
    }
    ui->applyTFButton->setVisible(true);
}

void SliceViewWidget::on_applyTFButton_clicked()
{
    switch (currentTransFuncType) {
    case TransfuncTypes::REGION_GROWING:{
        QMap<int, bool> map = sliceView->getIndexMap();
        if (map.size() == 0) {
            QMessageBox::warning(this, "Error", "Please select seed points before starting");
            return;
        }
        if (ui->dissimilarityCheckbox->isChecked() && termindationData == NULL) {
            QMessageBox::warning(this, "Error", "Please prepare dissimilairty data before starting");
            return;
        }
        if (ui->gradientCheckbox->isChecked() && gradientManigtude == NULL) {
            calcGradientArray(NULL);
        }
        QMap<int, bool> indexMap = neighborGrowing();
        QMap<int, bool>::Iterator it = indexMap.begin();
        // Reset opacity mask of its children
        for (int i = 0; i <= currentApplyNode->maxIndex; i++) {
            currentApplyNode->opacity[i] = 0;
            currentOtherNode->opacity[i] = 1 * currentParentNode->opacity[i];
        }
        for(; it != indexMap.end(); ++it) {
            if (indexMap[it.key()]) {
                currentApplyNode->opacity[it.key()] = 1;
                currentOtherNode->opacity[it.key()] = 0;
            }
        }

        TransfuncOptions options;
        options.type = TransfuncTypes::REGION_GROWING;
        NodeViewOptions viewOptions;
        viewOptions.opacity = currentApplyNode->opacity;
        viewOptions.colorTable = currentApplyNode->colorTable;
        currentApplyNode->transfunc = tfRegionGrowing;
        currentApplyNode->transfuncOptions = options;
        currentApplyNode->viewOptions = viewOptions;
        currentApplyNode->updateHistogram(md->globalTexture3DIntensityData);


        TransfuncOptions optionsInverted;
        optionsInverted.type = TransfuncTypes::REGION_GROWING;
        NodeViewOptions viewOptionsInverted;
        viewOptionsInverted.opacity = currentOtherNode->opacity;
        viewOptionsInverted.colorTable = currentOtherNode->colorTable;
        currentOtherNode->transfunc = tfRegionGrowing;
        currentOtherNode->transfuncOptions = optionsInverted;
        currentOtherNode->viewOptions = viewOptionsInverted;
        currentOtherNode->updateHistogram(md->globalTexture3DIntensityData);

        sliceView->clearAllSelectionData();
        if (ui->startSelectButton->isChecked()) {
            ui->startSelectButton->setChecked(false);
            sliceView->currentSlectionMode = SliceView::SelectionMode::NONE;
        }
        // Hide toolbox
        setToolboxVisibility(false);
        break;
    }
    default:
        break;
    }

    ui->applyTFButton->setVisible(false);
    emit endTransferFunc(currentTransFuncType);
    // Reset all nodes
    currentApplyNode = NULL;
    currentOtherNode = NULL;
    currentParentNode = NULL;
    currentTransFuncType = TransfuncTypes::NONE;
}

float SliceViewWidget::gradient(int index)
{
    if (md  == NULL) return 0;
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int maxIndex = wp * hp * n - 1;

    int i0, i1, i2;
    int i3, i4, i5;
    int i6, i7, i8;
    int a0, a1, a2, a3, a4, a5, a6, a7, a8;
    i0 = index - wp - 1; if(i0 > maxIndex || i0 < 0)  a0 = 0; else a0 = md->globalTexture3DIntensityData[i0];
    i1 = index - wp; if(i1 > maxIndex || i1 < 0)  a1 = 0; else a1 = md->globalTexture3DIntensityData[i1];
    i2 = index - wp + 1; if(i2 > maxIndex || i2 < 0)  a2 = 0; else a2 = md->globalTexture3DIntensityData[i2];

    i3 = index - 1;if(i3 > maxIndex || i3 < 0)  a3 = 0; else a3 = md->globalTexture3DIntensityData[i3];
    i4 = index; if(i4 > maxIndex || i4 < 0)  a4 = 0; else a4 = md->globalTexture3DIntensityData[i4];
    i5 = index + 1; if(i5 > maxIndex || i5 < 0)  a5 = 0; else a5 = md->globalTexture3DIntensityData[i5];

    i6 = index + wp - 1; if(i6 > maxIndex || i6 < 0)  a6 = 0; else a6 = md->globalTexture3DIntensityData[i6];
    i7 = index + wp; if(i7 > maxIndex || i7 < 0)  a7 = 0; else a7 = md->globalTexture3DIntensityData[i7];
    i8 = index + wp + 1; if(i8 > maxIndex || i8 < 0)  a8 = 0; else a8 = md->globalTexture3DIntensityData[i8];

    int gradX = -1 * a0 + a2 + -2 * a3 + 2 * a5 + -1 * a6 + a8;
    int gradY = -1 * a0 + -2 * a1 + -1 * a2 + a6 + 2 * a7 + a8;

    i0 = index - 1 - wp * hp;if(i0 > maxIndex || i0 < 0)  a0 = 0; else a0 = md->globalTexture3DIntensityData[i0];
    i1 = index - wp * hp; if(i1 > maxIndex || i1 < 0)  a1 = 0; else a1 = md->globalTexture3DIntensityData[i1];
    i2 = index + 1 - wp * hp; if(i2 > maxIndex || i2 < 0)  a2 = 0; else a2 = md->globalTexture3DIntensityData[i2];

    i3 = index - 1; if(i3 > maxIndex || i3 < 0)  a3 = 0; else a3 = md->globalTexture3DIntensityData[i3];
    i4 = index; if(i4 > maxIndex || i4 < 0)  a4 = 0; else a4 = md->globalTexture3DIntensityData[i4];
    i5 = index + 1; if(i5 > maxIndex || i5 < 0)  a5 = 0; else a5 = md->globalTexture3DIntensityData[i5];

    i6 = index - 1 + wp * hp; if(i6 > maxIndex || i6 < 0)  a6 = 0; else a6 = md->globalTexture3DIntensityData[i6];
    i7 = index + wp * hp; if(i7 > maxIndex || i7 < 0)  a7 = 0; else a7 = md->globalTexture3DIntensityData[i7];
    i8 = index + 1 + wp * hp; if(i8 > maxIndex || i8 < 0)  a8 = 0; else a8 = md->globalTexture3DIntensityData[i8];

    float gradMag = qSqrt(qPow(gradX, 2) + qPow(gradY, 2));
    return gradMag;
}

void SliceViewWidget::prepareTerminateDate(vcg::CallBackPos *cb)
{
    if (md  == NULL) return;
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int length = wp * hp * n;
    if (!termindationData) termindationData = new float[length];
    int offset = 1024;
    // Define parameters
    int k = 5;
    int sigmaPow = 500;
    double alpha = 0.01;
    signed short* intensityData = md->globalTexture3DIntensityData;

    int idx = 0;
    int count = 100;
    int step = length / count;
    if (step == 0) step = length;
    count = length / step;

    // Prepare data
    for (int index = 0; index < length; ++index) {
        vcg::Point3i currentCoord = indexToCoords(index, wp, hp);
        int x = currentCoord.X(), y = currentCoord.Y(), z = currentCoord.Z();
        double part1Sum = 0, counterPart1Sum = 0; // x1
        double part2Sum = 0, counterPart2Sum = 0; // x2
        double part3Sum = 0, counterPart3Sum = 0; // z1
        double part4Sum = 0, counterPart4Sum = 0; // z2
        double part5Sum = 0, counterPart5Sum = 0; // z3
        double part6Sum = 0, counterPart6Sum = 0; // z4
        double part7Sum = 0, counterPart7Sum = 0; // x
        double part8Sum = 0, counterPart8Sum = 0; // y
        double part9Sum = 0, counterPart9Sum = 0; // z

        for (int i = 1; i <= k; ++i) {
            // 2 parts for x1
            vcg::Point3i coord1 = paddingIndex(vcg::Point3i(x - i, y - i, z), wp, hp, n);
            part1Sum += intensityData[coordsToIndex(coord1, wp, hp)] - offset;
            coord1 = paddingIndex(vcg::Point3i(x + i, y + i, z), wp, hp, n);
            counterPart1Sum += intensityData[coordsToIndex(coord1, wp, hp)] - offset;
            // 2 parts for x2
            vcg::Point3i coord2 = paddingIndex(vcg::Point3i(x + i, y - i, z), wp, hp, n);
            part2Sum += intensityData[coordsToIndex(coord2, wp, hp)] - offset;
            coord2 = paddingIndex(vcg::Point3i(x - i, y + i, z), wp, hp, n);
            counterPart2Sum += intensityData[coordsToIndex(coord2, wp, hp)] - offset;
            // 2 parts for z1
            vcg::Point3i coord3 = paddingIndex(vcg::Point3i(x - i, y - i, z - i), wp, hp, n);
            part3Sum += intensityData[coordsToIndex(coord3, wp, hp)] - offset;
            coord3 = paddingIndex(vcg::Point3i(x + i, y + i, z + i), wp, hp, n);
            counterPart3Sum += intensityData[coordsToIndex(coord3, wp, hp)] - offset;
            // 2 parts for z2
            vcg::Point3i coord4 = paddingIndex(vcg::Point3i(x + i, y - i, z - i), wp, hp, n);
            part4Sum += intensityData[coordsToIndex(coord4, wp, hp)] - offset;
            coord4 = paddingIndex(vcg::Point3i(x - i, y + i, z + i), wp, hp, n);
            counterPart4Sum += intensityData[coordsToIndex(coord4, wp, hp)] - offset;
            // 2 parts for z3
            vcg::Point3i coord5 = paddingIndex(vcg::Point3i(x + i, y + i, z - i), wp, hp, n);
            part5Sum += intensityData[coordsToIndex(coord5, wp, hp)] - offset;
            coord5 = paddingIndex(vcg::Point3i(x - i, y - i, z + i), wp, hp, n);
            counterPart5Sum += intensityData[coordsToIndex(coord5, wp, hp)] - offset;
            // 2 parts for z4
            vcg::Point3i coord6 = paddingIndex(vcg::Point3i(x - i, y + i, z - i), wp, hp, n);
            part6Sum += intensityData[coordsToIndex(coord6, wp, hp)] - offset;
            coord6 = paddingIndex(vcg::Point3i(x + i, y - i, z + i), wp, hp, n);
            counterPart6Sum += intensityData[coordsToIndex(coord6, wp, hp)] - offset;
            // 2 parts for x
            vcg::Point3i coord7 = paddingIndex(vcg::Point3i(x - i, y, z), wp, hp, n);
            part7Sum += intensityData[coordsToIndex(coord7, wp, hp)] - offset;
            coord7 = paddingIndex(vcg::Point3i(x + i, y, z), wp, hp, n);
            counterPart7Sum += intensityData[coordsToIndex(coord7, wp, hp)] - offset;
            // 2 parts for y
            vcg::Point3i coord8 = paddingIndex(vcg::Point3i(x, y - i, z), wp, hp, n);
            part8Sum += intensityData[coordsToIndex(coord8, wp, hp)] - offset;
            coord8 = paddingIndex(vcg::Point3i(x, y + i, z), wp, hp, n);
            counterPart8Sum += intensityData[coordsToIndex(coord8, wp, hp)] - offset;
            // 2 parts for z // TODO padding indx for z
            vcg::Point3i coord9 = paddingIndex(vcg::Point3i(x, y, z - i), wp, hp, n);
            part9Sum += intensityData[coordsToIndex(coord9, wp, hp)] - offset;
            coord9 = paddingIndex(vcg::Point3i(x, y, z + i), wp, hp, n);
            counterPart9Sum += intensityData[coordsToIndex(coord9, wp, hp)] - offset;
        } // end k iterating

        double coefficient1 = qPow((part1Sum - counterPart1Sum) * 1.0 / k, 2);
        double coefficient2 = qPow((part2Sum - counterPart2Sum) * 1.0 / k, 2);
        double coefficient3 = qPow((part3Sum - counterPart3Sum) * 1.0 / k, 2);
        double coefficient4 = qPow((part4Sum - counterPart4Sum) * 1.0 / k, 2);
        double coefficient5 = qPow((part5Sum - counterPart5Sum) * 1.0 / k, 2);
        double coefficient6 = qPow((part6Sum - counterPart6Sum) * 1.0 / k, 2);
        double coefficient7 = qPow((part7Sum - counterPart7Sum) * 1.0 / k, 2);
        double coefficient8 = qPow((part8Sum - counterPart8Sum) * 1.0 / k, 2);
        double coefficient9 = qPow((part9Sum - counterPart9Sum) * 1.0 / k, 2);
        double sigPow1 = (alpha * (intensityData[index] + part1Sum + counterPart1Sum) / qPow(2 * k + 1, 2)) * (intensityData[index] + part1Sum + counterPart1Sum);
        double sigPow2 = (alpha * (intensityData[index] + part2Sum + counterPart2Sum) / qPow(2 * k + 1, 2)) * (intensityData[index] + part2Sum + counterPart2Sum);
        double sigPow3 = (alpha * (intensityData[index] + part3Sum + counterPart3Sum) / qPow(2 * k + 1, 2)) * (intensityData[index] + part3Sum + counterPart3Sum);
        double sigPow4 = (alpha * (intensityData[index] + part4Sum + counterPart4Sum) / qPow(2 * k + 1, 2)) * (intensityData[index] + part4Sum + counterPart4Sum);
        double sigPow5 = (alpha * (intensityData[index] + part5Sum + counterPart5Sum) / qPow(2 * k + 1, 2)) * (intensityData[index] + part5Sum + counterPart5Sum);
        double sigPow6 = (alpha * (intensityData[index] + part6Sum + counterPart6Sum) / qPow(2 * k + 1, 2)) * (intensityData[index] + part6Sum + counterPart6Sum);
        double sigPow7 = (alpha * (intensityData[index] + part7Sum + counterPart7Sum) / qPow(2 * k + 1, 2)) * (intensityData[index] + part7Sum + counterPart7Sum);
        double sigPow8 = (alpha * (intensityData[index] + part8Sum + counterPart8Sum) / qPow(2 * k + 1, 2)) * (intensityData[index] + part8Sum + counterPart8Sum);
        double sigPow9 = (alpha * (intensityData[index] + part9Sum + counterPart9Sum) / qPow(2 * k + 1, 2)) * (intensityData[index] + part9Sum + counterPart9Sum);
        // New method: calculate distinct sigma for each voxel
        double value = 1 - (1.0 / 9) * (qExp(-coefficient1 * 1.0 / (2 *sigPow1))
                            +  qExp(-coefficient2 * 1.0 / (2 * sigPow2))
                            + qExp(-coefficient3 * 1.0 / (2 * sigPow3))
                                        + qExp(-coefficient4 * 1.0 / (2 * sigPow4))
                                        + qExp(-coefficient5 * 1.0 / (2 * sigPow5))
                                        + qExp(-coefficient6 * 1.0 / (2 * sigPow6))
                                        + qExp(-coefficient7 * 1.0 / (2 * sigPow7))
                                        + qExp(-coefficient8 * 1.0 / (2 * sigPow8))
                                        + qExp(-coefficient9 * 1.0 / (2 * sigPow9)));
    // Old method: using sigma as constant value
//        double value = 1 - (1.0 / 9) * (qExp(-coefficient1 * 1.0 / (2 *sigmaPow))
//                                   +  qExp(-coefficient2 * 1.0 / (2 * sigmaPow))
//                                   + qExp(-coefficient3 * 1.0 / (2 * sigmaPow))
//                                               + qExp(-coefficient4 * 1.0 / (2 * sigmaPow))
//                                               + qExp(-coefficient5 * 1.0 / (2 * sigmaPow))
//                                               + qExp(-coefficient6 * 1.0 / (2 * sigmaPow))
//                                               + qExp(-coefficient7 * 1.0 / (2 * sigmaPow))
//                                               + qExp(-coefficient8 * 1.0 / (2 * sigmaPow))
//                                               + qExp(-coefficient9 * 1.0 / (2 * sigmaPow)));
        termindationData[index] = value;
//        if (value > 0.8)
//            qDebug() << "Value: " + QString::number(value);
        if (index % step == 0) {
            if (cb)
                cb(100 * idx * 1.0/ count, "Building termination data");
            idx++;
        }
    }
}

void SliceViewWidget::setToolboxVisibility(bool state)
{
    ui->tfToolbox->setVisible(state);
}

vcg::Color4f tfRegionGrowing(signed short* intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (options.opacity[index] == 1) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

void SliceViewWidget::calcGradientArray(vcg::CallBackPos *cb) {
    if (md == NULL) {
        qDebug() << "calcGradientArray: md NULL";
        return;
    }
    if (gradientManigtude != NULL) {
        return; // stuff already done
    }
    signed short *inputRawIntensity = md->globalTexture3DIntensityData;
    int sliceCols = md->measurementPixel.X();
    int sliceRows = md->measurementPixel.Y();
    int numOfSlice = md->measurementPixel.Z();
    float xSpace = md->measurementMillimeter.X() / ((float)sliceCols);
    float ySpace = md->measurementMillimeter.Y() / ((float)sliceRows);
    float zSpace = md->measurementMillimeter.Z() / ((float)(numOfSlice-1));

    int size = sliceRows * sliceCols * numOfSlice;
/*
 * Step 1: Build all cv::Mat (Oxy for Ox, Oy) and (Oyz for Oz)
 * */
    int currentProgress = 0;
    int value = 0;

    // Build Ox, Oy openCV Matrix
    QList<cv::Mat> xyMats;

    for (int i = 0; i < numOfSlice; i++) {
        value = (i*1.0 /(numOfSlice - 1) * 20);
        if (value - currentProgress > 0) {
            currentProgress = value;
            if (cb)
                cb(currentProgress, "");
        }

        cv::Mat mat(sliceRows, sliceCols, CV_32FC1);

        for (int y = 0; y < sliceRows; y++) {
            for(int x = 0; x < sliceCols; x++) {
                mat.at<float>(y, x) = (float)inputRawIntensity[i*sliceRows*sliceCols + y*sliceCols + x];
            }
        }

        xyMats.push_back(mat);
    }

    // Build Oz openCV Matrix
    int ozNumOfSlice = sliceCols;
    int ozSliceCols = sliceRows;
    int ozSliceRows = numOfSlice;

    QList<cv::Mat> zMats;
    for (int i = 0; i < ozNumOfSlice; i++) {
        value = (i*1.0 /(ozNumOfSlice - 1) * 20) + 20;
        if (value - currentProgress > 0) {
            currentProgress = value;
            if (cb)
                cb(currentProgress, "");
        }

        cv::Mat mat(ozSliceRows, ozSliceCols, CV_32FC1);

        for (int y = 0; y < ozSliceRows; y++) {
            for (int x = 0; x < ozSliceCols; x++) {
                mat.at<float>(y, x) = inputRawIntensity[y*sliceRows*sliceCols + x*sliceCols + i];
            }
        }

        zMats.push_back(mat);
    }


/*
 * Step 2: Apply sobel to get gradient Ox, Oy, Oz
 * */
    float* gradientX = new float[size];
    float* gradientY = new float[size];
    float* gradientZ = new float[size];

    for (int i = 0; i < numOfSlice; i++) {
        value = (i*1.0 /(numOfSlice - 1) * 20) + 40;
        if (value - currentProgress > 0) {
            currentProgress = value;
            if (cb)
                cb(currentProgress, "");
        }

        cv::Mat sobelX, sobelY;

        cv::Sobel(xyMats.at(i), sobelX, CV_32FC1, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
        cv::Sobel(xyMats.at(i), sobelY, CV_32FC1, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);

        for (int y = 0; y < sliceRows; y++) {
            for (int x = 0; x < sliceCols; x++) {
                gradientX[i*sliceRows*sliceCols + y*sliceCols + x] = sobelX.at<float>(y, x);
                gradientY[i*sliceRows*sliceCols + y*sliceCols + x] = sobelY.at<float>(y, x);
            }
        }
    }

    for (int i = 0; i < ozNumOfSlice; i++) {
        value = (i*1.0 /(ozNumOfSlice - 1) * 20) + 60;
        if (value - currentProgress > 0) {
            currentProgress = value;
            if (cb)
                cb(currentProgress, "");
        }

        cv::Mat sobelZ;

        cv::Sobel(zMats.at(i), sobelZ, CV_32FC1, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);

        for(int y = 0; y < ozSliceRows; y++) {
            for(int x = 0; x < ozSliceCols; x++) {
                gradientZ[y*sliceRows*sliceCols + x*sliceCols + i] = sobelZ.at<float>(y, x);
            }
        }
    }

/*
 * Step 3: Calculate gradient manigtude
 * */
    gradientManigtude = new float[size];
    for (int i = 0; i < size; i++) {
        gradientManigtude[i] = sqrt(gradientX[i]*gradientX[i] + gradientY[i]*gradientY[i] + gradientZ[i]*gradientZ[i]);
    }
}

void SliceViewWidget::resizeEvent(QResizeEvent *event)
{
    int currentWidth = this->sliceView->width();
    this->sliceView->setFixedHeight(currentWidth);
}

vcg::Point3i SliceViewWidget::indexToCoords(int index, int width, int height)
{
    int z = index / (width * height);
    int mod = (index % (width * height));
    int y = mod / width;
    int x = mod % width;
    return vcg::Point3i(x, y, z);
}

int SliceViewWidget::coordsToIndex(vcg::Point3i coords, int width, int height)
{
    return coords.Z() * (width * height) + coords.Y() * width + coords.X();
}

vcg::Point3i SliceViewWidget::paddingIndex(vcg::Point3i coord, int width, int height, int numSlice)
{
    int x = coord.X(), y = coord.Y(), z = coord.Z();
    if (x < 0) x = 0; else if (x >= width) x = width - 1;
    if (y < 0) y = 0; else if (y >= height) y = height - 1;
    if (z < 0) z = 0; else if (z >= numSlice) z = numSlice - 1;
    return vcg::Point3i(x, y, z);
}

void SliceViewWidget::loadTerminationData(vcg::CallBackPos *cb)
{
    if (md  == NULL) return;
    QString path = this->md->getDICOMPath();
    QDir patientDir(path);
    patientDir.cdUp();

    QString fileName = QFileDialog::getOpenFileName(this, "Import Termination Data", patientDir.absolutePath());
    if (fileName == "")
        return;

    QFile file(fileName);
    if(!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::information(this, tr("Information"), tr("Can not read file, please select a text file again!"));
        return;
    }

    QTextStream input(&file);
    QString line = input.readLine();
    int size = line.toInt();
    termindationData = new float[size];

    int index = 0;
    int count = 100;
    int step = size / count;
    if (step == 0) step = 1;
    count = size / step;


    int idx = 0;
    while(!input.atEnd()) {
        line = input.readLine();
        QStringList parts = line.split(" ");

        // THAY
//        float probability = parts.at(1).toFloat();
//        int x = (idx / hp) % wp;
//        int y = idx % hp;
//        int z = idx / (hp * wp);
//        int mapIndex = coordsToIndex(vcg::Point3i(x, y, z), wp, hp);
//        termindationData[mapIndex] = probability;

        // MINE
        float probability = parts.at(0).toFloat();
        termindationData[idx] = probability;

        idx++;

        if (idx % step == 0) {
            if (cb)
                cb(100 * index * 1.0/ count, "Reading termination data");
            index++;
        }
    }
}

void SliceViewWidget::loadTerminationDataBinary(vcg::CallBackPos *cb)
{
    if (md  == NULL) return;
    QString path = this->md->getDICOMPath();
    QDir patientDir(path);
    patientDir.cdUp();

    QString filter = "MeshBk dissimilarity data (*.mbk)";
    QString fileName = QFileDialog::getOpenFileName(this, "Import dissimilarity data", patientDir.absolutePath(), filter, &filter);
    if (fileName == "")
        return;

    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);
    quint32 size;
    in >> size;
    termindationData = new float[size];
    int index = 0;
    int count = 100;
    int step = size / count;
    if (step == 0) step = 1;
    count = size / step;
    for (int i = 0; i < size; ++i) {
        float value;
        in >> value;
        termindationData[i] = value;
        if (i % step == 0) {
            QCallBack(100 * index * 1.0/ count, "Importing dissimilarity data");
            index++;
        }
    }
    progressbar->hide();
    file.close();
}

bool SliceViewWidget::QCallBack(const int pos, const char *str)
{
    progressbar->show();
    progressbar->setValue(pos);
}

void SliceViewWidget::on_loadTerminationDataButton_clicked()
{
//    loadTerminationData(QCallBack);
    loadTerminationDataBinary(QCallBack);
    progressbar->hide();
}

void SliceViewWidget::on_calcTerminationDataButton_clicked()
{
    prepareTerminateDate(QCallBack);
    progressbar->hide();
}

void SliceViewWidget::on_pushButton_clicked()
{
    if (termindationData == NULL || this->md == NULL) return;
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int size = wp * hp * n;

    QString path = this->md->getDICOMPath();
    QDir patientDir(path);
    patientDir.cdUp();
    QString filter = "MeshBk dissimilarity data (*.mbk)";
    QString filename = QFileDialog::getSaveFileName(this, "Save to", patientDir.absolutePath() + "/dissimilarity.mbk", filter, &filter);
    if (filename == "") return;
    QFile file(filename);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    out << (qint32)size;

    int index = 0;
    int count = 100;
    int step = size / count;
    if (step == 0) step = 1;
    count = size / step;

    for (int i = 0; i < size; ++i) {
        out << (float)termindationData[i];
        if (i % step == 0) {
            QCallBack(100 * index * 1.0/ count, "Export termination data");
            index++;
        }
    }
    out.setVersion(QDataStream::Qt_4_8);
    file.close();
    progressbar->hide();
}

QList<int> SliceViewWidget::getNeighbors(int index, int width, int height, int sliceNum, vcg::Point3i radius)
{
    int wp = width;
    int hp = height;
    int n = sliceNum;
    int maxIndex = wp * hp * n - 1;

    QList<int> results;

    vcg::Point3i coords = indexToCoords(index, width, height);
    for (int rx = -radius.X(); rx <= radius.X(); ++rx) {
        for (int ry = -radius.Y(); ry <= radius.Y(); ++ry) {
            for (int rz = -radius.Z(); rz <= radius.Z(); ++rz) {
                int x = coords.X() + rx;
                int y = coords.Y() + ry;
                int z = coords.Z() + rz;
                int idx = coordsToIndex(vcg::Point3i(x, y, z), width, height);
                if (idx >= 0 && idx <= maxIndex && idx != index)
                    results.push_back(idx);
            }
        }
    }
    return results;
}

void SliceViewWidget::onBrightnessChanged(int value) {
    int color = value * 255 / 100;
    if (value == 100) color = 255;
    ui->sliceView->backgroundChange(color);
}

void SliceViewWidget::on_resetBrightnessButton_clicked()
{
    ui->brightnessSlider->setValue(0);
}

bool SliceViewWidget::isGrowing(float rate, int index) {
    float neighborRate = ui->neighborSpinbox->value() * 1.0 / 100;
    if (ui->neighborCheckbox->isChecked() && rate < neighborRate) return false;
    float dissimilarityTH = ui->thresholdSpinbox->value();
    if (ui->dissimilarityCheckbox->isChecked() && termindationData[index] > dissimilarityTH) return false;
    int gradTH = ui->gradTHSpinbox->value();
    if (ui->gradientCheckbox->isChecked() && gradientManigtude[index] > gradTH) return false;
    return true;
}
