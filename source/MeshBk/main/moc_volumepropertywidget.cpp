/****************************************************************************
** Meta object code from reading C++ file 'volumepropertywidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "volumepropertywidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'volumepropertywidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_VolumePropertyWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x05,
      41,   21,   21,   21, 0x05,
      67,   62,   21,   21, 0x05,

 // slots: signature, parameters, type, tag, flags
     109,   99,   21,   21, 0x08,
     135,   21,   21,   21, 0x08,
     153,   21,   21,   21, 0x08,
     208,  180,   21,   21, 0x08,
     272,   21,   21,   21, 0x08,
     297,   21,   21,   21, 0x08,
     330,  317,   21,   21, 0x0a,
     361,   21,   21,   21, 0x0a,
     381,   21,   21,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_VolumePropertyWidget[] = {
    "VolumePropertyWidget\0\0updateColorGraph()\0"
    "applyButtonClicked()\0type\0"
    "endTransferFunc(TransfuncTypes)\0"
    "isChecked\0usedColorTableChange(int)\0"
    "updateHistogram()\0on_applyTFButton_clicked()\0"
    "type,parentNode,organ,other\0"
    "startTransferFunc(TransfuncTypes,TreeItem*,TreeItem*,TreeItem*)\0"
    "on_applyButton_clicked()\0enableApplyButton()\0"
    "selectedItem\0selectedNodeChanged(TreeItem*)\0"
    "colorTableChanged()\0reloadHistogram()\0"
};

void VolumePropertyWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VolumePropertyWidget *_t = static_cast<VolumePropertyWidget *>(_o);
        switch (_id) {
        case 0: _t->updateColorGraph(); break;
        case 1: _t->applyButtonClicked(); break;
        case 2: _t->endTransferFunc((*reinterpret_cast< TransfuncTypes(*)>(_a[1]))); break;
        case 3: _t->usedColorTableChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->updateHistogram(); break;
        case 5: _t->on_applyTFButton_clicked(); break;
        case 6: _t->startTransferFunc((*reinterpret_cast< TransfuncTypes(*)>(_a[1])),(*reinterpret_cast< TreeItem*(*)>(_a[2])),(*reinterpret_cast< TreeItem*(*)>(_a[3])),(*reinterpret_cast< TreeItem*(*)>(_a[4]))); break;
        case 7: _t->on_applyButton_clicked(); break;
        case 8: _t->enableApplyButton(); break;
        case 9: _t->selectedNodeChanged((*reinterpret_cast< TreeItem*(*)>(_a[1]))); break;
        case 10: _t->colorTableChanged(); break;
        case 11: _t->reloadHistogram(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData VolumePropertyWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VolumePropertyWidget::staticMetaObject = {
    { &VolumeProperty::staticMetaObject, qt_meta_stringdata_VolumePropertyWidget,
      qt_meta_data_VolumePropertyWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VolumePropertyWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VolumePropertyWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VolumePropertyWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VolumePropertyWidget))
        return static_cast<void*>(const_cast< VolumePropertyWidget*>(this));
    return VolumeProperty::qt_metacast(_clname);
}

int VolumePropertyWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = VolumeProperty::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void VolumePropertyWidget::updateColorGraph()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void VolumePropertyWidget::applyButtonClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void VolumePropertyWidget::endTransferFunc(TransfuncTypes _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
