/****************************************************************************
** Meta object code from reading C++ file 'sliceview.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "sliceview.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sliceview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SliceView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      30,   11,   10,   10, 0x05,
      60,   11,   10,   10, 0x05,
      96,   90,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
     126,   90,   10,   10, 0x0a,
     151,   90,   10,   10, 0x0a,
     186,  173,   10,   10, 0x0a,
     248,   10,   10,   10, 0x0a,
     270,  264,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_SliceView[] = {
    "SliceView\0\0intensitySelection\0"
    "showSelection(QMap<int,bool>)\0"
    "hideSelection(QMap<int,bool>)\0color\0"
    "changeBackgroundColor(QColor)\0"
    "backgroundChange(QColor)\0backgroundChange(int)\0"
    "mm,md,normal\0"
    "updateIntersectionMesh(MeshModel*,MeshDocument*,vcg::Point3f)\0"
    "initTexture3D()\0value\0toleranceChanged(int)\0"
};

void SliceView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SliceView *_t = static_cast<SliceView *>(_o);
        switch (_id) {
        case 0: _t->showSelection((*reinterpret_cast< QMap<int,bool>(*)>(_a[1]))); break;
        case 1: _t->hideSelection((*reinterpret_cast< QMap<int,bool>(*)>(_a[1]))); break;
        case 2: _t->changeBackgroundColor((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 3: _t->backgroundChange((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 4: _t->backgroundChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->updateIntersectionMesh((*reinterpret_cast< MeshModel*(*)>(_a[1])),(*reinterpret_cast< MeshDocument*(*)>(_a[2])),(*reinterpret_cast< vcg::Point3f(*)>(_a[3]))); break;
        case 6: _t->initTexture3D(); break;
        case 7: _t->toleranceChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SliceView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SliceView::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_SliceView,
      qt_meta_data_SliceView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SliceView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SliceView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SliceView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SliceView))
        return static_cast<void*>(const_cast< SliceView*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int SliceView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void SliceView::showSelection(QMap<int,bool> _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SliceView::hideSelection(QMap<int,bool> _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void SliceView::changeBackgroundColor(QColor _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
