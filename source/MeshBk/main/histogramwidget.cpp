#include "histogramwidget.h"
#include "ui_histogramwidget.h"
#include <qmath.h>
#include <QColorDialog>

using namespace vcg;

HistogramWidget::HistogramWidget(QWidget *parent) :
    QGLWidget(parent)
{
    this->backgroundColor = QColor(0, 0, 0);
    activePoint = NULL;
    histogramColor = vcg::Color4f(0, 255, 0, 255);
}

HistogramWidget::~HistogramWidget()
{
    foreach(ColorPoint* point, currentColorPoints) {
        delete point->location;
        delete point;
    }
}

void HistogramWidget::resizeGL()
{
}

void HistogramWidget::initializeGL()
{
    float grayLength = grayMax - grayMin;
    qglClearColor(this->backgroundColor);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    float ratio = this->width() * 1.0 / this->height();
    float maxWidth = qMax(grayLength, this->width() * 1.0f);
    float maxHeight = maxWidth * 1.0 / ratio;
    glOrtho(grayMin, grayMax, 0, this->max, -0.5, 0.5);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 0, 0, 0, 0, -1, 0, 1, 0);
    this->alphaOffset = (maxHeight - colorPointRadius) * 1.0 / 255;
    this->setMouseTracking(true);
}

void HistogramWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    qglClearColor(this->backgroundColor);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
//    float ratio = this->width() * 1.0 / this->height();
//    float maxWidth = qMax(grayLength, this->width() * 1.0f);
//    float maxHeight = maxWidth * 1.0 / ratio;
    glOrtho(grayMin, grayMax, 0, this->max, -0.5, 0.5);



//    this->renderText(-1023, -1000, 0, QString::number(-1023), QFont("Times", 10, QFont::Normal));
//    this->renderText(1024, -1000, 0, QString::number(1024), QFont("Times", 10, QFont::Normal));
    // Draw base line
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPushMatrix();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_COLOR_LOGIC_OP);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glColor3f(1, 1, 1);
    glBegin(GL_LINES);
    glVertex2i(-1100, 0);
    glVertex2i(1100, 0);
    glEnd();

    if (this->histogram != NULL) {
        QMap<int, int>::Iterator it = this->histogram->begin();
        glBegin(GL_LINES);
        glColor3f(histogramColor[0] / 255, histogramColor[1] / 255, histogramColor[2] / 255);
        for (;it != this->histogram->end(); ++it) {
            glVertex2f(it.key(), 0);
            glVertex2f(it.key(), it.value() * 1.0);
        }
        glEnd();
    }

    // Draw external histogram (if any)
    for (int i = 0; i < externalHistogramList.size(); ++i) {
        vcg::Color4f color = externalColorList.at(i);
        QMap<int, int> histogram = externalHistogramList.at(i);

        QMap<int, int>::Iterator it = histogram.begin();
        glLogicOp(GL_XOR);
        glBegin(GL_LINES);
        glColor4f(color[0] / 255, color[1] / 255, color[2] / 255, color[3] / 255);
        for (;it != histogram.end(); ++it) {
            glVertex2f(it.key(), 0);
            glVertex2f(it.key(), it.value() * 1.0);
        }
        glEnd();
    }

    glPointSize(3);
    glBegin(GL_POINTS);

    for (int i = 1; i <= 10; ++i) {
        glVertex2i(i * 200, 0);
        glVertex2i(-i * 200, 0);
    }
    glEnd();

    // Draw origin
    glPointSize(5);
    glBegin(GL_POINTS);
    glColor3f(1, 0, 0);
    glVertex2i(0, 0);
    glEnd();

    // Draw threshold
    glColor4f(1, 1, 1, 0.4);
    glBegin(GL_POLYGON);
    glVertex2f(grayMin, -this->max / 10);
    glVertex2f(leftBound, -this->max / 10);
    glVertex2f(leftBound, this->max + this->max / 5);
    glVertex2f(grayMin, this->max + this->max / 5);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex2f(rightBound, -this->max / 10);
    glVertex2f(grayMax, -this->max / 10);
    glVertex2f(grayMax, this->max + this->max / 5);
    glVertex2f(rightBound, this->max + this->max / 5);
    glEnd();
    glPopMatrix();
    glPopAttrib();

    if (isDrawColor) {
        if (activePoint) {
            drawCircelScreen(activePoint);
        }
        drawCircelScreen(currentColorPoints);
        drawLinesScreen(currentColorPoints);
        if (isPointing) {
            this->setCursor(Qt::PointingHandCursor);
        } else {
            this->setCursor(Qt::ArrowCursor);
        }
        drawSelectedPointInfor();
    }
}

void HistogramWidget::setColorDict(QMap<float, Color4f> *colorDict)
{
    this->colorDict = colorDict;
    updatePinPoints();
}

void HistogramWidget::updatePinPoints()
{
    foreach (ColorPoint* point, currentColorPoints) {
        delete point->location;
        delete point;
    }

    currentColorPoints.clear();
    if (colorDict == NULL) return;
    QMap<float, vcg::Color4f>::Iterator it = colorDict->begin();
    for (; it != colorDict->end(); ++it) {
        float intensity = it.key();
        float opacity = it.value()[3];
        float ratio = this->width() * 1.0 / (grayMax - grayMin + 1);
        int x = (intensity - grayMin) * ratio;
        int y = this->height() * opacity / 255;
        ColorPoint *point = new ColorPoint();
        point->color = it.value();
        point->location = new QPoint(x, this->height() - y);
        point->intensity = it.key();
        currentColorPoints.push_back(point);
    }
    update();
}

void HistogramWidget::removeColorPoint(ColorPoint *colorPoint)
{
    currentColorPoints.removeOne(colorPoint);
    updateColorTableToPoints();
    delete activePoint->location;
    delete activePoint;
    activePoint = NULL;
    update();
}

void HistogramWidget::drawCirle(QPointF center, vcg::Color4f color) {
    int step = 300;
    center.setY(center.y() * alphaOffset);
//    glColor3f(1, 1, 1);
    glColor3f(color[0] / 255, color[1] / 255, color[2] / 255);
    glBegin(GL_POLYGON);
    for (int i = 0; i < step; ++i) {
        float angle = 2.0 * M_PI * i / step;
        float x = center.x() + qCos(angle) * colorPointRadius;
        float y = center.y() + qSin(angle) * colorPointRadius;
        glVertex2f(x, y);
    }
    glEnd();
//    glLineWidth(2);
//    glColor3f(0,0,0);
//    glBegin(GL_LINE_LOOP);
//    for (int i = 0; i < step; ++i) {
//        float angle = 2.0 * M_PI * i / step;
//        float x = center.x() + qCos(angle) * colorPointRadius;
//        float y = center.y() + qSin(angle) * colorPointRadius;
//        glVertex2f(x, y);
//    }
//    glEnd();
    glLineWidth(1);
//    glColor3f(color[0] / 255, color[1] / 255, color[2] / 255);
    glColor3f(0, 0, 0);
    glBegin(GL_LINE_LOOP);
    for (int i = 0; i < step; ++i) {
        float angle = 2.0 * M_PI * i / step;
        float x = center.x() + qCos(angle) * colorPointRadius;
        float y = center.y() + qSin(angle) * colorPointRadius;
        glVertex2f(x, y);
    }
    glEnd();
    glBegin(GL_LINE);
    glVertex2i(-1100, 0);
    glVertex2i(1100, 0);
    glEnd();


}

void HistogramWidget::drawNumber(QPointF center, QString number)
{
    float offsetX = -50;
    float offsetY = -105;
//    glColor3f(0, 0, 0);
    this->renderText(center.x() + offsetX, center.y() + offsetY, 0, number, QFont("Times", 10, QFont::Normal));
}

void HistogramWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
    QPoint newhit(event->pos().x(), event->pos().y());
    if (isDrawColor && colorDict != NULL){
        float ratio = this->width() * 1.0 / (grayMax - grayMin + 1);
        int intensity = newhit.x() / ratio + grayMin;
        int opacity = (this->height() - newhit.y()) * 255 / this->height();
        vcg::Color4f newColor;
        QColor color = QColorDialog::getColor();
        if (color.isValid() && colorDict != NULL) {
            newColor = vcg::Color4f(color.red(), color.green(), color.blue(), opacity);
            if (activePoint) {
                activePoint->color = vcg::Color4f(color.red(), color.green(), color.blue(), activePoint->color[3]);
                updateColorTableToPoints();
            } else {
                (*colorDict)[intensity] = newColor;
                updatePinPoints();
            }
        }
    }
    update();
}

void HistogramWidget::mouseMoveEvent(QMouseEvent *event)
{
    QPoint newhit(event->pos().x(), event->pos().y());
    bool pointing = false;
    for (int i = 0; i < currentColorPoints.size(); ++i) {
        if (isInCircle(*currentColorPoints.at(i)->location, colorPointRadius, newhit)) {
            pointing = true;
        }
    }
    this->isPointing = pointing;

    if (isDrag) {
        int x = newhit.x();
        if (x < 0) x = 0;
        if (x > this->width()) x = width();
        int y = newhit.y();
        if (y < 0) y = 0;
        if (y > this->height()) y = this->height();
        if (activePoint != NULL) {
            activePoint->location->setX(x);
            activePoint->location->setY(y);

            QPoint pointData = screenToWorld(activePoint->location->x(), activePoint->location->y());
            activePoint->color[3] = pointData.y();
            activePoint->intensity = pointData.x();
            updateColorTableToPoints();
        }
    }
    update();
}

void HistogramWidget::mousePressEvent(QMouseEvent *event)
{
    isDrag = true;
    QPoint newhit(event->pos().x(), event->pos().y());
    activePoint = NULL;
    for (int i = 0; i < currentColorPoints.size(); ++i) {
        if (isInCircle(*currentColorPoints.at(i)->location, colorPointRadius, newhit)) {
            activePoint = currentColorPoints.at(i);
        }
    }
    update();
    emit selectedPointChange(activePoint);
}

void HistogramWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (isDrag) {
        updateColorTableToPoints();
    }
    isDrag = false;
    isPointing = false;
    update();
}
void HistogramWidget::drawCircelScreen(QList<ColorPoint*> &points) {
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,this->width(),this->height(),0,-1,1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_COLOR_LOGIC_OP);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    float wi;
    glGetFloatv(GL_POINT_SIZE,&wi);
//    glLineWidth(4);
    glPointSize(1);
//    glLogicOp(GL_XOR);

    foreach (ColorPoint* point, points){
        int step = 300;
        glColor3f(1, 1, 1);
        glBegin(GL_POLYGON);
        for (int i = 0; i < step; ++i) {
            float angle = 2.0 * M_PI * i / step;
            float x = point->location->x() + qCos(angle) * colorPointRadius;
            float y = point->location->y() + qSin(angle) * colorPointRadius;
            glVertex2f(x, y);
        }
        glEnd();
    }

    glPopAttrib();
    glPopMatrix(); // restore modelview
//    glLineWidth(wi);
    glPointSize(wi);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glColor3f(1, 1, 1);
}

void HistogramWidget::drawCircelScreen(ColorPoint* point) {
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,this->width(),this->height(),0,-1,1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_COLOR_LOGIC_OP);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    float wi;
    glGetFloatv(GL_POINT_SIZE,&wi);
//    glLineWidth(4);
    glPointSize(1);
//    glLogicOp(GL_XOR);

    int step = 300;
    glColor3f(point->color[0] / 255, point->color[1] / 255, point->color[2] / 255);
    glBegin(GL_POLYGON);
    for (int i = 0; i < step; ++i) {
        float angle = 2.0 * M_PI * i / step;
        float x = point->location->x() + qCos(angle) * colorSelectedPointRadius;
        float y = point->location->y() + qSin(angle) * colorSelectedPointRadius;
        glVertex2f(x, y);
    }
    glEnd();

    glPopAttrib();
    glPopMatrix(); // restore modelview
//    glLineWidth(wi);
    glPointSize(wi);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glColor3f(1, 1, 1);
}

void HistogramWidget::drawLinesScreen(QList<ColorPoint*> &points)
{
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,this->width(),this->height(),0,-1,1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_COLOR_LOGIC_OP);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    float wi;
    glGetFloatv(GL_LINE_WIDTH,&wi);
    glLineWidth(1);
//    glLogicOp(GL_XOR);
    glBegin(GL_LINE_STRIP);

    foreach (ColorPoint* point, points){
        glColor3f(1, 1, 1);
        glVertex2f(point->location->x(), point->location->y());
    }
    glEnd();
    glPopAttrib();
    glPopMatrix(); // restore modelview
    glLineWidth(wi);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glColor3f(1, 1, 1);
}

bool isInCircle(QPoint center, int radius, QPoint point)
{
    QPoint temp = point - center;
    return (qPow(temp.x(), 2) + qPow(temp.y(), 2)) <= qPow(radius, 2);
}

void HistogramWidget::drawSelectedPointInfor()
{
    if (activePoint == NULL) return;
    QPoint pointData = screenToWorld(activePoint->location->x(), activePoint->location->y());
    vcg::Color4f color = (*colorDict).lowerBound(pointData.x()).value();
    this->renderText(grayMin + 10, this->max - this->max / 10, 0, "Intensity: " + QString::number(pointData.x()) + ". Opacity: " + QString::number(pointData.y()),
            QFont("Times", 8, QFont::Normal));
}

QPoint HistogramWidget::screenToWorld(int x, int y)
{
    float ratio = this->width() * 1.0 / (grayMax - grayMin + 1);
    int worldX = x / ratio + grayMin;
    int worldY = (this->height() - y) * 255 / this->height();
    return QPoint(worldX, worldY);
}

QPoint HistogramWidget::worldToScreen(int x, int y)
{
    float ratio = this->width() * 1.0 / (grayMax - grayMin + 1);
    int screenX = (x - grayMin) * ratio;
    int screenY = this->height() * y / 255;
    return QPoint(screenX, screenY);
}

void HistogramWidget::updateColorTableToPoints()
{
    if (colorDict == NULL) return;
    colorDict->clear();
    for (int i = 0; i < currentColorPoints.size(); i++) {
        ColorPoint *colorPoint = currentColorPoints.at(i);
        (*colorDict)[colorPoint->intensity] = colorPoint->color;
    }
    emit colorDictChanged(colorDict);
    emit colorTableChanged();
}

void HistogramWidget::setHistogram(QMap<int, int> *histogram)
{
    if (histogram == NULL) {
        this->histogram = NULL;
        return;
    }
    if (histogram->size() == 0) {
        qDebug() << "setHistogram: histogram empty";
        return;
    }
    this->histogram = histogram;
    QList<int> histogramValues = this->histogram->values();
    qSort(histogramValues);
    this->max = histogramValues.last();
}

void HistogramWidget::clearHistogram()
{
    if (this->histogram) {
        this->histogram->clear();
    }
    update();
}

void HistogramWidget::addExternalHistogram(QMap<int, int> histo)
{
    externalHistogramList.push_back(histo);
    externalColorList.push_back(vcg::Color4f(qrand() % 255, qrand() % 255, qrand() % 255, 60));
    QList<int> histogramValues = histo.values();
    qSort(histogramValues);
    this->max = histogramValues.last();
    if (histogramValues.last() > this->max)
        this->max = histogramValues.last();
    update();
}

void HistogramWidget::resetExternalHistogramList()
{
    externalHistogramList.clear();
    externalColorList.clear();
//    this->max = 100;
    update();
}
