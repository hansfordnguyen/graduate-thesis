/****************************************************************************
** Meta object code from reading C++ file 'histogramcolorwidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "histogramcolorwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'histogramcolorwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_HistogramColorWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      33,   22,   21,   21, 0x05,
      63,   21,   21,   21, 0x05,
      83,   21,   21,   21, 0x05,
     101,   21,   21,   21, 0x05,

 // slots: signature, parameters, type, tag, flags
     127,  121,   21,   21, 0x0a,
     150,   21,   21,   21, 0x0a,
     166,   22,   21,   21, 0x0a,
     200,  121,   21,   21, 0x0a,
     233,  229,   21,   21, 0x0a,
     256,   21,   21,   21, 0x08,
     290,   21,   21,   21, 0x08,
     334,  331,  317,   21, 0x08,
     373,   21,  317,   21, 0x28,
     395,   21,   21,   21, 0x08,
     434,  429,   21,   21, 0x08,
     475,   21,   21,   21, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_HistogramColorWidget[] = {
    "HistogramColorWidget\0\0colorPoint\0"
    "removeColorPoint(ColorPoint*)\0"
    "colorTableChanged()\0reloadHistogram()\0"
    "enableApplyButton()\0value\0"
    "onThresholdChange(int)\0initHistogram()\0"
    "selectedPointChanged(ColorPoint*)\0"
    "backgroundSliderChanged(int)\0key\0"
    "presetChanged(QString)\0"
    "on_removePinPointButton_clicked()\0"
    "on_loadHistogram_clicked()\0QMap<int,int>\0"
    "cb\0startLoadFromFolder(vcg::CallBackPos*)\0"
    "startLoadFromFolder()\0"
    "on_resetHistogramButton_clicked()\0"
    "arg1\0on_isUseColoorCheckbox_stateChanged(int)\0"
    "on_refreshHistoButton_clicked()\0"
};

void HistogramColorWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HistogramColorWidget *_t = static_cast<HistogramColorWidget *>(_o);
        switch (_id) {
        case 0: _t->removeColorPoint((*reinterpret_cast< ColorPoint*(*)>(_a[1]))); break;
        case 1: _t->colorTableChanged(); break;
        case 2: _t->reloadHistogram(); break;
        case 3: _t->enableApplyButton(); break;
        case 4: _t->onThresholdChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->initHistogram(); break;
        case 6: _t->selectedPointChanged((*reinterpret_cast< ColorPoint*(*)>(_a[1]))); break;
        case 7: _t->backgroundSliderChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->presetChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->on_removePinPointButton_clicked(); break;
        case 10: _t->on_loadHistogram_clicked(); break;
        case 11: { QMap<int,int> _r = _t->startLoadFromFolder((*reinterpret_cast< vcg::CallBackPos*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QMap<int,int>*>(_a[0]) = _r; }  break;
        case 12: { QMap<int,int> _r = _t->startLoadFromFolder();
            if (_a[0]) *reinterpret_cast< QMap<int,int>*>(_a[0]) = _r; }  break;
        case 13: _t->on_resetHistogramButton_clicked(); break;
        case 14: _t->on_isUseColoorCheckbox_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->on_refreshHistoButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData HistogramColorWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject HistogramColorWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_HistogramColorWidget,
      qt_meta_data_HistogramColorWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HistogramColorWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HistogramColorWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HistogramColorWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HistogramColorWidget))
        return static_cast<void*>(const_cast< HistogramColorWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int HistogramColorWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void HistogramColorWidget::removeColorPoint(ColorPoint * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void HistogramColorWidget::colorTableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void HistogramColorWidget::reloadHistogram()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void HistogramColorWidget::enableApplyButton()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE
