#ifndef DETECTBOUNDARY_H
#define DETECTBOUNDARY_H

#include "opencv2/opencv.hpp"
#include <common/interfaces.h>

#include <QList>

using namespace vcg;

class DetectBoundary
{
public:
    DetectBoundary();

    enum Direction{NONE, WEST, NORTH_WEST, NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST};
    enum StartDirection{TOP_LEFT, RIGHT_TOP, BOTTOM_RIGHT, LEFT_BOTTOM};

private:
    uchar **slice = NULL;
    Point2i b0 = Point2i(-1, -1);
    Point2i c0 = Point2i(-1, -1);
    Point2i b1 = Point2i(-1, -1);
    Point2i c1 = Point2i(-1, -1);

public:
    inline void setSlice(uchar **data) {slice = data;}
    inline void setB0(Point2i point) {b0[0] = point.X(); b0[1] = point.Y();}
    inline void setC0(Point2i point) {c0[0] = point.X(); c0[1] = point.Y();}
    inline void setB1(Point2i point) {b1[0] = point.X(); b1[1] = point.Y();}
    inline void setC1(Point2i point) {c1[0] = point.X(); c1[1] = point.Y();}

    inline uchar** getSelice() {return slice;}
    inline Point2i getB0() {return b0;}
    inline Point2i getC0() {return c0;}
    inline Point2i getB1() {return b1;}
    inline Point2i getC1() {return c1;}
public:
    Direction getCDirection(Point2i b, Point2i c);
    Point2i findUperLeftPoint(uchar **slice, int width, int height);
    Point2i findTopLeftPoint(uchar **slice, int width, int height);
    Point2i findRightTopPoint(uchar **slice, int width, int height);
    Point2i findBottomRightPoint(uchar **slice, int width, int height);
    Point2i findLeftBottomPoint(uchar **slice, int width, int height);

    QList<Point2i> findBoundaryPoints(Point2i uperLeft, uchar** slice, int width, int height);
    QList<Point2i> findBoundaryPoints(Point2i startPoint, StartDirection startDir, uchar** slice, int width, int height);
};

#endif // DETECTBOUNDARY_H
