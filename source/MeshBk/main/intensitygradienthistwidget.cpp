#include "intensitygradienthistwidget.h"
#include "ui_intensitygradienthistwidget.h"

QProgressBar* IntensityGradientHistWidget::progressBar;

IntensityGradientHistWidget::IntensityGradientHistWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::IntensityGradientHistWidget)
{
    ui->setupUi(this);

    this->initializeWidgets();
}

void IntensityGradientHistWidget::initializeWidgets() {

    ui->colorTable->horizontalHeader()->setResizeMode(0, QHeaderView::Stretch);
    ui->colorTable->setColumnWidth(1, 50);
    ui->colorTable->setColumnWidth(2, 70);
    ui->colorTable->setColumnWidth(3, 40);

    ui->progressBar->setMaximumHeight(10);
    ui->progressBar->hide();
    ui->progressBar->setMaximum(100);
    ui->progressBar->setMinimum(0);
    progressBar = ui->progressBar;

    ui->histColorView->regionColorTable = ui->colorTable;

    connect(this, SIGNAL(histogramUpdated(cv::Mat)), ui->histColorView, SLOT(updateHistogramMatAndTexture(cv::Mat)));
}

bool IntensityGradientHistWidget::QCallBack(const int pos, const char *str) {
    progressBar->show();
    progressBar->setValue(pos);
}

IntensityGradientHistWidget::~IntensityGradientHistWidget()
{
    delete ui;
}

void IntensityGradientHistWidget::setInputRawIntensity(signed short *raw) {
    int size = sliceCols * sliceRows * numOfSlice;
    inputRawIntensity = new signed short[size];

    for (int i = 0; i < size; i++) {
        signed short value = raw[i];
        inputRawIntensity[i] = value;
    }
}

void IntensityGradientHistWidget::setMeshDoc(MeshDocument *meshDoc) {
   this->md = meshDoc;
   this->md->currentLimitedShape = MeshDocument::LimitedShape::None;
}

void IntensityGradientHistWidget::setGLA(GLArea *glarea) {
    this->gla = glarea;
}

int** IntensityGradientHistWidget::buildHistogram(CallBackPos *cb) {
    ui->histColorView->updateGLForcefully();

    int size = this->sliceRows * this->sliceCols * this->numOfSlice;

/*
 * Step 1: Build all cv::Mat (Oxy for Ox, Oy) and (Oyz for Oz)
 * */
    progressBar->show();
    int currentProgress = 0;
    int value = 0;

    // Build Ox, Oy openCV Matrix
    QList<cv::Mat> xyMats;

    for (int i = 0; i < this->numOfSlice; i++) {
        value = (i*1.0 /(this->numOfSlice - 1) * 20);
        if (value - currentProgress > 0) {
            currentProgress = value;
            cb(currentProgress, "");
        }

        cv::Mat mat(sliceRows, sliceCols, CV_32FC1);

        for (int y = 0; y < sliceRows; y++) {
            for(int x = 0; x < sliceCols; x++) {
                mat.at<float>(y, x) = (float)this->inputRawIntensity[i*sliceRows*sliceCols + y*sliceCols + x];
            }
        }

        xyMats.push_back(mat);
    }

    // Build Oz openCV Matrix
    int ozNumOfSlice = this->sliceCols;
    int ozSliceCols = this->sliceRows;
    int ozSliceRows = this->numOfSlice;

    QList<cv::Mat> zMats;
    for (int i = 0; i < ozNumOfSlice; i++) {
        value = (i*1.0 /(ozNumOfSlice - 1) * 20) + 20;
        if (value - currentProgress > 0) {
            currentProgress = value;
            cb(currentProgress, "");
        }

        cv::Mat mat(ozSliceRows, ozSliceCols, CV_32FC1);

        for (int y = 0; y < ozSliceRows; y++) {
            for (int x = 0; x < ozSliceCols; x++) {
                mat.at<float>(y, x) = this->inputRawIntensity[y*sliceRows*sliceCols + x*sliceCols + i];
            }
        }

        zMats.push_back(mat);
    }


/*
 * Step 2: Apply sobel to get gradient Ox, Oy, Oz
 * */


    float* gradientX = new float[size];
    float* gradientY = new float[size];
    float* gradientZ = new float[size];

    for (int i = 0; i < this->numOfSlice; i++) {
        value = (i*1.0 /(this->numOfSlice - 1) * 20) + 40;
        if (value - currentProgress > 0) {
            currentProgress = value;
            cb(currentProgress, "");
        }

        cv::Mat sobelX, sobelY;

        cv::Sobel(xyMats.at(i), sobelX, CV_32FC1, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
        cv::Sobel(xyMats.at(i), sobelY, CV_32FC1, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);

        for (int y = 0; y < this->sliceRows; y++) {
            for (int x = 0; x < this->sliceCols; x++) {
                gradientX[i*sliceRows*sliceCols + y*sliceCols + x] = sobelX.at<float>(y, x);
                gradientY[i*sliceRows*sliceCols + y*sliceCols + x] = sobelY.at<float>(y, x);
            }
        }
    }

    for (int i = 0; i < ozNumOfSlice; i++) {
        value = (i*1.0 /(ozNumOfSlice - 1) * 20) + 60;
        if (value - currentProgress > 0) {
            currentProgress = value;
            cb(currentProgress, "");
        }

        cv::Mat sobelZ;

        cv::Sobel(zMats.at(i), sobelZ, CV_32FC1, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);

        for(int y = 0; y < ozSliceRows; y++) {
            for(int x = 0; x < ozSliceCols; x++) {
                gradientZ[y*sliceRows*sliceCols + x*sliceCols + i] = sobelZ.at<float>(y, x);
            }
        }
    }

/*
 * Step 3: Calculate gradient manigtude
 * */

    gradientManigtude = new float[size];
    for (int i = 0; i < size; i++) {
        gradientManigtude[i] = sqrt(gradientX[i]*gradientX[i] + gradientY[i]*gradientY[i] + gradientZ[i]*gradientZ[i]);
    }

    cv::Mat gradManigMat(1, size, CV_32FC1, gradientManigtude);
    double min;
    cv::minMaxLoc(gradManigMat, &min, &maxGradientManigtude);


/*
 * Step 4: Fill histogram 2D (Intensity - GradientManigtude)
 * */

    int** histogram = new int*[intensityAxisSize];
    assert(histogram);

    for(int i = 0; i < intensityAxisSize; i++) {
        histogram[i] = new int[gradManigtudeAxisSize];
        assert(histogram[i]);

        // Initialize histogram
        for (int j = 0; j < gradManigtudeAxisSize; j++) {
            histogram[i][j] = 0;
        }
    }

    // Start fill histogram
    assert(maxGradientManigtude > 0);
    for(int i = 0; i < size; i++) {
        value = (i*1.0 /(size - 1) * 20) + 80;
        if (value - currentProgress > 0) {
            currentProgress = value;
            cb(currentProgress, "");
        }

        if (currentParentNode->opacity[i] != 0) { // Only calculate histogram on Non-transparent voxel
            int intensity = this->inputRawIntensity[i];
            int gradManigtude = (int)round((gradientManigtude[i]/maxGradientManigtude)*gradManigtudeAxisSize);
            if (gradManigtude < 0 || gradManigtude > gradManigtudeAxisSize) {
                qDebug() << "Exist outsize:" << gradManigtude;
            }

            if (intensity < 0 || intensity > 2047)
                continue;

            histogram[intensity][gradManigtude]++;
        } // end if opacity != 0
    }

    progressBar->hide();
    delete [] gradientX; gradientX = NULL;
    delete [] gradientY; gradientY = NULL;
    delete [] gradientZ; gradientZ = NULL;

    return histogram;
}

void IntensityGradientHistWidget::updateHistogramView(int **histogram2D) {
    cv::Mat histMat(gradManigtudeAxisSize, intensityAxisSize, CV_64FC1);

    for (int i = 0; i < intensityAxisSize; i++) {
        for (int j = 0; j < gradManigtudeAxisSize; j++) {
            histMat.at<double>(j, i) = histogram2D[i][j];
        }
    }

    emit histogramUpdated(histMat);
}

void IntensityGradientHistWidget::applyTransferFunction() {
    QList<HistogramRegionRect> regionList = ui->histColorView->histRegionRectList;

    QList<RegionRange> regionRangeList = buildRegionRangeList(regionList);

    bool mapping = mapRegionRange(regionRangeList, QCallBack);

    if (mapping) {
        updateTFOutputAndOption();
    }

    currentParentNode->transfuncOptions.type = TransfuncTypes::HISTOGRAM2D;

    TransfuncOptions options;
    options.type = TransfuncTypes::NONE;
    NodeViewOptions viewOptions;
    viewOptions.histogram2DKeepRaw = ui->keepIntensityCheckbox->isChecked();
    viewOptions.opacity = currentApplyNode->opacity;
    viewOptions.colorTable = currentApplyNode->colorTable;
    viewOptions.histogram2DRgba = filteredRegionRgbaData;
    currentApplyNode->transfunc = tfhistogram2D;
    currentApplyNode->transfuncOptions = options;
    currentApplyNode->viewOptions = viewOptions;
    currentApplyNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);

    TransfuncOptions optionsInverted;
    optionsInverted.type = TransfuncTypes::NONE;
    NodeViewOptions viewInverted;
    viewInverted.histogram2DRgba = filteredRegionRgbaData;
    viewInverted.histogram2DKeepRaw = ui->keepIntensityCheckbox->isChecked();
    viewInverted.opacity = currentOtherNode->opacity;
    viewInverted.colorTable = currentOtherNode->colorTable;
    currentOtherNode->transfunc = tfhistogram2DInverted;
    currentOtherNode->transfuncOptions = optionsInverted;
    currentOtherNode->viewOptions = viewInverted;
    currentOtherNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
}

QList<RegionRange> IntensityGradientHistWidget::buildRegionRangeList(QList<HistogramRegionRect> regionRectList) {
    QList<RegionRange> regionRange;
    for(int i = 0; i < regionRectList.size(); i++) {
        HistogramRegionRect region = regionRectList.at(i);
        RegionRange rMapping;

        rMapping.minIntensity = std::min(region.point1.x(), region.point2.x());
        rMapping.maxIntensity = std::max(region.point1.x(), region.point2.x());
        rMapping.minGradient = std::min(region.point1.y(), region.point2.y()) * maxGradientManigtude / 1000.0;
        rMapping.maxGradient = std::max(region.point1.y(), region.point2.y()) * maxGradientManigtude / 1000.0;
        rMapping.color = region.color;

        regionRange.push_front(rMapping);
    }

    return regionRange;
}

bool IntensityGradientHistWidget::mapRegionRange(QList<RegionRange> regionRangeList, CallBackPos* cb) {

    int size = sliceRows * sliceCols * numOfSlice;
    filteredRegionRgbaData = new uchar[size*4]; // RGBA channels

    int currentProgress = 0;
    int value = 0;

    progressBar->show();
    for(int i = 0; i < size; i++) {
        // Update progress bar
        value = (i * 1.0 / (size-1)) * 100;
        if (value - currentProgress > 0) {
            currentProgress = value;
            cb(currentProgress, "");
        }
        // Initialize transparent data
        filteredRegionRgbaData[4*i] = 0;
        filteredRegionRgbaData[4*i + 1] = 0;
        filteredRegionRgbaData[4*i + 2] = 0;
        filteredRegionRgbaData[4*i + 3] = 0;

        if (currentParentNode->opacity[i] == 0) {
            continue;
        }

        float intensity = inputRawIntensity[i] - 1024;
        for(int idx = 0; idx < regionRangeList.size(); idx++) {
            RegionRange range = regionRangeList.at(idx);

            if (intensity >= range.minIntensity && intensity <= range.maxIntensity) {
                float gradient = gradientManigtude[i];

                if (gradient >= range.minGradient && gradient <= range.maxGradient) {
                    filteredRegionRgbaData[4*i] = range.color.red();
                    filteredRegionRgbaData[4*i + 1] = range.color.green();
                    filteredRegionRgbaData[4*i + 2] = range.color.blue();
                    filteredRegionRgbaData[4*i + 3] = range.color.alpha();
                }
            }
        } // end foreach regionRangeList
    } // end foreach rawIntensity

    progressBar->hide();

    return true;
}

void IntensityGradientHistWidget::updateTFOutputAndOption() {
    int size = sliceRows * sliceCols * numOfSlice;


    for (int i = 0; i < size; i++) {
        if (currentParentNode->opacity[i] != 0) {
            if (filteredRegionRgbaData[4*i + 3] != 0) { // check if opacity != 0

                currentApplyNode->opacity[i] = 1;
                currentOtherNode->opacity[i] = 0;
            } else {
                currentApplyNode->opacity[i] = 0;
                currentOtherNode->opacity[i] = 1;
            } // end if filterRegionRgbaData.opacity != 0
        } else {
            currentApplyNode->opacity[i] = 0;
            currentOtherNode->opacity[i] = 0;
        } // end if inputMaskOpacity != 0
    }
}


void IntensityGradientHistWidget::on_applyTFButton_clicked()
{
    applyTransferFunction();

    this->gla->updatePaintAreaForcefully();

    ui->applyTFButton->hide();
}

void IntensityGradientHistWidget::startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *organ, TreeItem *other) {
    if (type == TransfuncTypes::HISTOGRAM2D) {

        if (this->md != NULL) {
            currentApplyNode = organ;
            currentOtherNode = other;
            currentParentNode = parentNode;

            int sliceCols = this->md->measurementPixel.X();
            int sliceRows = this->md->measurementPixel.Y();
            int numOfSlice = this->md->measurementPixel.Z();
            float xSpace = this->md->measurementMillimeter.X() / ((float)sliceCols);
            float ySpace = this->md->measurementMillimeter.Y() / ((float)sliceRows);
            float zSpace = this->md->measurementMillimeter.Z() / ((float)(numOfSlice-1));

            this->setSliceMetaData(sliceRows, sliceCols, numOfSlice);
            this->setCoefficients(xSpace, ySpace, zSpace);

            ui->applyTFButton->setVisible(true);
            this->setInputRawIntensity(this->md->globalTexture3DIntensityData);

            // Clear color table
            while(this->ui->histColorView->regionColorTable->rowCount() > 0) {
                this->ui->histColorView->removeRegion(0);
            }

            int** histogram = this->buildHistogram(QCallBack);
            updateHistogramView(histogram);
        }
    }
}

vcg::Color4f tfhistogram2D(signed short* intensityArray, int index, NodeViewOptions options){

    uchar* histogram2DRgba = options.histogram2DRgba;

    uchar r = histogram2DRgba[index*4];
    uchar g = histogram2DRgba[index*4 + 1];
    uchar b = histogram2DRgba[index*4 + 2];
    uchar a = histogram2DRgba[index*4 + 3];

    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (a != 0) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            if (options.histogram2DKeepRaw) {
                return vcg::Color4f(color, color, color, 255);
            } else {
                return vcg::Color4f(r, g, b, a);
            }
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

vcg::Color4f tfhistogram2DInverted(signed short* intensityArray, int index, NodeViewOptions options) {

    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    uchar* histogram2DRgba = options.histogram2DRgba;
    uchar a = histogram2DRgba[index*4 + 3];

    if (a == 0 && options.opacity[index] == 1) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}
