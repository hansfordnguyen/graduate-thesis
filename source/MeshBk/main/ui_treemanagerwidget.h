/********************************************************************************
** Form generated from reading UI file 'treemanagerwidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TREEMANAGERWIDGET_H
#define UI_TREEMANAGERWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableWidget>
#include <QtGui/QTreeView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TreeManagerWidget
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTreeView *treeView;
    QLabel *label_2;
    QTableWidget *viewTable;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer;
    QProgressBar *progressbar;
    QHBoxLayout *controlbuttonlayout;
    QPushButton *splitButton;
    QPushButton *insertButton;
    QPushButton *deleteButton;
    QPushButton *viewButton;
    QSpacerItem *horizontalSpacer;

    void setupUi(QWidget *TreeManagerWidget)
    {
        if (TreeManagerWidget->objectName().isEmpty())
            TreeManagerWidget->setObjectName(QString::fromUtf8("TreeManagerWidget"));
        TreeManagerWidget->resize(400, 300);
        verticalLayout = new QVBoxLayout(TreeManagerWidget);
        verticalLayout->setSpacing(3);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(TreeManagerWidget);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        treeView = new QTreeView(TreeManagerWidget);
        treeView->setObjectName(QString::fromUtf8("treeView"));

        verticalLayout->addWidget(treeView);

        label_2 = new QLabel(TreeManagerWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        viewTable = new QTableWidget(TreeManagerWidget);
        if (viewTable->columnCount() < 2)
            viewTable->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        viewTable->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        viewTable->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        viewTable->setObjectName(QString::fromUtf8("viewTable"));

        verticalLayout->addWidget(viewTable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton = new QPushButton(TreeManagerWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        progressbar = new QProgressBar(TreeManagerWidget);
        progressbar->setObjectName(QString::fromUtf8("progressbar"));
        progressbar->setMaximumSize(QSize(16777215, 10));
        progressbar->setValue(24);
        progressbar->setTextVisible(false);

        verticalLayout->addWidget(progressbar);

        controlbuttonlayout = new QHBoxLayout();
        controlbuttonlayout->setObjectName(QString::fromUtf8("controlbuttonlayout"));
        splitButton = new QPushButton(TreeManagerWidget);
        splitButton->setObjectName(QString::fromUtf8("splitButton"));

        controlbuttonlayout->addWidget(splitButton);

        insertButton = new QPushButton(TreeManagerWidget);
        insertButton->setObjectName(QString::fromUtf8("insertButton"));

        controlbuttonlayout->addWidget(insertButton);

        deleteButton = new QPushButton(TreeManagerWidget);
        deleteButton->setObjectName(QString::fromUtf8("deleteButton"));

        controlbuttonlayout->addWidget(deleteButton);

        viewButton = new QPushButton(TreeManagerWidget);
        viewButton->setObjectName(QString::fromUtf8("viewButton"));

        controlbuttonlayout->addWidget(viewButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        controlbuttonlayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(controlbuttonlayout);


        retranslateUi(TreeManagerWidget);

        QMetaObject::connectSlotsByName(TreeManagerWidget);
    } // setupUi

    void retranslateUi(QWidget *TreeManagerWidget)
    {
        TreeManagerWidget->setWindowTitle(QApplication::translate("TreeManagerWidget", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("TreeManagerWidget", "Project Hiearachy", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("TreeManagerWidget", "Viewed Organs", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem = viewTable->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("TreeManagerWidget", "Organ", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("TreeManagerWidget", "Export Mask", 0, QApplication::UnicodeUTF8));
        splitButton->setText(QApplication::translate("TreeManagerWidget", "Split", 0, QApplication::UnicodeUTF8));
        insertButton->setText(QApplication::translate("TreeManagerWidget", "Insert", 0, QApplication::UnicodeUTF8));
        deleteButton->setText(QApplication::translate("TreeManagerWidget", "Delete", 0, QApplication::UnicodeUTF8));
        viewButton->setText(QApplication::translate("TreeManagerWidget", "View", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TreeManagerWidget: public Ui_TreeManagerWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TREEMANAGERWIDGET_H
