#include "opacityfromfilewidget.h"
#include "ui_opacityfromfilewidget.h"

QProgressBar* OpacityFromFileWidget::progressBar;

OpacityFromFileWidget::OpacityFromFileWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OpacityFromFileWidget)
{
    ui->setupUi(this);
    initializeWidget();
}

OpacityFromFileWidget::~OpacityFromFileWidget()
{
    delete ui;
}

void OpacityFromFileWidget::initializeWidget()
{
    ui->progressBar->setMaximumHeight(10);
    ui->progressBar->setMaximum(100);
    ui->progressBar->setMinimum(0);
    ui->progressBar->hide();
    progressBar = ui->progressBar;
}

bool OpacityFromFileWidget::QCallBack(const int pos, const char *str)
{
    progressBar->show();
    progressBar->setValue(pos);
}

void OpacityFromFileWidget::startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *organ, TreeItem *other)
{
    if (type == TransfuncTypes::OPACITY_FILE) {
        if (md != NULL) {
            currentParentNode = parentNode;
            currentApplyNode = organ;
            currentOtherNode = other;

            ui->applyTFButton->show();
        }
    }
}

void OpacityFromFileWidget::on_importFileButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Import Opacity File");
    if (fileName == "")
        return;

    ui->importFileEditText->setText(fileName);
}

void OpacityFromFileWidget::on_applyTFButton_clicked()
{
    progressBar->show();

    QString fileName = ui->importFileEditText->text();
    QFile file(fileName);
    if(!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::information(this, tr("Information"), tr("Can not read file, please select a text file again!"));
        return;
    }

    QTextStream input(&file);
    QString line = input.readLine();
    int size = line.toInt();

    int m = md->measurementPixel.X(); // Width of slice
    int n = md->measurementPixel.Y(); // Height of slice
    int h = md->measurementPixel.Z(); // Number of slice

    int modelSize = m * n * h;
    for (int i = 0; i < modelSize; i++) {
        currentApplyNode->opacity[i] = 0;
        currentOtherNode->opacity[i] = currentParentNode->opacity[i];
    }
    int count = 0;
    // Read opacity from file
    while(!input.atEnd()) {
        line = input.readLine();
        QStringList parts = line.split(" ");
        int y = parts.at(0).toInt();
        int x = parts.at(1).toInt();
        int z = parts.at(2).toInt();

        int index = z*m*n + y*m + x;

        currentApplyNode->opacity[index] = 1 * currentParentNode->opacity[index];
        currentOtherNode->opacity[index] = 0;
        if (currentApplyNode->opacity[index] == 1)
            count++;
    }
    qDebug() << "Total count: " << QString::number(count);

//    if (size != modelSize) {
//        QMessageBox::information(this, tr("Information"), tr("Size mismatches, please select a text file again!"));
//        qDebug() << "Size mismatchs";
//        return;
//    }

    // Mapping

    setTFOptions();

    ui->applyTFButton->hide();
    qDebug() << "Machine Learning Based TF Finishes";
}


void OpacityFromFileWidget::setTFOptions()
{
    currentParentNode->transfuncOptions.type = TransfuncTypes::OPACITY_FILE;

    TransfuncOptions tfOptions;
    tfOptions.type = TransfuncTypes::NONE;
    NodeViewOptions viewOption;
    viewOption.opacity = currentApplyNode->opacity;
    viewOption.colorTable = currentApplyNode->colorTable;
    currentApplyNode->transfunc = tfOpacityFromFile;
    currentApplyNode->transfuncOptions = tfOptions;
    currentApplyNode->viewOptions = viewOption;
    currentApplyNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);

    TransfuncOptions itfOptions;
    itfOptions.type = TransfuncTypes::NONE;
    NodeViewOptions viewOptionInverted;
    viewOptionInverted.opacity = currentOtherNode->opacity;
    viewOptionInverted.colorTable = currentOtherNode->colorTable;
    currentOtherNode->transfunc = tfOpacityFromFileInverted;
    currentOtherNode->transfuncOptions = itfOptions;
    currentOtherNode->viewOptions = viewOptionInverted;
    currentOtherNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);

}

vcg::Color4f tfOpacityFromFile(signed short intensityArray[], int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (options.opacity[index] != 0) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

vcg::Color4f tfOpacityFromFileInverted(signed short *intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;
    if (options.opacity[index] != 0) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}
