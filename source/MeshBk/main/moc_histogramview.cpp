/****************************************************************************
** Meta object code from reading C++ file 'histogramview.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "histogramview.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'histogramview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_HistogramView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   15,   14,   14, 0x0a,
      57,   14,   14,   14, 0x0a,
      93,   78,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_HistogramView[] = {
    "HistogramView\0\0mat\0"
    "updateHistogramMatAndTexture(cv::Mat)\0"
    "updateGLForcefully()\0histRegionList\0"
    "updatePaintArea(QList<HistogramRegionRect>)\0"
};

void HistogramView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HistogramView *_t = static_cast<HistogramView *>(_o);
        switch (_id) {
        case 0: _t->updateHistogramMatAndTexture((*reinterpret_cast< cv::Mat(*)>(_a[1]))); break;
        case 1: _t->updateGLForcefully(); break;
        case 2: _t->updatePaintArea((*reinterpret_cast< QList<HistogramRegionRect>(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData HistogramView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject HistogramView::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_HistogramView,
      qt_meta_data_HistogramView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HistogramView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HistogramView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HistogramView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HistogramView))
        return static_cast<void*>(const_cast< HistogramView*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int HistogramView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
