/********************************************************************************
** Form generated from reading UI file 'sliceviewwidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SLICEVIEWWIDGET_H
#define UI_SLICEVIEWWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include <sliceview.h>

QT_BEGIN_NAMESPACE

class Ui_SliceViewWidget
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *tfToolbox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *startSelectButton;
    QPushButton *clearSelectionButton;
    QLabel *label;
    QSlider *toleranceSlider;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QCheckBox *meanCheckbox;
    QSpinBox *offsetSpinbox;
    QCheckBox *neighborCheckbox;
    QSpinBox *neighborSpinbox;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_4;
    QCheckBox *dissimilarityCheckbox;
    QDoubleSpinBox *thresholdSpinbox;
    QCheckBox *gradientCheckbox;
    QSpinBox *gradTHSpinbox;
    QSpacerItem *horizontalSpacer_3;
    SliceView *sliceView;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_4;
    QSlider *brightnessSlider;
    QPushButton *resetBrightnessButton;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *calcTerminationDataButton;
    QPushButton *loadTerminationDataButton;
    QPushButton *pushButton;
    QSpacerItem *verticalSpacer;
    QPushButton *applyTFButton;
    QProgressBar *progressbar;

    void setupUi(QWidget *SliceViewWidget)
    {
        if (SliceViewWidget->objectName().isEmpty())
            SliceViewWidget->setObjectName(QString::fromUtf8("SliceViewWidget"));
        SliceViewWidget->resize(396, 437);
        verticalLayout = new QVBoxLayout(SliceViewWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        tfToolbox = new QWidget(SliceViewWidget);
        tfToolbox->setObjectName(QString::fromUtf8("tfToolbox"));
        verticalLayout_2 = new QVBoxLayout(tfToolbox);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        startSelectButton = new QPushButton(tfToolbox);
        startSelectButton->setObjectName(QString::fromUtf8("startSelectButton"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(startSelectButton->sizePolicy().hasHeightForWidth());
        startSelectButton->setSizePolicy(sizePolicy);
        startSelectButton->setMinimumSize(QSize(80, 24));
        startSelectButton->setStyleSheet(QString::fromUtf8(""));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/circleSelection.png"), QSize(), QIcon::Normal, QIcon::Off);
        startSelectButton->setIcon(icon);
        startSelectButton->setIconSize(QSize(20, 20));
        startSelectButton->setCheckable(true);

        horizontalLayout_2->addWidget(startSelectButton);

        clearSelectionButton = new QPushButton(tfToolbox);
        clearSelectionButton->setObjectName(QString::fromUtf8("clearSelectionButton"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(clearSelectionButton->sizePolicy().hasHeightForWidth());
        clearSelectionButton->setSizePolicy(sizePolicy1);
        clearSelectionButton->setMinimumSize(QSize(80, 24));

        horizontalLayout_2->addWidget(clearSelectionButton);

        label = new QLabel(tfToolbox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        toleranceSlider = new QSlider(tfToolbox);
        toleranceSlider->setObjectName(QString::fromUtf8("toleranceSlider"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(toleranceSlider->sizePolicy().hasHeightForWidth());
        toleranceSlider->setSizePolicy(sizePolicy2);
        toleranceSlider->setMinimumSize(QSize(150, 0));
        toleranceSlider->setValue(30);
        toleranceSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(toleranceSlider);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        meanCheckbox = new QCheckBox(tfToolbox);
        meanCheckbox->setObjectName(QString::fromUtf8("meanCheckbox"));
        meanCheckbox->setChecked(true);

        horizontalLayout->addWidget(meanCheckbox);

        offsetSpinbox = new QSpinBox(tfToolbox);
        offsetSpinbox->setObjectName(QString::fromUtf8("offsetSpinbox"));
        offsetSpinbox->setValue(35);

        horizontalLayout->addWidget(offsetSpinbox);

        neighborCheckbox = new QCheckBox(tfToolbox);
        neighborCheckbox->setObjectName(QString::fromUtf8("neighborCheckbox"));
        neighborCheckbox->setChecked(true);

        horizontalLayout->addWidget(neighborCheckbox);

        neighborSpinbox = new QSpinBox(tfToolbox);
        neighborSpinbox->setObjectName(QString::fromUtf8("neighborSpinbox"));
        neighborSpinbox->setMaximum(100);
        neighborSpinbox->setValue(50);

        horizontalLayout->addWidget(neighborSpinbox);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setSizeConstraint(QLayout::SetDefaultConstraint);
        dissimilarityCheckbox = new QCheckBox(tfToolbox);
        dissimilarityCheckbox->setObjectName(QString::fromUtf8("dissimilarityCheckbox"));
        dissimilarityCheckbox->setChecked(true);

        horizontalLayout_4->addWidget(dissimilarityCheckbox);

        thresholdSpinbox = new QDoubleSpinBox(tfToolbox);
        thresholdSpinbox->setObjectName(QString::fromUtf8("thresholdSpinbox"));
        thresholdSpinbox->setMaximum(1);
        thresholdSpinbox->setSingleStep(0.01);
        thresholdSpinbox->setValue(0.5);

        horizontalLayout_4->addWidget(thresholdSpinbox);

        gradientCheckbox = new QCheckBox(tfToolbox);
        gradientCheckbox->setObjectName(QString::fromUtf8("gradientCheckbox"));

        horizontalLayout_4->addWidget(gradientCheckbox);

        gradTHSpinbox = new QSpinBox(tfToolbox);
        gradTHSpinbox->setObjectName(QString::fromUtf8("gradTHSpinbox"));
        gradTHSpinbox->setMaximum(200);
        gradTHSpinbox->setValue(70);

        horizontalLayout_4->addWidget(gradTHSpinbox);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addWidget(tfToolbox);

        sliceView = new SliceView(SliceViewWidget);
        sliceView->setObjectName(QString::fromUtf8("sliceView"));
        sizePolicy1.setHeightForWidth(sliceView->sizePolicy().hasHeightForWidth());
        sliceView->setSizePolicy(sizePolicy1);
        sliceView->setMinimumSize(QSize(300, 300));

        verticalLayout->addWidget(sliceView);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_4 = new QLabel(SliceViewWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_6->addWidget(label_4);

        brightnessSlider = new QSlider(SliceViewWidget);
        brightnessSlider->setObjectName(QString::fromUtf8("brightnessSlider"));
        brightnessSlider->setMaximum(100);
        brightnessSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_6->addWidget(brightnessSlider);

        resetBrightnessButton = new QPushButton(SliceViewWidget);
        resetBrightnessButton->setObjectName(QString::fromUtf8("resetBrightnessButton"));

        horizontalLayout_6->addWidget(resetBrightnessButton);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        calcTerminationDataButton = new QPushButton(SliceViewWidget);
        calcTerminationDataButton->setObjectName(QString::fromUtf8("calcTerminationDataButton"));

        horizontalLayout_3->addWidget(calcTerminationDataButton);

        loadTerminationDataButton = new QPushButton(SliceViewWidget);
        loadTerminationDataButton->setObjectName(QString::fromUtf8("loadTerminationDataButton"));

        horizontalLayout_3->addWidget(loadTerminationDataButton);

        pushButton = new QPushButton(SliceViewWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_3->addWidget(pushButton);


        verticalLayout->addLayout(horizontalLayout_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        applyTFButton = new QPushButton(SliceViewWidget);
        applyTFButton->setObjectName(QString::fromUtf8("applyTFButton"));

        verticalLayout->addWidget(applyTFButton);

        progressbar = new QProgressBar(SliceViewWidget);
        progressbar->setObjectName(QString::fromUtf8("progressbar"));
        progressbar->setMaximumSize(QSize(16777215, 10));
        progressbar->setValue(0);
        progressbar->setTextVisible(false);

        verticalLayout->addWidget(progressbar);


        retranslateUi(SliceViewWidget);

        QMetaObject::connectSlotsByName(SliceViewWidget);
    } // setupUi

    void retranslateUi(QWidget *SliceViewWidget)
    {
        SliceViewWidget->setWindowTitle(QApplication::translate("SliceViewWidget", "Form", 0, QApplication::UnicodeUTF8));
        startSelectButton->setText(QApplication::translate("SliceViewWidget", "Select", 0, QApplication::UnicodeUTF8));
        clearSelectionButton->setText(QApplication::translate("SliceViewWidget", "Clear", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SliceViewWidget", "Tolerance", 0, QApplication::UnicodeUTF8));
        meanCheckbox->setText(QApplication::translate("SliceViewWidget", "Mean offset", 0, QApplication::UnicodeUTF8));
        neighborCheckbox->setText(QApplication::translate("SliceViewWidget", "Neighbor", 0, QApplication::UnicodeUTF8));
        dissimilarityCheckbox->setText(QApplication::translate("SliceViewWidget", "Dissimilarity", 0, QApplication::UnicodeUTF8));
        gradientCheckbox->setText(QApplication::translate("SliceViewWidget", "Gradient", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("SliceViewWidget", "Brightness", 0, QApplication::UnicodeUTF8));
        resetBrightnessButton->setText(QApplication::translate("SliceViewWidget", "Reset", 0, QApplication::UnicodeUTF8));
        calcTerminationDataButton->setText(QApplication::translate("SliceViewWidget", "Calc Dissimilarity", 0, QApplication::UnicodeUTF8));
        loadTerminationDataButton->setText(QApplication::translate("SliceViewWidget", "Load Dissimilarity", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("SliceViewWidget", "Write Dissimilarity", 0, QApplication::UnicodeUTF8));
        applyTFButton->setText(QApplication::translate("SliceViewWidget", "Apply", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SliceViewWidget: public Ui_SliceViewWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SLICEVIEWWIDGET_H
