/****************************************************************************
** Meta object code from reading C++ file 'treemanagerwidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "treemanagerwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'treemanagerwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TreeManagerWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: signature, parameters, type, tag, flags
      36,   19,   18,   18, 0x05,
     106,  100,   18,   18, 0x05,
     153,  148,   18,   18, 0x25,
     184,  177,   18,   18, 0x05,
     244,  238,   18,   18, 0x25,
     280,   18,   18,   18, 0x05,
     311,  298,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
     344,  342,   18,   18, 0x0a,
     412,  392,   18,   18, 0x0a,
     454,  449,   18,   18, 0x0a,
     490,  486,   18,   18, 0x0a,
     527,   18,   18,   18, 0x0a,
     540,   18,   18,   18, 0x0a,
     552,   18,   18,   18, 0x0a,
     565,   18,   18,   18, 0x0a,
     578,   18,   18,   18, 0x0a,
     597,   18,   18,   18, 0x0a,
     621,  615,   18,   18, 0x0a,
     641,   18,   18,   18, 0x0a,
     653,   18,   18,   18, 0x08,
     678,   18,   18,   18, 0x08,
     704,   18,   18,   18, 0x08,
     730,   18,   18,   18, 0x08,
     754,   18,   18,   18, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TreeManagerWidget[] = {
    "TreeManagerWidget\0\0transfuncType,,,\0"
    "startTransferFunc(TransfuncTypes,TreeItem*,TreeItem*,TreeItem*)\0"
    "item,\0viewThisItem(TreeItem*,vcg::CallBackPos*)\0"
    "item\0viewThisItem(TreeItem*)\0items,\0"
    "viewSelectedItems(QList<TreeItem*>,vcg::CallBackPos*)\0"
    "items\0viewSelectedItems(QList<TreeItem*>)\0"
    "viewButtonClick()\0selectedItem\0"
    "selectedNodeChanged(TreeItem*)\0,\0"
    "selectionChanged(QItemSelection,QItemSelection)\0"
    "topleft,bottomright\0"
    "dataChanged(QModelIndex,QModelIndex)\0"
    "type\0endTransferFunc(TransfuncTypes)\0"
    "pos\0onCustomContextMenuRequested(QPoint)\0"
    "removeNode()\0addToView()\0exportMask()\0"
    "importMask()\0viewSpecificNode()\0"
    "updateViewTable()\0index\0removeViewItem(int)\0"
    "resetMask()\0on_splitButton_clicked()\0"
    "on_insertButton_clicked()\0"
    "on_deleteButton_clicked()\0"
    "on_viewButton_clicked()\0on_pushButton_clicked()\0"
};

void TreeManagerWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TreeManagerWidget *_t = static_cast<TreeManagerWidget *>(_o);
        switch (_id) {
        case 0: _t->startTransferFunc((*reinterpret_cast< TransfuncTypes(*)>(_a[1])),(*reinterpret_cast< TreeItem*(*)>(_a[2])),(*reinterpret_cast< TreeItem*(*)>(_a[3])),(*reinterpret_cast< TreeItem*(*)>(_a[4]))); break;
        case 1: _t->viewThisItem((*reinterpret_cast< TreeItem*(*)>(_a[1])),(*reinterpret_cast< vcg::CallBackPos*(*)>(_a[2]))); break;
        case 2: _t->viewThisItem((*reinterpret_cast< TreeItem*(*)>(_a[1]))); break;
        case 3: _t->viewSelectedItems((*reinterpret_cast< QList<TreeItem*>(*)>(_a[1])),(*reinterpret_cast< vcg::CallBackPos*(*)>(_a[2]))); break;
        case 4: _t->viewSelectedItems((*reinterpret_cast< QList<TreeItem*>(*)>(_a[1]))); break;
        case 5: _t->viewButtonClick(); break;
        case 6: _t->selectedNodeChanged((*reinterpret_cast< TreeItem*(*)>(_a[1]))); break;
        case 7: _t->selectionChanged((*reinterpret_cast< QItemSelection(*)>(_a[1])),(*reinterpret_cast< QItemSelection(*)>(_a[2]))); break;
        case 8: _t->dataChanged((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 9: _t->endTransferFunc((*reinterpret_cast< TransfuncTypes(*)>(_a[1]))); break;
        case 10: _t->onCustomContextMenuRequested((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 11: _t->removeNode(); break;
        case 12: _t->addToView(); break;
        case 13: _t->exportMask(); break;
        case 14: _t->importMask(); break;
        case 15: _t->viewSpecificNode(); break;
        case 16: _t->updateViewTable(); break;
        case 17: _t->removeViewItem((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->resetMask(); break;
        case 19: _t->on_splitButton_clicked(); break;
        case 20: _t->on_insertButton_clicked(); break;
        case 21: _t->on_deleteButton_clicked(); break;
        case 22: _t->on_viewButton_clicked(); break;
        case 23: _t->on_pushButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TreeManagerWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TreeManagerWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_TreeManagerWidget,
      qt_meta_data_TreeManagerWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TreeManagerWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TreeManagerWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TreeManagerWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TreeManagerWidget))
        return static_cast<void*>(const_cast< TreeManagerWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int TreeManagerWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    }
    return _id;
}

// SIGNAL 0
void TreeManagerWidget::startTransferFunc(TransfuncTypes _t1, TreeItem * _t2, TreeItem * _t3, TreeItem * _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TreeManagerWidget::viewThisItem(TreeItem * _t1, vcg::CallBackPos * _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 3
void TreeManagerWidget::viewSelectedItems(QList<TreeItem*> _t1, vcg::CallBackPos * _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 5
void TreeManagerWidget::viewButtonClick()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void TreeManagerWidget::selectedNodeChanged(TreeItem * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_END_MOC_NAMESPACE
