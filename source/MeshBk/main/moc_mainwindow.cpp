/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      53,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   12,   11,   11, 0x05,
      58,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      77,   11,   11,   11, 0x0a,
      91,   11,   11,   11, 0x0a,
     107,   11,   11,   11, 0x0a,
     125,   11,   11,   11, 0x0a,
     146,   11,   11,   11, 0x0a,
     174,   11,   11,   11, 0x0a,
     202,   11,   11,   11, 0x0a,
     238,   11,   11,   11, 0x0a,
     257,   11,   11,   11, 0x0a,
     281,   11,   11,   11, 0x0a,
     312,  295,   11,   11, 0x0a,
     381,  376,   11,   11, 0x0a,
     413,   11,   11,   11, 0x08,
     431,   11,   11,   11, 0x08,
     456,   11,   11,   11, 0x08,
     483,   11,   11,   11, 0x08,
     505,   11,   11,   11, 0x08,
     529,   11,   11,   11, 0x08,
     551,   11,   11,   11, 0x08,
     577,   11,   11,   11, 0x08,
     611,  603,   11,   11, 0x08,
     633,   11,   11,   11, 0x08,
     668,  660,   11,   11, 0x08,
     700,  692,   11,   11, 0x08,
     733,  660,   11,   11, 0x08,
     763,  660,   11,   11, 0x08,
     798,  787,   11,   11, 0x08,
     823,   11,   11,   11, 0x08,
     845,   11,   11,   11, 0x08,
     870,   11,   11,   11, 0x08,
     896,   11,   11,   11, 0x08,
     921,   11,   11,   11, 0x08,
     950,  944,   11,   11, 0x08,
     970,   11,   11,   11, 0x08,
    1002,  996,   11,   11, 0x08,
    1026,   11,   11,   11, 0x08,
    1046,   11,   11,   11, 0x08,
    1070, 1062,   11,   11, 0x08,
    1102,   11,   11,   11, 0x08,
    1136, 1062,   11,   11, 0x08,
    1178, 1062,   11,   11, 0x08,
    1212,   11,   11,   11, 0x08,
    1230,   11,   11,   11, 0x08,
    1245, 1062,   11,   11, 0x08,
    1284, 1062,   11,   11, 0x08,
    1326, 1321,   11,   11, 0x08,
    1359,   11,   11,   11, 0x08,
    1391,   11,   11,   11, 0x08,
    1423,   11,   11,   11, 0x08,
    1457,   11,   11,   11, 0x08,
    1491,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0rps\0"
    "dispatchCustomSettings(RichParameterSet&)\0"
    "updateLayerTable()\0updateMenus()\0"
    "applyEditMode()\0suspendEditMode()\0"
    "applyPoissonFilter()\0applyNormalGenerateFilter()\0"
    "applyColoringFilter(QColor)\0"
    "applyInvertFacesOrientationFilter()\0"
    "updateRenderMode()\0applyXrayShaderRender()\0"
    "resetRender()\0transfuncType,,,\0"
    "startTransferFunc(TransfuncTypes,TreeItem*,TreeItem*,TreeItem*)\0"
    "type\0endTransferFunc(TransfuncTypes)\0"
    "onExitMenuClick()\0onApplyFilterMenuClick()\0"
    "onMenuBoundaryPointClick()\0"
    "onOpenFileMenuClick()\0onOpenFolderMenuClick()\0"
    "onColoringMenuClick()\0onButtonImportMeshClick()\0"
    "onButonImportDICOMClick()\0row,col\0"
    "onlayerClick(int,int)\0onMenuViewMeshLayerClick()\0"
    "isShown\0onLayerDockToggle(bool)\0isshown\0"
    "onVolumePropertyDockToggle(bool)\0"
    "onTreeManagerDockToggle(bool)\0"
    "onSliceDockToggle(bool)\0row,column\0"
    "onEditLayerName(int,int)\0setProgressBusyMode()\0"
    "setProgressPercentMode()\0"
    "updateImportMesh(QString)\0"
    "onStatisticButtonClick()\0"
    "cancelColoringFilter()\0index\0"
    "mapLayerEvents(int)\0onMenuXrayShaderChanged()\0"
    "value\0onBackgroundChange(int)\0"
    "onResetBackground()\0showTrackBall()\0"
    "checked\0on_menuViewAxis_triggered(bool)\0"
    "on_actionImport_DICOM_triggered()\0"
    "on_menuViewVolumeProperty_triggered(bool)\0"
    "on_menuViewVolume_triggered(bool)\0"
    "updateLayerDock()\0updateGLArea()\0"
    "on_menuViewTreeManager_triggered(bool)\0"
    "on_menuViewSliceView_triggered(bool)\0"
    "item\0changeCurrentTreeItem(TreeItem*)\0"
    "on_actionSlice_Tool_triggered()\0"
    "onMenuGeneratePointCloudClick()\0"
    "on_actionOpen_project_triggered()\0"
    "on_actionSave_Project_triggered()\0"
    "on_actionSave_Project_as_triggered()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->dispatchCustomSettings((*reinterpret_cast< RichParameterSet(*)>(_a[1]))); break;
        case 1: _t->updateLayerTable(); break;
        case 2: _t->updateMenus(); break;
        case 3: _t->applyEditMode(); break;
        case 4: _t->suspendEditMode(); break;
        case 5: _t->applyPoissonFilter(); break;
        case 6: _t->applyNormalGenerateFilter(); break;
        case 7: _t->applyColoringFilter((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 8: _t->applyInvertFacesOrientationFilter(); break;
        case 9: _t->updateRenderMode(); break;
        case 10: _t->applyXrayShaderRender(); break;
        case 11: _t->resetRender(); break;
        case 12: _t->startTransferFunc((*reinterpret_cast< TransfuncTypes(*)>(_a[1])),(*reinterpret_cast< TreeItem*(*)>(_a[2])),(*reinterpret_cast< TreeItem*(*)>(_a[3])),(*reinterpret_cast< TreeItem*(*)>(_a[4]))); break;
        case 13: _t->endTransferFunc((*reinterpret_cast< TransfuncTypes(*)>(_a[1]))); break;
        case 14: _t->onExitMenuClick(); break;
        case 15: _t->onApplyFilterMenuClick(); break;
        case 16: _t->onMenuBoundaryPointClick(); break;
        case 17: _t->onOpenFileMenuClick(); break;
        case 18: _t->onOpenFolderMenuClick(); break;
        case 19: _t->onColoringMenuClick(); break;
        case 20: _t->onButtonImportMeshClick(); break;
        case 21: _t->onButonImportDICOMClick(); break;
        case 22: _t->onlayerClick((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 23: _t->onMenuViewMeshLayerClick(); break;
        case 24: _t->onLayerDockToggle((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->onVolumePropertyDockToggle((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 26: _t->onTreeManagerDockToggle((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 27: _t->onSliceDockToggle((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 28: _t->onEditLayerName((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 29: _t->setProgressBusyMode(); break;
        case 30: _t->setProgressPercentMode(); break;
        case 31: _t->updateImportMesh((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 32: _t->onStatisticButtonClick(); break;
        case 33: _t->cancelColoringFilter(); break;
        case 34: _t->mapLayerEvents((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 35: _t->onMenuXrayShaderChanged(); break;
        case 36: _t->onBackgroundChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 37: _t->onResetBackground(); break;
        case 38: _t->showTrackBall(); break;
        case 39: _t->on_menuViewAxis_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 40: _t->on_actionImport_DICOM_triggered(); break;
        case 41: _t->on_menuViewVolumeProperty_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 42: _t->on_menuViewVolume_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 43: _t->updateLayerDock(); break;
        case 44: _t->updateGLArea(); break;
        case 45: _t->on_menuViewTreeManager_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 46: _t->on_menuViewSliceView_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 47: _t->changeCurrentTreeItem((*reinterpret_cast< TreeItem*(*)>(_a[1]))); break;
        case 48: _t->on_actionSlice_Tool_triggered(); break;
        case 49: _t->onMenuGeneratePointCloudClick(); break;
        case 50: _t->on_actionOpen_project_triggered(); break;
        case 51: _t->on_actionSave_Project_triggered(); break;
        case 52: _t->on_actionSave_Project_as_triggered(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 53)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 53;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::dispatchCustomSettings(RichParameterSet & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::updateLayerTable()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
