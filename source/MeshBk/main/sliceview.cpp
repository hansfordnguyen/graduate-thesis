#include "sliceview.h"
using namespace vcg;
SliceView::SliceView(QWidget *parent) :
    QGLWidget(parent)
{
    this->backgroundColor = QColor(0, 0, 0);
    // Setup render mode for the meshmodel
    this->rm.drawMode = vcg::GLW::DMWire;
    this->rm.colorMode = vcg::GLW::CMPerVert;
    this->rm.textureMode = vcg::GLW::TMNone;
    this->mm = NULL;
    this->scale = vcg::Point3f(1, 1, 1);
    this->lastHit = vcg::Point3f(0, 0, 0);
    this->setMouseTracking(true);
}

void SliceView::resizeGL()
{
}

void SliceView::initializeGL()
{
    qglClearColor(this->backgroundColor);
    initTexture3D();
}

void SliceView::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.beginNativePainting();
    glClearColor(backgroundColor.red() * 1.0 / 255, backgroundColor.green() * 1.0 / 255, backgroundColor.blue() * 1.0 / 255, 1);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//    drawLine(QPoint(0, 0), QPoint(lastHit.X(), this->height() - lastHit.Y()));
//    drawPoint(QPoint(100, 100));

    if (this->md->sliceMesh != NULL && this->md->meshList.size() > 0 && this->md->globalTexture3DAddr != 0) {
        vcg::Box3f bb = this->md->textureBBoxMillimeter->cm.bbox;
        this->mm = this->md->sliceMesh;
        this->normalVector = this->md->sliceNorm * 100 + this->md->mm()->cm.bbox.Center();
        float ratio = this->width() * 1.0 / this->height() * 1.0;
        float maxwidth = qMax(this->width() * 1.0f, bb.Diag());
        float maxheight = maxwidth * 1.0 / ratio;

        // Setting projection matrix
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-maxwidth / 2.0, maxwidth / 2.0, -maxheight / 2.0, maxheight / 2.0, -bb.Diag() / 2, bb.Diag() / 2);
        // Setting modelview matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        if (this->md->sliceNorm.Y() == 1 || this->md->sliceNorm.X() == 1) {
            gluLookAt(bb.Center().X() * scale.X(), bb.Center().Y() * scale.Y(), bb.Center().Z() * scale.Z(), this->normalVector.X() * scale.X(), this->normalVector.Y() * scale.Y(), this->normalVector.Z() * scale.Z(), 0, 0, 1);
        } else {
            gluLookAt(bb.Center().X() * scale.X(), bb.Center().Y() * scale.Y(), bb.Center().Z() * scale.Z(), this->normalVector.X() * scale.X(), this->normalVector.Y() * scale.Y(), this->normalVector.Z() * scale.Z(), 0, -1, 0);
        }


        glBindTexture(GL_TEXTURE_3D, texture3D);
        glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, md->getMeasurementPixel().X(), md->getMeasurementPixel().Y(), md->getMeasurementPixel().Z(), 0, GL_RGBA, GL_UNSIGNED_BYTE, md->globalTexture3DRgbaData);

        glPushAttrib(GL_ALL_ATTRIB_BITS);
        glPushMatrix();
        glEnable(GL_ALPHA_TEST );
        glAlphaFunc(GL_GREATER, (1.0f/255.0f));
//        glEnable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
        glEnable(GL_TEXTURE_3D);
        glScalef(this->scale.X(), this->scale.Y(), this->scale.Z());
        for (int i = 0; i < this->mm->cm.fn; i++) {
            vcg::Point3f v0 = this->mm->cm.face[i].V(0)->P();
            vcg::Point3f v1 = this->mm->cm.face[i].V(1)->P();
            vcg::Point3f v2 = this->mm->cm.face[i].V(2)->P();

            glBegin(GL_TRIANGLES);
            glTexCoord3f(v0.X()/this->md->getMeasurementMillimeter().X(), v0.Y()/this->md->getMeasurementMillimeter().Y(), v0.Z()/this->md->getMeasurementMillimeter().Z());
            glVertex3f(v0.X(), v0.Y(), v0.Z());

            glTexCoord3f(v1.X()/this->md->getMeasurementMillimeter().X(), v1.Y()/this->md->getMeasurementMillimeter().Y(), v1.Z()/this->md->getMeasurementMillimeter().Z());
            glVertex3f(v1.X(), v1.Y(), v1.Z());

            glTexCoord3f(v2.X()/this->md->getMeasurementMillimeter().X(), v2.Y()/this->md->getMeasurementMillimeter().Y(), v2.Z()/this->md->getMeasurementMillimeter().Z());
            glVertex3f(v2.X(), v2.Y(), v2.Z());
            glEnd();
        }
        glBindTexture(GL_TEXTURE_3D, 0);

        if (currentSlectionMode == SelectionMode::CIRCLE) {
            if (selectionPoints.size() > 0) {
                drawPoints(selectionPoints);
            }
            drawCirle(QPointF(lastHit.X(), this->height() - lastHit.Y()), radius + 2);

        }
        glPopMatrix();
        glPopAttrib();
    }

    painter.endNativePainting();
}

void SliceView::backgroundChange(QColor color)
{
    this->backgroundColor = color;
    update();
}

void SliceView::backgroundChange(int color)
{
    qDebug() << "change background color";
    this->backgroundColor = QColor(color, color, color);
    update();
    updateGL();
}

void SliceView::updateIntersectionMesh(MeshModel* mm, MeshDocument *md, vcg::Point3f normal)
{
    qDebug() << "Update new mesh intersection";
    if (mm != 0 && md != 0) {
        this->mm = mm;
        this->normalVector = normal * 100 + md->mm()->cm.bbox.Center();
    }
    if (md != 0) {
        this->md = md;
    }
    updateGL();
}


void SliceView::initTexture3D()
{
    glGenTextures(1, &texture3D);

    glBindTexture(GL_TEXTURE_3D, texture3D);

    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_3D, 0);
}

void SliceView::mousePressEvent(QMouseEvent *event) {
    this->lastHit = vcg::Point3f(event->x(), this->height() - event->y(), 0);
    this->isDrag = true;
    switch (currentSlectionMode) {
    case SelectionMode::CIRCLE:{
        readPixels(this->lastHit);
        break;
    }
    default:
        break;
    }
    this->update();
}

void SliceView::mouseMoveEvent(QMouseEvent *event) {
    vcg::Point3f newHit = vcg::Point3f(event->x(), this->height() - event->y(), 0);
    switch (currentSlectionMode) {
    case SelectionMode::NONE: {
        if (isDrag) {
            float offset = 0.01;
            float sign = (newHit.Y() - this->lastHit.Y());
            if (qAbs(sign) < 0.0001) return;
            sign = qAbs(sign) / sign;
            this->scale = vcg::Point3f(this->scale.X() + sign * offset, this->scale.Y() + sign * offset, this->scale.Z() + sign * offset);
        }
        break;
    }
    default:
        break;
    }
    this->lastHit = newHit;
    this->update();
}

void SliceView::mouseDoubleClickEvent(QMouseEvent *event) {
    this->lastHit = vcg::Point3f(event->x(), this->height() - event->y(), 0);
    switch (currentSlectionMode) {
    case SelectionMode::NONE: {
        this->scale = vcg::Point3f(1, 1, 1);
        break;
    }
    case SelectionMode::CIRCLE: {
        if (selections.size() > 0) {
            // Test for first point in selections
            GLfloat z;
            GLint viewport[4];
            GLdouble modelview[16], projection[16];
            GLdouble ox = 0.0, oy = 0.0, oz = 0.0;
            Point3f point = selections.at(0).first;
            glGetIntegerv(GL_VIEWPORT, viewport);
            glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
            glGetDoublev(GL_PROJECTION_MATRIX, projection);
            glReadPixels(point.X(), point.Y(), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &z);
            gluUnProject(point.X(), point.Y(), z, modelview, projection, viewport, &ox, &oy, &oz);
            qDebug() << "Original location: (" + QString::number(ox) + ", " + QString::number(oy) + ", " + QString::number(oz) + ")";
            qDebug() << "Texture index: " + QString::number(getTextureIndex(ox, oy, oz));
        }
        break;
    }
    default:
        break;
    }
    this->update();
}

void SliceView::wheelEvent(QWheelEvent *event)
{
    switch (currentSlectionMode) {
    case SelectionMode::NONE:{
        float offset = 0.01;
        float sign = event->delta() / 120;
        this->scale = vcg::Point3f(this->scale.X() + sign * offset, this->scale.Y() + sign * offset, this->scale.Z() + sign * offset);
        break;
    }
    case SelectionMode::CIRCLE: {
        int offset = 2;
        float sign = event->delta() / 120;
        this->radius = this->radius + sign * offset;
        if (this->radius < 2) this->radius = 2;
        break;
    }
    default:
        break;
    }
    this->update();
}

void SliceView::mouseReleaseEvent(QMouseEvent *event) {
    this->lastHit = vcg::Point3f(event->x(), this->height() - event->y(), 0);
    this->isDrag = false;
    this->update();
}

void SliceView::drawCirle(QPointF center, float radius) {
    int step = 300;

//    glLineWidth(1);
////    glColor3f(color[0] / 255, color[1] / 255, color[2] / 255);
////    glColor3f(0, 0, 0);
//    glBegin(GL_LINE_LOOP);
//    for (int i = 0; i < step; ++i) {
//        float angle = 2.0 * M_PI * i / step;
//        float x = center.x() + qCos(angle) * radius;
//        float y = center.y() + qSin(angle) * radius;
//        glVertex2f(x, y);
//    }
//    glEnd();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,this->width(),this->height(),0,-1,1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_COLOR_LOGIC_OP);
    float wi;
    glGetFloatv(GL_LINE_WIDTH,&wi);
    glLineWidth(1);
//    glLogicOp(GL_XOR);
    glBegin(GL_LINE_LOOP);
    for (int i = 0; i < step; ++i) {
        float angle = 2.0 * M_PI * i / step;
        float x = center.x() + qCos(angle) * radius;
        float y = center.y() + qSin(angle) * radius;
        glVertex2f(x, y);
    }
    glEnd();
    glPopAttrib();
    glPopMatrix(); // restore modelview
    glLineWidth(wi);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glColor3f(1, 1, 1);
}


void SliceView::readPixels(vcg::Point3f location) {
    this->update();
    //    selections.clear();
    int width = radius * 2;
    int height = radius * 2;
    int x = location.X() - width / 2;
    int y = location.Y() - height / 2;
    GLubyte pick_col[width * height];
    if (x < 0 || x > this->width()) return;
    if (y < 0 || y > this->height()) return;
    glReadPixels(x, y, width , height, GL_RED , GL_UNSIGNED_BYTE , pick_col);
    // Caculate mean
    int sumgray = 0;
    int count = 0;
    for (int row = 0; row < height; ++row) {
        for (int col = 0; col < width; ++col) {
            int startIndex = row * width + col;
            if (pick_col[startIndex] == 0) continue; // ignore black background
            if (!selectionFlag[(x + col) + (y + row) * this->width()]) {
                sumgray += pick_col[startIndex];
                count++;
            }
        }
    }
    int mean = sumgray / count;
    GLfloat z;
    glReadPixels(x + radius, y + radius, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &z);
    qDebug() << "Depth component: " + QString::number(z);
    for (int row = 0; row < height; ++row) {
        for (int col = 0; col < width; ++col) {
            int startIndex = row * width + col;
            Color4f color = Color4f(pick_col[startIndex], pick_col[startIndex], pick_col[startIndex], 0);
            if (color[0] < mean - this->tolerance) continue;
            if (color[0] > mean + this->tolerance) continue;
            int radiusPow = (x + col - location.X()) * (x+col - location.X()) + (y + row - location.Y()) * (y + row - location.Y());
            if (radiusPow > (radius * radius)) continue;

            if (!selectionFlag[(x + col) + (y + row) * this->width()]) {
//                glReadPixels(x + col, y + row, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &z);
                selections.push_back(qMakePair(Point3f(x + col, y + row, z), color));
                selectionPoints.push_back(QPoint(x + col, this->height() - y - row));
                selectionFlag[(x+col) + (y + row) * this->width()] = true;
            }

//            GLfloat z;
//            glReadPixels(x + radius, y + radius, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &z);
//            if (!selectionFlag[(x + radius) + (y + radius) * this->width()]) {
//                selections.push_back(qMakePair(Point3f(x + radius, y + radius, 0), color));
//                selectionPoints.push_back(QPoint(x + radius, this->height() - y - radius));
//                selectionFlag[(x + radius) + (y + radius) * this->width()] = true;
//            }

        }
    }
}

void SliceView::drawLine(QPoint start, QPoint cur) {
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,this->width(),this->height(),0,-1,1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_COLOR_LOGIC_OP);
    float wi;
    glGetFloatv(GL_LINE_WIDTH,&wi);
    glLineWidth(4);
    glLogicOp(GL_XOR);
    glColor3f(1,1,1);
    glBegin(GL_LINES);
    glVertex2f(start.x(),start.y());
    glVertex2f(cur.x(),cur.y());
    glEnd();
    glPopAttrib();
    glPopMatrix(); // restore modelview
    glLineWidth(wi);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}


void SliceView::drawPoint(QPoint point) {
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,this->width(),this->height(),0,-1,1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_COLOR_LOGIC_OP);
    float wi;
    glGetFloatv(GL_POINT_SIZE,&wi);
//    glLineWidth(4);
    glPointSize(1);
//    glLogicOp(GL_XOR);
    glBegin(GL_POINTS);
    glColor3f(1,0,0);
    glVertex2f(point.x(),point.y());
    glEnd();
    glPopAttrib();
    glPopMatrix(); // restore modelview
//    glLineWidth(wi);
    glPointSize(wi);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glColor3f(1, 1, 1);
}
void SliceView::drawPoints(QList<QPoint> &points) {
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,this->width(),this->height(),0,-1,1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_COLOR_LOGIC_OP);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    float wi;
    glGetFloatv(GL_POINT_SIZE,&wi);
//    glLineWidth(4);
    glPointSize(1);
//    glLogicOp(GL_XOR);
    glBegin(GL_POINTS);
    foreach (QPoint point, points) {
        glColor4f(0, 1, 0, 0.5);
        glVertex2f(point.x(), point.y());
    }
    glEnd();
    glPopAttrib();
    glPopMatrix(); // restore modelview
//    glLineWidth(wi);
    glPointSize(wi);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glColor3f(1, 1, 1);
}

void SliceView::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Space:{
        if (selections.size() > 0) {
            // Test for first point in selections
            GLint viewport[4];
            GLdouble modelview[16], projection[16];
            GLdouble ox = 0.0, oy = 0.0, oz = 0.0;
            Point3f point = selections.at(0).first;
            glGetIntegerv(GL_VIEWPORT, viewport);
            glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
            glGetDoublev(GL_PROJECTION_MATRIX, projection);
            gluUnProject(point.X(), point.Y(), point.Z(), modelview, projection, viewport, &ox, &oy, &oz);
            qDebug() << "Original location: (" + QString::number(ox) + ", " + QString::number(oy) + ", " + QString::number(oz) + ")";
        }
        break;
    }
    default:
        break;
    }
}

void SliceView::updateIntensitySelection() {
    for(int i = 0; i < selections.size(); ++i) {
        QPair<vcg::Point3f, vcg::Color4f> entry = selections.at(i);
        int colorOrigin = entry.second[0];
        int intensity = (int)(colorOrigin * 1.0 * 2048 / 255 - 1024);
        this->intensitySelection[intensity] = true;
    }

    QMap<int, bool>::Iterator it = this->intensitySelection.begin();
    int sum = 0;
    for (; it != this->intensitySelection.end(); ++it) {
        sum += it.key();
    }
    this->selectionMean = sum / this->intensitySelection.size();
}
void SliceView::toleranceChanged(int value){
    this->tolerance = value;
}
int SliceView::getTextureIndex(float x, float y, float z) {
    if (md  == NULL) return -1;
    float xSpace = md->measurementMillimeter.X() * 1.0 / md->measurementPixel.X();
    float ySpace = md->measurementMillimeter.Y() * 1.0 / md->measurementPixel.Y();
    float zSpace = md->measurementMillimeter.Z() * 1.0 / (md->measurementPixel.Z() - 1);
    int widthPixel = md->measurementPixel.X();
    int heightPixel = md->measurementPixel.Y();
//    qDebug() << "xSpace: " + QString::number(xSpace) + ", ySpace: " + QString::number(ySpace) + ", zSpace: " + QString::number(zSpace);
    if (xSpace == 0 || ySpace == 0 || zSpace == 0) return -1;
    return (int)(z / zSpace) * (widthPixel * heightPixel) + (int)(y / ySpace) * widthPixel + (int)(x / xSpace);
}
QList<int> SliceView::getNeighbors(int centerIndex) {
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int maxIndex = wp * hp * n - 1;
    QList<int> neighborIndexes;
    int index;
    if (centerIndex >= 0 && centerIndex <= maxIndex){
        // Middle Slice
        index = centerIndex - wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex - wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex - wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);

        index = centerIndex + wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex + wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex + wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
    }

    // Upper Slice
    int upperIndex = centerIndex - wp * hp;
    if (upperIndex >= 0 && upperIndex <= maxIndex) {
        index = upperIndex - wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = upperIndex - wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = upperIndex - wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);

        index = upperIndex - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = upperIndex;
        neighborIndexes.push_back(index);
        index = upperIndex + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);

        index = upperIndex + wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = upperIndex + wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = upperIndex + wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
    }

    // Below slice
    int belowIndex = centerIndex + wp * hp;
    if (belowIndex >= 0 && belowIndex <= maxIndex) {
        index = belowIndex - wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = belowIndex - wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = belowIndex - wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);

        index = belowIndex - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = belowIndex;
        neighborIndexes.push_back(index);
        index = belowIndex + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);

        index = belowIndex + wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = belowIndex + wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = belowIndex + wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
    }

    return neighborIndexes;
}
QMap<int, bool> SliceView::getIndexMap() {
    this->update();
    QMap<int, bool> indexMap;
    GLint viewport[4];
    GLdouble modelview[16], projection[16];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    glGetDoublev(GL_PROJECTION_MATRIX, projection);
    if (selections.size() > 0) {
        for(int i = 0; i < selections.size(); ++i) {
            Point3f point = selections.at(i).first;
//            GLfloat z;
            GLdouble ox = 0.0, oy = 0.0, oz = 0.0;
//            glReadPixels(point.X(), point.Y(), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &z);
            bool succ = gluUnProject(point.X(), point.Y(), point.Z(), modelview, projection, viewport, &ox, &oy, &oz);
//            qDebug() << "Original location: (" + QString::number(ox) + ", " + QString::number(oy) + ", " + QString::number(oz) + ")";
//            qDebug() << "Texture index: " + QString::number(getTextureIndex(ox, oy, oz));
            int index = getTextureIndex(ox / this->scale.X(), oy / this->scale.Y(), oz / this->scale.Z());
            if (indexMap[index]) continue; // already visit
            indexMap[index] = true;
//            QList<int> neighbors = getNeighbors(index);
//            for (int n = 0; n < neighbors.size(); ++n) {
//                if (indexMap[n]) continue; // already visit
//                indexMap[n] = true;
//            }
        }
    }
//    delete viewport;
//    delete modelview;
//    delete projection;
    return indexMap;
}

void SliceView::clearAllSelectionData()
{
    selections.clear();
    selectionPoints.clear();
    selectionFlag.clear();
    update();
}
