#ifndef HISTOGRAMCOLORVIEW_H
#define HISTOGRAMCOLORVIEW_H

#include "../common/interfaces.h"
#include <QComboBox>
#include <QGLWidget>
#include <QMouseEvent>
#include <QRect>
#include <QColorDialog>
#include <QList>
#include <QString>
#include <QTableWidget>
#include <QTextEdit>
#include <QPushButton>
#include <QDoubleSpinBox>
#include <QSignalMapper>

struct HistogramRegion {
    QString name;
    QColor color;
};

struct HistogramRegionRect: HistogramRegion {
    QPointF point1;
    QPointF point2;
};

class HistogramColorView: public QGLWidget {
    Q_OBJECT
public:
    explicit HistogramColorView(QWidget *parent = 0);
    ~HistogramColorView();

    void resizeGL(int width, int height);
    void initializeGL();
//    void paintEvent(QPaintEvent *event);
    void paintEvent(QPaintEvent *event);

    bool isMouseMoving = false;
    QColor backgroundColor;
//    MeshDocument* md;
    QTableWidget* regionColorTable;

    cv::Mat histogramMat;
    GLuint texture2DAddr = 0;
    GLuint texture2DLogAddr = 0;
    uchar* texture2DData = NULL;
    uchar* texture2DLogData = NULL;

    float minAxisX = -1024;
    float maxAxisX = 1023;
    float minAxisY = 0;
    float maxAxisY = 1000;

    double orthoLeft = -1070.0;
    double orthoRight = 1070;
    double orthoTop = 1050.0;
    double orthoBottom = -20.0;

    float modelViewWidth = orthoRight - orthoLeft;
    float modelViewHeight = orthoTop - orthoBottom;

    QPointF modelViewPoint1, modelViewPoint2;
    QPointF point1, point2;
    QColor rectColor;

    QList<HistogramRegionRect> histRegionRectList;

    QSignalMapper* nameMapper;
    QSignalMapper* colorMapper;
    QSignalMapper* opacityMapper;
    QSignalMapper* removeMapper;


public:
    void initTexture2D(int width, int height);
    void subTexture2D(cv::Mat);
    void setCameraView();
    void changeCoordIntoModelView();
    void drawRectangles(QPointF point1, QPointF point2, QColor color);
    void drawDashLineRectangles(QPointF point1, QPointF point2, QColor color);
    void addToHistRegionRectList();
    void updateRegionColorTable();
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
public slots:
    void updateHistogramMatAndTexture(cv::Mat mat);
    void updateGLForcefully();
    void changeRegionName(int index);
    void changeRegionColor(int index);
    void changeRegionOpacity(int index);
    void removeRegion(int index);
signals:
    void paintAreaUpdated(QList<HistogramRegionRect>);

};

#endif // HISTOGRAMCOLORVIEW_H
